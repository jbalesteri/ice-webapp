# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.


<a name="3.0.28"></a>
## [3.0.28](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.27...v3.0.28) (2017-05-24)



<a name="3.0.27"></a>
## [3.0.27](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.19...v3.0.27) (2017-05-24)


<a name="3.0.25"></a>
## [3.0.25](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.24...v3.0.25) (2017-05-24)



<a name="3.0.24"></a>
## [3.0.24](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.23...v3.0.24) (2017-05-24)



<a name="3.0.23"></a>
## [3.0.23](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.21...v3.0.23) (2017-05-24)



<a name="3.0.21"></a>



<a name="3.0.19"></a>
## [3.0.19](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.18...v3.0.19) (2017-05-24)



<a name="3.0.18"></a>
## [3.0.18](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.17...v3.0.18) (2017-05-23)



<a name="3.0.17"></a>
## [3.0.17](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.15...v3.0.17) (2017-05-23)



<a name="3.0.15"></a>
## [3.0.15](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.5...v3.0.15) (2017-05-23)



<a name="3.0.13"></a>
## [3.0.13](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.5...v3.0.13) (2017-05-23)



<a name="3.0.5"></a>
## [3.0.5](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.4...v3.0.5) (2017-05-23)



<a name="3.0.4"></a>
## [3.0.4](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.3...v3.0.4) (2017-05-23)



<a name="3.0.3"></a>
## [3.0.3](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.2...v3.0.3) (2017-04-28)


### Bug Fixes

* **groupDeviceListCard:** using batPowerOutW ([0127c8a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/0127c8a)), closes [#856](https://github.com/vpsinc/ice-frontend-react-mobx/issues/856)



<a name="3.0.2"></a>
## [3.0.2](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.1...v3.0.2) (2017-04-27)


### Bug Fixes

* **zonecard:** zone card collapible panel not hidden right ([840c0ee](https://github.com/vpsinc/ice-frontend-react-mobx/commit/840c0ee)), closes [#790](https://github.com/vpsinc/ice-frontend-react-mobx/issues/790)



<a name="3.0.1"></a>
## [3.0.1](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v3.0.0...v3.0.1) (2017-04-26)


### Bug Fixes

* **browser:** Fixing link for ice.css in index ([666afcd](https://github.com/vpsinc/ice-frontend-react-mobx/commit/666afcd))
* **groupCard:** Fixing observable issue with device list ([f29ce20](https://github.com/vpsinc/ice-frontend-react-mobx/commit/f29ce20))



<a name="3.0.0"></a>
# [3.0.0](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.3.2...v3.0.0) (2017-04-25)


### Bug Fixes

* **notifications:** Updating error response handling ([982074e](https://github.com/vpsinc/ice-frontend-react-mobx/commit/982074e))


### Features

* **feed-form:** Layout fixes ([70595bf](https://github.com/vpsinc/ice-frontend-react-mobx/commit/70595bf))
* **postcss:** CSS Linting ([d00a617](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d00a617))
* **reactToolbox:** Upgrading react toolbox ([936ff4d](https://github.com/vpsinc/ice-frontend-react-mobx/commit/936ff4d))
* **zone-builder:** 3 Col Breaker Limits ([3293bd8](https://github.com/vpsinc/ice-frontend-react-mobx/commit/3293bd8))



<a name="2.3.2"></a>
## [2.3.2](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.3.1...v2.3.2) (2017-03-31)


### Bug Fixes

* **device:** Device form had issues on refresh ([e9ea3e7](https://github.com/vpsinc/ice-frontend-react-mobx/commit/e9ea3e7))
* **devices:** More on load fixes for device form ([4ea106e](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4ea106e))
* **monitoring:** Fixing reference to old health store ([7327382](https://github.com/vpsinc/ice-frontend-react-mobx/commit/7327382))



<a name="2.3.1"></a>
## [2.3.1](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.3.0...v2.3.1) (2017-03-30)


### Bug Fixes

* **api-helper:** Removing ID prop from patch data ([c091a4b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/c091a4b)), closes [#753](https://github.com/vpsinc/ice-frontend-react-mobx/issues/753)
* **model:** Fixing deviceInfo model return ([f1867a7](https://github.com/vpsinc/ice-frontend-react-mobx/commit/f1867a7)), closes [#754](https://github.com/vpsinc/ice-frontend-react-mobx/issues/754)



<a name="2.3.0"></a>
# [2.3.0](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.3.0-beta.1...v2.3.0) (2017-03-29)


### Bug Fixes

* **eventInfoDialog:** Fixed dialog functionality and content ([#740](https://github.com/vpsinc/ice-frontend-react-mobx/issues/740)) ([e5f73ea](https://github.com/vpsinc/ice-frontend-react-mobx/commit/e5f73ea))
* **login:** Static login failure message ([#739](https://github.com/vpsinc/ice-frontend-react-mobx/issues/739)) ([8fcd7d6](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8fcd7d6))


### Features

* **devices:** Optinal IP and Port for devices ([#742](https://github.com/vpsinc/ice-frontend-react-mobx/issues/742)) ([2dd87a3](https://github.com/vpsinc/ice-frontend-react-mobx/commit/2dd87a3))



<a name="2.3.0-beta.1"></a>
# [2.3.0-beta.1](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.3.0-beta.0...v2.3.0-beta.1) (2017-03-24)


### Bug Fixes

* **devices:** Fixing devices a bit ([#724](https://github.com/vpsinc/ice-frontend-react-mobx/issues/724)) ([759a036](https://github.com/vpsinc/ice-frontend-react-mobx/commit/759a036)), closes [#719](https://github.com/vpsinc/ice-frontend-react-mobx/issues/719)
* **drconfig:** Fixing state varialbe misname ([#735](https://github.com/vpsinc/ice-frontend-react-mobx/issues/735)) ([4acbf10](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4acbf10))
* **rackCard:** Fixed topology device state ([#722](https://github.com/vpsinc/ice-frontend-react-mobx/issues/722)) ([01f337d](https://github.com/vpsinc/ice-frontend-react-mobx/commit/01f337d)), closes [#720](https://github.com/vpsinc/ice-frontend-react-mobx/issues/720)
* **stores:** Refactoring DataCenter store ([#732](https://github.com/vpsinc/ice-frontend-react-mobx/issues/732)) ([ebca68f](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ebca68f))
* **valueWrapper:** Added case for negative number padding. ([#727](https://github.com/vpsinc/ice-frontend-react-mobx/issues/727)) ([0a882ea](https://github.com/vpsinc/ice-frontend-react-mobx/commit/0a882ea))


### Features

* **docker:** Adding ICE-Analytics Env Vars ([#730](https://github.com/vpsinc/ice-frontend-react-mobx/issues/730)) ([8210c1c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8210c1c))
* **docker:** Moving things into env vars for easy config ([#728](https://github.com/vpsinc/ice-frontend-react-mobx/issues/728)) ([2eaf662](https://github.com/vpsinc/ice-frontend-react-mobx/commit/2eaf662))
* **docker:** Updating reactor:fetchjars to include dcsim jar ([#729](https://github.com/vpsinc/ice-frontend-react-mobx/issues/729)) ([d80470a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d80470a))


### Styles

* **trendcart:** Overall added number of enhancements ([0a1d442](https://github.com/vpsinc/ice-frontend-react-mobx/commit/0a1d442)), closes [#691](https://github.com/vpsinc/ice-frontend-react-mobx/issues/691) [#692](https://github.com/vpsinc/ice-frontend-react-mobx/issues/692) [#693](https://github.com/vpsinc/ice-frontend-react-mobx/issues/693) [#701](https://github.com/vpsinc/ice-frontend-react-mobx/issues/701)



<a name="2.3.0-beta.0"></a>
# [2.3.0-beta.0](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0...v2.3.0-beta.0) (2017-03-21)


### Bug Fixes

* **browser:** Fixing the event list ordering ([#688](https://github.com/vpsinc/ice-frontend-react-mobx/issues/688)) ([1056947](https://github.com/vpsinc/ice-frontend-react-mobx/commit/1056947))
* **dataTable:** Last login: Thu Mar 16 20:57:26 on ttys001 ([#703](https://github.com/vpsinc/ice-frontend-react-mobx/issues/703)) ([4a4435e](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4a4435e)), closes [#702](https://github.com/vpsinc/ice-frontend-react-mobx/issues/702)
* **deviceForm:** Fixed upstream failure sensor dropdown placement ([#695](https://github.com/vpsinc/ice-frontend-react-mobx/issues/695)) ([5f7aba3](https://github.com/vpsinc/ice-frontend-react-mobx/commit/5f7aba3))
* **dndDroppedRpdu:** Truncated name over 6 chars ([#707](https://github.com/vpsinc/ice-frontend-react-mobx/issues/707)) ([8480a4f](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8480a4f)), closes [#674](https://github.com/vpsinc/ice-frontend-react-mobx/issues/674)
* **phaseColors:** Changed feed phase indicators to match the US standard for color coding. ([#700](https://github.com/vpsinc/ice-frontend-react-mobx/issues/700)) ([b67636f](https://github.com/vpsinc/ice-frontend-react-mobx/commit/b67636f))
* **trendChart:** Fixing error in trenchart.js ([#680](https://github.com/vpsinc/ice-frontend-react-mobx/issues/680)) ([4c94e2a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4c94e2a))


### Features

* **charting:** PhaseBalance Chart and Phase Colors ([#681](https://github.com/vpsinc/ice-frontend-react-mobx/issues/681)) ([142df6c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/142df6c))
* **deviceList:** Used topology symbol for device state ([#698](https://github.com/vpsinc/ice-frontend-react-mobx/issues/698)) ([66c74b5](https://github.com/vpsinc/ice-frontend-react-mobx/commit/66c74b5))
* **rack builder:** Adding default rpdu selection ([#694](https://github.com/vpsinc/ice-frontend-react-mobx/issues/694)) ([ff08ba9](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ff08ba9)), closes [#578](https://github.com/vpsinc/ice-frontend-react-mobx/issues/578)
* **reports:** Zone Capacity Report ([#711](https://github.com/vpsinc/ice-frontend-react-mobx/issues/711)) ([922a8bf](https://github.com/vpsinc/ice-frontend-react-mobx/commit/922a8bf))



<a name="2.2.0"></a>
# [2.2.0](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.7...v2.2.0) (2017-03-06)



<a name="2.2.0-beta.7"></a>
# [2.2.0-beta.7](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.6...v2.2.0-beta.7) (2017-03-03)



<a name="2.2.0-beta.6"></a>
# [2.2.0-beta.6](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.5...v2.2.0-beta.6) (2017-03-03)



<a name="2.2.0-beta.5"></a>
# [2.2.0-beta.5](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.1...v2.2.0-beta.5) (2017-03-03)


### Features

* **$browser:** Implemented the Capacity Report Feature ([38d9c26](https://github.com/vpsinc/ice-frontend-react-mobx/commit/38d9c26)), closes [#613](https://github.com/vpsinc/ice-frontend-react-mobx/issues/613) [#614](https://github.com/vpsinc/ice-frontend-react-mobx/issues/614) [#615](https://github.com/vpsinc/ice-frontend-react-mobx/issues/615) [#616](https://github.com/vpsinc/ice-frontend-react-mobx/issues/616)



<a name="2.2.0-beta.4"></a>
# [2.2.0-beta.4](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.1...v2.2.0-beta.4) (2017-03-03)


### Bug Fixes

* **events:** Events was throwing errors ([#636](https://github.com/vpsinc/ice-frontend-react-mobx/issues/636)) ([8cb027f](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8cb027f))
* **RackCard:** Upating topology icons for rack card ([#635](https://github.com/vpsinc/ice-frontend-react-mobx/issues/635)) ([84de9cf](https://github.com/vpsinc/ice-frontend-react-mobx/commit/84de9cf)), closes [#632](https://github.com/vpsinc/ice-frontend-react-mobx/issues/632)
* **topologyDevice:** Adding case for "on" state ([#643](https://github.com/vpsinc/ice-frontend-react-mobx/issues/643)) ([d0074a9](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d0074a9))



<a name="2.2.0-beta.3"></a>
# [2.2.0-beta.3](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.1...v2.2.0-beta.3) (2017-03-02)


### Bug Fixes

* **datacenter:** Fixing refresh issue on datacenter page ([#626](https://github.com/vpsinc/ice-frontend-react-mobx/issues/626)) ([74fb483](https://github.com/vpsinc/ice-frontend-react-mobx/commit/74fb483)), closes [#620](https://github.com/vpsinc/ice-frontend-react-mobx/issues/620)
* **drConfig:** Hacky fix for updating DR State ([#623](https://github.com/vpsinc/ice-frontend-react-mobx/issues/623)) ([ac9bdbd](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ac9bdbd)), closes [#621](https://github.com/vpsinc/ice-frontend-react-mobx/issues/621)



<a name="2.2.0-beta.2"></a>
# [2.2.0-beta.2](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.1...v2.2.0-beta.2) (2017-03-01)


### Bug Fixes

* **events:** Fixing event model ([#598](https://github.com/vpsinc/ice-frontend-react-mobx/issues/598)) ([db16da6](https://github.com/vpsinc/ice-frontend-react-mobx/commit/db16da6))
* **stores:** Performance bandaids ([#612](https://github.com/vpsinc/ice-frontend-react-mobx/issues/612)) ([6fcd2b5](https://github.com/vpsinc/ice-frontend-react-mobx/commit/6fcd2b5))
* **webpack:** Hook for production conf ([#605](https://github.com/vpsinc/ice-frontend-react-mobx/issues/605)) ([1640d5b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/1640d5b))



<a name="2.2.0-beta.1"></a>
# [2.2.0-beta.1](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.2.0-beta.0...v2.2.0-beta.1) (2017-02-22)


### Bug Fixes

* **server:** Fixing server token ([#596](https://github.com/vpsinc/ice-frontend-react-mobx/issues/596)) ([410e245](https://github.com/vpsinc/ice-frontend-react-mobx/commit/410e245))



<a name="2.2.0-beta.0"></a>
# [2.2.0-beta.0](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.1.0-beta.0...v2.2.0-beta.0) (2017-02-21)


### Bug Fixes

* **device model:** Fixing typo in device model ([#531](https://github.com/vpsinc/ice-frontend-react-mobx/issues/531)) ([38b8efe](https://github.com/vpsinc/ice-frontend-react-mobx/commit/38b8efe))
* **RackCard:** Fix to data, adding defaults ([fa90305](https://github.com/vpsinc/ice-frontend-react-mobx/commit/fa90305))
* **rackForm:** Force 2N to be high priority. ([#508](https://github.com/vpsinc/ice-frontend-react-mobx/issues/508)) ([f16b707](https://github.com/vpsinc/ice-frontend-react-mobx/commit/f16b707)), closes [#492](https://github.com/vpsinc/ice-frontend-react-mobx/issues/492)
* fix save on builder ([af401fd](https://github.com/vpsinc/ice-frontend-react-mobx/commit/af401fd))


### Features

* **$browser:** Implemented 3 Phase Donut Chart ([44af1a6](https://github.com/vpsinc/ice-frontend-react-mobx/commit/44af1a6)), closes [#462](https://github.com/vpsinc/ice-frontend-react-mobx/issues/462)
* **FormWrapper:** Added form wrapper component ([#544](https://github.com/vpsinc/ice-frontend-react-mobx/issues/544)) ([bf18721](https://github.com/vpsinc/ice-frontend-react-mobx/commit/bf18721)), closes [#524](https://github.com/vpsinc/ice-frontend-react-mobx/issues/524)
* **navigation:** Added sticky navigation ([235eb3a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/235eb3a))
* **rackCard:** Refactor for new design ([#554](https://github.com/vpsinc/ice-frontend-react-mobx/issues/554)) ([c5374d9](https://github.com/vpsinc/ice-frontend-react-mobx/commit/c5374d9)), closes [#548](https://github.com/vpsinc/ice-frontend-react-mobx/issues/548)
* **zoneCard:** Moved rack cards inside zone card ([#561](https://github.com/vpsinc/ice-frontend-react-mobx/issues/561)) ([061594c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/061594c))


<a name="2.1.0"></a>
# [2.1.0](https://github.com/vpsinc/ice-frontend-react-mobx/compare/v2.1.0-beta.0...v2.1.0) (2017-01-26)


### Bug Fixes

* **$deviceForm:** Clear errors on cancel ([#477](https://github.com/vpsinc/ice-frontend-react-mobx/issues/477)) ([d074e30](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d074e30)), closes [#455](https://github.com/vpsinc/ice-frontend-react-mobx/issues/455)
* **$eventDialog:** Implemented ValueWrapper ([#480](https://github.com/vpsinc/ice-frontend-react-mobx/issues/480)) ([ab51855](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ab51855)), closes [#478](https://github.com/vpsinc/ice-frontend-react-mobx/issues/478)
* **$rackBuilder:** Device list filtering ([#488](https://github.com/vpsinc/ice-frontend-react-mobx/issues/488)) ([9c7eb6f](https://github.com/vpsinc/ice-frontend-react-mobx/commit/9c7eb6f)), closes [#402](https://github.com/vpsinc/ice-frontend-react-mobx/issues/402)
* **events:** Removed sorting dropdowns ([9e8d0f6](https://github.com/vpsinc/ice-frontend-react-mobx/commit/9e8d0f6)), closes [#401](https://github.com/vpsinc/ice-frontend-react-mobx/issues/401)
* **events:** Updating socket for events to send one event ([#368](https://github.com/vpsinc/ice-frontend-react-mobx/issues/368)) ([ad2b692](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ad2b692))
* **GroupCardDeviceList:** Added data for devices ([5d0f637](https://github.com/vpsinc/ice-frontend-react-mobx/commit/5d0f637)), closes [#230](https://github.com/vpsinc/ice-frontend-react-mobx/issues/230)
* **GroupMonitoringCard:** Fixed sizing and long number. ([1cbf4ed](https://github.com/vpsinc/ice-frontend-react-mobx/commit/1cbf4ed))
* **rackCard:** fixing bad ID injection on map statement ([53def6f](https://github.com/vpsinc/ice-frontend-react-mobx/commit/53def6f))
* **RackCard:** Fix to data, adding defaults ([fa90305](https://github.com/vpsinc/ice-frontend-react-mobx/commit/fa90305))
* **tokenCheck:** Token check now uses proper status code ([#389](https://github.com/vpsinc/ice-frontend-react-mobx/issues/389)) ([8952b1c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8952b1c))
* **zones:** Better updating of feeds for the zone ([#464](https://github.com/vpsinc/ice-frontend-react-mobx/issues/464)) ([cc548b0](https://github.com/vpsinc/ice-frontend-react-mobx/commit/cc548b0))
* **zones:** Deleting resources now updates feeds properly ([#476](https://github.com/vpsinc/ice-frontend-react-mobx/issues/476)) ([5bf24d0](https://github.com/vpsinc/ice-frontend-react-mobx/commit/5bf24d0))


### Features

* Adding a delete confirm component ([#370](https://github.com/vpsinc/ice-frontend-react-mobx/issues/370)) ([6325c97](https://github.com/vpsinc/ice-frontend-react-mobx/commit/6325c97)), closes [#336](https://github.com/vpsinc/ice-frontend-react-mobx/issues/336)
* Adding ability to delete zones ([#412](https://github.com/vpsinc/ice-frontend-react-mobx/issues/412)) ([6c44e46](https://github.com/vpsinc/ice-frontend-react-mobx/commit/6c44e46)), closes [#362](https://github.com/vpsinc/ice-frontend-react-mobx/issues/362)
* Adding in new notification system ([#371](https://github.com/vpsinc/ice-frontend-react-mobx/issues/371)) ([ff34ce4](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ff34ce4))
* Adding zones list component ([#408](https://github.com/vpsinc/ice-frontend-react-mobx/issues/408)) ([5da535d](https://github.com/vpsinc/ice-frontend-react-mobx/commit/5da535d))
* **$browser:** Adding interactivity to trend graph (displaying tracker info on hover) ([e654e30](https://github.com/vpsinc/ice-frontend-react-mobx/commit/e654e30))
* **$browser:** Grouped axes for feeds and load and another one for batterySoc ([7a4949b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/7a4949b)), closes [#449](https://github.com/vpsinc/ice-frontend-react-mobx/issues/449) [#450](https://github.com/vpsinc/ice-frontend-react-mobx/issues/450)
* **$browser:** Set `upstreamFailureSensor` on device ([5c7e871](https://github.com/vpsinc/ice-frontend-react-mobx/commit/5c7e871)), closes [#460](https://github.com/vpsinc/ice-frontend-react-mobx/issues/460)
* **$monitoring:** Added RackMonitoringCard component ([f13e7d9](https://github.com/vpsinc/ice-frontend-react-mobx/commit/f13e7d9)), closes [#199](https://github.com/vpsinc/ice-frontend-react-mobx/issues/199)
* **GroupCard:** Added view device toggle ([2d9382c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/2d9382c)), closes [#369](https://github.com/vpsinc/ice-frontend-react-mobx/issues/369)
* **monitoring:** Added event card ([92cb702](https://github.com/vpsinc/ice-frontend-react-mobx/commit/92cb702)), closes [#226](https://github.com/vpsinc/ice-frontend-react-mobx/issues/226)
* **navigation:** Added sticky navigation ([235eb3a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/235eb3a))
* **polling:** Adding rack metrics to polling ([#383](https://github.com/vpsinc/ice-frontend-react-mobx/issues/383)) ([8f4853a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8f4853a)), closes [#346](https://github.com/vpsinc/ice-frontend-react-mobx/issues/346)
* **rackMetrics:** Connecting Rack Metrics ([#384](https://github.com/vpsinc/ice-frontend-react-mobx/issues/384)) ([37fc311](https://github.com/vpsinc/ice-frontend-react-mobx/commit/37fc311)), closes [#366](https://github.com/vpsinc/ice-frontend-react-mobx/issues/366) [#357](https://github.com/vpsinc/ice-frontend-react-mobx/issues/357)
* **rowMonitoringCard:** Monitoring card data ([8c4d2d5](https://github.com/vpsinc/ice-frontend-react-mobx/commit/8c4d2d5))
* **settings:** Changed settings route to DC ([0044875](https://github.com/vpsinc/ice-frontend-react-mobx/commit/0044875)), closes [#398](https://github.com/vpsinc/ice-frontend-react-mobx/issues/398)
* **valueWrapper:** Added bubble color prop to ValueWrapper component ([d7c5595](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d7c5595))
* **views:** Adding Zones View ([#386](https://github.com/vpsinc/ice-frontend-react-mobx/issues/386)) ([ec28b56](https://github.com/vpsinc/ice-frontend-react-mobx/commit/ec28b56)), closes [#358](https://github.com/vpsinc/ice-frontend-react-mobx/issues/358)
* **ZoneCard:** Added ZoneCard ([2503520](https://github.com/vpsinc/ice-frontend-react-mobx/commit/2503520)), closes [#198](https://github.com/vpsinc/ice-frontend-react-mobx/issues/198)
* **zones:** Adding in a zone builder ([#457](https://github.com/vpsinc/ice-frontend-react-mobx/issues/457)) ([f0de5ff](https://github.com/vpsinc/ice-frontend-react-mobx/commit/f0de5ff)), closes [#364](https://github.com/vpsinc/ice-frontend-react-mobx/issues/364)
* **zones:** Adding the ability to add and edit zones. ([#411](https://github.com/vpsinc/ice-frontend-react-mobx/issues/411)) ([756fad4](https://github.com/vpsinc/ice-frontend-react-mobx/commit/756fad4)), closes [#359](https://github.com/vpsinc/ice-frontend-react-mobx/issues/359) [#360](https://github.com/vpsinc/ice-frontend-react-mobx/issues/360)
* **zones:** adding zone store and api methods ([#392](https://github.com/vpsinc/ice-frontend-react-mobx/issues/392)) ([680fcb9](https://github.com/vpsinc/ice-frontend-react-mobx/commit/680fcb9)), closes [#347](https://github.com/vpsinc/ice-frontend-react-mobx/issues/347) [#349](https://github.com/vpsinc/ice-frontend-react-mobx/issues/349)
* **zones:** Adding Zones to the Node.js API ([#385](https://github.com/vpsinc/ice-frontend-react-mobx/issues/385)) ([fd65552](https://github.com/vpsinc/ice-frontend-react-mobx/commit/fd65552)), closes [#345](https://github.com/vpsinc/ice-frontend-react-mobx/issues/345) [#344](https://github.com/vpsinc/ice-frontend-react-mobx/issues/344) [#343](https://github.com/vpsinc/ice-frontend-react-mobx/issues/343) [#342](https://github.com/vpsinc/ice-frontend-react-mobx/issues/342)
* **zones:** Connecting zone metrics socket messages ([#403](https://github.com/vpsinc/ice-frontend-react-mobx/issues/403)) ([3c8f867](https://github.com/vpsinc/ice-frontend-react-mobx/commit/3c8f867)), closes [#348](https://github.com/vpsinc/ice-frontend-react-mobx/issues/348) [#394](https://github.com/vpsinc/ice-frontend-react-mobx/issues/394)
* **Zones:** Added form for zones ([a91a35c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/a91a35c)), closes [#367](https://github.com/vpsinc/ice-frontend-react-mobx/issues/367)



<a name="2.1.0-beta.0"></a>
# 2.1.0-beta.0 (2017-01-07)


### Bug Fixes

* **auth:** fixing up user auth ([#254](https://github.com/vpsinc/ice-frontend-react-mobx/issues/254)) ([6ddcfac](https://github.com/vpsinc/ice-frontend-react-mobx/commit/6ddcfac))
* **deploy:** fixing bash logic for checking tags ([#181](https://github.com/vpsinc/ice-frontend-react-mobx/issues/181)) ([886ab61](https://github.com/vpsinc/ice-frontend-react-mobx/commit/886ab61))
* **devices:** fixing source feed property mapping ([d773181](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d773181))
* **docs:** fixing main readme link ([#176](https://github.com/vpsinc/ice-frontend-react-mobx/issues/176)) ([2d775a7](https://github.com/vpsinc/ice-frontend-react-mobx/commit/2d775a7))
* Fixing an error with updating data center info ([#236](https://github.com/vpsinc/ice-frontend-react-mobx/issues/236)) ([4e72f2b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4e72f2b))
* **travis:** deploy was missing a package ([#182](https://github.com/vpsinc/ice-frontend-react-mobx/issues/182)) ([9df5a1a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/9df5a1a))
* Fixing iceadmin and admin users ([963369b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/963369b))
* **release:** debugging gcs deployment ([#177](https://github.com/vpsinc/ice-frontend-react-mobx/issues/177)) ([3bed30e](https://github.com/vpsinc/ice-frontend-react-mobx/commit/3bed30e))
* Fixing phase up a bit ([#258](https://github.com/vpsinc/ice-frontend-react-mobx/issues/258)) ([79771d1](https://github.com/vpsinc/ice-frontend-react-mobx/commit/79771d1))
* Getting defaults set in user form, basic add seems to work ([#242](https://github.com/vpsinc/ice-frontend-react-mobx/issues/242)) ([6b9371b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/6b9371b))
* **server:** fixed a bug with editing devices ([03e3b33](https://github.com/vpsinc/ice-frontend-react-mobx/commit/03e3b33))
* **travis:** Fix for how we get and use tags ([fdae4df](https://github.com/vpsinc/ice-frontend-react-mobx/commit/fdae4df))
* **travis:** Revising deploy scripts and travis config ([#180](https://github.com/vpsinc/ice-frontend-react-mobx/issues/180)) ([7098dc6](https://github.com/vpsinc/ice-frontend-react-mobx/commit/7098dc6))
* Maps should now respect incoming deletes ([#197](https://github.com/vpsinc/ice-frontend-react-mobx/issues/197)) ([1a4c76b](https://github.com/vpsinc/ice-frontend-react-mobx/commit/1a4c76b)), closes [#196](https://github.com/vpsinc/ice-frontend-react-mobx/issues/196)
* Removing api catches ([474b23d](https://github.com/vpsinc/ice-frontend-react-mobx/commit/474b23d))
* setting up simple list and default for roles in the users list ([#237](https://github.com/vpsinc/ice-frontend-react-mobx/issues/237)) ([c33df7c](https://github.com/vpsinc/ice-frontend-react-mobx/commit/c33df7c))


### Features

* adding commitizen and cz-change-log ([927f1d5](https://github.com/vpsinc/ice-frontend-react-mobx/commit/927f1d5))
* adding in ui to config dr ([#232](https://github.com/vpsinc/ice-frontend-react-mobx/issues/232)) ([7a69cc6](https://github.com/vpsinc/ice-frontend-react-mobx/commit/7a69cc6))
* **docker:** exposing mongodb and elasticsearch ports ([#235](https://github.com/vpsinc/ice-frontend-react-mobx/issues/235)) ([4191446](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4191446))
* adding some build tools to package.json ([fda4370](https://github.com/vpsinc/ice-frontend-react-mobx/commit/fda4370))
* **auth:** Now preventing logins from accounts where "active" is set to false. ([4293810](https://github.com/vpsinc/ice-frontend-react-mobx/commit/4293810)), closes [#302](https://github.com/vpsinc/ice-frontend-react-mobx/issues/302)
* **drConfig:** Adding the Algorithm State Messages ([#319](https://github.com/vpsinc/ice-frontend-react-mobx/issues/319)) ([38d6125](https://github.com/vpsinc/ice-frontend-react-mobx/commit/38d6125)), closes [#301](https://github.com/vpsinc/ice-frontend-react-mobx/issues/301)
* **feedCards:** 3Phase Feed cards ([#194](https://github.com/vpsinc/ice-frontend-react-mobx/issues/194)) ([b72e926](https://github.com/vpsinc/ice-frontend-react-mobx/commit/b72e926))
* **feeds:** Feeds card with real data ([deccce9](https://github.com/vpsinc/ice-frontend-react-mobx/commit/deccce9))
* **groups:** adding rackshare and limit ([#231](https://github.com/vpsinc/ice-frontend-react-mobx/issues/231)) ([c13822e](https://github.com/vpsinc/ice-frontend-react-mobx/commit/c13822e))
* **server:** server now can use env var to set api base url ([#314](https://github.com/vpsinc/ice-frontend-react-mobx/issues/314)) ([d8bd83a](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d8bd83a))
* detecting when a user's password is expired ([eb29f54](https://github.com/vpsinc/ice-frontend-react-mobx/commit/eb29f54))
* Password Expiration handling ([df4404e](https://github.com/vpsinc/ice-frontend-react-mobx/commit/df4404e))
* puge containers command ([#256](https://github.com/vpsinc/ice-frontend-react-mobx/issues/256)) ([0ceb6fc](https://github.com/vpsinc/ice-frontend-react-mobx/commit/0ceb6fc))
* User change password ([d7e4a04](https://github.com/vpsinc/ice-frontend-react-mobx/commit/d7e4a04))
