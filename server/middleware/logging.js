var morgan = require('morgan');

let logType = 'combined';

if (process.env.NODE_ENV === 'development') {
  logType = 'dev';
}

module.exports = morgan(logType);
