let tokenCheck = (req, res, next) => {
  let token = req.get('Token');
  if (token) {
    req.token = token;
    next();
  } else {
    res.status(401).send({ error: 'A valid token is required.' });
  }
};

module.exports = tokenCheck;
