/*
When running in development mode we use webpack-dev-server to host our ui files.
So lets redirect to port 8081 which is where the webpack-dev-server should be running.
*/
const express = require('express');
const router = express.Router();

router.get('*', (req, res) => {
  console.log('dev middleware!');
  res.redirect('http://localhost:8081');
});

module.exports = router;
