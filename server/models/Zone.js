const debug = require('debug')('ice:models:zone'); // eslint-disable-line no-unused-vars

import { computed, observable, reaction } from 'mobx';
import debounce from '../helpers/debounce';

export const ZoneDefaults = {
  name: null,
  zoneType: null,
  allocatedPower: null
};

export class Zone {
  _id = null;
  @observable name = ZoneDefaults.name;
  @observable zoneType = ZoneDefaults.zoneType;
  @observable allocatedPower = ZoneDefaults.allocatedPower;
  rackIds = [];
  @observable rackIdsString = '';
  groups = [];
  @observable groupsString = '';
  breakerLimits = [];
  @observable breakerLimitsString = '';
  iceLimits = [];
  @observable iceLimitsString = '';

  io = null;
  broadcaster = null;

  constructor (io) {
    this.io = io;

    this.broadcaster = reaction(
      () => this.changed,
      debounce((json) => {
        debug('Zone updated:', json);
        this.io.emit('msg', { type: 'zone_updated', data: this.toJson });
      })
    );
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    return {
      id: this.id,
      name: this.name,
      zoneType: this.zoneType,
      allocatedPower: this.allocatedPower,
      rackIds: this.rackIds,
      groups: this.groups,
      breakerLimits: this.breakerLimits,
      iceLimits: this.iceLimits
    };
  }

  @computed get changed () {
    return {
      id: this.id,
      name: this.name,
      zoneType: this.zoneType,
      allocatedPower: this.allocatedPower,
      rackIds: this.rackIdsString,
      groups: this.groupsString,
      breakerLimits: this.breakerLimitsString,
      iceLimits: this.iceLimitsString
    };
  }

  updateFromJson (json) {
    this._id = json.id;
    this.name = json.name || ZoneDefaults.name;
    this.zoneType = json.zoneType || ZoneDefaults.zoneType;
    this.allocatedPower = json.allocatedPower || ZoneDefaults.allocatedPower;

    if (Array.isArray(json.rackIds)) {
      this.rackIds = json.rackIds;
      this.rackIdsString = json.rackIds.join(',');
    }

    if (Array.isArray(json.groups)) {
      this.groups = json.groups;
      this.groupsString = json.groups.join(',');
    }

    if (Array.isArray(json.breakerLimits)) {
      this.breakerLimits = json.breakerLimits;
      this.breakerLimitsString = JSON.stringify(json.breakerLimits);
    }

    if (Array.isArray(json.iceLimits)) {
      this.iceLimits = json.iceLimits;
      this.iceLimitsString = JSON.stringify(json.iceLimits);
    }
  }

  dispose () {
    this.io.emit('msg', { type: 'zone_deleted', data: this.id });
    this.broadcaster();
  }
}
