const debug = require('debug')('ice:models:rack'); // eslint-disable-line no-unused-vars

import { computed, observable, reaction } from 'mobx';
import debounce from '../helpers/debounce';

export const RackDefaults = {
  name: null,
  allocatedPower: null,
  priority: 1,
  redundancyStrategy: '2N',
  rPdus: [],
  defaultRpdus: []
};

export class Rack {
  _id = null;
  @observable name = RackDefaults.name;
  @observable allocatedPower = RackDefaults.allocatedPower;
  @observable priority = RackDefaults.priority;
  @observable redundancyStrategy = RackDefaults.redundancyStrategy;
  rPdus = RackDefaults.rPdus;
  @observable rPdusString = '';
  defaultRpdus = RackDefaults.defaultRpdus;
  @observable defaultRpdusString = '';

  io = null;
  broadcaster = null;

  constructor (io) {
    this.io = io;

    this.broadcaster = reaction(
      () => this.changed,
      debounce((json) => {
        debug('Rack updated:', json);
        this.io.emit('msg', { type: 'rack_updated', data: this.toJson });
      })
    );
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    return {
      id: this.id,
      name: this.name,
      allocatedPower: Number(this.allocatedPower),
      priority: this.priority,
      redundancyStrategy: this.redundancyStrategy,
      rPdus: this.rPdus,
      defaultRpdus: this.defaultRpdus
    };
  }

  @computed get changed () {
    return {
      id: this.id,
      name: this.name,
      allocatedPower: Number(this.allocatedPower),
      priority: this.priority,
      redundancyStrategy: this.redundancyStrategy,
      rPdus: this.rPdusString,
      defaultRpdus: this.defaultRpdusString
    };
  }

  updateFromJson (json) {
    this._id = json.id;
    this.name = json.name || RackDefaults.name;
    this.allocatedPower = json.allocatedPower || RackDefaults.allocatedPower;
    this.priority = json.priority || RackDefaults.priority;
    this.redundancyStrategy = json.redundancyStrategy || RackDefaults.redundancyStrategy;

    if (Array.isArray(json.rPdus)) {
      this.rPdus = json.rPdus;
      this.rPdusString = json.rPdus.join(',');
    }

    if (Array.isArray(json.defaultRpdus)) {
      this.defaultRpdus = json.defaultRpdus;
      this.defaultRpdusString = json.defaultRpdus.join(',');
    }
  }

  dispose () {
    this.io.emit('msg', { type: 'rack_deleted', data: this.id });
    this.broadcaster();
  }
}
