const debug = require('debug')('ice:models:group'); // eslint-disable-line no-unused-vars

import { computed, observable, reaction } from 'mobx';
import debounce from '../helpers/debounce';

export const GroupDefaults = {
  name: null,
  deviceIds: [],
  rackshareEnabled: false,
  maxLimit: 1E9
};

export class Group {
  _id = null;
  @observable name = GroupDefaults.name;
  deviceIds = GroupDefaults.deviceIds;
  @observable deviceIdsString = '';
  @observable rackshareEnabled = GroupDefaults.rackshareEnabled;
  @observable maxLimit = GroupDefaults.maxLimit;

  io = null;
  broadcaster = null;

  constructor (io) {
    this.io = io;

    this.broadcaster = reaction(
      () => this.changed,
      debounce((json) => {
        debug('Group updated:', json);
        this.io.emit('msg', { type: 'group_updated', data: this.toJson });
      })
    );
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    return {
      id: this._id,
      name: this.name,
      deviceIds: this.deviceIds,
      rackshareConfig: {
        rackshareEnabled: this.rackshareEnabled,
        maxLimit: this.maxLimit
      }
    };
  }

  @computed get changed () {
    return {
      id: this._id,
      name: this.name,
      deviceIds: this.deviceIdsString,
      rackshareConfig: {
        rackshareEnabled: this.rackshareEnabled,
        maxLimit: this.maxLimit
      }
    };
  }

  updateFromJson (json) {
    this._id = json.id;
    this.name = json.name || GroupDefaults.name;
    this.rackshareEnabled = json.rackshareConfig.rackshareEnabled || GroupDefaults.rackshareEnabled;
    this.maxLimit = json.rackshareConfig.maxLimit || GroupDefaults.maxLimit;

    if (Array.isArray(json.deviceIds)) {
      this.deviceIds = json.deviceIds;
      this.deviceIdsString = json.deviceIds.join(',');
    }
  }

  dispose () {
    this.io.emit('msg', { type: 'group_deleted', data: this.id });
    this.broadcaster();
  }
}
