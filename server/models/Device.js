const debug = require('debug')('ice:models:feed'); // eslint-disable-line no-unused-vars

import { computed, observable, reaction } from 'mobx';
import debounce from '../helpers/debounce';

export const DeviceDefaults = {
  name: null,
  ip: null,
  port: null,
  deviceType: null,
  deviceModel: null,
  sourceFeed: null,
  phase: null,
  desc: null,
  sensorStreamEnabled: null,
  upstreamFailureSensor: null,
  isConnected: false,
  state: null,
  deviceBreakerLimit: null
};

export class Device {
  _id = null;
  @observable name = DeviceDefaults.name;
  @observable ip = DeviceDefaults.ip;
  @observable port = DeviceDefaults.port;
  @observable deviceType = DeviceDefaults.deviceType;
  @observable deviceModel = DeviceDefaults.deviceModel;
  @observable sourceFeed = DeviceDefaults.sourceFeed;
  @observable phase = DeviceDefaults.phase;
  @observable desc = DeviceDefaults.desc;
  @observable sensorStreamEnabled = DeviceDefaults.sensorStreamEnabled;
  @observable upstreamFailureSensor = DeviceDefaults.upstreamFailureSensor;
  @observable isConnected = DeviceDefaults.isConnected;
  @observable state = DeviceDefaults.state;
  @observable deviceBreakerLimit = DeviceDefaults.deviceBreakerLimit;

  io = null;
  broadcaster = null;

  constructor (io) {
    this.io = io;

    this.broadcaster = reaction(
      () => this.toJson,
      debounce((json) => {
        debug('Device object updated...', json);
        this.io.emit('msg', { type: 'device_updated', data: json });
      })
    );
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    return {
      id: this.id,
      name: this.name,
      ip: this.ip,
      port: this.port,
      deviceType: this.deviceType,
      deviceModel: this.deviceModel,
      sourceFeed: this.sourceFeed,
      phase: this.phase,
      desc: this.desc,
      sensorStreamEnabled: this.sensorStreamEnabled,
      upstreamFailureSensor: this.upstreamFailureSensor,
      isConnected: this.isConnected,
      state: this.state,
      deviceBreakerLimit: this.deviceBreakerLimit
    };
  }

  updateFromJson (json) {
    this._id = json.id;
    this.name = json.name || DeviceDefaults.name;
    this.ip = json.ip || DeviceDefaults.ip;
    this.port = json.port || DeviceDefaults.port;
    this.deviceType = json.deviceType || DeviceDefaults.deviceType;
    this.deviceModel = json.deviceModel || DeviceDefaults.deviceModel;
    this.sourceFeed = json.sourceFeed || DeviceDefaults.sourceFeed;
    this.phase = json.phase || DeviceDefaults.phase;
    this.desc = json.desc || DeviceDefaults.desc;
    this.sensorStreamEnabled = json.sensorStreamEnabled || DeviceDefaults.sensorStreamEnabled;
    this.upstreamFailureSensor = json.upstreamFailureSensor || DeviceDefaults.upstreamFailureSensor;
    this.isConnected = json.isConnected || DeviceDefaults.isConnected;
    this.state = json.state || DeviceDefaults.state;
    this.deviceBreakerLimit = json.deviceBreakerLimit || DeviceDefaults.deviceBreakerLimit;
  }

  dispose () {
    this.io.emit('msg', { type: 'device_deleted', data: this.id });
    this.broadcaster();
  }
}
