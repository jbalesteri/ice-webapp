const debug = require('debug')('ice:models:feed'); // eslint-disable-line no-unused-vars

const { computed, observable, reaction } = require('mobx');
import debounce from '../helpers/debounce';

export const FeedDefaults = {
  name: null,
  symbol: null,
  feedType: null
};

export class Feed {
  _id = null;
  @observable name = FeedDefaults.name;
  @observable symbol = FeedDefaults.symbol;
  @observable feedType = FeedDefaults.feedType;

  io = null;
  broadcaster = null;

  constructor (io) {
    this.io = io;

    this.broadcaster = reaction(
      () => this.toJson,
      debounce((json) => {
        debug('Feed object changed...', json);
        this.io.emit('msg', { type: 'feed_updated', data: json });
      })
    );
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    return {
      id: this.id,
      name: this.name,
      symbol: this.symbol,
      feedType: this.feedType
    };
  }

  updateFromJson (json) {
    this._id = json.id;
    this.name = json.name || FeedDefaults.name;
    this.symbol = json.symbol || FeedDefaults.symbol;
    this.feedType = json.feedType || FeedDefaults.feedType;
  }

  dispose () {
    this.io.emit('msg', { type: 'feed_deleted', data: this.id });
    this.broadcaster();
  }
}
