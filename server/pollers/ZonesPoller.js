const debug = require('debug')('ice:pollers:zones');

import { Zone } from '../models/Zone';

export default class ZonesPoller {
  map = null;
  io = null;
  api = null;

  interval = null;

  constructor (map, io, api) {
    this.map = map;
    this.io = io;
    this.api = api;
  }

  startPolling () {
    debug('Starting zones polling');
    this.interval = setInterval(this.poll, 500);
  }

  stopPolling () {
    debug('Stopping zones polling');
    clearInterval(this.interval);
  }

  poll = () => {
    this.api.getZones().then((zones) => {
      this.updateFromJson(zones);
    }).catch((error) => {
      this.pollError(error);
    });
  }

  updateFromJson (json) {
    if (Array.isArray(json)) {
      let newIds = [];
      json.forEach((zoneJson) => {
        let zone = this.map.get(zoneJson.id);

        if (!zone) {
          zone = new Zone(this.io);
          this.map.set(zoneJson.id, zone);
        }

        zone.updateFromJson(zoneJson);
        newIds.push(zone.id);
      });
    }
  }

  pollError (error) {
    debug('Error polling zones:', error);
  }
}
