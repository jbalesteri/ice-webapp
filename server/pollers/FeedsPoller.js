const debug = require('debug')('ice:pollers:feeds');

import { Feed } from '../models/Feed';

export default class FeedsPoller {
  map = null;
  io = null;
  api = null;

  interval = null;

  constructor (map, io, api) {
    this.map = map;
    this.io = io;
    this.api = api;
  }

  startPolling () {
    debug('Starting feed polling');
    this.interval = setInterval(this.poll, 500);
  }

  stopPolling () {
    debug('Stopping feed polling');
    clearInterval(this.interval);
  }

  poll = () => {
    return this.api.getFeeds().then((json) => {
      this.updateFromJson(json);
    }).catch((error) => {
      this.pollError(error);
    });
  }

  updateFromJson (json) {
    if (Array.isArray(json)) {
      let newIds = [];

      json.forEach((feedJson) => {
        let id = feedJson.id;
        newIds.push(id);

        let feed = this.map.get(id);

        if (!feed) {
          feed = new Feed(this.io);
          this.map.set(id, feed);
        }

        feed.updateFromJson(feedJson);
      });

      let deleteIds = Array.from(this.map.keys()).filter((id) => (newIds.indexOf(id) === -1));

      deleteIds.forEach((id) => {
        this.map.get(id).dispose();
        this.map.delete(id);
      });
    } else {
      debug('Error updating feeds from json, json is not an array.');
    }
  }

  pollError (error) {
    debug('Error polling feeds:', error);
  }
}
