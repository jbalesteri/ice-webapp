const debug = require('debug')('ice:pollers:devices');

import { Device } from '../models/Device';

export default class DevicesPoller {
  map = null;
  io = null;
  api = null;

  interval = null;

  constructor (map, io, api) {
    this.map = map;
    this.io = io;
    this.api = api;
  }

  startPolling () {
    debug('Starting device polling');
    this.interval = setInterval(this.poll, 500);
  }

  stopPolling () {
    debug('Stopping device polling');
    clearInterval(this.interval);
  }

  poll = () => {
    return this.api.getDevices().then((json) => {
      this.updateFromJson(json);
    }).catch((error) => {
      this.pollError(error);
    });
  }

  updateFromJson (json) {
    if (Array.isArray(json)) {
      let newIds = [];
      json.forEach((deviceJson) => {
        newIds.push(deviceJson.id);
        let device = this.map.get(deviceJson.id);

        if (!device) {
          device = new Device(this.io);
          this.map.set(deviceJson.id, device);
        }

        device.updateFromJson(deviceJson);
      });

      let deleteIds = Array.from(this.map.keys()).filter((id) => (newIds.indexOf(id) === -1));

      deleteIds.forEach((id) => {
        this.map.get(id).dispose();
        this.map.delete(id);
      });
    }
  }

  pollError (error) {
    debug('Error polling devices:', error);
  }
}
