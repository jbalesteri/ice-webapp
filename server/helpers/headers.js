var headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
  'Access-Control-Allow-Credentials': 'true',
  'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,HEAD',
  'Access-Control-Allow-Headers': 'Token, Content-Type, X-Requested-With, Authorization'
};

module.exports = headers;
