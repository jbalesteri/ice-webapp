const ReactorApi = require('@vpsinc/reactor-api-lib');
var cache = require('./cache');
const toJS = require('mobx').toJS;
var api = new ReactorApi();

class apiMocker {
  constructor () {
    var users = [
            {id: 1, userName: 'bc', password: 'test1', email: 'a@b.com', role: 'admin'},
            {id: 2, userName: 'bd', password: 'test2', email: 'a@c.com', role: 'admin'},
            {id: 3, userName: 'be', password: 'test3', email: 'a@d.com', role: 'admin'},
            {id: 4, userName: 'bf', password: 'test4', email: 'a@e.com', role: 'admin'}
    ];

    users.forEach(function (user) {
      cache.users.set(user.id, user);
    });

    api.getUsers = function () {
      var p = new Promise((resolve) => {
        setTimeout(() => {
          var myUsers = [];
          cache.users.forEach(function (user) {
            myUsers.push(toJS(user));
          });

          resolve(myUsers);
        }, 500);
      });

      return p;
    };

    api.createUser = function (body) {
      var p = new Promise((resolve) => {
        setTimeout(() => {
          var user = {};
          user.id = Math.round(Math.random() * 1000);
          user.name = body.userName;
          user.email = body.email;
          user.role = body.role;
          user.password = body.password;
          cache.users.set(user.id, user);
          resolve(user);
        }, 500);
      });

      return p;
    };

    api.updateUser = function (id, body) {
      var p = new Promise((resolve) => {
        setTimeout(() => {
          var existingUser = cache.users.get(id);
          for (var j in body) {
            existingUser[j] = body[j];
          }
          cache.users.set(existingUser.id, existingUser);
          cache.updatedUser = existingUser;
          resolve(toJS(existingUser));
        }, 500);
      });

      return p;
    };

    api.deleteUser = function (id) {
      var p = new Promise((resolve) => {
        setTimeout(() => {
          var existingUser = cache.users.get(id);
          cache.users.delete(id);
          resolve(toJS(existingUser));
        }, 500);
      });

      return p;
    };

    return api;
  }
}

module.exports = apiMocker;
