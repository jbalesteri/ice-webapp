let errorMessage = (defaultMessage, error) => {
  let message = defaultMessage;

  if (error.response) {
    if (error.response.data) {
      message = error.response.data;
    } else if (error.response.body) {
      message = error.response.body;
    }
  } else if (error.message) {
    message = error.message;
  }

  return message;
};

module.exports = errorMessage;
