var observable = require('mobx').observable;

var cache = module.exports = { // eslint-disable-line no-unused-vars
  dataCenter: observable({
    description: null,
    location: null,
    name: null
  }),
  rand: Math.random(),
  health: observable({
    clusterLeader: null,
    clusterHealth: null,
    elasticsearchStatus: null,
    groupsStatus: null,
    mongoStatus: null,
    recentlyActiveDevices: null,
    usedDiskSpace: null,
    usedMemory: null
  }),
  capacityReport: observable({
    zoneName: null,
    allocatedPower: null,
    available2nCapacity: 0.00,
    available1nCapacity: 0.00,
    availableCapacity: 0.00,
    zone2nPeak: 0.00,
    zone1nPeak: 0.00,
    zonePeak: 0.00,
    breakers: [],
    rackData: []
  }),
  catalogDevices: observable.map({}),
  feeds: new Map(),
  devices: new Map(),
  groups: new Map(),
  racks: new Map(),
  zones: new Map(),
  events: observable.map({}),
  drConfig: observable.map({})
};
