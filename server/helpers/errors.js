let getMessage = (error) => {
  let message = null;

  if (error.response) {
    if (error.response.data) {
      message = error.response.data;
    } else if (error.response.body) {
      message = error.response.body;
    }
  } else if (error.message) {
    message = error.message;
  }

  return message;
};

let getStatus = (error) => {
  let status = null;

  if (error && error.response && error.response.status) {
    status = error.response.status;
  }

  return status;
};

let getErrorResponse = (error, defaultMessage) => {
  let status = getStatus(error) || 500;
  let message = getMessage(error) || defaultMessage;

  let body = (error.response && error.response.body) ? error.response.body : null;
  let data = (error.response && error.response.data) ? error.response.data : null;
  let errorMessage = error.message || null;

  return {
    status: status,
    message: message,
    body: body,
    data: data,
    errorMsg: errorMessage
  };
};

module.exports = {
  getErrorResponse: getErrorResponse,
  getMessage: getMessage,
  getStatus: getStatus
};
