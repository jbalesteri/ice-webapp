const debug = require('debug')('ice:api-base');

const API_BASE = (process.env.API_BASE) ? process.env.API_BASE : 'http://localhost:9001';

debug('Base being set to: %s', API_BASE);

module.exports = API_BASE;
