const debug = require('debug')('ice:server:polling');
const chalk = require('chalk');

const adminUser = require('./admin-user');
const cache = require('./cache');
const ReactorApi = require('@vpsinc/reactor-api-lib');
const API_BASE = require('./api-base');

const api = new ReactorApi(API_BASE);

import FeedsPoller from '../pollers/FeedsPoller';
import DevicesPoller from '../pollers/DevicesPoller';
import GroupsPoller from '../pollers/GroupsPoller';
import RacksPoller from '../pollers/RacksPoller';
import ZonesPoller from '../pollers/ZonesPoller';

module.exports = function (io) {
  let token = null;
  let user = adminUser.user;
  let pass = adminUser.pass;
  let connectTimeout = null;

  let pollers = new Map();

  var auth = function () {
    return api.login(user, pass).then((session) => {
      token = session.token;
      return token;
    });
  };

  // Connect and Poll
  var connect = () => {
    console.log(chalk.cyan('Logging into ice-reactor @%s...'), API_BASE);
    auth().then((token) => {
      if (token) {
        console.log(chalk.green('Logged in to ice-reactor @%s, given token: %s!'), API_BASE, token);
        api.API_TOKEN = token;

        pollers.set('feedsPoller', new FeedsPoller(cache.feeds, io, api));
        pollers.set('devicesPoller', new DevicesPoller(cache.devices, io, api));
        pollers.set('groupsPoller', new GroupsPoller(cache.groups, io, api));
        pollers.set('racksPoller', new RacksPoller(cache.racks, io, api));
        pollers.set('zonesPoller', new ZonesPoller(cache.zones, io, api));

        // immediately grab catalog, it polls slowly
        getCatalogDevices();
        startPolling();
      } else {
        console.log(chalk.red('Unable to log into ice-reactor@%s.'), API_BASE);
        debug('Unable to authorize, trying again.');
        clearTimeout(connectTimeout);
        connectTimeout = setTimeout(connect, 1000);
      }
    }).catch((error) => {
      debug('Unable to connect, trying again.');
      debug('Error:', error);
      clearTimeout(connectTimeout);
      connectTimeout = setTimeout(connect, 1000);
    });

    let checkSession = setInterval(() => {
      // if we have a token, check it
      if (token) {
        // call the api with our token
        api.getSession(token, token).then((session) => {
          if (!session || !session.token) {
            clearInterval(checkSession);
            token = false;
            stopPolling();
            connect();
          }
        }).catch(() => {
          clearInterval(checkSession);
          stopPolling();
          connect();
        });
      }
    }, 1000);
  };

  connect();

  // pollers and intervals
  let racksMetricsPoller;
  let racksMeticsPollRate = 500;
  let feedMetricsPoller;
  let feedMetricsPollRate = 500;
  let isPollingFeedMetrics = false;
  let eventsPoller;
  let eventsPollRate = 500;
  let datacenterPoller;
  let datacenterPollRate = 5000;
  let healthPoller;
  let healthPollRate = 5000;
  let catalogPoller;
  let catalogPollRate = 10000;
  let sensorDataPoller;
  let sensorDataPollRate = 500;
  let isPollingSensorData = false;
  let groupSensorDataPoller;
  let groupSensorDataPollRate = 500;
  let isPollingGroupSensorData = false;
  let zoneMetricsPoller;
  let zoneMetricsPollRate = 1000;

  var pollRacksMetrics = () => {
    return setInterval(() => {
      return api.getRackMetrics(token).then((metrics) => {
        io.emit('msg', { type: 'racks_metrics', data: metrics });
      }).catch(() => {
        debug('No rack metrics data');
      });
    }, racksMeticsPollRate);
  };

  var pollFeedMetrics = () => {
    return setInterval(() => {
      if (isPollingFeedMetrics) return;

      isPollingFeedMetrics = true;
      api.getFeedMetrics().then((sensorData) => {
        isPollingFeedMetrics = false;
        io.emit('msg', { type: 'feeds_sensorData', data: sensorData });
      }).catch(() => {
        debug('No feed metrics data');
        isPollingFeedMetrics = false;
      });
    }, feedMetricsPollRate);
  };

  var pollEvents = () => {
    return setInterval(() => {
      return api.getEvents(token).then((events) => {
        reconcileMap(cache.events, events);
        events = events.reduce(createIdMap, {});
        cache.events.merge(events);
        return cache.events;
      });
    }, eventsPollRate);
  };

  var pollDatacenter = () => {
    return setInterval(() => {
      return api.getDataCenter(token).then((datacenter) => {
        if (datacenter.name === '4Y1G3hs8eIu3gTvr5bCWfojOgvv6aJkGdfFJ8DTXJojshYeiDSYio9KasccR3xVq') {
          datacenter.name = null;
        }

        cache.dataCenter.name = datacenter.name || '';
        cache.dataCenter.description = datacenter.description || '';
        cache.dataCenter.location = datacenter.location || '';
        cache.dataCenter.metricsAggregateResetTimestamp = datacenter.metricsAggregateResetTimestamp || '';
      });
    }, datacenterPollRate);
  };

  var pollHealth = () => {
    return setInterval(() => {
      return api.getHealth(token).then((health) => {
        cache.health.clusterLeader = health.clusterLeader || null;
        cache.health.clusterHealth = health.clusterHealth || null;
        cache.health.elasticsearchStatus = health.elasticsearchStatus || null;
        cache.health.groupsStatus = health.groupsStatus || null;
        cache.health.mongoStatus = health.mongoStatus || null;
        cache.health.recentlyActiveDevices = health.recentlyActiveDevices || null;
        cache.health.usedDiskSpace = health.usedDiskSpace || null;
        cache.health.usedMemory = health.usedMemory || null;
      }).catch((error) => {
        debug('error fetching system health: %s', error.message);
      });
    }, healthPollRate);
  };

  var pollCatalog = () => {
    return setInterval(() => {
      return getCatalogDevices(token);
    }, catalogPollRate);
  };

  var pollSensorData = () => {
    return setInterval(() => {
      if (isPollingSensorData) return;

      isPollingSensorData = true;
      api.getDevicesSensorData(token).then((sensorData) => {
        io.emit('msg', { type: 'devices_sensorData', data: sensorData });
      }).catch(() => {
        debug('No device sensor data for devices');
      });

      isPollingSensorData = false;
    }, sensorDataPollRate);
  };

  var pollGroupSensorData = () => {
    return setInterval(() => {
      if (isPollingGroupSensorData) return;

      isPollingGroupSensorData = true;

      api.getGroupsSensorData(token).then((sensorData) => {
        io.emit('msg', { type: 'groups_sensorData', data: sensorData });
      }).catch(() => {
        debug('No group sensor data for groups');
      });
      isPollingGroupSensorData = false;
    }, groupSensorDataPollRate);
  };

  var pollZoneMetrics = () => {
    return setInterval(() => {
      api.getZonesMetrics(token).then((metrics) => {
        io.emit('msg', { type: 'zone_metrics', data: metrics });
      }).catch((error) => {
        debug('No zone metrics');
        debug(error.message);
      });
    }, zoneMetricsPollRate);
  };

  // always poll health
  healthPoller = pollHealth();

  var startPolling = () => {
    console.log(chalk.green('Starting polling @%s.'), API_BASE);
    pollers.forEach((poller) => {
      poller.startPolling();
    });

    racksMetricsPoller = pollRacksMetrics();
    eventsPoller = pollEvents();
    datacenterPoller = pollDatacenter();
    catalogPoller = pollCatalog();
    sensorDataPoller = pollSensorData();
    groupSensorDataPoller = pollGroupSensorData();
    feedMetricsPoller = pollFeedMetrics();
    zoneMetricsPoller = pollZoneMetrics();
  };

  var stopPolling = () => { // eslint-disable-line no-unused-vars
    console.log(chalk.red('Stopping polling @%s.'), API_BASE);
    pollers.forEach((poller) => {
      poller.stopPolling();
    });

    clearInterval(racksMetricsPoller);
    clearInterval(eventsPoller);
    clearInterval(datacenterPoller);
    clearInterval(catalogPoller);
    clearInterval(healthPoller);
    clearInterval(sensorDataPoller);
    clearInterval(groupSensorDataPoller);
    clearInterval(feedMetricsPoller);
    clearInterval(zoneMetricsPoller);
    console.log(chalk.green('Polling stoped @%s.'), API_BASE);
  };

  var getCatalogDevices = () => {
    return api.getCatalog(token).then((devices) => {
      if (!devices.reduce) { return; };
      ; devices = devices.reduce((map, obj) => {
        map[obj.deviceModel] = obj;
        return map;
      }, {});

      cache.catalogDevices.merge(devices);
      return cache.catalogDevices;
    });
  };
};

var reconcileMap = (orig, newItems) => {
  // Check lenghts for deletes
  if (newItems.length < orig.size) {
    let origKeys = orig.keys();
    let newKeys = newItems.map((item) => (item.id));

    let deletedKeys = origKeys.filter((key) => {
      return newKeys.indexOf(key) === -1;
    });

    deletedKeys.forEach((key) => {
      orig.delete(key);
    });
  }
};

var createIdMap = function (map, obj) {
  map[obj.id] = obj;
  return map;
};
