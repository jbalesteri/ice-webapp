const reaction = require('mobx').reaction;
const toJS = require('mobx').toJS;

const cache = require('./cache');

module.exports = (io) => {
  if (!io) {
    throw new Error('An instance of Socket.io must be passed to the watcher module.');
  }

  // Catalog
  reaction(() => {
    return cache.catalogDevices.size;
  }, () => {
    io.emit('msg', { type: 'catalog_updated', data: cache.catalogDevices.values() });
  });

  // DataCenter
  reaction(() => {
    return cache.dataCenter;
  }, () => {
    io.emit('msg', { type: 'datacenter_updated', data: toJS(cache.dataCenter) });
  });

  // Events
  reaction(() => {
    return cache.events.size;
  }, () => {
    let events = cache.events.values();
    let event = events[events.length - 1];
    io.emit('msg', { type: 'events_updated', data: event });
  });

  // Health
  reaction(() => {
    return cache.health;
  }, () => {
    io.emit('msg', { type: 'health_updated', data: toJS(cache.health) });
  });
};
