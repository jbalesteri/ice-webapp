const debug = require('debug')('ice:server:msg');

var msg = function (socket) {
  debug('message received on io==>socket:', socket);
};

module.exports = msg;
