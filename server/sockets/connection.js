// const io = require('../socket');

let connectedClients = [];

var connection = function (io) {
  connectedClients.push(io);

  // Broadcast to all
  io.emit('msg', {type: 'client_connected'});

  io.on('msg', (evt) => {
    switch (evt.type) {
      case 'authenticate':
        io.emit('msg', {
          type: 'user_joined',
          id: evt.id,
          userId: evt.userId,
          userName: evt.username
        });
        break;
    }
  });
};

module.exports = connection;
