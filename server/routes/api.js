const proxy = require('http-proxy-middleware');
const express = require('express');
const router = express.Router();

const API_BASE = require('../helpers/api-base');

router.use(proxy('/api', {
  target: API_BASE,
  pathRewrite: {
    '^/api': '/v3'
  },
  xfwd: true,
  logLevel: 'debug'
}));

module.exports = router;
