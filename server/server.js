require('babel-register');

const debug = require('debug')('ice:server:servrer');
const chalk = require('chalk');

const http = require('http');

const app = require('express')();

console.log(chalk.bold.cyan('Starting ICE-Console Server'));

// --------- Compression ------ //
const compression = require('compression');
app.use(compression());

// ----------- PORT --------- //
let port = process.env.PORT || 8080;
app.set('port', port);

// ----------- HTTP --------- //
var server = http.createServer(app);

// ----------- SOCKET --------- //
var io = require('socket.io').listen(server);

const connection = require('./sockets/connection');
io.on('connection', connection);

const msg = require('./sockets/msg');
io.on('msg', msg);

app.use((req, res, next) => {
  req.io = io;
  next();
});

// ---------- LOGGING --------- //
const logging = require('./middleware/logging');
app.use(logging);

// ---------- ROUTES --------- //
// API Routes
const api = require('./routes/api');
app.use(api);

// History Fallback Middleware
if (process.env.NODE_ENV === 'production') {
  var history = require('connect-history-api-fallback');
  app.use(history());
}

// static routes
const pub = require('./routes/pub');
app.use(pub);

// ---------- Middleware -------- //
// Body Parser
app.use(require('body-parser').json());

// ---------- DEVELOPMENT --------- //
// Development Middleware
if (process.env.NODE_ENV === 'development') {
  let devMiddleware = require('./middleware/development');
  app.use(devMiddleware);
}

// ---------- LISTEN ---------- //
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError (error) {
  debug('server::onError()==>error.code:', error.code);
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(chalk.red('%s requires elevated privileges'), bind);
      process.exit(1);
      break; // eslint-disable-line no-unreachable
    case 'EADDRINUSE':
      console.error(chalk.red('%s is already in use'), bind);
      process.exit(1);
      break; // eslint-disable-line no-unreachable
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
let poller; // eslint-disable-line
let watcher; // eslint-disable-line

function onListening () {
  poller = require('./helpers/polling')(io);
  watcher = require('./helpers/watchers')(io);

  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;

  console.log(chalk.bold.green('ICE-Console frontend listening on %s'), bind);
}

module.exports = server;
