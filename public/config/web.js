(function (window) {
  window.__env = window.__env || {};

  // API Config
  window.__env.api = {
    url: '/api'
  };

  // debugging
  window.__env.debug = {
    mobxDevtools: false
  };
}(this));
