(function (window) {
  window.__env = window.__env || {};

  // API Config
  window.__env.api = {
    url: '/api'
  };

  window.__env.socket = {
    url: 'http://localhost:8080'
  };

  // debugging
  window.__env.debug = {
    mobxDevtools: false
  };
}(this));
