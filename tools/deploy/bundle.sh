#!/bin/bash
set -ev

tar_file="bundle.tgz"
bundle_files="public/ server/ node_modules/ package.json ecosystem.production.config.js"

date="`date`"

# Tag Our Commit hash into our index
echo "<!-- TRAVIS BUILD #: $TRAVIS_BUILD_NUMBER -->" >> public/index.html
echo "<!-- TRAVIS BUILT AT: $date -->" >> public/index.html
echo "<!-- BUILD COMMIT HASH: $TRAVIS_COMMIT -->" >> public/index.html

tar czf $tar_file $bundle_files
