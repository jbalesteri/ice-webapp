#!/bin/bash
set -ev

ssh_opts="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=quiet"

server="develop.virtualpowersystems.com"
user="vpsbot"

file="bundle.tgz"
dir="reactroot"

ssh $ssh_opts $user@$server rm -rf $dir $file

scp $ssh_opts $file $user@$server:~/

ssh $ssh_opts $user@$server mkdir $dir
ssh $ssh_opts $user@$server tar -xzf $file -C $dir

ssh $ssh_opts $user@$server "cd $dir && npm run server:start"

echo "Deploy to $server complete!"
