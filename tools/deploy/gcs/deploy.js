if (!process.env.TRAVIS_TAG) {
  console.log('Not deploying to GCS, no tag is present.');
  console.log('Travis Tag: ', process.env.TRAVIS_TAG);
  process.exit(0);
}

const fs = require('fs');
const path = require('path');
const gcs = require('@google-cloud/storage')({
  projectId: 'vpsincprod',
  keyFilename: path.join(__dirname, 'key.json')
});

const tag = process.env.TRAVIS_TAG;

console.log('Beginning deploy to GCS.');

let localFile = 'bundle.tgz';

if (fs.statSync(localFile)) {
  let bucket = gcs.bucket('ice-frontend');
  let filename = 'ice-frontend_' + tag + '.tgz';
  let opts = {
    destination: 'react/' + filename
  };

  bucket.upload(localFile, opts, function (error) {
    if (error) {
      console.log('Error uploading file!');
      process.exit(1);
    } else {
      console.log('File uploaded to gcs!');
      process.exit(0);
    }
  });
} else {
  console.log('Local file not found: %s', localFile);
  process.exit(1);
}
