#!/bin/bash

# if on branch master, push to develop
if [[ ${TRAVIS_BRANCH} == "master" ]]; then
  bash tools/deploy/develop/deploy.sh
fi

# if a tag is present push to gcs
if [[ -n "${TRAVIS_TAG}" ]]; then
  npm install @google-cloud/storage
  node tools/deploy/gcs/deploy.js
fi
