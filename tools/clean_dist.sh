#!/bin/bash
current_dir="`pwd`"

dist_dir="$current_dir/public/"

index_file="$dist_dir/index.html"
bundle_file="$dist_dir/ice.js"
map_file="$dist_dir/ice.js.map"
css_file="$dist_dir/ice.css"

if [ -e $index_file ]
  then
    echo "Deleting index.html file..."
    rm $index_file
fi

if [ -e $bundle_file ]
  then
    echo "Deleting JS Bundle file..."
    rm $bundle_file
fi

if [ -e $map_file ]
  then
    echo "Deleting JS Map file..."
    rm $map_file
fi

if [ -e $css_file ]
  then
    echo "Deleting CSS file..."
    rm $css_file
fi
