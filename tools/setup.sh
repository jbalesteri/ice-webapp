#!/bin/bash
current_dir="`pwd`"

git_prepush_file="$current_dir/tools/git-hooks/pre-push"

echo "Setting up git hooks..."
git_dir="$current_dir/.git"
git_hooks_dir="$git_dir/hooks"

git_prepush_path="$git_hooks_dir/pre-push"

if [ -d $git_hooks_dir ]
  then
    if [ -e $git_pre-push_path ]
      then
        unlink git_pre-push_path
      fi

    ln -s $git_prepush_file $git_prepush_path
fi
