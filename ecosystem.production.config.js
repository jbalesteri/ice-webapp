module.exports = {
  apps: [
    {
      name: 'ICE-FRONTEND',
      script: 'server/server.js',
      env: {
        NODE_ENV: 'production'
      }
    }
  ]
};
