const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const sourcePath = path.resolve(__dirname, 'src');
const buildPath = path.resolve(__dirname, 'public');

const webEntry = path.resolve(sourcePath, 'entry/web/client.js');
const webIndex = path.resolve(sourcePath, 'entry/web/index.html');
const webBundle = 'ice.js';

module.exports = () => {
  const nodeEnv = process.env && process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
  const isProd = nodeEnv === 'production';
  const config = (isProd) ? 'web.js' : 'web.development.js';

  const cssUse = (isProd) ? ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: true,
          importloaders: 1,
          localIdentName: '[name]__[local]--[hash:base64:5]'
        }
      },
      {
        loader: 'postcss-loader'
      }
    ]
  }) : [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        modules: true,
        importloaders: 1,
        localIdentName: '[name]__[local]--[hash:base64:5]'
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        sourceMap: 'inline'
      }
    }
  ];

  const plugins = [
    new HtmlWebpackPlugin({
      template: webIndex,
      filename: 'index.html',
      inject: false,
      configFilename: config,
      isProd: isProd
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'BABEL_ENV': JSON.stringify(nodeEnv),
        'NODE_ENV': JSON.stringify(nodeEnv)
      }
    }),
    new ExtractTextPlugin('ice.css')
  ];

  if (isProd) {
    // Production plugins
    plugins.push(
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        mangle: true,
        comments: false,
        sourceMap: false,
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
          drop_console: true
        },
        output: {
          comments: false
        }
      })
    );
  } else {
    const DashboardPlugin = require('webpack-dashboard/plugin');
    const WebpackNotifierPlugin = require('webpack-notifier');

    plugins.push(
      new WebpackNotifierPlugin(),
      new DashboardPlugin()
    );
  }

  const rules = [
    {
      test: /\.js$/,
      include: sourcePath,
      exclude: /node_modules/,
      loader: 'babel-loader'
    },
    {
      test: /\.css$/,
      use: cssUse
    },
    {
      test: /\.(svg|gif|jpg|png)$/,
      loader: 'url-loader'
    }
  ];

  return {
    stats: { children: false },
    devtool: 'eval-source-map', // isProd ? 'source-map' : 'eval',
    context: sourcePath,
    entry: [webEntry],
    output: {
      path: buildPath,
      filename: webBundle
    },
    resolve: {
      extensions: ['.js', '.css'],
      modules: [
        'node_modules',
        sourcePath
      ]
    },
    module: {
      rules
    },
    plugins,
    devServer: {
      contentBase: './public',
      port: 8081,
      proxy: {
        '/api': {
          target: 'http://localhost:8080',
          logLevel: 'debug'
        }
      }
    }
  };
};
