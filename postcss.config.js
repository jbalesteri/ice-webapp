const variables = require('./src/styles/variables');

module.exports = {
  plugins: {
    'postcss-import': {
      root: __dirname,
      path: [ './src/styles/' ]
    },
    'postcss-mixins': {},
    'postcss-cssnext': {
      features: {
        customProperties: {
          variables: variables
        }
      }
    },
    'postcss-nested': {},
    'postcss-each': {}
  }
};
