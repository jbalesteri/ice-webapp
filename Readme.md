# VPS ICE Frontend in React with MobX

[![Build Status](https://travis-ci.com/vpsinc/ice-frontend-react-mobx.svg?token=NezMcMTiCsbTZ5GWAB7d&branch=master)](https://travis-ci.com/vpsinc/ice-frontend-react-mobx)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg)](https://github.com/conventional-changelog/standard-version)
[![NSP Status](https://nodesecurity.io/orgs/vps/projects/2aca7b25-3144-4c50-8919-010390448fc0/badge)](https://nodesecurity.io/orgs/vps/projects/2aca7b25-3144-4c50-8919-010390448fc0)

Site for the ICE Frontend in React using MobX.

## [Documentation](https://github.com/vpsinc/ice-frontend-react-mobx/wiki)

Documentation for this project can be found on it's wiki.


