module.exports = {
  apps: [
    {
      name: 'ICE-FRONTEND',
      script: 'server/server.js',
      watch: ['server'],
      ignore_watch: ['node_modules', 'src'],
      env: {
        NODE_ENV: 'development',
        DEBUG: 'ice:*,vps:*',
        PORT: 8080
      }
    }
  ]
};
