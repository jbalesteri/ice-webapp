var path = require('path');
var fs = require('fs');
var storage = require('@google-cloud/storage');
var del = require('del');
var chalk = require('chalk');

const localCoreFile = path.join(__dirname, 'reactor/core-assembly.jar');
const localAnalyticsFile = path.join(__dirname, 'analytics/analytics-assembly.jar');
const localDcsimFile = path.join(__dirname, 'dcsim/dcsim-assembly.jar');

/* del the files */
console.log(chalk.dim.cyan('Deleting existing jars...'));
del.sync(localCoreFile, localAnalyticsFile);
console.log(chalk.dim.green('delete successful!'));

var gcloud = storage({
  projectId: 'vpsincprod',
  keyFilename: path.join(__dirname, 'key.json')
});

var bucket = gcloud.bucket('reactor-builds');

var opts = {
  autoPaginate: false,
  prefix: 'core-assembly'
};

/* get core-assembly jar */
console.log(chalk.dim.cyan('fetching core-assembly jar files...'));
bucket.getFiles(opts, (err, files) => {
  if (err) {
    console.log(chalk.bold.red('Error getting core-assembly files!'));
    console.log(chalk.red(err));
    return;
  }

  console.log(chalk.dim.cyan('finding latest core-assembly...'));
  files = files.sort(compareFileDates).reverse();

  let newestCoreAseembly = files[0];

  console.log(chalk.cyan('Saving core-assembly.jar'));
  newestCoreAseembly.createReadStream()
    .on('error', function (err) {
      console.log(chalk.bold.red('Error reading core-assembly file from google cloud...'));
      console.log(chalk.red(err));
    })
    .on('end', () => { console.log(chalk.bold.green('Success: core-assembly.jar saved!')); })
    .pipe(fs.createWriteStream(localCoreFile));
});

/* get analytics-assembly jar */
opts.prefix = 'analytics-assembly';

console.log(chalk.dim.cyan('fetching analytics-assembly jar files...'));
bucket.getFiles(opts, (err, files) => {
  if (err) {
    console.log(chalk.bold.red('Error getting analytics-assembly files!'));
    console.log(chalk.red(err));
    return;
  }

  console.log(chalk.dim.cyan('finding latest analytics-assembly...'));
  files = files.sort(compareFileDates).reverse();

  let newestAnalyticsJar = files[0];

  console.log(chalk.cyan('Saving analytics-assembly.jar'));
  newestAnalyticsJar.createReadStream()
    .on('error', function (err) {
      console.log(chalk.bold.red('Error reading analytics-assembly file from google cloud...'));
      console.log(chalk.red(err));
    })
    .on('end', () => { console.log(chalk.bold.green('Success: analytics-assembly.jar saved!')); })
    .pipe(fs.createWriteStream(localAnalyticsFile));
});

/* get dcsim-assembly jar */
var dcsimBucket = gcloud.bucket('dcsimscala');

opts.prefix = 'dcsimScala';

console.log(chalk.dim.cyan('fetching latest dcsimScala jar file...'));
dcsimBucket.getFiles(opts, (err, files) => {
  if (err) {
    console.log(chalk.bold.red('Error getting dcsimScala jar file!'));
    console.log(chalk.red(err));
    return;
  }

  console.log(chalk.dim.cyan('finding latest dcsimScala file...'));
  files = files.sort(compareFileDates).reverse();

  let newestDcsimJar = files[0];

  console.log(chalk.cyan('Saving dcsimScala jar'));
  newestDcsimJar.createReadStream()
    .on('error', (err) => {
      console.log(chalk.bold.red('error reading dcsimScala jar file from google cloud.'));
      console.log(chalk.red(err));
      return err;
    })
    .on('end', () => {
      console.log(chalk.bold.green('Success: dcsimScala jar saved!'));
    })
    .pipe(fs.createWriteStream(localDcsimFile));
});

/* Helper functions */
const compareFileDates = (a, b) => {
  let aDate = new Date(a.metadata.timeCreated);
  let bDate = new Date(b.metadata.timeCreated);
  if (aDate > bDate) {
    return 1;
  } else if (aDate < bDate) {
    return -1;
  }
  return 0;
};
