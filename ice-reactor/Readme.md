# ICE-Reactor Docker Wrapper

This is a running instance of ICE-Reactor (API and Analytics), DCSimScala, Elasticsearch and Mongo that the frontend uses in their local development environments.

## Configuration

Reactor starts with an empty config so the main config to worry about is DCSimScala's. the following is the list of important configuration loaded for DCSimScala.

### Feeds

| name | limit |
| ---- | ----- |
| Feed A | 5000 |
| Feed B | 12000 |

### Racks

| name | RPDUs |
| ---- | ----- |
| Rack 1 | pdu1FeedA, pdu1FeedB |
| Rack 2 | pdu2FeedA, pdu2FeedB |
| Rack 3 | pdu3FeedA, pdu3FeedB |

### Devices

| name | type | port |
| ---- | ---- | ---- |
| pdu1FeedA | ap8941 | 5001 |
| pdu2FeedA | ap8941 | 5002 |
| pdu3FeedA | ap8941 | 5003 |
| pdu1FeedB | ap8941 | 5004 |
| pdu2FeedB | ap8941 | 5005 |
| pdu3FeedB | ap8941 | 5006 |
| SchniederUpsB | schneiderUps | 5007 |
| AC6KFeedA | ac6k | 5008 |
