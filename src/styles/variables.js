const red = '#F44336';
const pink = '#F06292';
const purple = '#BA68C8';
const deepPurple = '#9575CD';
const indigo = '#7986CB';
const blue = '#2196F3';
const lightBlue = '#4FC3F7';
const cyan = '#00BCD4';
const teal = '#009688';
const green = '#43A047';
const lightGreen = '#7CB342';
const lime = '#AFB42B';
const yellow = '#FDD835';
const amber = '#FFC107';
const orange = '#FF9800';
const deepOrange = '#FF5722';
const brown = '#795548';
const grey = '#9E9E9E';
const blueGrey = '#78909C';

module.exports = {
  'padding-xs': '0.5rem',
  'padding-sm': '1rem',
  'padding-md': '2rem',
  'padding-lg': '3rem',
  'padding-xl': '5rem',

  // typography
  'font-size': '1rem',
  'font-size-xs': '0.7rem',
  'font-size-sm': '0.75rem',
  'font-size-md': '0.9rem',
  'font-size-lg': '1.1rem',
  'font-weight-thin': '300',
  'font-weight-normal': '400',
  'font-weight-semi-bold': '500',
  'font-weight-bold': '700',

  // borders
  'border-light': '#E0E0E0',
  'border-dark': '#9E9E9E',

  // colors
  'color-red': red,
  'color-pink': pink,
  'color-purple': purple,
  'color-deep-purple': deepPurple,
  'color-indigo': indigo,
  'color-blue': blue,
  'color-light-blue': lightBlue,
  'color-cyan': cyan,
  'color-teal': teal,
  'color-green': green,
  'color-light-green': lightGreen,
  'color-lime': lime,
  'color-yellow': yellow,
  'color-amber': amber,
  'color-orange': orange,
  'color-deep-orange': deepOrange,
  'color-brown': brown,
  'color-grey': grey,
  'color-blue-grey': blueGrey,

  'color-background': '#37474F',
  'color-primary': blue,
  'color-accent': yellow,

  'color-black': '#000',
  'color-white': '#fff',

  'color-success': green,
  'color-warning': yellow,
  'color-error': red,

  'color-text': '#fff',
  'color-text-inverse': '#000',
  'color-text-secondary': grey,

  'color-phase1': grey,
  'color-phase2': deepOrange,
  'color-phase3': lightBlue,

  // RT overrides
  'switch-text-color': '#fff',
  'table-row-height': '48px',
  'table-row-divider': 'solid 1px rgba(0,0,0,.12)',
  'table-text-color': '#fff',
  'table-striped-background': '#37474F',

  'table-row-highlight': 'trasparent',
  'table-striped-hover': 'trasparent',

  'tab-text-color': '#FFF',
  'tab-text-inactive-color': '#9E9E9E',

  'radio-inner-color': '#fff',
  'radio-text-color': '#fff',
  'radio-disabled-color': '#777',
  'drawer-desktop-width': '224px',
  'chip-color': '#fff',
  'chip-background': grey,
  'autocomplete-suggestions-background': 'rgb(55, 71, 79)',
  'autocomplete-suggestion-active-background': '#2196F3',
  'dropdown-value-hover-background': '#2196F3',
  'input-text-label-color': '#fff'
};
