import FeedSensorData from './FeedSensorData';

let feedSensorDataModel;

// Associated API call : /feeds/feedMetrics
const testFeedSensorDataObj = {
  allocated1nW: 3000,
  allocated2nW: 4000,
  drLimitW: 400000,
  feedId: 'a1274ceb-078f-4aeb-8bc2-b0bb6588494b',
  maxAllocated1nW: 4000,
  maxAllocated2nW: 5000,
  maxPowerW: 6000,
  phase: '1-2',
  powerW: 4000
};

describe('FeedSensorData Model Tests', () => {
  beforeEach(() => {
    feedSensorDataModel = new FeedSensorData();
  });

  it('Should let you update properties on the Feed Model with updateFromJson(), return the properties with toJson(), )', () => {
    feedSensorDataModel.updateFromJson(testFeedSensorDataObj);
    expect(feedSensorDataModel.drLimitW).toBe(testFeedSensorDataObj.drLimitW);
    expect(feedSensorDataModel.allocated1nW).toBe(testFeedSensorDataObj.allocated1nW);
    expect(feedSensorDataModel.allocated2nW).toBe(testFeedSensorDataObj.allocated2nW);
    expect(feedSensorDataModel.maxAllocated1nW).toBe(testFeedSensorDataObj.maxAllocated1nW);
    expect(feedSensorDataModel.maxAllocated2nW).toBe(testFeedSensorDataObj.maxAllocated2nW);
    expect(feedSensorDataModel.maxPowerW).toBe(testFeedSensorDataObj.maxPowerW);
    expect(feedSensorDataModel.phase).toBe(testFeedSensorDataObj.phase);
    expect(feedSensorDataModel.powerW).toBe(testFeedSensorDataObj.powerW);
    expect(feedSensorDataModel.id).toBe(testFeedSensorDataObj.feedId);

    let modelToJson = feedSensorDataModel.toJson;
    expect(modelToJson.feedId).toBe(feedSensorDataModel.feedId);
    expect(modelToJson.drLimitW).toBe(feedSensorDataModel.drLimitW);
    expect(modelToJson.allocated1nW).toBe(feedSensorDataModel.allocated1nW);
    expect(modelToJson.allocated2nW).toBe(feedSensorDataModel.allocated2nW);
    expect(modelToJson.maxAllocated1nW).toBe(feedSensorDataModel.maxAllocated1nW);
    expect(modelToJson.maxAllocated2nW).toBe(feedSensorDataModel.maxAllocated2nW);
    expect(modelToJson.maxPowerW).toBe(feedSensorDataModel.maxPowerW);
    expect(modelToJson.phase).toBe(feedSensorDataModel.phase);
    expect(modelToJson.powerW).toBe(feedSensorDataModel.powerW);
  });
});
