import User from './User';

let model;
let fakeSavedData = null;
let fakeIdToDelete = '';

let storeMock = {
  save: function (thingToSave) {
    fakeSavedData = thingToSave;
  },
  delete: function (idToDelete) {
    fakeIdToDelete = idToDelete;
  }
};

describe('UserModel Tests', () => {
  beforeEach(() => {
    model = new User(storeMock);
    fakeSavedData = null;
    fakeIdToDelete = '';
  });

  it('Should add the store to the model', () => {
    expect(model.store).toBe(storeMock);
  });

  it('Should let you update properties on the object with the updateFromJson method', () => {
    let myFakeData = {
      id: Math.random(),
      firstName: 'brian',
      lastName: 'coburn',
      active: 'true',
      email: 'brian@virtualpowersystems.com',
      password: 'admin123*',
      oldPassword: 'password1',
      phoneNumber: '666-666-6666',
      roles: ['user', 'admin'],
      logins: []
    };
    model.password = 'admin123*';
    model.oldPassword = 'password1';
    model.updateFromJson(myFakeData);
    expect(model.id).toBe(myFakeData.id);
    expect(model.firstName).toBe(myFakeData.firstName);
    expect(model.lastName).toBe(myFakeData.lastName);
    expect(model.active).toBe(myFakeData.active);
    expect(model.email).toBe(myFakeData.email);
    expect(model.phoneNumber).toBe(myFakeData.phoneNumber);
    expect(model.roles[0]).toBe(myFakeData.roles[0]);
    expect(model.roles[1]).toBe(myFakeData.roles[1]);
  });

  it('Should return properties on the object with the toJson method', () => {
    let myFakeData = {
      id: Math.random(),
      firstName: 'brian',
      lastName: 'coburn',
      active: 'true',
      email: 'brian@virtualpowersystems.com',
      password: 'admin123*',
      oldPassword: 'password1',
      phoneNumber: '666-666-6666',
      roles: ['user', 'admin'],
      logins: []
    };

    model.password = 'admin123*';
    model.oldPassword = 'password1';
    model.updateFromJson(myFakeData);
    let myJson = model.toJson;

    expect(myJson.id).toBe(myFakeData.id);
    expect(myJson.firstName).toBe(myFakeData.firstName);
    expect(myJson.lastName).toBe(myFakeData.lastName);
    expect(myJson.active).toBe(myFakeData.active);
    expect(myJson.email).toBe(myFakeData.email);
    expect(myJson.phoneNumber).toBe(myFakeData.phoneNumber);
    expect(myJson.roles[0]).toBe(myFakeData.roles[0]);
    expect(myJson.roles[1]).toBe(myFakeData.roles[1]);
  });

  it('Should return fullName for a user added with firstName and lastName', () => {
    let myFakeData = {
      id: Math.random(),
      firstName: 'brian',
      lastName: 'coburn',
      active: 'true',
      email: 'brian@virtualpowersystems.com',
      phoneNumber: '666-666-6666',
      roles: ['user', 'admin'],
      logins: []
    };
    model.updateFromJson(myFakeData);
    expect(model.fullName).toBe(myFakeData.firstName + ' ' + myFakeData.lastName);
  });

  it('Should save a user to the userStore', () => {
    let myFakeData = {
      id: Math.random(),
      firstName: 'brian',
      lastName: 'coburn',
      active: 'true',
      email: 'brian@virtualpowersystems.com',
      phoneNumber: '666-666-6666',
      roles: ['user', 'admin'],
      logins: []
    };
    model.updateFromJson(myFakeData);
    model.save();
    expect(fakeSavedData.id).toBe(myFakeData.id);
    expect(fakeSavedData.firstName).toBe(myFakeData.firstName);
    expect(fakeSavedData.lastName).toBe(myFakeData.lastName);
    expect(fakeSavedData.active).toBe(myFakeData.active);
    expect(fakeSavedData.email).toBe(myFakeData.email);
    expect(fakeSavedData.phoneNumber).toBe(myFakeData.phoneNumber);
    expect(fakeSavedData.roles[0]).toBe(myFakeData.roles[0]);
    expect(fakeSavedData.roles[0]).toBe(myFakeData.roles[0]);
  });

  it('Should delete a user from the userStore', () => {
    let myFakeData = {
      id: Math.random(),
      firstName: 'brian',
      lastName: 'coburn',
      active: 'true',
      email: 'brian@virtualpowersystems.com',
      phoneNumber: '666-666-6666',
      roles: ['user', 'admin'],
      logins: []
    };
    model.updateFromJson(myFakeData);
    model.save();
    expect(fakeSavedData.id).toBe(myFakeData.id);
    expect(fakeSavedData.firstName).toBe(myFakeData.firstName);
    expect(fakeSavedData.lastName).toBe(myFakeData.lastName);
    expect(fakeSavedData.active).toBe(myFakeData.active);
    expect(fakeSavedData.email).toBe(myFakeData.email);
    expect(fakeSavedData.phoneNumber).toBe(myFakeData.phoneNumber);
    expect(fakeSavedData.roles[0]).toBe(myFakeData.roles[0]);
    expect(fakeSavedData.roles[1]).toBe(myFakeData.roles[1]);
    model.delete();
    expect(fakeIdToDelete).toBe(myFakeData.id);
  });
});
