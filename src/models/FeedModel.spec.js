import Feed, {FeedMetrics} from './Feed';

// models
let feedModel;
let feedMetricsModel;
let testSavedObj = null;
let testDeletedId = '';

const testFeedObj = {
  id: 1,
  name: 'Feed A',
  symbol: 'A',
  feedType: '3Phase',
  metrics: [new FeedMetrics(), new FeedMetrics(), new FeedMetrics()]
};

const testFeedMetricObj = {
  allocated1nW: 2000,
  allocated2nW: 3000,
  maxAllocated1nW: 3000,
  maxAllocated2nW: 3000,
  maxPowerW: 5000,
  drLimitW: 40000,
  phase: '1-2',
  powerW: 4000,
  timestamp: new Date()
};

let mockStore = {
  save: function (saveObj) {
    testSavedObj = saveObj;
  },
  delete: function (id) {
    testDeletedId = id;
  }
};

describe('Feed/FeedMetrics Model Tests', () => {
  beforeEach(() => {
    feedModel = new Feed(mockStore);
    feedMetricsModel = new FeedMetrics();
  });

  it('Should let you update properties on the Feed Model with updateFromJson(), return the properties with toJson(). Tests save() and delete())', () => {
    feedModel.updateFromJson(testFeedObj);
    expect(feedModel['name']).toBe(testFeedObj['name']);
    expect(feedModel['symbol']).toBe(testFeedObj['symbol']);
    expect(feedModel['type']).toBe(testFeedObj['feedType']);
    expect(feedModel['_id']).toBe(testFeedObj['id']);
    expect(feedModel.metrics[0]).toBeInstanceOf(FeedMetrics);
    expect(feedModel.metrics[1]).toBeInstanceOf(FeedMetrics);
    expect(feedModel.metrics[2]).toBeInstanceOf(FeedMetrics);

    let modelToJson = feedModel.toJson;
    expect(modelToJson.name).toBe(feedModel.name);
    expect(modelToJson.symbol).toBe(feedModel.symbol);
    expect(modelToJson.feedType).toBe(feedModel.type);

    feedModel.updateFromJson(testFeedObj);
    feedModel.save(testFeedObj);
    expect(testSavedObj.name).toBe('Feed A');
    feedModel.delete(1);
    expect(testDeletedId).toBe(1);
  });

  it('Should let you update properties on the FeedMetrics model with updateFromJson() and return the properties with toJson()', () => {
    feedMetricsModel.updateFromJson(testFeedMetricObj);
    for (var prop in feedMetricsModel) {
      if (feedMetricsModel.hasOwnProperty(prop)) {
        if (prop !== 'timestamp') {
          expect(feedMetricsModel[prop]).toBe(testFeedMetricObj[prop]);
        } else {
          let testDate = testFeedMetricObj[prop].timestamp;
          let modelDate = feedMetricsModel.timestamp;
          for (var prop1 in modelDate) {
            expect(modelDate[prop1]).toBe(testDate[prop1]);
          }
        }
      }
    };
  });

  it('Test computed methods of FeedMetrics', () => {
    feedMetricsModel.updateFromJson(testFeedMetricObj);
    expect(feedMetricsModel.allocated1n).toBe(testFeedMetricObj.allocated1nW / 1000);
    expect(feedMetricsModel.allocated2n).toBe(testFeedMetricObj.allocated2nW / 1000);
    expect(feedMetricsModel.maxAllocated1n).toBe(testFeedMetricObj.maxAllocated1nW / 1000);
    expect(feedMetricsModel.maxAllocated2n).toBe(testFeedMetricObj.maxAllocated2nW / 1000);
    expect(feedMetricsModel.power).toBe(testFeedMetricObj.powerW / 1000);
    expect(feedMetricsModel.maxPower).toBe(testFeedMetricObj.maxPowerW / 1000);
    expect(feedMetricsModel.limit).toBe(testFeedMetricObj.drLimitW / 1000);
  });
});
