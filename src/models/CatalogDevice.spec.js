import CatalogDevice from './CatalogDevice';

let model;

describe('CatalogDevice Tests', () => {
  beforeEach(() => {
    model = new CatalogDevice();
  });

  it('Should let you update properties on the object with the updateFromJson method and return the properties from the toJson method', () => {
    let myFakeData = {
      model: 'ElectroZap20',
      manufacturer: 'ShortCircuits, Inc',
      completeName: 'ElectroZap second generation circuit melter',
      deviceType: 'disruptive thermally enhanced particle accelerator',
      deviceModel: 'ElectroZap20',
      maxPowerW: 9700,
      batteryCapacityWh: 9701,
      isDumb: true
    };
    model.updateFromJson(myFakeData);
    expect(model.id).toBe(myFakeData.model);
    expect(model.name).toBe(myFakeData.completeName);// this model assigns completeName to name and back again
    expect(model.type).toBe(myFakeData.deviceType);// this model assigns type to deviceType and back again
    expect(model.model).toBe(myFakeData.deviceModel);// this model assigns deviceModel to model and back again
    expect(model.maxPowerW).toBe(myFakeData.maxPowerW);
    expect(model.batteryCapacityWh).toBe(myFakeData.batteryCapacityWh);
    expect(model.isDumb).toBe(myFakeData.isDumb);
    let myJson = model.toJson;
    expect(myJson.model).toBe(myFakeData.model);
    expect(myJson.completeName).toBe(myFakeData.completeName);
    expect(myJson.deviceType).toBe(myFakeData.deviceType);
    expect(myJson.deviceModel).toBe(myFakeData.deviceModel);
    expect(myJson.maxPowerW).toBe(myFakeData.maxPowerW);
    expect(myJson.batteryCapacityWh).toBe(myFakeData.batteryCapacityWh);
    expect(myJson.isDumb).toBe(myFakeData.isDumb);
  });
});
