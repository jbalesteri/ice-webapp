import { observable } from 'mobx';
import TimeSeriesData from '../models/TimeSeriesData';

const debug = require('debug')('ice:models:DeviceSensorData'); // eslint-disable-line no-unused-vars

export default class DeviceSensorData extends TimeSeriesData {
  _id = '';

  @observable backfeedFault = '';
  @observable backplaneFault = '';
  @observable backplaneRelayFault = '';
  @observable batCommLostFault = '';
  @observable batOvervoltFault = '';
  @observable batReplace = '';
  @observable batShortCircuitFault = '';
  @observable batSocShutdown = '';
  @observable batSocWarning = '';
  @observable batTempFault = '';
  @observable batUndervoltFault = '';
  @observable battCurrentA = '';
  @observable battPowerOutW = '';
  @observable battSocProp = '';
  @observable battSocWh = '';
  @observable battTempC = '';
  @observable battUpsActive = '';
  @observable battVoltageV = '';
  @observable capacityUtilizationProp = '';
  @observable deviceId = '';
  @observable externalShortCircuit = '';
  @observable inputPower1W = '';
  @observable inputPower2W = '';
  @observable inputPower3W = '';
  @observable inputPowerW = '';
  @observable inputVoltageV = '';
  @observable outletStatus = '';

  @observable outputPower1W = '';
  @observable outputPower2W = '';
  @observable outputPower3W = '';
  @observable outputPowerW = '';
  @observable outputVoltageV = '';
  @observable pcbShortCircuitFaultTrap = '';
  @observable pcmReplace = '';
  @observable pcmTempFault = '';
  @observable powerLimitW = '';
  @observable readyToBeManaged = '';
  @observable stateOfCharge = '';
  @observable timestamp = '';
  @observable name = '';

  get id () {
    return this._id;
  }

  updateFromJson (json) {
    debug('ufj:', json);
    let newData = {};
    newData.backfeedFault = json.backfeedFault || '';
    newData.backplaneFault = json.backplaneFault || '';
    newData.backplaneRelayFault = json.backplaneRelayFault || '';
    newData.batCommLostFault = json.batCommLostFault || '';
    newData.batOvervoltFault = json.batOvervoltFault || '';
    newData.batReplace = json.batReplace || '';
    newData.batShortCircuitFault = json.batShortCircuitFault || '';
    newData.batSocShutdown = json.batSocShutdown || '';
    newData.batSocWarning = json.batSocWarning || '';
    newData.batTempFault = json.batTempFault || '';
    newData.batUndervoltFault = json.batUndervoltFault || '';
    newData.battCurrentA = json.battCurrentA || '';
    newData.battPowerOutW = json.battPowerOutW || '';
    newData.battSocProp = json.battSocProp || '';
    newData.battSocWh = json.battSocWh || '';
    newData.battTempC = json.battTempC || '';
    newData.battUpsActive = json.battUpsActive || '';
    newData.battVoltageV = json.battVoltageV || '';
    newData.capacityUtilizationProp = json.capacityUtilizationProp || '';
    newData.deviceId = json.deviceId || '';
    newData.externalShortCircuit = json.externalShortCircuit || '';
    newData.inputPower1W = json.inputPower1W || '';
    newData.inputPower2W = json.inputPower2W || '';
    newData.inputPower3W = json.inputPower3W || '';
    newData.inputPowerW = json.inputPowerW || '';
    newData.inputVoltageV = json.inputVoltageV || '';
    newData.outletStatus = json.outletStatus || '';
    newData.outputPower1W = json.outputPower1W || '';
    newData.outputPower2W = json.outputPower2W || '';
    newData.outputPower3W = json.outputPower3W || '';
    newData.outputPowerW = json.outputPowerW || '';
    newData.outputVoltageV = json.outputVoltageV || '';
    newData.pcbShortCircuitFaultTrap = json.pcbShortCircuitFaultTrap || '';
    newData.pcmReplace = json.pcmReplace || '';
    newData.pcmTempFault = json.pcmTempFault || '';
    newData.powerLimitW = json.powerLimitW || '';
    newData.readyToBeManaged = json.readyToBeManaged || '';
    newData.stateOfCharge = json.stateOfCharge || '';
    newData.timestamp = json.timestamp || '';
    this.series.push(newData);
  }
}
