import { computed } from 'mobx';

export const EventDefaults = {
  description: '',
  deviceId: null,
  deviceName: '',
  entityId: null,
  entityName: '',
  severity: '',
  summary: '',
  timeRemaining: null,
  timestamp: null,
  type: ''
};

export default class Event {
  _id = null;
  description = EventDefaults.description;
  deviceId = EventDefaults.deviceId;
  deviceName = EventDefaults.deviceName;
  entityId = EventDefaults.entityId;
  entityName = EventDefaults.entityName;
  severity = EventDefaults.severity;
  summary = EventDefaults.summary;
  timeRemaining = EventDefaults.timeRemaining;
  timestamp = EventDefaults.timestamp;
  type = EventDefaults.type;

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    let event = {
      description: this.description,
      deviceId: this.deviceId,
      deviceName: this.deviceName,
      entityId: this.entityId,
      entityName: this.entityName,
      severity: this.severity,
      summary: this.summary,
      timeRemaining: this.timeRemaining,
      timestamp: this.timestamp,
      type: this.type
    };

    if (this.id) {
      event.id = this.id;
    }

    return event;
  }

  updateFromJson (json) {
    this._id = json.id || '';
    this.description = json.description || EventDefaults.description;
    this.deviceId = json.deviceId || EventDefaults.deviceId;
    this.deviceName = json.deviceName || EventDefaults.deviceName;
    this.entityId = json.entityId || EventDefaults.entityId;
    this.entityName = json.entityName || EventDefaults.entityName;
    this.severity = json.severity || EventDefaults.severity;
    this.summary = json.summary || EventDefaults.summary;
    this.timeRemaining = json.timeRemaining || EventDefaults.timeRemaining;
    this.timestamp = json.timestamp || EventDefaults.timestamp;
    this.type = json.type || EventDefaults.type;
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}
