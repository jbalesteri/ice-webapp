const debug = require('debug')('ice:models:zone'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class Zone {
  _id = null;
  @observable name = null;
  @observable type = null;
  @observable allocatedPower = 0;
  @observable racks = [];
  @observable groups = [];
  @observable breakerLimits = [];
  @observable iceLimits = [];

  metrics = new ZoneMetrics();

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get groupIds () {
    return this.groups.map((group) => (group.id));
  }

  @computed get rackIds () {
    return this.racks.map((rack) => (rack.id));
  }

  @computed get devices () {
    let groupDevices = this.groups.reduce((devices, group) => {
      let gDevices = group.devices ? group.devices.slice() : [];
      return devices.concat(gDevices);
    }, []);

    let rackDevices = this.racks.reduce((devices, rack) => {
      let rpdus = rack.rpdus ? rack.rpdus.slice() : [];
      return devices.concat(rpdus);
    }, []);

    return groupDevices.concat(rackDevices);
  }

  @computed get deviceIds () {
    return this.devices.map((device) => (device.id));
  }

  @computed get feeds () {
    let hasSeen = {};
    return this.devices.map((device) => {
      if (device.feed && !hasSeen[device.feed.id]) {
        hasSeen[device.feed.id] = true;
        return device.feed;
      }
    }).filter((feed) => {
      return !!feed;
    });
  }

  @computed get feedIds () {
    return this.feeds.map((feed) => (feed.id));
  }

  @computed get toJson () {
    let zone = {
      name: this.name,
      zoneType: this.type,
      allocatedPower: Number(this.allocatedPower),
      rackIds: this.rackIds,
      groups: this.groupIds,
      breakerLimits: this.breakerLimits,
      iceLimits: this.iceLimits
    };

    if (this.id) {
      zone.id = this.id;
    }

    return zone;
  }

  updateFromJson (json) {
    this._id = json.id || null;
    this.name = json.name || null;
    this.type = json.zoneType || null;
    this.allocatedPower = json.allocatedPower || 0;
    this.breakerLimits = json.breakerLimits || [];
    this.iceLimits = json.iceLimits || [];

    this.groups.clear();
    if (json.groups) {
      json.groups.forEach((groupId) => {
        let group = this.store.groupStore.getById(groupId);
        if (group) {
          this.groups.push(group);
        }
      });
    }

    this.racks.clear();
    if (json.rackIds) {
      json.rackIds.forEach((rackId) => {
        let rack = this.store.rackStore.getById(rackId);
        if (rack) {
          this.racks.push(rack);
        }
      });
    }
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}

export class ZoneMetrics {
  @observable available1nCapacityW = '';
  @observable available2nCapacityW = '';
  @observable availableCapacityW = '';
  @observable averageSocProp = '';
  @observable instantaneousInputPowerW = '';
  @observable maxInputPowerW = '';
  @observable maxOutputPowerW = '';
  @observable maxUtilizationProp = '';

  @observable phasePowerIn = [];
  @observable powerLimits = [];

  updateFromJson (json) {
    this.available1nCapacityW = (json.hasOwnProperty('available1nCapacityW')) ? json.available1nCapacityW : '';
    this.available2nCapacityW = (json.hasOwnProperty('available2nCapacityW')) ? json.available2nCapacityW : '';
    this.availableCapacityW = (json.hasOwnProperty('availableCapacityW')) ? json.availableCapacityW : '';
    this.averageSocProp = (json.hasOwnProperty('averageSocProp')) ? json.averageSocProp : '';
    this.instantaneousInputPowerW = (json.hasOwnProperty('instantaneousInputPowerW')) ? json.instantaneousInputPowerW : '';
    this.maxInputPowerW = (json.hasOwnProperty('maxInputPowerW')) ? json.maxInputPowerW : '';
    this.maxOutputPowerW = (json.hasOwnProperty('maxOutputPowerW')) ? json.maxOutputPowerW : '';
    this.maxUtilizationProp = (json.hasOwnProperty('maxUtilizationProp')) ? json.maxUtilizationProp : '';

    this.powerLimits.replace(json.powerLimits);
    this.phasePowerIn.replace(json.phasePowerIn);
  }
}
