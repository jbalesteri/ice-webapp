import { observable, computed } from 'mobx';

export default class FeedSensorData {
  _id = '';

  @observable allocated1nW = '';
  @observable allocated2nW = '';
  @observable drLimitW = '';
  @observable feedId = '';
  @observable maxAllocated1nW = '';
  @observable maxAllocated2nW = '';
  @observable maxPowerW = '';
  @observable phase = '';
  @observable powerW = '';

  get id () {
    return this._id;
  }

  @computed get toJson () {
    let feedSensorData = {

      allocated1nW: this.allocated1nW,
      allocated2nW: this.allocated2nW,
      drLimitW: this.drLimitW,
      feedId: this.feedId,
      maxAllocated1nW: this.maxAllocated1nW,
      maxAllocated2nW: this.maxAllocated2nW,
      maxPowerW: this.maxPowerW,
      phase: this.phase,
      powerW: this.powerW
    };

    return feedSensorData;
  }

  updateFromJson (json) {
    this.allocated1nW = json.allocated1nW;
    this.allocated2nW = json.allocated2nW;
    this.drLimitW = json.drLimitW;
    this.feedId = json.feedId;
    this._id = json.feedId;
    this.maxAllocated1nW = json.maxAllocated1nW;
    this.maxAllocated2nW = json.maxAllocated2nW;
    this.maxPowerW = json.maxPowerW;
    this.phase = json.phase;
    this.powerW = json.powerW;
  }
}
