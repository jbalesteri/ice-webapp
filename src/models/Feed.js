const debug = require('debug')('ice:models:feed'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class Feed {
  _id = null;
  @observable name = null;
  @observable symbol = null;
  @observable type = null;

  metrics = [new FeedMetrics(), new FeedMetrics(), new FeedMetrics()];

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get toJson () {
    let feed = {
      name: this.name,
      symbol: this.symbol,
      feedType: this.type
    };

    if (this.id) {
      feed.id = this.id;
    }

    return feed;
  }

  updateFromJson (json) {
    this._id = json.id || null;
    this.name = json.name || null;
    this.symbol = json.symbol || null;
    this.type = json.feedType || null;
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}

export class FeedMetrics {
  @observable allocated1nW = '';
  @observable allocated2nW = '';
  @observable drLimitW = '';
  @observable maxAllocated1nW = '';
  @observable maxAllocated2nW = '';
  @observable maxPowerW = '';
  @observable phase = '';
  @observable powerW = '';

  @computed get allocated1n () {
    return this.allocated1nW / 1000;
  }

  @computed get allocated2n () {
    return this.allocated2nW / 1000;
  }

  @computed get power () {
    return this.powerW / 1000;
  }

  @computed get maxPower () {
    return this.maxPowerW / 1000;
  }

  @computed get limit () {
    return this.drLimitW / 1000;
  }

  @computed get maxAllocated1n () {
    return this.maxAllocated1nW / 1000;
  }

  @computed get maxAllocated2n () {
    return this.maxAllocated2nW / 1000;
  }

  updateFromJson (json) {
    this.allocated1nW = (json.hasOwnProperty('allocated1nW')) ? json.allocated1nW : '';
    this.allocated2nW = (json.hasOwnProperty('allocated2nW')) ? json.allocated2nW : '';
    this.drLimitW = (json.hasOwnProperty('drLimitW')) ? json.drLimitW : '';
    this.maxAllocated1nW = (json.hasOwnProperty('maxAllocated1nW')) ? json.maxAllocated1nW : '';
    this.maxAllocated2nW = (json.hasOwnProperty('maxAllocated2nW')) ? json.maxAllocated2nW : '';
    this.maxPowerW = (json.hasOwnProperty('maxPowerW')) ? json.maxPowerW : '';
    this.phase = (json.hasOwnProperty('phase')) ? json.phase : '';
    this.powerW = (json.hasOwnProperty('powerW')) ? json.powerW : '';

    this.timestamp = (json.timestamp) ? new Date(json.timestamp) : '';
  }
}
