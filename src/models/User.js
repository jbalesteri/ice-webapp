import { observable, computed } from 'mobx';

export default class User {
  _id = null;
  @observable active = true;
  @observable firstName = '';
  @observable lastName = '';
  @observable email = '';
  @observable phoneNumber = '';
  @observable roles = [];
  @observable logins = [];
  password = '';
  oldPassword = '';

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get fullName () {
    return this.firstName + ' ' + this.lastName;
  }

  @computed get toJson () {
    let user = {
      active: this.active,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      phoneNumber: this.phoneNumber,
      roles: this.roles
    };

    if (this.id) {
      user.id = this.id;
    }

    if (this.password) {
      user.password = this.password;
    }

    if (this.oldPassword) {
      user.oldpassword = this.oldPassword;
    }
    return user;
  }

  updateFromJson (json) {
    this._id = json.id;
    this.active = json.active || true;
    this.firstName = json.firstName || '';
    this.lastName = json.lastName || '';
    this.email = json.email || '';
    this.phoneNumber = json.phoneNumber || '';
    this.roles = json.roles || [];
    this.logins = json.lastLoginDate || [];
  }

  save () {
    return this.store.save(this.toJson);
  }

  delete () {
    return this.store.delete(this.id);
  }
}
