const debug = require('debug')('ice:models:rack'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class Rack {
  _id = null;
  @observable name = null;
  @observable allocatedPower = null;
  @observable redundancy = '2N';
  @observable priority = 1;
  @observable rpdus = [];
  @observable defaultRpdus = [];

  metrics = new RackMetrics();

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get rpduIds () {
    return this.rpdus.map((rpdu) => (rpdu.id));
  }

  @computed get toJson () {
    let rack = {
      name: this.name,
      allocatedPower: Number(this.allocatedPower),
      priority: this.priority,
      redundancyStrategy: this.redundancy,
      rPdus: this.rpduIds,
      defaultRpdus: this.defaultRpdus
    };

    if (this.id) {
      rack.id = this.id;
    }

    return rack;
  }

  updateFromJson (json) {
    this.rpdus.clear();

    this._id = json.id;
    this.name = json.name || null;
    this.allocatedPower = json.allocatedPower || null;
    this.redundancy = json.redundancyStrategy || '2N';
    this.priority = json.priority || 1;
    this.defaultRpdus = json.defaultRpdus || [];

    if (json.rPdus) {
      json.rPdus.forEach((rpdu) => {
        let device = this.store.deviceStore.getById(rpdu);
        if (device) {
          this.rpdus.push(device);
        }
      });
    }
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}

export class RackMetrics {
  @observable availableCapacityW = '';
  @observable averageSocProp = '';
  @observable instantaneousInputPowerW = '';
  @observable maxInputPowerW = '';
  @observable maxUtilizatinProp = '';
  @observable phasePowerIn = [];
  @observable powerLimits = [];

  @computed get instantaneousInputPower () {
    return this.instantaneousInputPowerW / 1000;
  }

  @computed get maxInputPower () {
    return this.maxInputPowerW / 1000;
  }

  @computed get averageSoc () {
    return this.averageSocProp * 100;
  }

  updateFromJson (json) {
    this.availableCapacityW = (json.hasOwnProperty('availableCapacityW')) ? json.availableCapacityW : '';
    this.averageSocProp = (json.hasOwnProperty('averageSocProp')) ? json.averageSocProp : '';
    this.instantaneousInputPowerW = (json.hasOwnProperty('instantaneousInputPowerW')) ? json.instantaneousInputPowerW : '';
    this.maxInputPowerW = (json.hasOwnProperty('maxInputPowerW')) ? json.maxInputPowerW : '';
    this.maxUtilizationProp = (json.hasOwnProperty('maxUtilizationProp')) ? json.maxUtilizationProp : '';

    this.phasePowerIn.replace(json.phasePowerIn);
    this.powerLimits.replace(json.powerLimits);
  }
}
