import { observable, computed } from 'mobx';

const debug = require('debug')('ice:models:device'); // eslint-disable-line no-unused-vars
let bOff = false;
let bInactive = false;

export default class Device {
  _id = null;
  @observable name = '';
  @observable ip = '';
  @observable port = ''
  @observable type = '';
  @observable model = '';
  @observable feed = '';
  @observable phase = '';
  @observable description = '';
  @observable tags = '';
  @observable location = '';
  @observable upstreamFailureSensor = '';
  @observable sensorStreamEnabled = true;
  @observable isConnected = false;
  @observable state = '';
  @observable breakerLimit = '';
  @observable bOff = false;
  @observable bInactive = false;

  sensorData = new DeviceSensorData();

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get breakerRating () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.breakerLimit / 1000;
  }

  @computed get deviceInfo () {
    return JSON.stringify({ type: this.type, model: this.model, isDumb: (this.model === 'dumbRpdu') });
  }

  set deviceInfo (info) {
    info = JSON.parse(info);
    this.type = info.type;
    this.model = info.model;
  }

  @computed get feedId () {
    let id = null;

    if (this.feed) {
      id = this.feed.id;
    }

    return id;
  }

  @computed get toJson () {
    let device = {
      name: this.name,
      ip: this.ip,
      port: Number(this.port),
      deviceType: this.type,
      deviceModel: this.model,
      sourceFeed: this.feed,
      desc: this.description,
      tags: this.tags,
      location: this.location,
      sensorStreamEnabled: this.sensorStreamEnabled,
      upstreamFailureSensor: this.upstreamFailureSensor
    };

    if (this.id) {
      device.id = this.id;
    }

    if (this.phase) {
      device.phase = this.phase;
    }

    if (this.breakerLimit !== '') {
      device.deviceBreakerLimit = this.breakerLimit;
    }

    return device;
  }

  updateFromJson (json) {
    this._id = json.id || '';
    this.name = json.name || '';
    this.ip = json.ip || '';
    this.port = json.port || '';
    this.type = json.deviceType || '';
    this.model = json.deviceModel || '';
    this.description = json.desc || '';
    this.tags = json.tags || '';
    this.location = json.location || '';
    this.sensorStreamEnabled = json.sensorStreamEnabled || true;
    this.upstreamFailureSensor = json.upstreamFailureSensor || '';
    this.isConnected = json.isConnected || false;
    this.state = json.state || '';
    this.breakerLimit = json.deviceBreakerLimit || '';
    bOff = json.state === 'off';
    bInactive = json.state === 'inactive' || json.state === 'notConnected';

    if (json.sourceFeed) {
      let feed = this.store.feedStore.getById(json.sourceFeed);
      if (feed) {
        this.feed = feed;
      }
    }

    if (json.phase) {
      this.phase = json.phase;
    }
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}

export class DeviceSensorData {
  @observable battCurrentA = '';
  @observable battPowerOutW = '';
  @observable battSocProp = '';
  @observable battSocWh = '';
  @observable battTempC = '';
  @observable battUpsActive = '';
  @observable battVoltageV = '';

  @observable capacityUtilizationProp = '';

  @observable inputCurrentA = '';
  @observable inputPower1W = '';
  @observable inputPower2W = '';
  @observable inputPower3W = '';
  @observable inputPowerW = '';
  @observable inputVoltageV = '';

  @observable outletStatus = '';

  @observable outputCurrentA = '';
  @observable outputPower1W = '';
  @observable outputPower2W = '';
  @observable outputPower3W = '';
  @observable outputPowerW = '';
  @observable outputVoltageV = '';

  @observable powerLimitW = '';
  @observable stateOfCharge = '';
  @observable _timestamp = '';

  @computed get timestamp () {
    return new Date(this._timestamp);
  }

  @computed get battCurrent () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.battCurrentA;
  }

  @computed get battPowerOut () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.battPowerOutW / 1000;
  }

  @computed get battSoc () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.battSocProp * 100;
  }

  @computed get battSocWH () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.battSocWh;
  }

  @computed get battTemp () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.battTempC;
  }

  @computed get battVoltage () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.battVoltageV;
  }

  @computed get inputCurrent () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.inputCurrentA;
  }

  @computed get inputPower () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.inputPowerW / 1000;
  }

  @computed get inputPower1 () {
    return this.inputPower1W / 1000;
  }

  @computed get inputPower2 () {
    return this.inputPower2W / 1000;
  }

  @computed get inputPower3 () {
    return this.inputPower3W / 1000;
  }

  @computed get inputVoltage () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.inputVoltageV;
  }

  @computed get outputCurrent () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.outputCurrentA;
  }

  @computed get outputPower () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.outputPowerW / 1000;
  }

  @computed get outputPower1 () {
    return this.outputPower1W / 1000;
  }

  @computed get outputPower2 () {
    return this.outputPower2W / 1000;
  }

  @computed get outputPower3 () {
    return this.outputPower3W / 1000;
  }

  @computed get outputVoltage () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.outputVoltageV / 1000;
  }

  @computed get powerLimit () {
    if (bOff) {
      return 0;
    } else if (bInactive) {
      return '--.--';
    }
    return this.powerLimitW / 1000;
  }

  updateFromJson (json) {
    this.battCurrentA = (json.hasOwnProperty('battCurrentA')) ? json.battCurrentA : '';
    this.battPowerOutW = (json.hasOwnProperty('battPowerOutW')) ? json.battPowerOutW : '';
    this.battSocProp = (json.hasOwnProperty('battSocProp')) ? json.battSocProp : '';
    this.battSocWh = (json.hasOwnProperty('battSocWh')) ? json.battSocWh : '';
    this.battTempC = (json.hasOwnProperty('battTempC')) ? json.battTempC : '';
    this.battUpsActive = (json.hasOwnProperty('battUpsActive')) ? json.battUpsActive : '';
    this.battVoltageV = (json.hasOwnProperty('battVoltageV')) ? json.battVoltageV : '';
    this.capacityUtilizationProp = (json.hasOwnProperty('capacityUtilizationProp')) ? json.capacityUtilizationProp : '';
    this.inputCurrentA = (json.hasOwnProperty('inputCurrentA')) ? json.inputCurrentA : '';
    this.inputPower1W = (json.hasOwnProperty('inputPower1W')) ? json.inputPower1W : '';
    this.inputPower2W = (json.hasOwnProperty('inputPower2W')) ? json.inputPower2W : '';
    this.inputPower3W = (json.hasOwnProperty('inputPower3W')) ? json.inputPower3W : '';
    this.inputPowerW = (json.hasOwnProperty('inputPowerW')) ? json.inputPowerW : '';
    this.inputVoltageV = (json.hasOwnProperty('inputVoltageV')) ? json.inputVoltageV : '';
    this.outletStatus = (json.hasOwnProperty('outletStatus')) ? json.outletStatus : '';
    this.outputCurrentA = (json.hasOwnProperty('outputCurrentA')) ? json.outputCurrentA : '';
    this.outputPower1W = (json.hasOwnProperty('outputPower1W')) ? json.outputPower1W : '';
    this.outputPower2W = (json.hasOwnProperty('outputPower2W')) ? json.outputPower2W : '';
    this.outputPower3W = (json.hasOwnProperty('outputPower3W')) ? json.outputPower3W : '';
    this.outputPowerW = (json.hasOwnProperty('outputPowerW')) ? json.outputPowerW : '';
    this.outputVoltageV = (json.hasOwnProperty('outputVoltageV')) ? json.outputVoltageV : '';
    this.powerLimitW = (json.hasOwnProperty('powerLimitW')) ? json.powerLimitW : '';
    this.stateOfCharge = (json.hasOwnProperty('stateOfCharge')) ? json.stateOfCharge : '';

    this._timestamp = json.timestamp;
  }
}
