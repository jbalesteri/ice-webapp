const debug = require('debug')('ice:models:group'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class Group {
  _id = null;
  @observable name = null;
  @observable limit = 1E9;
  @observable rackshareEnabled = false;
  @observable devices = [];

  sensorData = new GroupSensorData();

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this._id;
  }

  @computed get deviceIds () {
    return this.devices.slice().map((device) => (device.id));
  }

  @computed get toJson () {
    let group = {
      name: this.name,
      deviceIds: this.deviceIds,
      rackshareConfig: {
        rackshareEnabled: this.rackshareEnabled,
        maxLimit: Number(this.limit)
      }
    };

    if (this.id) {
      group.id = this.id;
    }

    return group;
  }

  updateFromJson (json) {
    // clear out our devices
    this.devices.clear();

    // parse the json
    this._id = json.id || null;
    this.name = json.name || null;
    this.limit = (json.rackshareConfig && json.rackshareConfig.maxLimit)
      ? json.rackshareConfig.maxLimit : 1E9;
    this.rackshareEnabled = (json.rackshareConfig && json.rackshareConfig.rackshareEnabled)
      ? json.rackshareConfig.rackshareEnabled : false;

    // resolve our devices
    if (json.deviceIds) {
      json.deviceIds.forEach((deviceId) => {
        let device = this.store.deviceStore.getById(deviceId);
        if (device) {
          this.devices.push(device);
        }
      });
    }
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}

export class GroupSensorData {
  @observable battAvgSocProp = '';
  @observable battAvgSocSec = '';
  @observable battAvgSocWh = '';
  @observable battChargePowerW = '';
  @observable battDischargePowerW = '';
  @observable battMinSocProp = '';
  @observable battMinSocSec = '';
  @observable battMinSocWh = '';
  @observable battTotalSocWh = '';
  @observable battUpsActive = '';

  @observable capacityUtilizationProp = '';

  @observable inputPower1W = '';
  @observable inputPower2W = '';
  @observable inputPower3W = '';
  @observable inputPowerW = '';

  @observable numDevicesCharging = '';
  @observable numDevicesDischarging = '';
  @observable numDevicesUps = '';

  @observable outputPower1W = '';
  @observable outputPower2W = '';
  @observable outputPower3W = '';
  @observable outputPowerW = '';

  @observable powerLimitW = '';

  @observable timestamp = '';

  @computed get battChargePower () {
    return this.battChargePowerW / 1000;
  }

  @computed get battDischargePower () {
    return this.battDischargePowerW / 1000;
  }

  @computed get inputPower () {
    return this.inputPowerW / 1000;
  }

  @computed get inputPower1 () {
    return this.inputPower1W / 1000;
  }

  @computed get inputPower2 () {
    return this.inputPower2W / 1000;
  }

  @computed get inputPower3 () {
    return this.inputPower3W / 1000;
  }

  @computed get outputPower () {
    return this.outputPowerW / 1000;
  }

  @computed get outputPower1 () {
    return this.outputPower1W / 1000;
  }

  @computed get outputPower2 () {
    return this.outputPower2W / 1000;
  }

  @computed get outputPower3 () {
    return this.outputPower3W / 1000;
  }

  @computed get powerLimit () {
    return this.powerLimitW / 1000;
  }

  updateFromJson (json) {
    this.battAvgSocProp = (json.hasOwnProperty('battAvgSocProp')) ? json.battAvgSocProp : '';
    this.battAvgSocSec = (json.hasOwnProperty('battAvgSocSec')) ? json.battAvgSocSec : '';
    this.battAvgSocWh = (json.hasOwnProperty('battAvgSocWh')) ? json.battAvgSocWh : '';
    this.battChargePowerW = (json.hasOwnProperty('battChargePowerW')) ? json.battChargePowerW : '';
    this.battDischargePowerW = (json.hasOwnProperty('battDischargePowerW')) ? json.battDischargePowerW : '';
    this.battMinSocProp = (json.hasOwnProperty('battMinSocProp')) ? json.battMinSocProp : '';
    this.battMinSocSec = (json.hasOwnProperty('battMinSocSec')) ? json.battMinSocSec : '';
    this.battMinSocWh = (json.hasOwnProperty('battMinSocWh')) ? json.battMinSocWh : '';
    this.battTotalSocWh = (json.hasOwnProperty('battTotalSocWh')) ? json.battTotalSocWh : '';
    this.battUpsActive = (json.hasOwnProperty('battUpsActive')) ? json.battUpsActive : '';
    this.capacityUtilizationProp = (json.hasOwnProperty('capacityUtilizationProp')) ? json.capacityUtilizationProp : '';
    this.inputPower1W = (json.hasOwnProperty('inputPower1W')) ? json.inputPower1W : '';
    this.inputPower2W = (json.hasOwnProperty('inputPower2W')) ? json.inputPower2W : '';
    this.inputPower3W = (json.hasOwnProperty('inputPower3W')) ? json.inputPower3W : '';
    this.inputPowerW = (json.hasOwnProperty('inputPowerW')) ? json.inputPowerW : '';
    this.numDevicesCharging = (json.hasOwnProperty('numDevicesCharging')) ? json.numDevicesCharging : '';
    this.numDevicesDischarging = (json.hasOwnProperty('numDevicesDischarging')) ? json.numDevicesDischarging : '';
    this.numDevicesUps = (json.hasOwnProperty('numDevicesUps')) ? json.numDevicesUps : '';
    this.outputPower1W = (json.hasOwnProperty('outputPower1W')) ? json.outputPower1W : '';
    this.outputPower2W = (json.hasOwnProperty('outputPower2W')) ? json.outputPower2W : '';
    this.outputPower3W = (json.hasOwnProperty('outputPower3W')) ? json.outputPower3W : '';
    this.outputPowerW = (json.hasOwnProperty('outputPowerW')) ? json.outputPowerW : '';
    this.powerLimitW = (json.hasOwnProperty('powerLimitW')) ? json.powerLimitW : '';

    this.timestamp = (json.timestamp) ? new Date(json.timestamp) : '';
  }
}
