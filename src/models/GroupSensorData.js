import { observable } from 'mobx';

export default class GroupSensorData {
  _id = '';

  @observable battAvgSocProp = '';
  @observable battAvgSocSec = '';
  @observable battAvgSocWh = '';
  @observable battChargePowerW = '';
  @observable battDischargePowerW = '';
  @observable battMinSocProp = '';
  @observable battMinSocSec = '';
  @observable battMinSocWh = '';
  @observable battTotalSocWh = '';
  @observable battUpsActive = '';
  @observable capacityUtilizationProp = '';
  @observable groupId = '';
  @observable inputPower1W = '';
  @observable inputPower2W = '';
  @observable inputPower3W = '';
  @observable inputPowerW = '';
  @observable numDevicesCharging = '';
  @observable numDevicesDischarging = '';
  @observable numDevicesUps = '';
  @observable outputPower1W = '';
  @observable outputPower2W = '';
  @observable outputPower3W = '';
  @observable outputPowerW = '';
  @observable powerLimitW = '';
  @observable timestamp = '';

  get id () {
    return this._id;
  }

  updateFromJson (json) {
    this.battAvgSocProp = json.battAvgSocProp;
    this.battAvgSocSec = json.battAvgSocSec;
    this.battAvgSocWh = json.battAvgSocWh;
    this.battChargePowerW = json.battChargePowerW;
    this.battDischargePowerW = json.battDischargePowerW;
    this.battMinSocProp = json.battMinSocProp;
    this.battMinSocSec = json.battMinSocSec;
    this.battMinSocWh = json.battMinSocWh;
    this.battTotalSocWh = json.battTotalSocWh;
    this.battUpsActive = json.battUpsActive;
    this.capacityUtilizationProp = json.capacityUtilizationProp;
    this.groupId = json.groupId;
    this._id = json.groupId;
    this.inputPower1W = json.inputPower1W;
    this.inputPower2W = json.inputPower2W;
    this.inputPower3W = json.inputPower3W;
    this.inputPowerW = json.inputPowerW;
    this.numDevicesCharging = json.numDevicesCharging;
    this.numDevicesDischarging = json.numDevicesDischarging;
    this.numDevicesUps = json.numDevicesUps;
    this.outputPower1W = json.outputPower1W;
    this.outputPower2W = json.outputPower2W;
    this.outputPower3W = json.outputPower3W;
    this.outputPowerW = json.outputPowerW;
    this.powerLimitW = json.powerLimitW;
    this.timestamp = json.timestamp;
  }
}
