import { observable } from 'mobx';

export default class TimeSeriesData {
  _id = null;
  @observable series = [];

  constructor (id, series) {
    this._id = id;
    this.series = series || [];
  }

  get id () {
    return this._id;
  }

  updateFromJson (json) {
    this._id = json.id || '';
    this.series = json.series || [];
  }
}
