import CapacityReport from './CapacityReport';

// sample API call :
// models
let capacityReportModel;
let testSavedObj = null;
let testDeletedId = '';

const testCapacityReport = {
  id: 1,
  timestampGenerated: 1484848783044,
  peakSinceDate: 1484848783044,
  zoneName: 'Test Zone',
  allocatedPower: 2000,
  available2nCapacity: 2000,
  available1nCapacity: 2000,
  availableCapacity: 2000,
  zone2nPeak: 1000,
  zone1nPeak: 2000,
  zonePeak: 1000,
  breakers: 2000,
  rackData: 1000
};

const testDefaultCapacityReport = {
  id: '',
  timestampGenerated: '',
  peakSinceDate: '',
  zoneName: 'N/A',
  allocatedPower: 0,
  available2nCapacity: 0,
  available1nCapacity: 0,
  availableCapacity: 0,
  zone2nPeak: 0,
  zone1nPeak: 0,
  zonePeak: 0,
  breakers: [],
  rackData: []
};

const emptyObj = {};

let mockStore = {
  save: function (saveObj) {
    testSavedObj = saveObj;
  },
  delete: function (id) {
    testDeletedId = id;
  }
};

describe('Capacity Model Tests', () => {
  beforeEach(() => {
    capacityReportModel = new CapacityReport(mockStore);
  });

  it('Should let you update properties on the CapacityReport Model with updateFromJson() with an EMPTY object, using default values for each property', () => {
    capacityReportModel.updateFromJson(emptyObj);
    expect(capacityReportModel['zoneName']).toBe(testDefaultCapacityReport['zoneName']);
    expect(capacityReportModel['allocatedPower']).toBe(testDefaultCapacityReport['allocatedPower']);
    expect(capacityReportModel['available2nCapacity']).toBe(testDefaultCapacityReport['available2nCapacity']);
    expect(capacityReportModel['available1nCapacity']).toBe(testDefaultCapacityReport['available1nCapacity']);
    expect(capacityReportModel['availableCapacity']).toBe(testDefaultCapacityReport['availableCapacity']);
    expect(capacityReportModel['zone2nPeak']).toBe(testDefaultCapacityReport['zone2nPeak']);
    expect(capacityReportModel['zone1nPeak']).toBe(testDefaultCapacityReport['zone1nPeak']);
    expect(capacityReportModel['zonePeak']).toBe(testDefaultCapacityReport['zonePeak']);
    expect(capacityReportModel['breakers'].length).toBe(testDefaultCapacityReport['breakers'].length);
    expect(capacityReportModel['rackData'].length).toBe(testDefaultCapacityReport['rackData'].length);

    let modelToJson = capacityReportModel.toJson;
    expect(modelToJson.timestamp).toBe(capacityReportModel.timestamp);
    expect(modelToJson.peakSinceDate).toBe(capacityReportModel.peakSinceDate);
    expect(modelToJson.zoneName).toBe(capacityReportModel.zoneName);

    capacityReportModel.updateFromJson(testCapacityReport);
    capacityReportModel.save(testCapacityReport);
    expect(testSavedObj.zoneName).toBe('Test Zone');
    capacityReportModel.delete();
    expect(testDeletedId).toBe('Test Zone');
  });

  it('Should let you update properties on the CapacityReport Model with updateFromJson(), return the properties with toJson(). Tests save() and delete())', () => {
    capacityReportModel.updateFromJson(testCapacityReport);
    expect(capacityReportModel.timestamp).toBe(testCapacityReport.timestampGenerated);
    expect(capacityReportModel.peakSinceDate).toBe(testCapacityReport.peakSinceDate);
    expect(capacityReportModel['zoneName']).toBe(testCapacityReport['zoneName']);
    expect(capacityReportModel['allocatedPower']).toBe(testCapacityReport['allocatedPower']);
    expect(capacityReportModel['available2nCapacity']).toBe(testCapacityReport['available2nCapacity']);
    expect(capacityReportModel['available1nCapacity']).toBe(testCapacityReport['available1nCapacity']);
    expect(capacityReportModel['availableCapacity']).toBe(testCapacityReport['availableCapacity']);
    expect(capacityReportModel['zone2nPeak']).toBe(testCapacityReport['zone2nPeak']);
    expect(capacityReportModel['zone1nPeak']).toBe(testCapacityReport['zone1nPeak']);
    expect(capacityReportModel['zonePeak']).toBe(testCapacityReport['zonePeak']);
    expect(capacityReportModel['breakers']).toBe(testCapacityReport['breakers']);
    expect(capacityReportModel['rackData']).toBe(testCapacityReport['rackData']);

    let modelToJson = capacityReportModel.toJson;
    expect(modelToJson.timestamp).toBe(capacityReportModel.timestamp);
    expect(modelToJson.peakSinceDate).toBe(capacityReportModel.peakSinceDate);
    expect(modelToJson.zoneName).toBe(capacityReportModel.zoneName);

    capacityReportModel.updateFromJson(testCapacityReport);
    capacityReportModel.save(testCapacityReport);
    expect(testSavedObj.zoneName).toBe('Test Zone');
    capacityReportModel.delete();
    expect(testDeletedId).toBe('Test Zone');
  });

  it('Test computed methods of CapacityReport Model', () => {
    capacityReportModel.updateFromJson(testCapacityReport);
    expect(capacityReportModel.id).toBe(testCapacityReport.zoneName);
    expect(capacityReportModel.reportDate.getTime()).toBe(testCapacityReport.timestampGenerated);
  });
});
