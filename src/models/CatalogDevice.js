import { observable, computed } from 'mobx';

export default class CatalogDevice {
  @observable _model = null;
  @observable manufacturer = null;
  @observable name = null;
  @observable type = null;
  @observable model = null;
  @observable maxPowerW = null;
  @observable batteryCapacityWh = 0;
  @observable isDumb = false;

  store = null;

  constructor (store) {
    this.store = store;
  }

  @computed get id () {
    return this._model;
  }

  @computed get toJson () {
    return {
      model: this._model,
      manufacturer: this.manufacturer,
      completeName: this.name,
      deviceType: this.type,
      deviceModel: this.model,
      maxPowerW: this.maxPowerW,
      batteryCapacityWh: this.batteryCapacityWh,
      isDumb: this.isDumb
    };
  }

  updateFromJson (json) {
    this._model = json.model;
    this.manufacturer = json.manufacturer;
    this.name = json.completeName;
    this.type = (json.deviceType === 'iceSwitch') ? 'iceswitch' : json.deviceType;
    this.model = json.deviceModel;
    this.maxPowerW = json.maxPowerW;
    this.batteryCapacityWh = json.batteryCapacityWh;
    this.isDumb = json.isDumb;
  }
}
