const debug = require('debug')('ice:models:capacityReport'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class CapacityReport {
  @observable zoneName = null;
  @observable allocatedPower = 0;
  @observable available2nCapacity = 0;
  @observable available1nCapacity = 0;
  @observable availableCapacity = 0;
  @observable zone2nPeak = 0;
  @observable zone1nPeak = 0;
  @observable zonePeak = 0;
  @observable breakers = [];
  @observable rackData = [];
  @observable timestamp = null;

  store = null;

  constructor (store) {
    this.store = store;
  }

  get id () {
    return this.zoneName;
  }

  @computed get reportDate () {
    return new Date(this.timestamp);
  }

  @computed get toJson () {
    let capacityReport = {
      timestamp: this.timestamp,
      peakSinceDate: this.peakSinceDate,
      zoneName: this.zoneName,
      allocatedPower: this.allocatedPower,
      available2nCapacity: this.available2nCapacity,
      available1nCapacity: this.available1nCapacity,
      availableCapacity: this.availableCapacity,
      zone2nPeak: this.zone2nPeak,
      zone1nPeak: this.zone1nPeak,
      zonePeak: this.zonePeak,
      breakers: this.breakers,
      rackData: this.rackData
    };

    if (this.id) {
      capacityReport.id = capacityReport.id;
    }

    return capacityReport;
  }

  updateFromJson (json) {
    debug('UpdateFromJson:', json);
    this.timestamp = json.timestampGenerated;
    this.peakSinceDate = json.peakSinceDate || '';
    this.zoneName = json.zoneName || 'N/A';
    this.allocatedPower = json.allocatedPower || 0;
    this.available2nCapacity = json.available2nCapacity || 0;
    this.available1nCapacity = json.available1nCapacity || 0;
    this.availableCapacity = json.availableCapacity || 0;
    this.zone2nPeak = json.zone2nPeak || 0;
    this.zone1nPeak = json.zone1nPeak || 0;
    this.zonePeak = json.zonePeak || 0;
    this.breakers = json.breakers || [];
    this.rackData = json.rackData || [];
  }

  save () {
    return this.store.save(this);
  }

  delete () {
    return this.store.delete(this.id);
  }
}
