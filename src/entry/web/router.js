import React from 'react';
import { Provider } from 'mobx-react';
import { BrowserRouter, Match, Miss } from 'react-router';

import MatchWhenAuthorized from '../../components/MatchWhenAuthorized/MatchWhenAuthorized';

import DevTools from 'mobx-react-devtools';

import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import Main from '../../views/Main/Main';
import Users from '../../views/Users/Users';
import Devices from '../../views/Devices/Devices';
import Racks from '../../views/Racks/Racks';
import Groups from '../../views/Groups/Groups';
import Feeds from '../../views/Feeds/Feeds';
import DataCenter from '../../views/DataCenter/DataCenter';
import Events from '../../views/Events/Events';
import Account from '../../views/Account/Account';
import Zones from '../../views/Zones/Zones';
import Capacity from '../../views/Reports/Capacity/Capacity';

import Support from '../../views/Support/Support';
import Demo from '../../views/Demo/Demo';
import HealthCheck from '../../views/HealthCheck/HealthCheck';
import Login from '../../views/Login/Login';
import Logout from '../../views/Logout/Logout';
import ForgotPassword from '../../views/ForgotPassword/ForgotPassword';
import ExpiredPassword from '../../views/ExpiredPassword/ExpiredPassword';

@DragDropContext(HTML5Backend)
export default class Router extends React.Component {
  renderDevtools () {
    if (window && window.__env && window.__env.debug && window.__env.debug.mobxDevtools) {
      return <DevTools />;
    }
  }
  render () {
    // putting this in as there seems to be a bug in the miss handler in react-router
    // where the miss will get shown even when there is a match
    let routes = [
      '/',
      '/users',
      '/devices',
      '/racks',
      '/groups',
      '/feeds',
      '/dataCenter',
      '/events',
      '/account',
      '/support',
      '/healthCheck',
      '/login',
      '/logout',
      '/forgotPassword',
      '/passwordExpired',
      '/zones',
      '/reports/capacity'
    ];

    return (
      <BrowserRouter>
        <Provider store={this.props.store}>
          <div>
            <MatchWhenAuthorized exactly pattern='/' component={Main} />
            <MatchWhenAuthorized exactly pattern='/users' component={Users} />
            <MatchWhenAuthorized exactly pattern='/devices' component={Devices} />
            <MatchWhenAuthorized exactly pattern='/racks' component={Racks} />
            <MatchWhenAuthorized exactly pattern='/groups' component={Groups} />
            <MatchWhenAuthorized exactly pattern='/feeds' component={Feeds} />
            <MatchWhenAuthorized exactly pattern='/dataCenter' component={DataCenter} />
            <MatchWhenAuthorized exactly pattern='/events' component={Events} />
            <MatchWhenAuthorized exactly pattern='/account' component={Account} />
            <MatchWhenAuthorized exactly pattern='/zones' component={Zones} />
            <MatchWhenAuthorized exactly pattern='/reports/capacity/:id' component={Capacity} />

            <Match exactly pattern='/support' component={Support} />
            <Match exactly pattern='/demo' component={Demo} />
            <Match exactly pattern='/healthCheck' component={HealthCheck} />
            <Match exactly pattern='/login' component={Login} />
            <Match exactly pattern='/logout' component={Logout} />
            <Match exactly pattern='/forgotPassword' component={ForgotPassword} />
            <Match exactly pattern='/passwordExpired' component={ExpiredPassword} />

            <Miss render={({ location }) => {
              if (location.pathname === '/') return <span></span>;
              if (routes.indexOf(location.pathname) > -1) return <span></span>;
              return (
                <div>Nothing matched {location.pathname}.</div>
              );
            }} />
            {this.renderDevtools()}
          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}
