// vps router//
var debug = require('debug')('ice:VPSRouter: '); // eslint-disable-line no-unused-vars
var React = require('react');
import { Provider } from 'mobx-react';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import * as AppStore from '../../stores/AppStore';
import Main from '../../views/Main/Main';
import Users from '../../views/Users/Users';
import Devices from '../../views/Devices/Devices';
import Racks from '../../views/Racks/Racks';
import Groups from '../../views/Groups/Groups';
import Feeds from '../../views/Feeds/Feeds';
import DataCenter from '../../views/DataCenter/DataCenter';
import Events from '../../views/Events/Events';
import Account from '../../views/Account/Account';
import Zones from '../../views/Zones/Zones';

import Support from '../../views/Support/Support';
import Demo from '../../views/Demo/Demo';
import HealthCheck from '../../views/HealthCheck/HealthCheck';
import Login from '../../views/Login/Login';
import Logout from '../../views/Logout/Logout';
import Capacity from '../../views/Reports/Capacity/Capacity';
import PageChrome from '../../components/PageChrome/PageChrome';
// import ForgotPassword from '../../views/ForgotPassword/ForgotPassword';
// import ExpiredPassword from '../../views/ExpiredPassword/ExpiredPassword';

let component = null;
let _isMounted = false;
let currentLocation = '/';

@DragDropContext(HTML5Backend)
export default class VPSRouter extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.zoneStore = this.props.store.zoneStore;
    this.authStore = this.props.store.authStore;

    this.routes = {
      'main': <Main />,
      '/dashboard': <Main />,
      '/devices': <Devices />,
      '/users': <Users />,
      '/racks': <Racks />,
      '/groups': <Groups />,
      '/zones': <Zones />,
      '/feeds': <Feeds />,
      '/events': <Events />,
      '/healthCheck': <HealthCheck />,
      '/dataCenter': <DataCenter />,
      '/account': <Account />,
      '/support': <Support />,
      '/demo': <Demo />,
      '/login': <Login />,
      '/logout': <Logout />,
      '/reports/capacity/:id': <Capacity />,
      '/': <Main />
    };

    let currentPath = '';
    if (this.authStore.isAuthenticated) {
      currentPath = this.store.history.location.pathname;
    } else {
      currentPath = '/login';
      this.store.history.push(currentPath);
    }

    this.store.history.listen(this.onHistoryChange.bind(this));
    let newState = {routes: this.routes};
    newState.currentLocation = this.getRoute(currentPath);
    this.state = newState;
  }

  /*
   * if we tried to set state.currentLocation before we mounted, we saved it to currentLocation
   * and just assign it to state here
   */
  componentDidMount () {
    _isMounted = true;
    if (!this.state.currentLocation) {
      this.setState({currentLocation: currentLocation});
    }
  }

  /*
   * pass a path, and return the defined route corresponding to that path
   */
  getRoute (currentPathIn) {
    let lastSlashIndex = currentPathIn.lastIndexOf('/');// this only works if the id is at the end of the url!!
    this.currentId = null;
    let routeKey = '';
    if (this.routes[currentPathIn]) {
      return currentPathIn;
    } else {
      this.currentId = currentPathIn.substring(lastSlashIndex + 1);
      let potentialDynamicRoutes = [];
      for (let j in this.routes) {
        let idIndex = j.indexOf(':id');
        if (idIndex > 0) {
          routeKey = j;
          let routeStringSansId = j.substring(0, idIndex);
          let routeObj = {
            routeString: routeStringSansId,
            routeKey: routeKey
          };
          potentialDynamicRoutes.push(routeObj);
        }
      }
      let currentRouteKey = '';
      potentialDynamicRoutes.forEach((routeObj) => {
        if (currentPathIn.indexOf(routeObj.routeString) > -1) {
          currentRouteKey = routeObj.routeKey;
        }
      });
      return currentRouteKey;
    }
  }

  /*
   * respond to events on the history object
   */
  onHistoryChange (evt) {
    this.setLocation(this.getRoute(evt.pathname));
  }

  /*
   * assign a valid path to state.currentLocation, or currentLocation if we're not mounted yet
   */
  setLocation (pathIn) {
    if (_isMounted) {
      if (this.store.authStore.needsPasswordReset) {
        this.setState({currentLocation: '/passwordExpired'});
      } else {
        this.setState({currentLocation: pathIn});
      }
    } else {
      if (this.store.authStore.needsPasswordReset) {
        currentLocation = '/passwordExpired';
      } else {
        currentLocation = pathIn;
      }
    }
  }

  render () {
    component = null;
    if (AppStore.appStore.authStore.isAuthenticated && this.routes[this.state.currentLocation]) {
      if (this.currentId) {
        component = <PageChrome>{React.cloneElement(this.routes[this.state.currentLocation], {params: {id: this.currentId}})}</PageChrome>;
      } else {
        component = <PageChrome>{React.cloneElement(this.routes[this.state.currentLocation])}</PageChrome>;
      }
    } else {
      component = <Login />;
    }
    return (
      <div>
        <div id='notification-wrapper'>
          <Provider store={AppStore.appStore} >
            {component}
          </Provider>
        </div>
      </div>
    );
  };
};
