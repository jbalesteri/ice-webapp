import React from 'react';
import { render } from 'react-dom';
import debug from 'debug';

import VPSRouter from './vps-router';

import { appStore } from '../../stores/AppStore';

window.onload = () => {
  window.__debug = debug;
  render(<VPSRouter store={appStore} />, document.getElementById('app'));
};
