import AuthStore from './AuthStore';

var store;

describe('Auth Store Tests', () => {
  beforeEach(() => {
    store = new AuthStore();
  });

  it('isAuthenticated should be false by default', () => {
    expect(store.isAuthenticated).toBeFalsy();
  });

  it('authStore should be an empty object by default', () => {
    expect(store.authInfo).toBeUndefined();
  });
});
