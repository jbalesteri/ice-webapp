const debug = require('debug')('ice:stores:datacenter'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class DataCenterStore {
  @observable name = '';
  @observable description = '';
  @observable location = '';
  @observable metricsAggregateResetTimestamp = '';

  @observable loading = false;

  api = null;

  constructor (api) {
    this.api = api;
  }

  @computed get metricsReset () {
    return new Date(this.metricsAggregateResetTimestamp);
  }

  @computed get toJson () {
    return {
      name: this.name,
      description: this.description,
      location: this.location
    };
  }

  updateFromJson (json) {
    debug('Updating from json:', json);

    if (json.name === '4Y1G3hs8eIu3gTvr5bCWfojOgvv6aJkGdfFJ8DTXJojshYeiDSYio9KasccR3xVq') {
      json.name = '';
    }

    this.name = json.name || '';
    this.description = json.description || '';
    this.location = json.location || '';
    this.metricsAggregateResetTimestamp = json.metricsAggregateResetTimestamp || '';
  }

  load () {
    this.loading = true;
    return this.api.dataCenter.get().then((dc) => {
      if (dc) {
        this.updateFromJson(dc);
      }
      return this.toJson;
    });
  }

  save () {
    return this.api.dataCenter.update(this.toJson).then((dc) => {
      this.updateFromJson(dc);
      return this.toJson;
    });
  }

  metricsAggregateReset () {
    return this.api.dataCenter.metricsAggregateReset().then((json) => {
      this.updateFromJson(json);
      return this.toJson;
    });
  }
}
