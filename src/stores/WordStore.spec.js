import moxios from 'moxios';

import WordStore from './WordStore';

describe('WordStore Test', () => {
  beforeEach(() => {
    moxios.install();
    moxios.stubRequest('/data/translations/en/translations.json', {
      status: 200,
      response: [
        { 'hi': 'Hello' },
        { 'mornin': 'Good Morning' }
      ]
    });

    moxios.stubRequest('/data/translations/de/translations.json', {
      status: 200,
      response: [
        { 'hi': 'Hallo' },
        { 'mornin': 'Guten Morgen' }
      ]
    });
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('should fetch english by default when created', () => {
    let store = new WordStore();

    moxios.wait(() => {
      expect(store.loading).toBeFalsy();
      expect(store.lang).toBe('en');
      expect(store._words.length).toBe(2);
    });
  });

  it('should translate', () => {
    let store = new WordStore();

    moxios.wait(() => {
      expect(store.loading).toBeFalsy();
      expect(store.translate('hi')).toBe('Hello');
      expect(store.translate('mornin')).toBe('Good Morning');
    });
  });

  it('should return the key when a translation is not found', () => {
    let store = new WordStore();

    moxios.wait(() => {
      expect(store.loading).toBeFalsy();
      expect(store.translate('hi')).toBe('Hello');
      expect(store.translate('foo')).toBe('foo');
    });
  });
});
