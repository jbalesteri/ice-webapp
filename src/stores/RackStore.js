const debug = require('debug')('ice:RackStore'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import IdStore from './IdStore';
import Rack from '../models/Rack';

export default class RackStore extends IdStore {
  api = null;
  store = null;

  constructor (api, deviceStore) {
    super({
      fetch: api.racks.get.bind(api),
      create: api.racks.create.bind(api),
      update: api.racks.update.bind(api),
      remove: api.racks.delete.bind(api)
    });

    this.api = api;
    this.deviceStore = deviceStore;
  }

  @computed get racks () {
    console.warn('DEPRECATED: use rackStore.all for all racks.');
    return this.all;
  }

  @computed get all () {
    return this.items;
  }

  createRack () {
    let rack = new Rack(this);
    return rack;
  }

  load () {
    return this._fetchFunction().then((racks) => {
      racks.forEach((rack) => {
        this.updateFromServer(rack);
      });
    });
  }

  updateFromServer (json) {
    let rack = this.map.get(json.id);
    if (!rack) {
      rack = this.createRack();
      this.map.set(json.id, rack);
    }
    rack.updateFromJson(json);
  }

  onMetricsRecieved (evtData) {
    let data = [].concat(evtData);
    data.forEach((metrics) => {
      let rack = this.map.get(metrics.rackId);
      if (rack) {
        rack.metrics.updateFromJson(metrics);
      }
    });
  }

  @computed get usedDevices () {
    return this.items.reduce((devices, rack) => {
      return devices.concat(rack.rpduIds);
    }, []);
  }
}
