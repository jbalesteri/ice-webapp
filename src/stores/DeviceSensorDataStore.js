const debug = require('debug')('ice:stores:DeviceSensorData'); // eslint-disable-line no-unused-vars
import {toJS} from 'mobx';
import TimeSeriesStore from './TimeSeriesStore';
import DeviceSensorData from '../models/DeviceSensorData';

export default class DeviceSensorDataStore extends TimeSeriesStore {
  onDeviceSensorDataReceived (data) {
    this.addDataPoint(data.id, data);
  }

  onDevicesSensorDataReceived (data) {
    if (Array.isArray(data)) {
      data.forEach((sensorData) => {
        this.updateFromServer(sensorData);
      });
    }
  }

  currentSensorData (id) {
    let myDeviceData = this._data.get(id);
    if (myDeviceData) {
      let series = toJS(myDeviceData.series);
      let myLastSeriesItem = series[series.length - 1];
      return myLastSeriesItem;
    }
  }

  getSensorData (id) {
    return this.getDataForId(id);
  }

  getIds () {
    return this._data.keys();
  }

  load () {
    return this._fetchFunction().then((devices) => {
      devices.forEach((device) => {
        this.updateFromServer(device);
      });
    });
  }

  updateFromServer (json) {
    let device = this._data.get(json.deviceId);
    if (!device) {
      device = new DeviceSensorData();
      this._data.set(json.deviceId, device);
    }
    device.updateFromJson(json);
  }
}
