import IdStore from './IdStore';

var store;

var operations = {
  fetch: () => {
    return new Promise((resolve) => {
      process.nextTick(() => {
        resolve([
          { id: 1, value: 'one' },
          { id: 2, value: 'two' }
        ]);
      });
    });
  },

  create: (item) => {
    return new Promise((resolve) => {
      item.id = 3;
      resolve(item);
    });
  },

  update: (item) => {
    return new Promise((resolve) => {
      resolve(item);
    });
  },

  remove: (id) => {
    return new Promise((resolve) => {
      resolve(id);
    });
  }
};

describe('IdStore Tests', () => {
  beforeEach(() => {
    store = new IdStore(operations);
  });

  it('should start empty', () => {
    expect(store.items.length).toBe(0);
  });

  it('should take an array of Id\'ed objects', () => {
    let items = [
      { id: 1, value: 'one' },
      { id: 2, value: 'two' }
    ];

    store.items = items;

    expect(store.items.length).toBe(2);
    expect(store.getById(1).value).toBe('one');
  });

  it('should take an object map', () => {
    let items = {
      1: { id: 1, value: 'one' },
      2: { id: 2, value: 'two' }
    };

    store.items = items;

    expect(store.items.length).toBe(2);
    expect(store.getById(1).value).toBe('one');
  });

  it('items should return an array', () => {
    expect(Array.isArray(store.items)).toBe(true);
  });

  it('map should return an object', () => {
    expect(typeof store.map).toBe('object');
  });

/*
  it('should fetch items using a provided function', () => {
    return store.fetch().then(() => {
      expect(store.items.length).toBe(2);
    });
  });

  it('Should be able to save new items', () => {
    return store.save({ value: 'three' }).then(() => {
      expect(store.items.length).toBe(1);
      expect(store.items[0].id).toBeDefined();
    });
  });
*/

  it('should remove items by id', () => {
    let items = {
      1: { id: 1, value: 'one' },
      2: { id: 2, value: 'two' }
    };

    store.items = items;

    return store.remove('2').then(() => {
      expect(store.items.length).toBe(1);
    });
  });

/*
  it('should be able to update items', () => {
    let items = {
      1: { id: 1, value: 'one' },
      2: { id: 2, value: 'two' }
    };

    store.items = items;

    return store.save({ id: 1, value: 'oneone' }).then(() => {
      let item = store.getById(1);
      expect(item.value).toBe('oneone');
    });
  });
*/
});
