import { computed } from 'mobx';

import IdStore from './IdStore';
import CatalogDevice from '../models/CatalogDevice';

export default class CatalogStore extends IdStore {
  api = null;

  constructor (api) {
    super({
      fetch: api.catalog.get.bind(api)
    });

    this.api = api;
  }

  @computed get all () {
    return this.items;
  }

  @computed get types () {
    let types = {};
    this.all.map((device) => {
      types[device.type] = true;
    });
    return Object.keys(types);
  }

  @computed get models () {
    return this.all.map((device) => (device.model));
  }

  createCatalogDevice () {
    let catalogDevice = new CatalogDevice(this);
    return catalogDevice;
  }

  updateFromJson (json) {
    let data = [].concat(json);
    data.forEach((device) => {
      let catalogDevice = this.map.get(device.model);
      let newDevice = !catalogDevice;
      if (newDevice) {
        catalogDevice = this.createCatalogDevice();
      }
      catalogDevice.updateFromJson(device);
      if (newDevice) {
        this.map.set(catalogDevice.id, catalogDevice);
      }
    });
  }

  load () {
    return this._fetchFunction().then((catalog) => {
      this.updateFromJson(catalog);
      return this.all;
    });
  }
}
