const debug = require('debug')('ice:stores:dynamicredundancystore'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class DynamicRedundancyStateStore {
  @observable passed = false;
  @observable messages = [];

  api = null;

  constructor (api) {
    this.api = api;
  }

  updateFromJson (json) {
    this.passed = json.proactiveTestsPassed || false;
    let messages = Array.isArray(json.message) ? json.message : [];
    this.messages.replace(messages);
  }

  @computed get toJson () {
    return {
      proactiveTestsPassed: this.passed,
      message: this.messages.slice()
    };
  }

  load () {
    return this.api.drConfig.state().then((state) => {
      if (state) {
        this.updateFromJson(state);
      }
      return this.toJson;
    });
  }
}
