var debug = require('debug')('ice:GroupStore'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import IdStore from './IdStore';
import Group from '../models/Group';

export default class GroupStore extends IdStore {
  deviceStore = null;

  constructor (api, deviceStore) {
    super({
      fetch: api.groups.get.bind(api),
      create: api.groups.create.bind(api),
      update: api.groups.update.bind(api),
      remove: api.groups.delete.bind(api)
    });

    this.deviceStore = deviceStore;
  }

  createGroup () {
    let group = new Group(this);
    return group;
  }

  load () {
    return this._fetchFunction().then((groups) => {
      groups.forEach((group) => {
        this.updateFromServer(group);
      });
    });
  }

  onSensorDataReceived (evtData) {
    let data = [].concat(evtData);
    data.forEach((sensorData) => {
      let group = this.map.get(sensorData.groupId);
      if (group) {
        group.sensorData.updateFromJson(sensorData);
      }
    });
  }

  updateFromServer (json) {
    let group = this.map.get(json.id);
    if (!group) {
      group = new Group(this);
      this.map.set(json.id, group);
    }
    group.updateFromJson(json);
  }

  @computed get all () {
    return this.items;
  }

  @computed get usedDevices () {
    return this.items.reduce((devices, group) => {
      return devices.concat(group.deviceIds);
    }, []);
  }
}
