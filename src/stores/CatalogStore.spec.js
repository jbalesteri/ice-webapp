import CatalogStore from './CatalogStore';
import CatalogDevice from '../models/CatalogDevice';

let store;

const devices = [
  {
    'model': 'AC6000 V2',
    'manufacturer': 'Methode',
    'completeName': 'Methode AC6000 V2',
    'maxPowerW': 6000,
    'batteryCapacityWh': 1038,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'iceblock',
    'deviceModel': 'ac6kV2',
    'isDumb': false
  },
  {
    'model': 'Model 1',
    'manufacturer': 'VPS Inc.',
    'completeName': 'VPS ICE Block',
    'maxPowerW': 12000,
    'batteryCapacityWh': 1200,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'iceblock',
    'deviceModel': 'model1',
    'isDumb': false
  },
  {
    'model': 'No Model',
    'manufacturer': 'No Manufacturer',
    'completeName': 'Three phase sensor for the group level',
    'maxPowerW': 20000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '^[0-9]+.[0-9]+.[0-9]+.[0-9]+$',
    'deviceType': 'sensor',
    'deviceModel': 'threePhaseSensor',
    'isDumb': false
  },
  {
    'model': 'AP8941',
    'manufacturer': 'Schneider Electric APC',
    'completeName': 'APC AP8941 PDU',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [
      'ac6k',
      'gen2'
    ],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'rpdu',
    'deviceModel': 'ap8941',
    'isDumb': false
  },
  {
    'model': 'AP8xxx',
    'manufacturer': 'Schneider Electric APC',
    'completeName': 'APC AP8 Series PDU',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [
      'ac6k',
      'gen2'
    ],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'rpdu',
    'deviceModel': 'apc8',
    'isDumb': false
  },
  {
    'model': 'Dumb Rpdu',
    'manufacturer': 'Unspecified',
    'completeName': 'Dumb RPDU',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [
      'ac6k',
      'gen2'
    ],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'rpdu',
    'deviceModel': 'dumbRpdu',
    'isDumb': true
  },
  {
    'model': 'FDC',
    'manufacturer': 'Emerson Liebert',
    'completeName': 'Liebert FDC',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [
      'ac6k',
      'gen2'
    ],
    'oidEntensionRequired': true,
    'oidExtensionRegex': '^[0-9]+.[0-9]+$',
    'deviceType': 'rpdu',
    'deviceModel': 'liebertfdc',
    'isDumb': false
  },
  {
    'model': 'gateway',
    'manufacturer': 'Packet Power',
    'completeName': 'Packet Power Monitoring',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [
      'ac6k',
      'gen2'
    ],
    'oidEntensionRequired': true,
    'oidExtensionRegex': '^[0-9]+.[0-9]+.[0-9]+.[0-9]+$',
    'deviceType': 'fpdu',
    'deviceModel': 'packetpower',
    'isDumb': false
  },
  {
    'model': 'GW03',
    'manufacturer': 'Packet Power',
    'completeName': 'Packet Power Monitoring GW03',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': true,
    'oidExtensionRegex': '^[0-9]+.[0-9]+.[0-9]+.[0-9]+$',
    'deviceType': 'fpdu',
    'deviceModel': 'packetpowergw03',
    'isDumb': false
  },
  {
    'model': 'pm8000',
    'manufacturer': 'Schneider Electric APC',
    'completeName': 'PM8000 PDU',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'fpdu',
    'deviceModel': 'pm8000',
    'isDumb': false
  },
  {
    'model': 'pm5000',
    'manufacturer': 'Schneider Electric APC',
    'completeName': 'PM5000 PDU',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'fpdu',
    'deviceModel': 'pm5000',
    'isDumb': false
  },
  {
    'model': 'schneiderUps',
    'manufacturer': 'Schneider',
    'completeName': 'Schneider UPS',
    'maxPowerW': 20000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '^[0-9]+.[0-9]+.[0-9]+.[0-9]+$',
    'deviceType': 'iceblock',
    'deviceModel': 'schneiderUps',
    'isDumb': false
  },
  {
    'model': 'AP7xxx',
    'manufacturer': 'Schneider Electric APC',
    'completeName': 'APC AP7 Series PDU',
    'maxPowerW': 6000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [
      'ac6k',
      'gen2'
    ],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'rpdu',
    'deviceModel': 'apc7',
    'isDumb': false
  },
  {
    'model': 'AC6000',
    'manufacturer': 'Methode',
    'completeName': 'Methode AC6000',
    'maxPowerW': 6000,
    'batteryCapacityWh': 1038,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '',
    'deviceType': 'iceblock',
    'deviceModel': 'ac6k',
    'isDumb': false
  },
  {
    'model': 'iceSwitchV1',
    'manufacturer': 'VPS',
    'completeName': 'ice switch',
    'maxPowerW': 18000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '^[0-9]+.[0-9]+.[0-9]+.[0-9]+$',
    'deviceType': 'iceSwitch',
    'deviceModel': 'iceSwitchV1',
    'isDumb': false
  },
  {
    'model': 'schneiderOutlet',
    'manufacturer': 'Schneider',
    'completeName': 'Schneider Outlet',
    'maxPowerW': 20000,
    'batteryCapacityWh': 0,
    'compatibleDeviceEmulators': [],
    'oidEntensionRequired': false,
    'oidExtensionRegex': '^[0-9]+.[0-9]+.[0-9]+.[0-9]+$',
    'deviceType': 'rpdu',
    'deviceModel': 'schneiderOutlet',
    'isDumb': false
  }
];

let apiMock = {
  catalog: {
    get: () => {
      return new Promise((resolve) => {
        resolve(devices);
      });
    }
  }
};

describe('CatalogStore Tests', () => {
  beforeEach(() => {
    store = new CatalogStore(apiMock);
  });

  afterEach(() => {
    store = null;
  });

  it('should "load" catalog devices', async() => {
    expect(store.all.length).toBe(0);
    let catalog = await store.load();
    expect(catalog.length).toBe(16);
    expect(store.all.length).toBe(16);
    expect(store.all[0]).toBeInstanceOf(CatalogDevice);
    catalog = await store.load();
    expect(catalog.length).toBe(16);
    expect(store.all.length).toBe(16);
    expect(store.all[0]).toBeInstanceOf(CatalogDevice);
  });

  it('should "get models" as an array', async() => {
    expect(Array.isArray(store.models)).toBeTruthy();
    let catalog = await store.load();
    expect(catalog.length).toBe(16);
    expect(Array.isArray(store.models)).toBeTruthy();
    expect(store.models.length).toBe(16);
  });

  it('should "get types" as an array', async() => {
    expect(Array.isArray(store.types)).toBeTruthy();
    let catalog = await store.load();
    expect(catalog.length).toBe(16);
    expect(Array.isArray(store.types)).toBeTruthy();
    expect(store.types.length).toBe(5);
  });
});
