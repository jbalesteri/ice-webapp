const debug = require('debug')('ice:stores:device'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import IdStore from './IdStore';
import Device from '../models/Device';
import DeviceTypes from '../common/constants/DeviceTypes';

export default class DeviceStore extends IdStore {
  api = null;
  feedStore = null;

  constructor (api, feedStore) {
    super({
      fetch: api.devices.get.bind(api),
      create: api.devices.create.bind(api),
      update: api.devices.update.bind(api),
      remove: api.devices.delete.bind(api)
    });

    this.api = api;
    this.feedStore = feedStore;
  }

  get all () {
    return this.items;
  }

  createDevice () {
    let device = new Device(this);
    return device;
  }

  load () {
    return this._fetchFunction().then((devices) => {
      devices.forEach((device) => {
        this.updateFromServer(device);
      });
    });
  }

  updateFromServer (json) {
    let device = this.map.get(json.id);
    if (!device) {
      device = this.createDevice();
      this.map.set(json.id, device);
    }
    device.updateFromJson(json);
  }

  onSensorDataReceived (evtData) {
    let data = [].concat(evtData);
    data.forEach((sensorData) => {
      let device = this.map.get(sensorData.deviceId);
      if (device) {
        device.sensorData.updateFromJson(sensorData);
      }
    });
  }

  @computed get rackConfigurationDevices () {
    let allRackConfigurationDevices = this._filterByType(DeviceTypes.RPDU);
    allRackConfigurationDevices = allRackConfigurationDevices.concat(this._filterByType(DeviceTypes.ICESWITCH));
    return allRackConfigurationDevices;
  }

  @computed get rpdus () {
    return this._filterByType(DeviceTypes.RPDU);
  }

  @computed get iceblocks () {
    debug('iceblocks...');
    return this._filterByType(DeviceTypes.ICEBLOCK);
  }

  @computed get sensors () {
    return this._filterByType(DeviceTypes.SENSOR);
  }

  @computed get fpdus () {
    return this._filterByType(DeviceTypes.FPDU);
  }

  _filterByType (type) {
    return this.items.filter((device) => {
      return device.type === type;
    });
  }
}
