const debug = require('debug')('ice:stores:GroupSensorData'); // eslint-disable-line no-unused-vars

import TimeSeriesStore from './TimeSeriesStore';
import GroupSensorData from '../models/GroupSensorData';

export default class GroupSensorDataStore extends TimeSeriesStore {
  onGroupSensorDataReceived (data) {
    this.addDataPoint(data.id, data);
  }

  onGroupsSensorDataReceived (data) {
    if (Array.isArray(data)) {
      data.forEach((sensorData) => {
        let myDataItem = new GroupSensorData();
        myDataItem.updateFromJson(sensorData);
        this.addDataPoint(sensorData.groupId, myDataItem);
      });
    }
  }

  currentSensorData (id) {
    return this.currentDataForId(id);
  }

  getSensorData (id) {
    return this.getDataForId(id);
  }

  getIds () {
    return this._data.keys();
  }

  load () {
    return this._fetchFunction().then((groups) => {
      groups.forEach((group) => {
        this.updateFromServer(group);
      });
    });
  }

  updateFromServer (json) {
    let group = this._data.get(json.groupId);

    if (!group) {
      group = new GroupSensorData();
      this._data.set(json.groupId, group);
    }
    group.updateFromJson(json);
  }
}
