const debug = require('debug')('ice:stores:TimeSeries'); // eslint-disable-line no-unused-vars

import { observable, computed, action } from 'mobx';
import TimeSeriesData from '../models/TimeSeriesData';

let cacheLimit = 100;

export default class TimeSeriesStore {
  @observable _data = observable.map({});

  constructor (cacheLimitIn) {
    if (cacheLimitIn && !isNaN(cacheLimitIn) && cacheLimitIn > 0) {
      cacheLimit = cacheLimitIn;
    }
  }

  @computed get data () {
    return this._data.values();
  }

  setDataForId (id, series) {
    this._data.set(id, new TimeSeriesData(id, series));// bc - this was an implementation bug uncovered in testing
  }

  currentDataForId (id) {
    let item = this._data.get(id);
    if (item) {
      if (item.series.length > 0) {
        return item.series[item.series.length - 1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  getDataForId (id) {
    let item = this._data.get(id);
    if (item) {
      return item.series;
    } else {
      return null;
    }
  }

  @action addDataPoint (id, data) {
    let item;
    if (this._data.get(id)) {
      item = this._data.get(id);
      data && item.series.push(data);
      if (item.series.length > cacheLimit) {
        item.series.shift();
      }
    } else if (data) {
      item = new TimeSeriesData(id, [data]);
    } else {
      item = new TimeSeriesData(id);
    }
    this._data.set(id, item);
  }
}
