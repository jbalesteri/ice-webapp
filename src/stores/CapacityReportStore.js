const debug = require('debug')('ice:stores:capacityReport'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import IdStore from './IdStore';
import CapacityReport from '../models/CapacityReport';

export default class CapacityReportStore extends IdStore {
  constructor (api) {
    super({
      fetch: api.capacityReport.get.bind(api)
    });
  }

  @computed get all () {
    return this.items.sort(this._defaultSort);
  }

  createCapacityReport () {
    let capacityReport = new CapacityReport(this);
    return capacityReport;
  }

  load () {
    return this._fetchFunction().then((data) => {
      debug('REPORT STORE: ', data);
      let zoneCapacityArray = data.zoneCapacities || [];
      let timestampGenerated = data.timestampGenerated;
      zoneCapacityArray.forEach((zoneCapacity) => {
        zoneCapacity.timestampGenerated = timestampGenerated;
        this.updateFromServer(zoneCapacity);
      });
    });
  }

  updateFromServer (json) {
    let report = this.map.get(json.zoneName);
    if (!report) {
      report = this.createCapacityReport();
      this.map.set(json.zoneName, report);
    }
    report.updateFromJson(json);
  }
}
