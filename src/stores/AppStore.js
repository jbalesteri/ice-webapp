var debug = require('debug')('ice:AppStore');

import mobx from 'mobx';

import { api } from '../common/helpers/Api';
import * as SocketManager from '../common/helpers/SocketManager';

import WordStore from './WordStore';
import AuthStore from './AuthStore';
import UserStore from './UserStore';
import CatalogStore from './CatalogStore';
import HealthStore from './HealthStore';
import EventStore from './EventStore';
import FeedStore from './FeedStore';
import DeviceStore from './DeviceStore';
import GroupStore from './GroupStore';
import RackStore from './RackStore';
import ZoneStore from './ZoneStore';
import CapacityReportStore from './CapacityReportStore';
import DataCenterStore from './DataCenterStore';
import DynamicRedundancyStore from './DynamicRedundancyStore';
import DynamicRedundancyStateStore from './DynamicRedundancyStateStore';
import createHistory from 'history/createBrowserHistory';

class AppStore {
  constructor () {
    debug('AppStore->constructor===>');

    // Application Stores
    this.wordStore = new WordStore();
    this.nothing = false;// bc change for git only
    // these stores use the api
    this.authStore = new AuthStore(api);
    this.catalogStore = new CatalogStore(api);
    this.userStore = new UserStore(api);
    this.healthStore = new HealthStore(api);
    this.eventStore = new EventStore(api);

    this.feedStore = new FeedStore(api);
    this.deviceStore = new DeviceStore(api, this.feedStore);
    this.groupStore = new GroupStore(api, this.deviceStore);
    this.rackStore = new RackStore(api, this.deviceStore);
    this.zoneStore = new ZoneStore(api, this.groupStore, this.rackStore);
    this.capacityReportStore = new CapacityReportStore(api);
    this.dataCenterStore = new DataCenterStore(api);
    this.dynamicRedundancyStore = new DynamicRedundancyStore(api);
    this.dynamicRedundancyStateStore = new DynamicRedundancyStateStore(api);
    // this.history = history;
    this.history = createHistory();

    // the auto run hooks into mobx observables
    mobx.autorun(() => {
      if (this.authStore.isAuthenticated) {
        this.loadInitialData();
      }
    });
  }

  loadInitialData () {
    this.pendingPromises = [];
    this.pendingPromises.push(this.capacityReportStore.load());
    this.pendingPromises.push(this.catalogStore.load());
    this.pendingPromises.push(this.dataCenterStore.load());
    this.pendingPromises.push(this.dynamicRedundancyStore.load());

    this.pendingPromises.push(this.userStore.load());
    this.pendingPromises.push(this.eventStore.load());
    this.pendingPromises.push(this.healthStore.load());
    // joined data, loads are dependent
    this.pendingPromises.push(this.feedStore.load().then(() => {
      debug('Feeds Loaded!');
      return this.deviceStore.load().then(() => {
        debug('Devices Loaded!');
        return Promise.all([this.groupStore.load(), this.rackStore.load()]).then(() => {
          debug('Groups and Racks Loaded!');
          return this.zoneStore.load();
        });
      });
    }));

    let socketConfig = {
      callback: (evt) => {
        this.onSocketEvent(evt);
      }
    };

    SocketManager.init(socketConfig);
  }

  onSocketEvent (evt) {
    // console.log('Socket Event:', evt);
    switch (evt.type) {
      // DEVICES
      case 'device_updated':
        // update a device
        this.deviceStore.updateFromServer(evt.data);
        break;
      case 'device_deleted':
        this.deviceStore.delete(evt.data);
        break;
      case 'device_sensorData':
      case 'devices_sensorData':
        this.deviceStore.onSensorDataReceived(evt.data);
        break;

      // GROUPS
      case 'group_updated':
        // update a group
        this.groupStore.updateFromServer(evt.data);
        break;
      case 'group_deleted':
        this.groupStore.delete(evt.data);
        break;
      case 'group_sensorData':
      case 'groups_sensorData':
        this.groupStore.onSensorDataReceived(evt.data);
        break;

      // FEEDS
      case 'feed_updated':
        // update a feed
        this.feedStore.updateFromServer(evt.data);
        break;
      case 'feed_deleted':
        this.feedStore.delete(evt.data);
        break;
      case 'feeds_sensorData':
        this.feedStore.onSensorDataReceived(evt.data);
        break;

      // RACKS
      case 'rack_updated':
        // update a rack
        this.rackStore.updateFromServer(evt.data);
        break;
      case 'rack_deleted':
        this.rackStore.delete(evt.data);
        break;
      case 'racks_metrics':
        this.rackStore.onMetricsRecieved(evt.data);
        break;

      // ZONES
      case 'zone_updated':
        this.zoneStore.updateFromServer(evt.data);
        break;
      case 'zone_deleted':
        this.zoneStore.delete(evt.data);
        break;
      case 'zone_metrics':
        this.zoneStore.onMetricsRecieved(evt.data);
        break;

      // CATALOG
      case 'catalog_updated':
        // update catalog
        this.catalogStore.fetch();
        break;

      // Datacenter
      case 'datacenter_updated':
        // update datacenter
        break;

      // EVENTS
      case 'events_updated':
        // update events
        this.eventStore.updateFromServer(evt.data);
        break;

      // HEALTH
      case 'health_updated':
        // update health
        this.healthStore.fetch();
        break;

      case 'user_joined':
        // console.log('I got user_joined==>data:', data);
        break;

      default:
        debug('Unhandled socket message:', evt);
        break;
    }
  }
}

export const appStore = new AppStore();
