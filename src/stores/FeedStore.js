const debug = require('debug')('ice:stores:feeds'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import IdStore from './IdStore';
import Feed from '../models/Feed';
import Phase from '../common/constants/Phase';

export default class FeedStore extends IdStore {
  constructor (api) {
    super({
      fetch: api.feeds.get.bind(api),
      create: api.feeds.create.bind(api),
      update: api.feeds.update.bind(api),
      remove: api.feeds.delete.bind(api)
    });
  }

  @computed get all () {
    return this.items.sort(this._defaultSort);
  }

  createFeed () {
    let feed = new Feed(this);
    return feed;
  }

  load () {
    return this._fetchFunction().then((feeds) => {
      feeds.forEach((feed) => {
        this.updateFromServer(feed);
      });
      return this.all;
    });
  }

  updateFromServer (json) {
    let feed = this.map.get(json.id);
    if (!feed) {
      feed = this.createFeed();
      this.map.set(json.id, feed);
    }
    feed.updateFromJson(json);
  }

  onSensorDataReceived (evtData) {
    let data = [].concat(evtData);
    data.forEach((sensorData) => {
      let feed = this.map.get(sensorData.feedId);
      if (feed) {
        let phase;
        switch (sensorData.phase) {
          case Phase.ONE:
            phase = 0;
            break;
          case Phase.TWO:
            phase = 1;
            break;
          case Phase.THREE:
            phase = 2;
            break;
        }
        feed.metrics[phase].updateFromJson(sensorData);
      }
    });
  }

  _defaultSort (a, b) {
    let symbolA = (a && a.symbol) ? a.symbol.toLowerCase() : null;
    let symbolB = (b && b.symbol) ? b.symbol.toLowerCase() : null;
    if (symbolA < symbolB) {
      return -1;
    }
    if (symbolB < symbolA) {
      return 1;
    }
    return 0;
  }
}
