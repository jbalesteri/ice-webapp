const debug = require('debug')('ice:stores:dynamicredundancy'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class DynamicRedundancyStore {
  @observable enabled = false;
  @observable feedLimits = [];

  api = null;

  constructor (api) {
    this.api = api;
  }

  @computed get toJson () {
    return {
      redundancyEnabled: this.enabled,
      feedLimits: this.feedLimits.slice()
    };
  }

  updateFromJson (json) {
    this.enabled = json.redundancyEnabled || false;
    let feedLimits = Array.isArray(json.feedLimits) ? json.feedLimits : [];
    this.feedLimits.replace(feedLimits);
  }

  load () {
    return this.api.drConfig.get().then((config) => {
      this.updateFromJson(config);
      return this.toJson;
    });
  }

  save () {
    return this.api.drConfig.update(this.toJson).then((config) => {
      this.updateFromJson(config);
      return this.toJson;
    });
  }
}
