const debug = require('debug')('ice:stores:zone'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import ZoneTypes from '../common/constants/ZoneTypes';
import IdStore from './IdStore';
import Zone from '../models/Zone';

export default class ZoneStore extends IdStore {
  api = null;
  groupStore = null;
  rackStore = null;

  constructor (api, groupStore, rackStore) {
    super({
      fetch: api.zones.get.bind(api),
      create: api.zones.create.bind(api),
      update: api.zones.update.bind(api),
      remove: api.zones.delete.bind(api)
    });

    this.api = api;
    this.groupStore = groupStore;
    this.rackStore = rackStore;
  }

  get all () {
    return this.items;
  }

  createZone () {
    let zone = new Zone(this);
    return zone;
  }

  load () {
    return this._fetchFunction().then((zone) => {
      zone.forEach((zone) => {
        this.updateFromServer(zone);
      });
    });
  }

  updateFromServer (json) {
    let zone = this.map.get(json.id);
    if (!zone) {
      zone = this.createZone();
      this.map.set(json.id, zone);
    }
    zone.updateFromJson(json);
  }

  onMetricsRecieved (json) {
    let data = [].concat(json);
    data.forEach((metrics) => {
      let zone = this.map.get(metrics.zoneId);
      if (zone) {
        zone.metrics.updateFromJson(metrics);
      }
    });
  }

  @computed get rooms () {
    return this._filterByType(ZoneTypes.ROOM);
  }

  @computed get rows () {
    return this._filterByType(ZoneTypes.ROW);
  }

  _filterByType (type) {
    return this.items.filter((zone) => {
      return zone.type === type;
    });
  }

  @computed get usedRacks () {
    return this.items.reduce((racks, zone) => {
      return racks.concat(zone.rackIds.slice());
    }, []);
  }

  @computed get usedGroups () {
    return this.items.reduce((groups, zone) => {
      return groups.concat(zone.groups.slice());
    }, []);
  }
}
