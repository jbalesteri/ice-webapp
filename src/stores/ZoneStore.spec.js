import ZoneStore from './ZoneStore';

var store;
let fakeZonesArr = [];

let fakeZones = {
  '123': {
    zoneType: 'room',
    allocatedPower: 23,
    rackIds: ['21', '32'],
    breakerLimits: ['31', '42'],
    iceLimits: ['41', '52'],
    groups: ['101', '102']
  },
  '456': {
    zoneType: 'row',
    allocatedPower: 13,
    rackIds: ['11', '22'],
    breakerLimits: ['21', '32'],
    iceLimits: ['31', '42'],
    groups: ['201', '202']
  }
};

let operations = {
  zones: {
    get: () => {
      return new Promise((resolve) => {
        process.nextTick(() => {
          fakeZonesArr = [];
          for (var j in fakeZones) {
            fakeZones[j].id = j;
            fakeZonesArr.push(fakeZones[j]);
          }
          resolve(fakeZonesArr);
        });
      });
    },
    create: (item) => {
      return new Promise((resolve) => {
        item.id = 3;
        resolve(item);
      });
    },

    update: (item) => {
      return new Promise((resolve) => {
        resolve(item);
      });
    },

    delete: (id) => {
      return new Promise((resolve) => {
        resolve(id);
      });
    }
  }
};

let fakeGroupStore = {
  getById: (idIn) => { return {id: idIn}; }
};

let fakeRackStore = {
  getById: (idIn) => { return {id: idIn}; }
};

describe('ZoneStore Tests', () => {
  beforeEach(() => {
    store = new ZoneStore(operations, fakeGroupStore, fakeRackStore);
  });

  it('should load zones', async() => {
    await store.load();
    expect(store.all.length).toBe(2);
    expect(store.all[0].id).toBe(fakeZonesArr[0].id);
    expect(store.all[1].id).toBe(fakeZonesArr[1].id);
  });

  it('should return rooms', async() => {
    await store.load();
    expect(store.rooms[0].id).toBe('123');
    expect(store.rooms[0].type).toBe('room');
  });

  it('should return rows', async() => {
    await store.load();
    expect(store.rows[0].id).toBe('456');
    expect(store.rows[0].type).toBe('row');
  });

  it('should return usedGroups', async() => {
    await store.load();
    expect(store.usedGroups[0].id).toBe('101');
    expect(store.usedGroups[1].id).toBe('102');
    expect(store.usedGroups[2].id).toBe('201');
    expect(store.usedGroups[3].id).toBe('202');
  });

  it('should return usedRacks', async() => {
    await store.load();
    expect(store.usedRacks[0]).toBe('21');
    expect(store.usedRacks[1]).toBe('32');
    expect(store.usedRacks[2]).toBe('11');
    expect(store.usedRacks[3]).toBe('22');
  });

  it('should update the zone.metrics object with id matching the id of received sensorData', async() => {
    await store.load();

    let fakeSensorData = {
      available1nCapacityW: 48000,
      available2nCapacityW: -80384,
      availableCapacityW: -32384,
      averageSocProp: 0.775,
      instantaneousInputPowerW: 16642,
      maxInputPowerW: 44282,
      maxOutputPowerW: 43508,
      maxUtilizationProp: 2.2141,
      phasePowerIn: [
        {
          breakerLimitW: 16000,
          instantaneousW: 23305,
          maxW: 43891,
          phase: '1-2'
        },
        {
          breakerLimitW: 16000,
          instantaneousW: 23305,
          maxW: 43891,
          phase: '2-3'
        },
        {
          breakerLimitW: 16000,
          instantaneousW: 23305,
          maxW: 43891,
          phase: '3-1'
        }
      ],
      powerLimits: [
        {
          feed: 'this is the feed id',
          limitW: 20000
        },
        {
          feed: 'this is the feed id',
          limitW: 20000
        }
      ],
      zone1nPeak: 0,
      zone2nPeak: 44282,
      zoneId: '123'
    };

    store.onMetricsRecieved(fakeSensorData);
    let myZone = store.getById('123');
    expect(myZone.metrics.available1nCapacityW).toBe(fakeSensorData.available1nCapacityW);
    expect(myZone.metrics.available2nCapacityW).toBe(fakeSensorData.available2nCapacityW);
    expect(myZone.metrics.availableCapacityW).toBe(fakeSensorData.availableCapacityW);
    expect(myZone.metrics.averageSocProp).toBe(fakeSensorData.averageSocProp);
    expect(myZone.metrics.instantaneousInputPowerW).toBe(fakeSensorData.instantaneousInputPowerW);
    expect(myZone.metrics.maxInputPowerW).toBe(fakeSensorData.maxInputPowerW);
    expect(myZone.metrics.maxOutputPowerW).toBe(fakeSensorData.maxOutputPowerW);
    expect(myZone.metrics.maxUtilizationProp).toBe(fakeSensorData.maxUtilizationProp);
  });

  it('should remove items by id', async() => {
    await store.load();
    return store.remove('123').then(() => {
      expect(store.items.length).toBe(1);
    });
  });
});
