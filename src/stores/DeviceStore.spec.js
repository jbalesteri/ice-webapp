import DeviceStore from './DeviceStore';

let store;
let fakeDeviceName = 'brian@vpsi.io';
let fakeDeviceRole = 'device';
let storedDevices = {};

let apiMock = {
  devices: {
    get: () => {
      let p = new Promise((resolve) => {
        process.nextTick(() => {
          resolve(storedDevices);
        });
      });

      return p;
    },

    create: (device) => {
      let p = new Promise((resolve) => {
        process.nextTick(() => {
          device.id = Math.round(Math.random() * 1000);
          storedDevices[device.id] = device;
          resolve(device);
        });
      });
      return p;
    },

    update: (device) => {
      let deviceToUpdate = storedDevices[device.id];
      for (let j in device) {
        if (device.hasOwnProperty(j)) {
          deviceToUpdate[j] = device[j];
        }
      }
      let p = new Promise((resolve) => {
        process.nextTick(() => {
          resolve(deviceToUpdate);
        });
      });
      return p;
    },

    delete: (id) => {
      let p = new Promise((resolve) => {
        process.nextTick(() => {
          storedDevices[id] = null;
          delete storedDevices[id];
          resolve();
        });
      });
      return p;
    },

    adminPassword: () => {
      return true;
    }
  }
};

describe('DeviceStore Tests', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    storedDevices = {};
    store = new DeviceStore(apiMock);
  });

  it('Should start with an empty devices array', () => {
    expect(store.loading).toBeFalsy();
    expect(store.all.length).toBe(0);
  });

  it('Should let you create a device', () => {
    let fakeDevice = {
      email: fakeDeviceName,
      role: fakeDeviceRole,
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    return store.save(fakeDevice).then(() => {
      expect(store.all.length).toBe(1);
      expect(store.all[0].id).toBeDefined();
      expect(store.all[0].email).toBe(fakeDeviceName);
      expect(store.all[0].role).toBe(fakeDeviceRole);
    });
  });

  it('Should let you create multiple devices', () => {
    let fakeDevice1 = {
      email: 'a@a.com',
      role: 'device',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    let fakeDevice2 = {
      email: 'a@b.com',
      role: 'admin',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    store.save(fakeDevice1);
    return store.save(fakeDevice2).then(() => {
      expect(store.all.length).toBe(2);
      expect(store.all[0].id).toBeDefined();
      expect(store.all[0].email).toBe('a@a.com');
      expect(store.all[0].role).toBe('device');
      expect(store.all[1].id).toBeDefined();
      expect(store.all[1].email).toBe('a@b.com');
      expect(store.all[1].role).toBe('admin');
    });
  });

  it('Should persist changes when calling fetch', () => {
    let fakeDevice1 = {
      email: 'a@a.com',
      role: 'device',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    let fakeDevice2 = {
      email: 'a@b.com',
      role: 'admin',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    store.save(fakeDevice1);
    return store.save(fakeDevice2).then(() => {
      return store.fetch().then(() => {
        expect(store.all.length).toBe(2);
        expect(store.all[0].id).toBeDefined();
        expect(store.all[0].email).toBe('a@a.com');
        expect(store.all[0].role).toBe('device');
        expect(store.all[1].id).toBeDefined();
        expect(store.all[1].email).toBe('a@b.com');
        expect(store.all[1].role).toBe('admin');
      });
    });
  });

  it('should let you update an existing device', () => {
    let fakeDevice1 = {
      email: 'barack@obama.com',
      role: 'admin',
      firstName: 'Barack',
      lastName: 'Obama',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role, firstName: this.firstName, lastName: this.lastName};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    let fakeDevice2 = {
      email: 'vladimir@putin.com',
      role: 'admin',
      firstName: 'Vladimir',
      lastName: 'Putin',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    store.save(fakeDevice1).then(() => {
      return store.save(fakeDevice2).then(() => {
        expect(store.all.length).toBe(2);
        expect(store.all[0].role).toBe('admin');
        expect(store.all[0].email).toBe(fakeDevice1.email);
        expect(store.all[0].firstName).toBe(fakeDevice1.firstName);
        expect(store.all[0].lastName).toBe(fakeDevice1.lastName);
        expect(store.all[1].role).toBe('admin');
        expect(store.all[1].email).toBe(fakeDevice2.email);
        expect(store.all[1].firstName).toBe(fakeDevice2.firstName);
        expect(store.all[1].lastName).toBe(fakeDevice2.lastName);

        let updatedFakeDevice = {
          id: store.all[0].id,
          email: 'donald@trump.com',
          firstName: 'Donald',
          lastName: 'Trump',
          role: 'device',
          toJson: function () {
            return {id: this.id, email: this.email, role: this.role, firstName: this.firstName, lastName: this.lastName};
          },
          updateFromJson: function (input) {
            for (var j in input) {
              this[j] = input[j];
            }
          }
        };
        return store.save(updatedFakeDevice).then(() => {
          expect(store.all.length).toBe(2);
          expect(store.all[0].role).toBe(updatedFakeDevice.role);
          expect(store.all[0].email).toBe(updatedFakeDevice.email);
          expect(store.all[0].firstName).toBe(updatedFakeDevice.firstName);
          expect(store.all[0].lastName).toBe(updatedFakeDevice.lastName);

          expect(store.all[1].role).toBe(fakeDevice2.role);
          expect(store.all[1].email).toBe(fakeDevice2.email);
          expect(store.all[1].firstName).toBe(fakeDevice2.firstName);
          expect(store.all[1].lastName).toBe(fakeDevice2.lastName);
        });
      }).catch((err) => {
        console.log('error updating device===>err:', err);
      });
    });
  });

  it('should let you delete an existing device', () => {
    let fakeDevice = {
      email: fakeDeviceName,
      role: 'admin',
      toJson: function () {
        return {id: this.id, email: this.email, role: this.role, firstName: this.firstName, lastName: this.lastName};
      },
      updateFromJson: function (input) {
        for (var j in input) {
          this[j] = input[j];
        }
      }
    };
    store.save(fakeDevice).then(() => {
      expect(store.all.length).toBe(1);
      expect(store.all[0].role).toBe('admin');
      expect(store.all[0].email).toBe(fakeDeviceName);

      let idToDelete = store.all[0].id + '';
      return store.remove(idToDelete).then(() => {
        expect(store.all.length).toBe(0);
      });
    }).catch((err) => {
      console.log('error updating device===>err:', err);
    });
  });
});
