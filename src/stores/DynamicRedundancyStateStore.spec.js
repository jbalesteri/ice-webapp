import DynamicRedundancyStateStore from './DynamicRedundancyStateStore';

let store;

const drStateFalse = {
  proactiveTestsPassed: false,
  message: [
    {
      desc: 'If Feed B were to fail, then instantaneously Feed A-1-2 will fail due to too many 1N racks.',
      fix: 'Turn off the low-priority 1N rack(s) (Rack1). If Rack1 were to be turned off, then this feed-phase won\'t be overloaded.',
      rootCause: 'The Feed A-1-2 limit is 5.0 kW which is less than the total power (6.0 kW of which 2.0 KW is 1N) on this feed-phase. '
    },
    {
      desc: 'If Feed A were to fail, then instantaneously Feed B-1-2 will fail due to too many 2N racks despite turning off all 1N racks (Rack1) on this feed',
      fix: 'Convert more \'2N\' racks to \'1N\'.',
      rootCause: 'The Feed B-1-2 limit is 1.5 kW which is less than the total power (4.0 kW, of which 2.0 KW is 1N) on this feed-phase. Even if Rack1 were to be turned off, this feed-phase is still overloaded'
    },
    {
      desc: 'If Feed A were to fail, then eventually Feed B-1-2 will fail due to too many 2N racks.',
      fix: 'Convert more \'2N\' racks to \'1N\'.',
      rootCause: 'The Feed B-1-2 limit is 1.5 kW which is less than the eventual 2N power 4.0 kW on this feed-phase. '
    }
  ]
};

const apiMock = {
  drConfig: {
    state: () => {
      return new Promise((resolve) => {
        resolve(drStateFalse);
      });
    }
  }
};

describe('DynamicRedundancyStateStore Tests', () => {
  beforeEach(() => {
    store = new DynamicRedundancyStateStore(apiMock);
  });

  it('should "updateFromJson"', async () => {
    expect(store.passed).toBeFalsy();
    expect(store.messages.length).toBe(0);
    let state = await store.load();
    expect(state.message.length).toBe(3);
    expect(store.passed).toBeFalsy();
    store.updateFromJson({ proactiveTestsPassed: true, message: '' });
    expect(store.messages.length).toBe(0);
    expect(store.passed).toBeTruthy();
  });

  it('should "load"', async () => {
    expect(store.passed).toBeFalsy();
    expect(store.messages.length).toBe(0);
    let state = await store.load();
    expect(state.message.length).toBe(3);
    expect(store.passed).toBeFalsy();
  });
});
