const debug = require('debug')('ice:stores:FeedSensorData'); // eslint-disable-line no-unused-vars

import TimeSeriesStore from './TimeSeriesStore';
import FeedSensorData from '../models/FeedSensorData';

export default class FeedSensorDataStore extends TimeSeriesStore {
  onFeedMetricsReceived (data) {
    if (Array.isArray(data)) {
      data.forEach((feed) => {
        let id = feed.feedId + '-' + feed.phase;
        let myFeed = new FeedSensorData();
        myFeed.updateFromJson(feed);
        this.addDataPoint(id, myFeed);
      });
    }
  }

  currentSensorData (id, phase) {
    phase = phase || '1-2';
    id = id + '-' + phase;
    return this.currentDataForId(id);
  }

  getSensorData (id, phase) {
    phase = phase || '1-2';
    id = id + '-' + phase;
    return this.getDataForId(id);
  }

  getIds () {
    return this._data.keys();
  }

  load () {
    return this._fetchFunction().then((feeds) => {
      feeds.forEach((feed) => {
        this.updateFromServer(feed);
      });
    });
  }

  updateFromServer (json) {
    let feed = this._data.get(json.feedId);
    if (!feed) {
      feed = new FeedSensorData();
      this._data.set(json.feedId, feed);
    }
    feed.updateFromJson(json);
  }
}
