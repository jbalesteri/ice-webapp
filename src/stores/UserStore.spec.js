import UserStore from './UserStore';
import User from '../models/User';

let store;

const users = [
  {
    id: 1,
    email: 'justin@virtualpowersystems.com',
    roles: ['admin'],
    active: true,
    firstName: 'justin',
    lastName: 'bastedo',
    phoneNumber: '1231231234'
  },
  {
    id: 2,
    email: 'bill@virtualpowersystems.com',
    roles: ['user'],
    active: true,
    firstName: 'bill',
    lastName: 'smith',
    phoneNumber: '3213214321'
  },
  {
    id: 3,
    email: 'xena@virtualpowersystems.com',
    roles: ['user'],
    active: true,
    firstName: 'xena',
    lastName: 'johnson',
    phoneNumber: '9879879876'
  }
];

let apiMock = {
  users: {
    get: () => {
      return new Promise((resolve) => {
        resolve(users);
      });
    },

    create: (user) => {
      return users.concat(user);
    },

    update: (user) => {
      return user;
    },

    delete: () => {
      return true;
    },

    adminPassword: () => {
      return true;
    }
  }
};

describe('UserStore Tests', () => {
  beforeEach(() => {
    store = new UserStore(apiMock);
  });

  afterEach(() => {
    store = null;
  });

  it('should "load" replacing any current data', async() => {
    expect(store.all.length).toBe(0);
    let users = await store.load();
    expect(users.length).toBe(3);
    expect(store.all.length).toBe(3);
    expect(store.all[0]).toBeInstanceOf(User);
    expect(store.all[1]).toBeInstanceOf(User);
    expect(store.all[2]).toBeInstanceOf(User);
    users = await store.load();
    expect(users.length).toBe(3);
    expect(store.all.length).toBe(3);
    expect(store.all[0]).toBeInstanceOf(User);
    expect(store.all[1]).toBeInstanceOf(User);
    expect(store.all[2]).toBeInstanceOf(User);
  });

  it('should "updateFromServer" updating existing models', () => {
    store.updateFromServer(users[0]);
    let modUser = Object.assign({}, users[0]);
    modUser.email = 'foo@virtualpowersystems.com';
    store.updateFromServer(modUser);
    expect(store.all.length).toBe(1);
    expect(store.all[0].email).toBe(modUser.email);
  });
});
