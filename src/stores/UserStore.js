const debug = require('debug')('ice:User:Store'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';
import IdStore from './IdStore';
import User from '../models/User';

export default class UserStore extends IdStore {
  constructor (api) {
    super({
      fetch: api.users.get.bind(api),
      create: api.users.create.bind(api),
      update: api.users.update.bind(api),
      remove: api.users.delete.bind(api)
    });

    this.adminPassword = api.users.adminPassword.bind(api);
  }

  @computed get all () {
    return this.items;
  }

  createUser () {
    let user = new User(this);
    return user;
  }

  updateFromServer (json) {
    let user = this.map.get(json.id);
    if (!user) {
      user = this.createUser();
      this.map.set(json.id, user);
    }
    user.updateFromJson(json);
  }

  load () {
    return this._fetchFunction().then((users) => {
      users.forEach((user) => {
        this.updateFromServer(user);
      });
      return this.all;
    });
  }
}
