import DynamicRedundancyStore from './DynamicRedundancyStore';

let store;

const drConfig = {
  feedLimits: [
    {
      feed: '6d6bee22-3e1e-444c-b6f5-c0f4f04453fb',
      phases: [
        {
          phase: '1-2',
          limit: 5000
        },
        {
          phase: '2-3',
          limit: 5000
        },
        {
          phase: '3-1',
          limit: 5000
        }
      ]
    },
    {
      feed: 'feedb6d8-4728-44f3-bfd4-c4002598ed7b',
      phases: [
        {
          phase: '1-2',
          limit: 5000
        },
        {
          phase: '2-3',
          limit: 5000
        },
        {
          phase: '3-1',
          limit: 5000
        }
      ]
    }
  ],
  redundancyEnabled: true,
  powerDetectionPolling: {
    length: 10000,
    unit: 'MILLISECONDS',
    finite: true
  },
  sensorDataDuration: {
    length: 30000,
    unit: 'MILLISECONDS',
    finite: true
  },
  proactiveTurnoff: false,
  useSoc: false,
  globalSocCutoff: 0.4,
  localSocCutoff: 0.3,
  usePowerUpperBound: true
};

const apiMock = {
  drConfig: {
    get: () => {
      return new Promise((resolve) => {
        resolve(drConfig);
      });
    },
    update: (config) => {
      return new Promise((resolve) => {
        resolve(config);
      });
    }
  }
};

describe('DynamicRedundancyStore Tests', () => {
  beforeEach(() => {
    store = new DynamicRedundancyStore(apiMock);
  });

  afterEach(() => {
    store = null;
  });

  it('should "updateFromJson" and "toJson"', () => {
    expect(store.toJson.redundancyEnabled).toBeFalsy();
    expect(store.toJson.feedLimits.length).toBe(0);
    store.updateFromJson({ wonkles: true });
    expect(store.toJson.redundancyEnabled).toBeFalsy();
    expect(store.toJson.feedLimits.length).toBe(0);
    store.updateFromJson({ redundancyEnabled: true });
    expect(store.toJson.redundancyEnabled).toBeTruthy();
    expect(store.toJson.feedLimits.length).toBe(0);
    store.updateFromJson(drConfig);
    expect(store.toJson.redundancyEnabled).toBeTruthy();
    expect(store.toJson.feedLimits.length).toBe(2);
  });

  it('should "load"', async () => {
    expect(store.toJson.redundancyEnabled).toBeFalsy();
    expect(store.toJson.feedLimits.length).toBe(0);
    let config = await store.load();
    expect(config.redundancyEnabled).toBeTruthy();
    expect(store.enabled).toBeTruthy();
    expect(store.feedLimits.length).toBe(2);
    config = await store.load();
    expect(config.redundancyEnabled).toBeTruthy();
    expect(store.enabled).toBeTruthy();
    expect(store.feedLimits.length).toBe(2);
  });

  it('should "save" from json', async () => {
    expect(store.toJson.redundancyEnabled).toBeFalsy();
    expect(store.toJson.feedLimits.length).toBe(0);
    store.enabled = true;
    store.feedLimits.push({ feedId: 1, limit: 1, phase: 1 });
    store.feedLimits.push({ feedId: 2, limit: 2, phase: 1 });
    expect(store.enabled).toBeTruthy();
    expect(store.feedLimits.length).toBe(2);
    let config = await store.save();
    expect(config.redundancyEnabled).toBeTruthy();
    expect(store.enabled).toBeTruthy();
    expect(store.feedLimits.length).toBe(2);
  });
});
