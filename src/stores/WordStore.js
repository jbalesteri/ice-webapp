import { observable } from 'mobx';
import axios from 'axios';

export default class WordStore {
  @observable _words = {};
  @observable loading = false;
  @observable lang = window.navigator.language;

  constructor () {
    // this store should just autoload its data
    this.fetch();
  }

  translate (word) {
    if (this._words[word]) {
      return this._words[word];
    }
    return word;
  }

  fetch () {
    let url = '';
    let myLanguage = window.navigator.language;// bc - or for testing, set to 'de' for German or 'sp' for Spanish
    switch (myLanguage) {
      case 'en':
      case 'en-US':
        url = '/data/translations/en/translations.json';
        break;
      case 'de':
        url = '/data/translations/de/translations.json';
        break;
      case 'sp':
        url = '/data/translations/sp/translations.json';
        break;
    }

    axios.get(url).then((res) => {
      let words = res.data;

      if (words && words.translations) {
        this._words = words.translations;
      }
    });
  }
}
