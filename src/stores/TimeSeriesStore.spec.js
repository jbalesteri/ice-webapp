import TimeSeriesStore from './TimeSeriesStore';

let store;
let fakeCacheLimit = 100;

describe('TimeSeriesStore Tests', () => {
  beforeEach(() => {
    store = new TimeSeriesStore(fakeCacheLimit);
  });

  afterEach(() => {
    store = null;
  });

  it('should have no data initially', async() => {
    expect(store.data.length).toBe(0);
  });

  it('should update data by id', () => {
    let fakeId = 'xxxx';
    let fakeTimeSeries = [1, 2, 3, 4, 5];
    store.setDataForId(fakeId, fakeTimeSeries);
    let myData = store.getDataForId(fakeId);
    expect(myData[0]).toBe(1);
    let updatedFakeTimeSeries = [5, 4, 3, 2, 1];
    store.setDataForId(fakeId, updatedFakeTimeSeries);
    let myUpdatedData = store.getDataForId(fakeId);
    expect(myUpdatedData[0]).toBe(5);
  });
  it('should add a data point to an object containing a timeseries', () => {
    let fakeId = 'xxxx';
    let fakeData1 = {'name': 'fake1'};
    let fakeData2 = {'name': 'fake2'};
    store.addDataPoint(fakeId, fakeData1);
    let myData = store.getDataForId(fakeId);
    expect(myData[0].name).toBe(fakeData1.name);
    store.addDataPoint(fakeId, fakeData2);
  });

  it('should return the most recently added timeseries object from the currentDataForId method', () => {
    let fakeId = 'xxxx';
    let fakeData1 = {'name': 'fake1'};
    let fakeData2 = {'name': 'fake2'};
    store.addDataPoint(fakeId, fakeData1);
    let myData = store.getDataForId(fakeId);
    expect(myData[0].name).toBe(fakeData1.name);
    store.addDataPoint(fakeId, fakeData2);

    let myReturnedData = store.currentDataForId(fakeId);
    expect(myReturnedData.name).toBe('fake2');
  });

  it('should return null from currentDataForId if there is no series on the associated object', () => {
    let fakeId1 = 'xxxx1';
    let fakeId2 = 'xxxx2';
    let myNonExistentData = store.getDataForId(fakeId1);
    expect(myNonExistentData).toBe(null);
    expect(myNonExistentData).toBe(null);
    store.setDataForId(fakeId1);
    store.addDataPoint(fakeId1);
    store.addDataPoint(fakeId2);
    let myReturnedData = store.currentDataForId(fakeId2);
    expect(myReturnedData).toBe(null);
  });

  it('should not allow series array length to exceed the object pool cache limit', () => {
    let fakeId1 = 'xxxx1';
    let myNonExistentData = store.getDataForId(fakeId1);
    expect(myNonExistentData).toBe(null);
    expect(myNonExistentData).toBe(null);
    store.setDataForId(fakeId1);
    for (var i = 0; i < 200; i += 1) {
      let fakeDataPoint = {data: fakeId1};
      store.addDataPoint(fakeId1, fakeDataPoint);
    }
    let fakeData = store.getDataForId(fakeId1);
    expect(fakeData.length).toBe(fakeCacheLimit);
  });
});
