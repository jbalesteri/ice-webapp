const debug = require('debug')('ice:stores:events'); // eslint-disable-line no-unused-vars

import { computed } from 'mobx';

import IdStore from './IdStore';
import Event from '../models/Event';
import EventTypes from '../common/constants/EventTypes';

export default class EventStore extends IdStore {
  api = null;

  constructor (api) {
    super({
      fetch: api.events.get.bind(api),
      create: api.feeds.create.bind(api),
      update: api.feeds.update.bind(api),
      remove: api.feeds.delete.bind(api)
    });
    this.api = api;
  }

  @computed get all () {
    return this.items.reverse();
  }

  createEvent () {
    let event = new Event(this);
    return event;
  }

  generateFakeEvents () {
    let myEvents = [];
    for (var i = 0; i < 300; i += 1) {
      myEvents.push({
        id: i,
        severity: 'bad',
        summary: 'something bad happened',
        timestamp: '1',
        info: 'nothing to see here',
        type: 'warning'

      });
    }
    return myEvents;
  }

  load () {
    return this._fetchFunction().then((events) => {
      events.forEach((event) => {
        this.updateFromServer(event);
      });
    });
  }

  updateFromServer (json) {
    let event = this.map.get(json.id);
    let newEvent = false;
    if (!event) {
      newEvent = true;
      event = this.createEvent();
    }
    event.updateFromJson(json);
    if (newEvent) {
      this.map.set(json.id, event);
    }
  }

  @computed get criticalCount () {
    return this._filterByType(EventTypes.CRITICAL).length;
  }

  @computed get warningCount () {
    return this._filterByType(EventTypes.WARNING).length;
  }

  @computed get infoCount () {
    return this._filterByType(EventTypes.INFO).length;
  }

  _filterByType (type) {
    return this.items.filter((event) => {
      return event.severity === type;
    });
  }
}
