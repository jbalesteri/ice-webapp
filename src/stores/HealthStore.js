const debug = require('debug')('ice:stores:health'); // eslint-disable-line no-unused-vars

import { observable, computed } from 'mobx';

export default class HealthStore {
  @observable elasticsearch = false;
  @observable mongo = false;
  @observable groupsStatus = [];
  @observable usedDiskSpace = [];
  @observable usedMemory = 0;
  @observable clusterLeader = '';
  @observable clusterNodes = '';
  @observable recentlyActiveDevices = 0;

  api = null;

  constructor (api) {
    this.api = api;
  }

  @computed get toJson () {
    return {
      elasticsearchStatus: { up: this.elasticsearch },
      mongo: { up: this.mongo },
      groupsStatus: this.groupsStatus.slice(),
      usedDiskSpace: this.usedDiskSpace.slice(),
      usedMemory: this.usedMemory,
      clusterLeader: this.clusterLeader,
      clusterNodes: this.clusterNodes,
      recentlyActiveDevices: this.recentlyActiveDevices
    };
  }

  updateFromJson (json) {
    this.elasticsearch = json.elasticsearchStatus.up || false;
    this.mongo = json.mongoStatus.up || false;
    let groupsStatus = Array.isArray(json.groupsStatus) ? json.groupsStatus : [];
    this.groupsStatus.replace(groupsStatus);
    let usedDiskSpace = Array.isArray(json.usedDiskSpace) ? json.usedDiskSpace : [];
    this.usedDiskSpace.replace(usedDiskSpace);
    this.usedMemory = json.usedMemory || 0;
    this.clusterLeader = json.clusterLeader || '';
    this.clusterNodes = json.clusterNodes || '';
    this.recentlyActiveDevices = json.recentlyActiveDevices || 0;
  }

  load () {
    return this.api.health.get().then((health) => {
      this.updateFromJson(health);
      return this.toJson;
    });
  }
}
