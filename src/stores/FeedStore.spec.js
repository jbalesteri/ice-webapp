import FeedStore from './FeedStore';
import Feed, { FeedMetrics } from '../models/Feed';
// import  from '../models/Feed'; // eslint-disable-line no-duplicate-imports

let store;
const feeds = [
  {
    id: 1,
    name: 'Feed A',
    symbol: 'A',
    phase: '3Phase'
  }, {
    id: 3,
    name: 'Feed C',
    symbol: 'C',
    phase: '3Phase'
  }, {
    id: 2,
    name: 'Feed B',
    symbol: 'B',
    phase: '1Phase'
  },
  {
    id: 4,
    name: 'Feed C',
    symbol: 'C',
    phase: '3Phase'
  }
];

let apiMock = {
  feeds: {
    get: () => {
      return new Promise((resolve) => {
        resolve(feeds);
      });
    },

    create: (feed) => {
      return feeds.concat(feed);
    },

    update: (feed) => {
      return feed;
    },

    delete: () => {
      return true;
    }
  }
};

describe('Feedstore Tests', () => {
  beforeEach(() => {
    store = new FeedStore(apiMock);
  });

  afterEach(() => {
    store = null;
  });

  it('should "load" replacing any current data', async() => {
    expect(store.all.length).toBe(0);
    let feeds = await store.load();
    expect(feeds.length).toBe(4);
    expect(store.all.length).toBe(4);
    expect(store.all[0]).toBeInstanceOf(Feed);
    expect(store.all[1]).toBeInstanceOf(Feed);
    expect(store.all[2]).toBeInstanceOf(Feed);
    feeds = await store.load();
    expect(feeds.length).toBe(4);
    expect(store.all.length).toBe(4);
    expect(store.all[0]).toBeInstanceOf(Feed);
    expect(store.all[1]).toBeInstanceOf(Feed);
    expect(store.all[2]).toBeInstanceOf(Feed);
  });

  it('should populate feedMetrics correctly', () => {
    let evtData = [
      {
        allocated1nW: 0,
        allocated2nW: 0,
        drLimitW: 10000,
        feedId: 1,
        maxAllocated1nW: 0,
        maxAllocated2nW: 0,
        phase: '1-2',
        powerW: 12895
      },
      {
        allocated1nW: 0,
        allocated2nW: 0,
        drLimitW: 11000,
        feedId: 2,
        maxAllocated1nW: 0,
        maxAllocated2nW: 0,
        phase: '2-3',
        powerW: 12890
      },
      {
        allocated1nW: 0,
        allocated2nW: 0,
        drLimitW: 12000,
        feedId: 3,
        maxAllocated1nW: 0,
        maxAllocated2nW: 0,
        phase: '3-1',
        powerW: 12891
      },
      {
        allocated1nW: 0,
        allocated2nW: 0,
        drLimitW: 12000,
        feedId: 4,
        maxAllocated1nW: 0,
        maxAllocated2nW: 0,
        phase: '3-1',
        powerW: 12880
      }
    ];
    // let store.create();
    store.updateFromServer(feeds[0]);
    store.updateFromServer(feeds[1]);
    store.updateFromServer(feeds[2]);
    store.updateFromServer(feeds[3]);
    store.onSensorDataReceived(evtData);
    expect(store.all[0].metrics[0]).toBeInstanceOf(FeedMetrics);
  });
});
