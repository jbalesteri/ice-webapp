export default {
  CONNECTION_LOSS: 'ConnectionLossEvent',
  UPS_BELOW_THRESHOLD: 'UPSBelowThresholdEvent',
  NEW_OPTIMIZED_LIMIT: 'NewOptimizedLimitEvent',
  CRITICAL: 'CRITICAL',
  WARNING: 'WARNING',
  INFO: 'INFO'
};
