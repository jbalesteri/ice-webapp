export default {
  RPDU: 'rpdu',
  FPDUS: 'fpdus',
  ICEBLOCK: 'iceblock',
  ICESWITCH: 'iceswitch',
  SENSORS: 'sensors'
};
