let io = require('socket.io-client');

export function init (authConfig) {
  let opts = {
    reconnectionAttempts: 5,
    path: '/socket.io'
  };

  // default to window or localhost:8080 if window doesn't exist
  let socketUrl = (window && window.location && window.location.origin) ? window.location.origin : 'http://localhost:8080';
  // check for some config on window
  if (window && window.__env && window.__env.socket && window.__env.socket.url) {
    socketUrl = window.__env.socket.url;
  }

  this.socket = io.connect(socketUrl, opts);

  // socket sends 'msg' and receives 'evt'
  this.socket.on('connect', onSocketConnect.bind(this));
  this.socket.on('msg', onSocketMessageReceived.bind(this));
  this.id = 'socket';
  this.authConfig = authConfig;
}

export function onSocketConnect () {
  let socketOpts = {
    type: 'authenticate',
    userName: this.authConfig.userName,
    userId: this.authConfig.userId,
    Token: this.authConfig.Token
  };

  this.socket.emit('msg', socketOpts);
}

export function onSocketMessageReceived (msg) {
  this.authConfig.callback(msg);
}
