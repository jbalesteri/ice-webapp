import React from 'react';
import styles from '../../styles/variables';
import { Button } from 'react-toolbox';
const strokeColor = styles['color-blue'];

export class SampleChart extends React.Component {
  render () {
    return (
      <svg width='741px' height='435px' viewBox='50 73 741 435' version='1.1' xmlns='http://www.w3.org/2000/svg'>
          <defs>
              <rect id='path-1' x='50' y='75' width='738' height='433' rx='8'></rect>
              <mask id='mask-2' maskContentUnits='userSpaceOnUse' maskUnits='objectBoundingBox' x='0' y='0' width='738' height='433' fill='white'>
                  <use xlinkHref='#path-1'></use>
              </mask>
          </defs>
          <g id='Group' opacity='0.150000006' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' transform='translate(53.000000, 74.000000)' strokeLinecap='square'>
              <path d='M26.5,0.5 L26.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M56.5,0.5 L56.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M86.5,0.5 L86.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M116.5,0.5 L116.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M146.5,0.5 L146.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M176.5,0.5 L176.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M206.5,0.5 L206.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M236.5,0.5 L236.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M266.5,0.5 L266.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M296.5,0.5 L296.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M326.5,0.5 L326.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M356.5,0.5 L356.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M386.5,0.5 L386.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M416.5,0.5 L416.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M446.5,0.5 L446.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M476.5,0.5 L476.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M506.5,0.5 L506.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M536.5,0.5 L536.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M566.5,0.5 L566.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M596.5,0.5 L596.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M626.5,0.5 L626.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M656.5,0.5 L656.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M686.5,0.5 L686.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M716.5,0.5 L716.5,432.5' id='Line' stroke='#979797'></path>
              <path d='M736.504243,31 L0,31' id='Line' stroke='#979797'></path>
              <path d='M736.504243,61 L0,61' id='Line' stroke='#979797'></path>
              <path d='M736.504243,91 L0,91' id='Line' stroke='#979797'></path>
              <path d='M736.504243,121 L0,121' id='Line' stroke='#979797'></path>
              <path d='M736.504243,151 L0,151' id='Line' stroke='#979797'></path>
              <path d='M736.504243,181 L0,181' id='Line' stroke='#979797'></path>
              <path d='M736.504243,211 L0,211' id='Line' stroke='#979797'></path>
              <path d='M736.504243,241 L0,241' id='Line' stroke='#979797'></path>
              <path d='M736.504243,271 L0,271' id='Line' stroke='#979797'></path>
              <path d='M736.504243,301 L0,301' id='Line' stroke='#979797'></path>
              <path d='M736.504243,331 L0,331' id='Line' stroke='#979797'></path>
              <path d='M736.504243,361 L0,361' id='Line' stroke='#979797'></path>
              <path d='M736.504243,391 L0,391' id='Line' stroke='#979797'></path>
              <path d='M736.504243,421 L0,421' id='Line' stroke='#979797'></path>
          </g>
          <use id='Rectangle' stroke={strokeColor} mask='url(#mask-2)' strokeWidth='4' fill='none' xlinkHref='#path-1'></use>
          <polyline id='Path-2' stroke={strokeColor} strokeWidth='2' fill='none' points='52.7734375 405.820312 98.4069566 265.488281 107.157841 302.730469 131.109481 166.375 144.751485 367.640625 155.506478 224.4375 174.895157 296.652344 204.613005 161.273438 233.194019 453.332031 244.31233 275.953125 276.596844 218.339844 286.316576 256.253906 310.303376 219.589844 353.319443 345.464844 355.968148 257.308594 393.702431 297.972656 409.743115 168.667969 492.884332 342.476562 532.892282 165.070312 553.484208 460.863281 563.446152 297.40625 571.341482 319.980469 577.541327 259.484375 617.353945 337.144531 624.737504 217.21875 670.156158 336.988281 712.562788 168.25 728.044822 246.519531 760.309802 183.355469 787 403.132812'></polyline>
      </svg>
    );
  }
};

export class ButtonGroup extends React.Component {
  render () {
    return (
      <div style={{width: '100%', display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap'}}>
        <Button icon='info' floating mini style={{backgroundColor: styles['color-red']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-pink']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-purple']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-deep-purple']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-indigo']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-light-blue']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-cyan']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-teal']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-green']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-light-green']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-lime']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-yellow']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-amber']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-orange']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-deep-orange']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-brown']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-grey']}} />
        <Button icon='info' floating mini style={{backgroundColor: styles['color-blue-grey']}} />
      </div>
    );
  }
}
