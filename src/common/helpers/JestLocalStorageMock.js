var localStorageMock = (function () {
  var store = {};
  return {
    getItem: function (key) {
      if (store[key]) {
        return store[key];
      } else {
        return undefined;
      }
    },
    setItem: function (key, value) {
      store[key] = value.toString();
    },
    clear: function () {
      store = {};
    }
  };
})();
Object.defineProperty(window, 'localStorage', { value: localStorageMock });
