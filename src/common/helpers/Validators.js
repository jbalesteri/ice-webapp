const debug = require('debug')('ice:validators');

export const ip = (ip) => {
  let v4 = '(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(?:\\.(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])){3}';
  let v6 = '(?:(?:[0-9a-fA-F:]){1,4}(?:(?::(?:[0-9a-fA-F]){1,4}|:)){2,7})+';

  return ip.match(v4) || ip.match(v6);
};

export const port = (port) => {
  return Number.isInteger(port) && port > 0 && port < 65536;
};

export const required = (value) => {
  let string = String(value);
  return (string) && string.trim().length > 0;
};

export const unique = (value, already, original) => {
  if (original && value === original) {
    return true;
  }

  let compare = already.map((item) => (item.toLowerCase()));
  return compare.indexOf(value.toLowerCase()) === -1;
};

export const device = {
  name: (value, names, original) => {
    let error = '';

    if (!required(value)) {
      error = 'Device name is required';
    } else if (!unique(value, names, original)) {
      error = 'Device name is not unique';
    }
    return error;
  },

  ip: (value, port, combos, original, isDumb) => {
    let error = '';

    let changed = value !== original;

    if (!isDumb) {
      if (!required(value)) {
        error = 'Device IP is required';
      } else if (!ip(value)) {
        error = 'Device IP is not valid';
      } else if (changed && port && Array.isArray(combos)) {
        if (combos.indexOf(value + ':' + port) !== -1) {
          error = 'IP and port combination is already in use';
        }
      }
    }

    return error;
  },

  port: (value, ip, combos, original, isDumb) => {
    let error = '';
    let newPort = Number(value);

    let changed = newPort !== original;

    if (!isDumb) {
      if (!required(value)) {
        error = 'Device port is required';
      } else if (!port(newPort)) {
        error = 'Device port is not valid';
      } else if (changed && ip && combos) {
        if (combos.indexOf(ip + ':' + newPort) !== -1) {
          error = 'IP and port combination is already in use';
        }
      }
    }

    return error;
  },

  model: (value) => {
    let error = '';
    if (!required(value)) {
      error = 'Device model is required';
    }

    return error;
  },
  breakerLimit: (value) => {
    let error = '';
    if (!required(value) || isNaN(value)) {
      error = 'Breaker Limit should be a Number';
    }

    return error;
  }
};

export const rack = {
  name: (value, names, original) => {
    let error = '';

    if (!required(value)) {
      error = 'Rack name is required';
    } else if (!unique(value, names, original)) {
      error = 'Rack name is not unique';
    }

    return error;
  },

  limit: (value) => {
    let error = '';

    if (!required(value)) {
      error = 'Rack limit is required';
    } else if (!Number.isInteger(Number(value))) {
      error = 'Rack limit must be an integer';
    }

    return error;
  }
};

export const group = {
  name: (value, names, original) => {
    let error = '';

    if (!required(value)) {
      error = 'Group name is required';
    } else if (!unique(value, names, original)) {
      error = 'Group name is not unique';
    }

    return error;
  }
};

export const feed = {
  name: (value, names, original) => {
    let error = '';

    if (!required(value)) {
      error = 'Feed name is required';
    } else if (!unique(value, names, original)) {
      error = 'Feed name is not unique';
    }

    return error;
  },

  symbol: (value, symbols, original) => {
    let error = '';

    if (!required(value)) {
      error = 'Feed symbol is required';
    } else if (!unique(value, symbols, original)) {
      error = 'Feed symbol is not unique';
    }

    return error;
  }
};

export const zone = {
  name: (value, names, original) => {
    debug('Validators::name==>value:', value);
    debug('Validators::name==>names:', names);
    debug('Validators::name==>original:', original);
    let error = '';

    if (!required(value)) {
      error = 'Zone name is required';
    } else if (!unique(value, names, original)) {
      error = 'Zone name is not unique';
    }

    debug('Validators::name==>returning error:', error);
    return error;
  },

  type: (value) => {
    let error = '';

    if (!required(value)) {
      error = 'Type is required';
    }
    return error;
  },

  allocatedPower: (value) => {
    let error = '';

    if (!required(value)) {
      error = 'Allocated power is a required field';
    } else if (parseInt(value) < 0) {
      error = 'Allocated power should not be a negative number';
    }
    return error;
  }
};

export const regexes = {
  strongPasswordRegex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})/,// eslint-disable-line

  // separate regexes for each pattern used in password matching///
  hasLowerCaseRegex: /^(?=.*[a-z])/,// eslint-disable-line
  hasUpperCaseRegex: /(?=.*[A-Z])/,// eslint-disable-line
  hasNumberRegex: /(?=.*[0-9])/,// eslint-disable-line
  hasSpecialCharacterRegex: /(?=.*[!@#\$%\^&\*])/,// eslint-disable-line
  hasMinimumCharactersRegex: /(?=.{10,})/,// eslint-disable-line
  emailRegex: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,// eslint-disable-line
  phoneNumberRegex: /^\(?[0-9]{3}(\-|\)) ?[0-9]{3}-[0-9]{4}$/,// eslint-disable-line
  phoneNumberRegex2: /^\(?[0-9]{10}$/// eslint-disable-line
};

export default {
  ip,
  port,
  unique,
  required,
  device,
  rack,
  group,
  feed,
  zone,
  regexes
};
