import React from 'react';

const styles = {
  'colorPhase1': '#9E9E9E',
  'colorPhase2': '#FF5722',
  'colorPhase3': '#4FC3F7'
};

export class PhaseBalanceIcon extends React.Component {
  render () {
    return (
      <svg width='20px' height='20px' viewBox='165 10 20 20' version='1.1' xmlns='http://www.w3.org/2000/svg'>
        <g id='Group-36' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' transform='translate(165.000000, 10.000000)'>
            <g id='Page-1'>
                <path d='M10,5 C11.85225,5 13.4635,6.007 14.3275,7.5015 L18.377,4.53725 C16.59275,1.80525 13.51025,0 10,0 C6.48975,0 3.40725,1.80525 1.623,4.53725 L5.6725,7.5015 C6.5365,6.007 8.14775,5 10,5' id='Phase-1' fill={styles.colorPhase1}></path>
                <path d='M5,10 C5,9.0885 5.24725,8.237 5.6725,7.5015 L1.623,4.53725 C0.59775,6.1075 0,7.98275 0,10 C0,15.52725 4.47275,20 10,20 L10,15 C7.23625,15 5,12.76375 5,10' id='Phase-2' fill={styles.colorPhase2}></path>
                <path d='M15,10 C15,12.76375 12.76375,15 10,15 L10,20 C15.52725,20 20,15.52725 20,10 C20,7.98275 19.40225,6.1075 18.377,4.53725 L14.3275,7.5015 C14.75275,8.237 15,9.0885 15,10' id='Phase-3' fill={styles.colorPhase3}></path>
            </g>
        </g>
      </svg>
    );
  }
};

export class DynamicRedundancyIcon extends React.Component {
  render () {
    return (
      <svg width='20px' height='20px' viewBox='195 10 20 20' version='1.1' xmlns='http://www.w3.org/2000/svg'>
        <g id='Group-8' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' transform='translate(195.000000, 10.000000)'>
            <g id='Group-4' fill='#f06292'>
                <g id='Group-50'>
                    <circle id='Oval-2' cx='10' cy='10' r='10'></circle>
                </g>
            </g>
            <path d='M14.625,7 C13.725,7 12.875,7.35 12.26875,7.95625 L10.5,9.525 L9.55,10.3625 L9.55625,10.3625 L7.875,11.85625 C7.475,12.25625 6.94375,12.475 6.375,12.475 C5.20625,12.475 4.25625,11.53125 4.25625,10.3625 C4.25625,9.19375 5.20625,8.25 6.375,8.25 C6.94375,8.25 7.475,8.46875 7.9,8.89375 L8.60625,9.51875 L9.55,8.68125 L8.7625,7.9875 C8.125,7.35 7.275,7 6.375,7 C4.5125,7 3,8.5125 3,10.3625 C3,12.2125 4.5125,13.725 6.375,13.725 C7.275,13.725 8.125,13.375 8.73125,12.76875 L10.5,11.20625 L10.50625,11.2125 L11.45,10.3625 L11.44375,10.3625 L13.125,8.86875 C13.525,8.46875 14.05625,8.25 14.625,8.25 C15.79375,8.25 16.74375,9.19375 16.74375,10.3625 C16.74375,11.53125 15.79375,12.475 14.625,12.475 C14.0625,12.475 13.525,12.25625 13.1,11.83125 L12.3875,11.2 L11.44375,12.0375 L12.2375,12.7375 C12.875,13.36875 13.71875,13.71875 14.625,13.71875 C16.4875,13.71875 18,12.2125 18,10.35625 C18,8.5 16.4875,7 14.625,7 L14.625,7 Z' id='Shape' fill='#424242'></path>
        </g>
      </svg>
    );
  }
};

export class SignwaveIcon extends React.Component {
  render () {
    return (
      <svg width='20px' height='20px' viewBox='225 10 20 20' version='1.1' xmlns='http://www.w3.org/2000/svg'>
        <g id='Group-8' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' transform='translate(225.000000, 10.000000)'>
          <g id='Group-4' fill='#50E3C2'>
              <g id='Group-50'>
                  <circle id='Oval-2' cx='10' cy='10' r='10'></circle>
              </g>
          </g>
          <circle id='Oval-5' fill='#4A4A4A' cx='6.5' cy='6.5' r='1.5'></circle>
          <circle id='Oval-5' fill='#4A4A4A' cx='13.5' cy='13.5' r='1.5'></circle>
          <path d='M6,6 L13.7781746,13.7781746' id='Path-6' stroke='#4A4A4A'></path>
          <path d='M6.53553391,6 L3,9.53553391' id='Path-6' stroke='#4A4A4A'></path>
          <path d='M17.5,9.5 L13.5,13.5' id='Path-6' stroke='#4A4A4A'></path>
        </g>
      </svg>
    );
  }
};

export class Logo extends React.Component {
  render () {
    return (
      <svg width='158px' height='51px' viewBox='262 176 158 51' version='1.1' xmlns='http://www.w3.org/2000/svg'>
          <defs></defs>
          <g id='logo' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' transform='translate(262.000000, 176.000000)'>
              <rect id='Rectangle-path-Copy' fill='#4fc3f7' x='1.93195427' y='21.9103745' width='9.25462256' height='9.29778203'></rect>
              <ellipse id='Oval-Copy-2' fill='#fdd835' cx='6.72600558' cy='44.7527091' rx='5.72600558' ry='5.75270913'></ellipse>
              <ellipse id='Oval-Copy' fill='#ff5722' cx='6.72600558' cy='7.75270913' rx='5.72600558' ry='5.75270913'></ellipse>
              <path d='M38.6854579,1.00374423 L32.7200319,1.00374423 L32.7200319,48.3051912 L38.6854579,48.3051912 L38.6854579,1.00374423 Z M103.360267,3.19943474 C99.7429338,1.00374423 95.4909812,0 91.3024906,0 C77.2139312,0 65.8542369,10.9157185 65.8542369,24.9054038 C65.8542369,38.5814189 77.1504692,49.3089354 90.7947947,49.3089354 C94.9832854,49.3089354 99.6794718,48.1169891 103.360267,45.9840327 L103.360267,39.020557 C99.8063958,42.1572578 95.3005953,43.9138102 90.6044088,43.9138102 C79.9427963,43.9138102 71.819663,35.0683141 71.819663,24.7172017 C71.819663,14.2406213 80.0062583,5.39512525 90.7313328,5.39512525 C95.2371333,5.39512525 99.9333197,7.02620963 103.360267,10.0374423 L103.360267,3.19943474 Z M131.354051,48.3051912 L157.056153,48.3051912 L157.056153,42.9100659 L137.319477,42.9100659 L137.319477,25.0308718 L156.484995,25.0308718 L156.484995,19.6357465 L137.319477,19.6357465 L137.319477,6.39886948 L157.056153,6.39886948 L157.056153,1.00374423 L131.354051,1.00374423 L131.354051,48.3051912 Z' id='ICE' fill='#FFFFFF'></path>
          </g>
      </svg>
    );
  }
}
