import { notify } from 'react-notify-toast';

const NOTIFY_TIMEOUT = 3000;

/*
notify.show(message, type, timeout, color)

let myColor = { background: '#0E1717', text: "#FFFFFF" };
notify.show("this is sample text", "custom", 5000, myColor);

type consists of three variants:

success to render a success notification.
warning to render a warning notification.
error to render an error notification.
custom to render user defined colors for the notification.
*/

export let success = function (message) {
  notify.show(message, 'success', NOTIFY_TIMEOUT);
};

export let info = function (message) {
  notify.show(message, 'custom', NOTIFY_TIMEOUT, { background: '#03A9F4' });
};

export let warn = function (message) {
  notify.show(message, 'warning', NOTIFY_TIMEOUT);
};

export let error = function (message) {
  notify.show(message, 'error', NOTIFY_TIMEOUT);
};

export default {
  success: success,
  info: info,
  warn: warn,
  error: error
};
