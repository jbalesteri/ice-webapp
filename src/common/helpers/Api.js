const debug = require('debug')('ice:helpersApi');

import axios from 'axios';

// Get our env config
export const ApiConfig = (window && window.__env && window.__env.api) ? window.__env.api : {};

// Our Urls
export const URLS = {
  _BASE: ApiConfig.url,
  AUTH: '/sessions',
  CATALOG: '/catalog/device',
  DATACENTER: '/dataCenter',
  DEVICES: '/devices',
  EVENTS: '/events',
  FEEDS: '/feeds',
  GROUPS: '/groups',
  HEALTH: '/health',
  RACKS: '/racks',
  USERS: '/users',
  DRCONFIG: '/dynamicRedundancy',
  ZONES: '/zones',
  CAPACITY_REPORT: '/reports/capacity/zones'
};

class Api {
  constructor () {
    this._token = null;
    this._request = null;

    this._configureRequest();
  }

  get token () {
    return this._token;
  }

  set token (token) {
    this._token = token;
    this._configureRequest();
  }

  _configureRequest () {
    let headers = {'Token': this._token};
    if (!this._request) {
      this._request = axios.create({
        baseURL: URLS._BASE,
        timeout: 30000,
        headers: headers
      });
    } else {
      this._request.defaults.headers = headers;
    }
  }

  // Generic Handlers
  _responseHandler (response) {
    debug('_responseHandler:', response);
    return response.data;
  }

  _errorHandler (error) {
    debug('_errorHandler:', error);
    return error;
  }

  _get (url) {
    return this._request.get(url).then(this._responseHandler);
  }

  _post (url, data) {
    return this._request.post(url, data).then(this._responseHandler);
  }

  _put (url, data) {
    return this._request.put(url, data).then(this._responseHandler);
  }

  _patch (url, data) {
    if (data.id) {
      delete data.id;
    }
    return this._request.patch(url, data).then(this._responseHandler);
  }

  _delete (url) {
    return this._request.delete(url).then(this._responseHandler);
  }

  // Authentication Methods
  auth = {
    post: (username, password) => {
      return this._post(URLS.AUTH, {username: username, password: password});
    }
  }

  // Catalog Methods
  catalog = {
    get: () => {
      return this._get(URLS.CATALOG);
    }
  }

  // Data Center Methods
  dataCenter = {
    get: () => {
      return this._get(URLS.DATACENTER);
    },

    update: (dataCenter) => {
      return this._patch(URLS.DATACENTER, dataCenter);
    },

    metricsAggregateReset: () => {
      return this._patch(URLS.DATACENTER, { metricsAggregateResetTimestamp: 'now' });
    }
  }

  // DR Config Methods
  drConfig = {
    get: () => {
      return this._get(URLS.DRCONFIG);
    },

    update: (config) => {
      let url = URLS.DRCONFIG;
      return this._patch(url, config);
    },

    state: () => {
      let url = URLS.DRCONFIG + '/algorithmState';
      return this._get(url);
    }
  }

  capacityReport = {
    get: () => {
      return this._get(URLS.CAPACITY_REPORT);
    }
  }

  // Devices
  devices = {
    get: () => {
      return this._get(URLS.DEVICES);
    },

    create: (device) => {
      return this._post(URLS.DEVICES, device);
    },

    update: (device) => {
      let url = URLS.DEVICES + '/' + device.id;
      let data = Object.assign({}, device);
      return this._patch(url, data);
    },

    delete: (id) => {
      let url = URLS.DEVICES + '/' + id;
      return this._delete(url);
    }
  }

  // Events
  events = {
    get: () => {
      return this._get(URLS.EVENTS);
    }
  }

  // FEEDS
  feeds = {
    get: () => {
      return this._get(URLS.FEEDS);
    },

    create: (feed) => {
      return this._post(URLS.FEEDS, feed);
    },

    update: (feed) => {
      let url = URLS.FEEDS + '/' + feed.id;
      return this._put(url, feed);
    },

    delete: (id) => {
      let url = URLS.FEEDS + '/' + id;
      return this._delete(url);
    }
  }

  // Groups
  groups = {
    get: () => {
      return this._get(URLS.GROUPS);
    },

    create: (group) => {
      return this._post(URLS.GROUPS, group);
    },

    update: (group) => {
      let url = URLS.GROUPS + '/' + group.id;
      return this._put(url, group);
    },

    delete: (id) => {
      let url = URLS.GROUPS + '/' + id;
      return this._delete(url);
    }
  }

  // Health
  health = {
    get: () => {
      return this._get(URLS.HEALTH);
    }
  }

  // Racks
  racks = {
    get: () => {
      return this._get(URLS.RACKS);
    },

    create: (rack) => {
      return this._post(URLS.RACKS, rack);
    },

    update: (rack) => {
      let url = URLS.RACKS + '/' + rack.id;
      return this._put(url, rack);
    },

    delete: (id) => {
      let url = URLS.RACKS + '/' + id;
      return this._delete(url);
    }
  }

  // Users
  users = {
    get: () => {
      return this._get(URLS.USERS);
    },

    getUser: (id) => {
      let url = URLS.USERS + '/' + id;
      return this._get(url);
    },

    create: (user) => {
      return this._post(URLS.USERS, user);
    },

    update: (user) => {
      let url = URLS.USERS + '/' + user.id;
      return this._patch(url, user);
    },

    delete: (id) => {
      let url = URLS.USERS + '/' + id;
      return this._delete(url);
    },

    changePassword: (id, current, password) => {
      let url = URLS.USERS + '/' + id;
      let data = { oldpassword: current, password: password };
      return this._patch(url, data);
    },

    adminPassword: (id, password) => {
      let url = URLS.USERS + '/adminPassword/' + id;
      return this._patch(url, { password: password });
    }
  }

  zones = {
    get: () => {
      return this._get(URLS.ZONES);
    },

    create: (zone) => {
      return this._post(URLS.ZONES, zone);
    },

    update: (zone) => {
      let url = URLS.ZONES + '/' + zone.id;
      return this._patch(url, zone);
    },

    delete: (id) => {
      let url = URLS.ZONES + '/' + id;
      return this._delete(url);
    },

    sensorSummary: (id, period, interval) => {
      period = period || '24h';
      interval = interval || '5m';
      let url = `${URLS.ZONES}/${id}/sensorSummary?period=${period}&interval=${interval}`;

      return this._get(url);
    }
  }
}

export const api = new Api();
