var debug = require('debug')('ice:Settings'); // eslint-disable-line no-unused-vars

import React from 'react';

import DataCenterForm from '../../components/DataCenterForm/DataCenterForm';

import SettingsTabDR from '../../components/SettingsTabDR/SettingsTabDR';

import SubNav from '../../components/SubNav/SubNav';

export default class DataCenter extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      index: 0
    };
  }

  handleTabChange = (index) => {
    this.setState({index});
  };

  render () {
    return (
      <div>
        <SubNav title='Data Center' />
        <DataCenterForm />
        <SettingsTabDR />
      </div>
    );
  }
}
