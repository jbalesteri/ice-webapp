var debug = require('debug')('ice: Users'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
// import { toJS } from 'mobx';

import { Card } from 'react-toolbox';
import SubNav from '../../components/SubNav/SubNav';

import { UserActivity, UserForm, UserPasswordUpdate, UsersList } from '../../components/Users';

import Notify from '../../common/helpers/Notify';

@inject('store') @observer
export default class Users extends React.Component {
  constructor (props) {
    super(props);

    this.userStore = this.props.store.userStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      addEditDialogActive: false,
      activityDialogActive: false,
      passwordUpdateDialogActive: false,
      editId: null };
  }

  toggleFormDialog = () => {
    let newState = { addEditDialogActive: !this.state.addEditDialogActive };

    if (this.state.addEditDialogActive && this.state.editId) {
      newState.editId = null;
    }
    this.setState(newState);
  }

  add = () => {
    this.setState({
      addEditDialogActive: true,
      activityDialogActive: false,
      passwordUpdateDialogActive: false,
      editId: '',
      editUser: null
    });
  }

  edit = (user) => {
    this.setState({
      addEditDialogActive: true,
      activityDialogActive: false,
      passwordUpdateDialogActive: false,
      editId: user.id,
      editUser: user
    });
  }

  updatePassword = (user) => {
    this.setState({
      addEditDialogActive: false,
      activityDialogActive: false,
      passwordUpdateDialogActive: true,
      updatePasswordId: user.id,
      editUser: user
    });
  }

  activity = (user) => {
    this.setState({
      addEditDialogActive: false,
      activityDialogActive: true,
      passwordUpdateDialogActive: false,
      activityId: user.id
    });
  }

  remove = (user) => {
    this.userStore.remove(user).then(() => {
      Notify.success('User removed');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error removing user');
      }
    });
  }

  renderUsers () {
    return <UsersList
      random={Math.random()}
      onActivity={this.activity}
      onEdit={this.edit}
      onRemove={this.remove}
      onUpdatePassword={this.updatePassword} />;
  }

  addEditChangeHandler () {
    // console.log('addEditChangeHandler()==>evt:', evt);
  }

  onClickCloseActivityDialog () {
    this.setState({
      activityDialogActive: false
    });
  }

  handleCancelAddEdit () {
    this.setState({
      addEditDialogActive: false
    });
  }

  handleSaveAddEdit () {
    this.userStore.load().then(() => {
      this.setState({
        addEditDialogActive: false
      });
    });
  }

  handleCancelActivity () {
    this.setState({
      activityDialogActive: false
    });
  }

  handleCancelUpdateUserPassword () {
    this.setState({
      passwordUpdateDialogActive: false
    });
  }

  handleSaveUpdateUserPassword () {
    this.userStore.fetch().then(() => {
      this.setState({
        passwordUpdateDialogActive: false
      });
    });
  }

  render () {
    let userForm = null;
    let activityForm = null;
    let passwordUpdateForm = null;
    let addEditProps = null;
    let activityProps = null;
    let passwordUpdateProps = null;

    if (this.state.addEditDialogActive) {
      addEditProps = {
        changeHandler: this.addEditChangeHandler.bind(this),
        editId: this.state.editId,
        editUser: this.state.editUser,
        onCancel: this.handleCancelAddEdit.bind(this),
        title: (this.state.editId && this.state.editId.length > 0) ? this.wordStore.translate('Edit User') : this.wordStore.translate('Add User'),
        onSave: this.handleSaveAddEdit.bind(this)
      };

      userForm = <UserForm active={this.state.addEditDialogActive} {...addEditProps} />;
    }

    if (this.state.activityDialogActive) {
      activityProps = {
        active: this.state.activityDialogActive,
        userId: this.state.activityId,
        onCancel: this.handleCancelActivity.bind(this),
        onEditUser: this.edit.bind(this)

      };
      activityForm = <UserActivity active={this.state.activityDialogActive} {...activityProps}/>;
    }

    if (this.state.passwordUpdateDialogActive) {
      passwordUpdateProps = {
        changeHandler: this.addEditChangeHandler.bind(this),
        updatePasswordId: this.state.updatePasswordId,
        title: this.wordStore.translate('Update User Password'),
        onCancel: this.handleCancelUpdateUserPassword.bind(this),
        onSave: this.handleSaveUpdateUserPassword.bind(this)
      };

      passwordUpdateForm = <UserPasswordUpdate active={this.state.passwordUpdateDialogActive} {...passwordUpdateProps} />;
    }

    let subnavButtons = [
      {
        icon: 'add',
        tooltip: 'Add User',
        onClick: this.add.bind(this)
      }
    ];

    return (
      <div>
        <SubNav title='Users' buttons={subnavButtons}/>
          <Card raised style={{width: '90%', margin: '5rem auto'}}>
            {this.renderUsers()}
          </Card>
          {userForm}
          {activityForm}
          {passwordUpdateForm}
      </div>
    );
  }
}
