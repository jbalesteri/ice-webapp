import React from 'react';
import { inject, observer } from 'mobx-react';

@inject('store') @observer
export default class Logout extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.authStore;
  }

  componentWillMount () {
    this.store.signout();
  }
  componentDidMount () {
    this.props.store.history.push('/');
  }

  render () {
    return null;
  }
}
