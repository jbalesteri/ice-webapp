const debug = require('debug')('ice:Dashboard:'); // eslint-disable-line no-unused-vars
import React from 'react';
import { inject, observer } from 'mobx-react';

import MonitoringEventCard from '../../components/MonitoringEventCard/MonitoringEventCard';

import { GroupCard } from '../../components/Groups';
import { ZoneCard } from '../../components/Zones';

import { FeedCard } from '../../components/Feeds';
let styles = require('./Main.css');

@inject('store') @observer
export default class Main extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
    this.feedStore = this.props.store.feedStore;
    this.sensorDataStore = this.props.store.sensorDataStore;
  }

  renderGroupCards () {
    let groupCards = this.store.groupStore.all.map((group) => {
      return (
        <div key={group.id} className={styles.groupCardWrapper}>
          <GroupCard className={styles.groupCard} group={group} />
        </div>
      );
    });
    return groupCards;
  }

  renderFeedCards () {
    let feedCards = this.store.feedStore.all.map((feed) => {
      return (
        <div key={feed.id} className={styles.feedCardWrapper}>
          <FeedCard className={styles.feedCard} feed={feed} />
        </div>
      );
    });
    return feedCards;
  }

  renderZoneCards () {
    let zoneCards = this.store.zoneStore.all.map((zone) => {
      return (
        <div key={zone.id + zone.name} className={styles.zoneCardWrapper}>
          <ZoneCard zone={zone} />
        </div>
      );
    });
    return zoneCards;
  }

  render () {
    let groupCount = this.store.groupStore.all.length || 0;
    let zoneCount = this.store.zoneStore.all.length || 0;
    let feedCount = this.store.feedStore.all.length || 0;

    return (
      <div>
        <div className={styles.dashboard} >
          <div className={styles.columns}>
            <div className={styles.dashboardColOne}>
              <div className={styles.zoneSection}>
                <h4 className={styles.sectionHead}>
                  {this.wordStore.translate('Rows')}
                  <span className={styles.sectionCount}>({zoneCount})</span>
                </h4>
                <div className={styles.zones} >
                  {this.renderZoneCards()}
                </div>
              </div>
            </div>
            <div className={styles.dashboardColTwo}>
              <div className={styles.feedSection}>
                <h4 className={styles.sectionHead}>
                  {this.wordStore.translate('Feeds')}
                  <span className={styles.sectionCount}>({feedCount})</span>
                </h4>
                <div className={styles.groups}>
                  {this.renderFeedCards()}
                </div>
              </div>
              <div className={styles.groupSection}>
                <h4 className={styles.sectionHead}>
                  {this.wordStore.translate('Groups')}
                  <span className={styles.sectionCount}>({groupCount})</span>
                </h4>
                <div className={styles.groups}>
                  {this.renderGroupCards()}
                </div>
              </div>
              <div className={styles.healthSection}>
                <h4 className={styles.sectionHead}>
                  {this.wordStore.translate('Health')}
                </h4>
                <div className={styles.eventCardWrapper}>
                  <MonitoringEventCard />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
