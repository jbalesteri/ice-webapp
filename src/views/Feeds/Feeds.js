import React from 'react';
import { inject, observer } from 'mobx-react';
import { Card } from 'react-toolbox';
import SubNav from '../../components/SubNav/SubNav';
import { FeedsList, FeedForm } from '../../components/Feeds';
import Notify from '../../common/helpers/Notify';

@inject('store') @observer
export default class Feeds extends React.Component {
  constructor (props) {
    super(props);

    this.feedStore = this.props.store.feedStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      formDialogActive: false,
      editId: null
    };
  }

  toggleFormDialog = () => {
    let newState = { formDialogActive: !this.state.formDialogActive };

    if (this.state.formDialogActive && this.state.editId) {
      newState.editId = null;
    }

    this.setState(newState);
  }

  edit = (feed) => {
    if (feed && feed.id) {
      this.setState({ ...this.state, editId: feed.id }, () => {
        this.toggleFormDialog();
      });
    }
  }

  remove = (feed) => {
    this.feedStore.remove(feed).then(() => {
      Notify.success('Feed removed');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error removing feed');
      }
    });
  }

  render () {
    let subnavButtons = [
      {
        icon: 'add',
        tooltip: 'Add Feed',
        onClick: this.toggleFormDialog.bind(this)
      }
    ];

    return (
      <div>
        <SubNav title='Feeds' buttons={subnavButtons}/>
        <Card raised style={{width: '90%', margin: '5rem auto'}}>
          <FeedsList onEdit={this.edit} onRemove={this.remove} />
        </Card>
        <FeedForm
          active={this.state.formDialogActive}
          onSave={this.toggleFormDialog}
          onCancel={this.toggleFormDialog}
          feedId={this.state.editId} />
      </div>
    );
  }
}
