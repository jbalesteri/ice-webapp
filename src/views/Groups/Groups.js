var debug = require('debug')('ice:Groups'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card } from 'react-toolbox';

import SubNav from '../../components/SubNav/SubNav';
import { GroupsList, GroupForm, GroupBuilder } from '../../components/Groups';
import Notify from '../../common/helpers/Notify';

@inject('store') @observer
export default class Groups extends React.Component {
  constructor (props) {
    super(props);

    this.groupStore = this.props.store.groupStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      formDialogActive: false,
      builderDialogActive: false,
      editId: null,
      selectedGroupId: null
    };
  }

  toggleConfigDialog = () => {
    let newState = {
      builderDialogActive: !this.state.builderDialogActive
    };

    if (!newState.builderDialogActive) {
      newState.selectedGroupId = null;
    }

    this.setState(newState);
  }

  config = (config) => {
    debug('Configure Group:', config);
    if (config && config.id) {
      this.setState({ builderDialogActive: true, selectedGroupId: config.id });
    }
  }

  toggleFormDrawer = () => {
    let newState = { formDialogActive: !this.state.formDialogActive };

    if (this.state.formDialogActive && this.state.editId) {
      newState.editId = null;
    }

    this.setState(newState);
  }

  edit = (group) => {
    if (group && group.id) {
      this.setState({ editId: group.id }, () => {
        this.toggleFormDrawer();
      });
    }
  }

  remove = (group) => {
    this.groupStore.remove(group).then(() => {
      Notify.success('Group removed');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error removing group');
      }
    });
  }

  render () {
    let subnavButtons = [
      {
        icon: 'add',
        tooltip: 'Add User',
        onClick: this.toggleFormDrawer
      }
    ];

    return (
      <div>
        <SubNav title='Groups' buttons={subnavButtons}/>
        <Card raised style={{width: '90%', margin: '5rem auto'}}>
          <GroupsList onEdit={this.edit} onConfig={this.config} onRemove={this.remove} />
        </Card>
        <GroupForm
          active={this.state.formDialogActive}
          onSave={this.toggleFormDrawer}
          onCancel={this.toggleFormDrawer}
          groupId={this.state.editId} />
        <GroupBuilder
          groupId={this.state.selectedGroupId}
          active={this.state.builderDialogActive}
          onSave={this.toggleConfigDialog}
          onCancel={this.toggleConfigDialog} />
      </div>
    );
  }
}

// Groups.wrappedComponent.contextTypes = {
//   router: React.PropTypes.object.isRequired
// };
