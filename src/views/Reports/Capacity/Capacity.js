const debug = require('debug')('ice:reports:capacity'); // eslint-disable-line no-unused-vars

import React, { Component } from 'react';
import { Card, Dropdown } from 'react-toolbox';

import { api } from '../../../common/helpers/Api';
import ZoneCapacity from '../../../components/Reports/Capacity/ZoneCapacity';
import TrendChart from '../../../components/Dataviz/TrendChart';
import SubNav from '../../../components/SubNav/SubNav';

const timeRangeConstants = {
  'day': {
    range: '24',
    interval: '5'
  },
  'week': {
    range: '168',
    interval: '60'
  },
  'month': {
    range: '720',
    interval: '60'
  },
  '3months': {
    range: '2160',
    interval: '60'
  }
};
const timeRangeOptions = [
    {value: 'day', label: '1 day (5m interval)'},
    {value: 'week', label: '1 week (1h interval)'},
    {value: 'month', label: '1 month (1h interval)'},
    {value: '3months', label: '3 months (1h interval)'}
];
export default class Capacity extends Component {
  constructor (props) {
    super(props);
    this.state = {
      chartData: null,
      timeSelected: 'day', // Default range
      loading: null
    };
  }

  componentDidMount () {
    this.loadReport(this.props.params.id, this.state.timeSelected);
  }

  handleTimeChange = (valSelected) => {
    this.loadReport(this.props.params.id, valSelected);
  };

  //  timerange in hours and interval in min.
  loadReport (id, rangeSelected) {
    let timerangeObj = timeRangeConstants[rangeSelected];
    let timerange = timerangeObj.range;
    let interval = timerangeObj.interval;
    return api.zones.sensorSummary(id, timerange + 'h', interval + 'm').then((report) => {
      debug('got report!', report);
      let chartData = [].concat(report);
      if (chartData.length === 0) {
        // we have no chart data, leave null and show alternate view
        return;
      }

      let emptyDataPoint = {
        'outputPowerW': 0,
        'battAvgSocProp': 0,
        'powerLimitW': 0,
        'outputPower1W': 0,
        'outputPower2W': 0,
        'outputPower3W': 0
      };

      let timeStep = interval * 60 * 1000;  // in milliseconds
      let noOfDataPoints = (timerange * 60) / interval; // e.g. 288 data points for 24 hrs with 5 min. interval. (24 * 60) / 5
      if (chartData.length < noOfDataPoints) {
        while (chartData.length < noOfDataPoints) {
          let timestamp = chartData[0].timestamp - timeStep;
          let dataPoint = Object.assign({}, emptyDataPoint);
          dataPoint.timestamp = timestamp;
          chartData.unshift(dataPoint);
        }
      }
      debug('Chart Data:', chartData);
      this.setState({
        timeSelected: rangeSelected,
        chartData: chartData
      });
    }).catch((error) => {
      debug('error getting report:', error);
    });
  }

  renderChart () {
    if (this.state.loading) {
      return <div>Loading.</div>;
    } else if (this.state.chartData && this.state.chartData.length && this.state.chartData.length > 0) {
      return <TrendChart sensorSummaryData={this.state.chartData} />;
    } else {
      return <div>No Trend Data Found</div>;
    }
  }

  render () {
    debug('Capacity:', this.props);
    return (
      <div>
        <SubNav title='Zone Capacity Report' />
        <div style={{margin: '10px'}}>
          <Card style={{width: '90%', margin: '0 auto'}}>
            <ZoneCapacity zoneId={this.props.params.id} />
            <div style={{padding: '10px'}}>
              <Dropdown
                onChange={this.handleTimeChange.bind(this)}
                source={timeRangeOptions}
                value={this.state.timeSelected}
                label='Pick a time range'
              />
              {this.renderChart()}
            </div>
          </Card>
        </div>
      </div>
    );
  }
}
