var debug = require('debug')('ice:Account: '); // eslint-disable-line no-unused-vars

import React from 'react';

import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Card, CardTitle, CardText } from 'react-toolbox';

import AccountForm from '../../components/AccountForm/AccountForm';
import { ChangePasswordForm } from '../../components/Account';
import SubNav from '../../components/SubNav/SubNav';

@inject('store') @observer
export default class Account extends React.Component {
  constructor (props) {
    super(props);

    this.authStore = this.props.store.authStore;
    this.userStore = this.props.store.userStore;
    this.wordStore = this.props.store.wordStore;
  }

  render () {
    let myUserId = this.authStore.authStore.userId;
    let myUsers = toJS(this.userStore.all);
    let myUser = null;
    myUsers.forEach(function (user) {
      if (user.id === myUserId) {
        myUser = user;
      }
    });

    let accountFormProps = {
      store: this.props.store,
      user: myUser,
      editId: myUserId
    };

    return (
      <div>
        <SubNav title='My Account' />
        <Card raised style={{width: '90%', margin: '5rem auto'}}>
          <CardTitle title='Account Information' />
          <CardText data-qa-tag='account-info'>
            <AccountForm {...accountFormProps} />
          </CardText>
        </Card>
        <Card raised style={{width: '90%', margin: '5rem auto'}}>
          <CardTitle title='Change Password' />
          <CardText>
            <ChangePasswordForm />
          </CardText>
        </Card>
      </div>

    );
  }
}
