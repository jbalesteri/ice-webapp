const debug = require('debug')('ice:RacksView'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card } from 'react-toolbox';
import SubNav from '../../components/SubNav/SubNav';
import { RacksList, RackBuilder, RackForm } from '../../components/Racks';
import Notify from '../../common/helpers/Notify';

@inject('store') @observer
export default class Racks extends React.Component {
  constructor (props) {
    super(props);
    this.rackStore = this.props.store.rackStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      formDialogActive: false,
      builderDialogActive: false,
      selectedRackId: null
    };
  }

  toggleAddForm = () => {
    let newState = { formDialogActive: !this.state.formDialogActive };

    if (this.state.formDialogActive && this.state.selectedRackId) {
      newState.selectedRackId = null;
    }
    this.setState(newState);
  }

  toggleConfigDialog = () => {
    let newState = { builderDialogActive: !this.state.builderDialogActive };
    if (!newState.builderDialogActive) {
      newState.selectedRackId = null;
    }
    this.setState(newState);
  }

  edit = (edit) => {
    debug('Edit Racks:', edit);
    if (edit && edit.id) {
      this.setState({ selectedRackId: edit.id, formDialogActive: true });
    }
  }

  config = (rack) => {
    this.setState({ selectedRackId: rack.id, builderDialogActive: true });
  }

  remove = (rack) => {
    debug('Remove Rack:', rack);
    this.rackStore.remove(rack).then(() => {
      Notify.success('Rack removed');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error removing rack');
      }
    });
  }

  render () {
    let subnavButtons = [
      {
        icon: 'add',
        tooltip: 'Add User',
        onClick: this.toggleAddForm.bind(this)
      }
    ];

    return (
      <div>
        <SubNav title='Racks' buttons={subnavButtons}/>
        <Card raised style={{width: '90%', margin: '5rem auto'}}>
          <RacksList onEdit={this.edit} onConfig={this.config} onRemove={this.remove} />
        </Card>
        <RackForm
          active={this.state.formDialogActive}
          onSave={this.toggleAddForm}
          onCancel={this.toggleAddForm}
          rackId={this.state.selectedRackId} />
        <RackBuilder
          rackId={this.state.selectedRackId}
          active={this.state.builderDialogActive}
          onSave={this.toggleConfigDialog.bind(this)}
          onCancel={this.toggleConfigDialog.bind(this)}
          onEscKeyDown={this.toggleConfigDialog.bind(this)} />
      </div>
    );
  }
}
