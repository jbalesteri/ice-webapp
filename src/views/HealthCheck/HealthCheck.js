var debug = require('debug')('ice: HealthCheck'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card } from 'react-toolbox';
import SubNav from '../../components/SubNav/SubNav';
import ValueWrapper from '../../components/common/ValueWrapper/ValueWrapper';

const styles = require('./HealthCheck.css');

@inject('store') @observer
export default class HealthCheck extends React.Component {
  constructor (props) {
    super(props);
    this.auth = this.props.store.authStore;
    this.store = this.props.store.healthStore;
  }

  componentDidMount () {
    this.store.load();
  }

  getGroups () {
    let groups = this.store.groupsStatus.map((group) => {
      return {
        name: group.name,
        numberOfConnectedDevices: group.numberOfConnectedDevices,
        totalNumberOfDevices: group.totalNumberOfDevices,
        summaryDataExists: group.summaryDataExists,
        recentDataExists: group.recentDataExists
      };
    });

    if (groups.length === 0) {
      return <p>No groups are present</p>;
    } else {
      return groups.map((grp, index) => {
        debug('grp; grp.summaryDataExists; grp.recentDataExists', grp + ' : ' + grp.summaryDataExists + ' : ' + grp.recentDataExists);
        return (
          <div key={index} className={styles.group}>
            <div className={styles.setWidth}>
              <ValueWrapper
                label='Group Name:'
                value={grp.name}
                size='med'
                inline={true}
                formatNumber={false} />
              <ValueWrapper
                label='Connected Devices:'
                value={grp.numberOfConnectedDevices}
                size='sm'
                inline={true}
                formatNumber={false} />
              <ValueWrapper
                label='Summary Data Exists:'
                value={JSON.stringify(grp.summaryDataExists)}
                size='sm'
                inline={true}
                formatNumber={false} />
              <ValueWrapper
                label='Recent Data Exists:'
                value={JSON.stringify(grp.recentDataExists)}
                size='sm'
                inline={true}
                formatNumber={false} />
            </div>
        </div>
        );
      });
    }
  }

  render () {
    let esStatus = (this.store.elasticsearch) ? 'Up' : 'Down';
    let mongoStatus = (this.store.mongo) ? 'Up' : 'Down';
    let usedDisk = this.store.usedDiskSpace || 'Disk Usage Unvailable';
    let usedMem = this.store.usedMemory || 'Memory Usage Unavailable';
    let leader = this.store.clusterLeader || 'Not Available';
    let nodes = this.store.clusterNodes || 'No Nodes';
    let recentlyActive = this.store.recentlyActiveDevices;
    return (
      <div>
        <div className={styles.healthCheck}>
          <SubNav title='Health Check' />
          <Card raised style={{width: '90%', margin: '5rem auto', padding: '2rem'}}>
            <div className={styles.pageSection}>
              <h5>Processes:</h5>
                <ValueWrapper
                  label='ES Status:'
                  value={esStatus}
                  size='med'
                  inline={true}
                  formatNumber={false} />
                <ValueWrapper
                  label='Mongo DB Status:'
                  value={mongoStatus}
                  size='med'
                  inline={true}
                  formatNumber={false} />
            </div>
            <div className={styles.groupSection}>
              <h5>Groups:</h5>
              <div className={styles.groupsWrapper}>
                {this.getGroups()}
              </div>
            </div>
            <div className={styles.pageSection}>
              <h5>Memory:</h5>
              <ValueWrapper
                label='Used Disk Space:'
                value={(Number(usedDisk) * 100)}
                suffix='%'
                size='med'
                inline={true}
                formatNumber={true} />
              <ValueWrapper
                label='Used Memory:'
                value={(Number(usedMem) * 100)}
                suffix='%'
                size='med'
                inline={true}
                formatNumber={true} />
            </div>
            <div className={styles.pageSection}>
              <h5>Additional Data:</h5>
                <ValueWrapper
                  label='Cluster Leader:'
                  value={leader}
                  size='med'
                  inline={true}
                  formatNumber={false} />
                <ValueWrapper
                  label='Cluster Nodes:'
                  value={nodes}
                  size='med'
                  inline={true}
                  formatNumber={false} />
                <ValueWrapper
                  label='Recently Active Devices:'
                  value={recentlyActive}
                  size='med'
                  inline={true}
                  formatNumber={false} />
            </div>
          </Card>
        </div>
      </div>
    );
  }
}
