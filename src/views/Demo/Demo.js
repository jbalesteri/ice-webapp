var debug = require('debug')('ice: Demo'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Card, CardText, Dropdown } from 'react-toolbox';
import TrendChart from '../../components/Dataviz/TrendChart';
import TrendChartData from '../../components/Dataviz/TrendChartData';
import PhaseBalance from '../../components/charts/PhaseBalance/PhaseBalance';
import {Accordion, CollapsingPanel} from '../../components/Accordion';
import {ButtonGroup} from '../../common/helpers/SampleGraphics';
import SubNav from '../../components/SubNav/SubNav';

@inject('store') @observer
export default class Support extends React.Component {
  timeRangeOptions = [
      {value: 'd', label: '1 day'},
      {value: 'wk', label: '1 week'},
      {value: 'mth', label: '1 month'},
      {value: '24h|4m', label: '1 qtr'}
  ];

  constructor (props) {
    super(props);

    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;

    this.phaseBalanceTicker = null;
    this.chartData = TrendChartData.sensorSummaryDataSec();
    this.state = {
      phase1: 33,
      phase2: 33,
      phase3: 33,
      timeSelected: 'd',
      chartData: TrendChartData.sensorSummaryDataSec()
    };
  }

  componentDidMount () {
    this.phaseBalanceTicker = setInterval(() => {
      this.setState({
        phase1: Math.random(),
        phase2: Math.random(),
        phase3: Math.random()
      });
    }, 1000);
  }

  componentWillUnmount () {
    clearInterval(this.phaseBalanceTicker);
  }

  handleTimeChange = (value) => {
    switch (value) {
      case 'd':
        this.chartData = TrendChartData.sensorSummaryDataSec();
        break;
      case 'wk':
        this.chartData = TrendChartData.sensorSummaryDataMin();
        break;
      case 'mth':
        this.chartData = TrendChartData.sensorSummaryData3Day();
        break;
      default:
        this.chartData = TrendChartData.sensorSummaryDataSec();
    }
    this.setState({
      timeSelected: value,
      chartData: this.chartData
    });
  };

  render () {
    let metrics = (this.store.zoneStore.all.length > 0) ? this.store.zoneStore.all[0].metrics : { phasePowerIn: [] };
    let [phase1, phase2, phase3] = metrics.phasePowerIn.map((phase) => (phase.instantaneousW));

    return (
      <div>
        <SubNav title='Demo' />
        <Card raised style={{width: '90%', margin: '2rem auto'}}>
          <div style={{width: '10rem'}}>
            <Dropdown
              source={this.timeRangeOptions}
              onChange={this.handleTimeChange.bind(this)}
              value={this.state.timeSelected}
              label='Pick a time range'
            />
          </div>
           <TrendChart sensorSummaryData={this.state.chartData} />
        </Card>
        <Card raised style={{width: '90%', margin: '2rem auto'}}>
          <CardText style={{display: 'flex', justifyContent: 'space-between'}}>
            <Accordion>
              <CollapsingPanel title='CollapsingPanel Title One'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet nemo harum voluptas aliquid rem possimus nostrum excepturi!</CollapsingPanel>
              <CollapsingPanel title='CollapsingPanel Title Two'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet nemo harum voluptas aliquid rem possimus nostrum excepturi!</CollapsingPanel>
              <CollapsingPanel title='CollapsingPanel Title Three'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet nemo harum voluptas aliquid rem possimus nostrum excepturi!</CollapsingPanel>
            </Accordion>
          </CardText>
        </Card>
        <Card raised style={{width: '90%', margin: '2rem auto'}}>
          <CardText style={{display: 'flex', justifyContent: 'space-between'}}>
            <ButtonGroup />
          </CardText>
        </Card>
        <Card raised style={{width: '90%', margin: '2rem auto'}}>
          <h2>Phase Balance Chart</h2>
          <div style={{width: '100px', height: '100px', margin: '20px'}}>
            <PhaseBalance phase1={phase1} phase2={phase2} phase3={phase3} />
          </div>
        </Card>
      </div>
    );
  }
}
