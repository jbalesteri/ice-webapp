var debug = require('debug')('ice: HealthCheck'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card, CardTitle, CardText, Input } from 'react-toolbox';

import PageChrome from '../../components/PageChrome/PageChrome';

const styles = require('./ForgotPassword.css');

@inject('store') @observer
export default class ForgotPassword extends React.Component {
  constructor (props) {
    super(props);
    this.auth = this.props.store.authStore;

    this.state = {
      email: ''
    };
  }

  handleChange = (name, value) => {
    this.setState({...this.state, [name]: value});
  };

  render () {
    let hideNav = !this.auth.isAuthenticated;

    return (
      <PageChrome hideNav={hideNav}>
        <section>
          <h1 className={styles.pageHeading} >Forgot Password</h1>
          <Card className={styles.forgotPasswordContent} raised >
            <CardTitle
              title='Plese input [username/email]' />
            <CardText>
              <Input
                type='email'
                name='email'
                label='Email'
                value={this.state.email}
                onChange={this.handleChange.bind(this, 'email')} />
            </CardText>
          </Card>
        </section>
      </PageChrome>
    );
  }
}
