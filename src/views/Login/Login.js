const debug = require('debug')('ice:LoginView');

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Button, Card, CardMedia, CardText, CardActions, Input } from 'react-toolbox';

import Notify from '../../common/helpers/Notify';

const logo = require('./assets/logo-ice-signin.svg');
const styles = require('./Login.css');

@inject('store') @observer
export default class Login extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.authStore;
    this.messageStore = this.props.store.messageStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      username: '',
      password: ''
    };
  }

  doLogin = () => {
    this.store.authenticate(this.state.username, this.state.password)
      .then((loggedIn) => {
        if (loggedIn) {
          Notify.success('Login Successful');
          this.props.store.history.push('/');
        } else {
          this.messageStore.addMessage({ text: 'Login Failed', icon: 'error_outline' });
        }
      }).catch((error) => {
        debug('Login Failed:', error);
        Notify.error('Login Failed');
      });
  }

  formHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.doLogin();
  }

  handleUserChange = (value) => {
    this.setState({ username: value });
  }

  handlePasswordChange = (value) => {
    this.setState({ password: value });
  }

  resetPasswordHandler () {
    this.setState({});
  }

  render () {
    if (this.store.isAuthenticated) {
      return null;
    } else {
      return (
        <section className={styles.loginWrapper}>
          <Card raised>
            <CardMedia color='#1B1B1B'>
              <img src={logo} className={styles.logo} />
            </CardMedia>
            <form onSubmit={this.formHandler}>
              <CardText>
                <Input type='email' label={this.wordStore.translate('Email address')} icon='email' value={this.state.username} onChange={this.handleUserChange}/>
                <Input type='password' label={this.wordStore.translate('Password')} icon='lock' value={this.state.password} onChange={this.handlePasswordChange}/>
              </CardText>
              <CardActions className={styles.loginCardActions}>
                <Button type='submit' icon='open_in_browser' label={this.wordStore.translate('Login')} raised primary />
              </CardActions>
            </form>
          </Card>
        </section>
      );
    }
  }
}
