const debug = require('debug')('ice:views:Zones'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card, Dialog } from 'react-toolbox';
import SubNav from '../../components/SubNav/SubNav';

import { ZonesList, ZoneForm, ZoneBuilder, ZoneMetrics } from '../../components/Zones';
import Notify from '../../common/helpers/Notify';

@inject('store') @observer
export default class Zones extends React.Component {
  constructor (props) {
    super(props);
    this.wordStore = this.props.store.wordStore;
    this.zoneStore = this.props.store.zoneStore;

    this.state = {
      formDialogActive: false,
      builderDialogActive: false,
      metricsDialogActive: false,
      selectedZoneId: null
    };
  }

  toggleFormDialog = () => {
    this.setState({ formDialogActive: !this.state.formDialogActive, selectedZoneId: '' });
  }

  toggleMetricsDialog = () => {
    this.setState({ metricsDialogActive: !this.state.metricsDialogActive, selectedZoneId: '' });
  }

  toggleBuilderDialog = () => {
    let newState = { builderDialogActive: !this.state.builderDialogActive };
    if (!newState.builderDialogActive) {
      newState.selectedZoneId = null;
    }
    this.setState(newState);
  }

  edit = (zone) => {
    if (zone && zone.id) {
      this.setState({ selectedZoneId: zone.id, formDialogActive: true });
    }
  }

  config = (zone) => {
    this.setState({ selectedZoneId: zone.id, builderDialogActive: true });
  }

  info = (zone) => {
    this.setState({ selectedZoneId: zone.id, metricsDialogActive: true });
  }

  remove = (zone) => {
    this.zoneStore.remove(zone).then(() => {
      Notify.success('Zone deleted');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error deleting zone');
      }
    });
  }

  render () {
    let metricsDialogActions = [
      { label: 'Close', onClick: this.toggleMetricsDialog.bind(this) }
    ];

    let subnavButtons = [
      {
        icon: 'add',
        tooltip: 'Add User',
        onClick: this.toggleFormDialog.bind(this)
      }
    ];

    return (
      <div>
        <SubNav title='Zones' buttons={subnavButtons}/>
        <Card raised style={{width: '90%', margin: '5rem auto'}}>
          <ZonesList
            onEdit={this.edit}
            onRemove={this.remove}
            onConfig={this.config}
            onInfo={this.info}/>
        </Card>
        <ZoneForm
         active={this.state.formDialogActive}
         onEscKeyDown={this.toggleFormDialog}
         onOverlayClick={this.toggleFormDialog}
         onCancel={this.toggleFormDialog}
         onSave={this.toggleFormDialog}
         zoneId={this.state.selectedZoneId}/>
       <ZoneBuilder
         active={this.state.builderDialogActive}
         zoneId={this.state.selectedZoneId}
         onCancel={this.toggleBuilderDialog}
         onSave={this.toggleBuilderDialog} />
       <Dialog
         actions={metricsDialogActions}
         active={this.state.metricsDialogActive}
         onEscKeyDown={this.toggleMetricsDialog}
         onOverlayClick={this.toggleMetricsDialog}
         title='Zone Metrics'>
         <ZoneMetrics selectedZone={this.state.selectedZoneId} />
       </Dialog>
      </div>
    );
  }
}
