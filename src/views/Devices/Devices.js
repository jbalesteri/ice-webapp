const debug = require('debug')('ice:DevicesView'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card, CardText, Dialog } from 'react-toolbox';

import { DevicesList, DeviceForm, DeviceSensorData } from '../../components/Devices';
import Notify from '../../common/helpers/Notify';
import PowerStatusLegend from '../../components/common/PowerStatusLegend/PowerStatusLegend';
import SubNav from '../../components/SubNav/SubNav';

const styles = require('./Devices.css');

@inject('store') @observer
export default class Devices extends React.Component {
  constructor (props) {
    super(props);
    this.deviceStore = this.props.store.deviceStore;
    this.sensorDataStore = this.props.store.sensorDataStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      formDialogOpen: false,
      sensorDialogOpen: false,
      editDeviceId: null
    };
  }

  remove = (device) => {
    this.deviceStore.remove(device).then(() => {
      Notify.success('Device removed');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error removing device');
      }
    });
  }

  edit = (device) => {
    if (device && device.id) {
      this.setState({ editDeviceId: device.id, formDialogOpen: true });
    }
  }

  showSensorDataPopup = (evt) => {
    this.setState({
      dialogOpen: true,
      selectedDevice: evt
    });
  }

  // Dialogs Start
  formDialogToggle = () => {
    let newState = { formDialogOpen: !this.state.formDialogOpen };

    if (this.state.formDialogOpen && this.state.editDeviceId) {
      newState.editDeviceId = null;
    }

    this.setState(newState);
  }

  sensorDialogToggle = (evt) => {
    this.setState({
      sensorDialogOpen: true,
      selectedDevice: evt
    });
  }

  closeDialog = () => {
    this.setState({sensorDialogOpen: false});
    this.setState({formDialogOpen: false});
  }

  render () {
    let sensorDialogActions = [
      { label: 'Close', onClick: this.closeDialog.bind(this) }
    ];

    let subnavButtons = [
      {
        icon: 'add',
        tooltip: 'Add Device',
        onClick: this.formDialogToggle.bind(this)
      }
    ];

    return (
      <div>
        <SubNav title='Devices' buttons={subnavButtons}/>
        <div>
          <Card raised style={{width: '90%', margin: '5rem auto', overflow: 'visible'}}>
            <DevicesList onEdit={this.edit} onRemove={this.remove} onInfo={this.sensorDialogToggle} />
          </Card>
          <Card className={styles.legendCard}>
            <CardText>
              <div className={styles.legend}>
                <span className={styles.legendLabel}>Device State: </span><PowerStatusLegend />
              </div>
            </CardText>
          </Card>
        </div>
        <DeviceForm
          active={this.state.formDialogOpen}
          onSave={this.formDialogToggle}
          onCancel={this.formDialogToggle}
          deviceId={this.state.editDeviceId} />
        <Dialog
          actions={sensorDialogActions}
          active={this.state.sensorDialogOpen}
          onEscKeyDown={this.closeDialog}
          onOverlayClick={this.closeDialog}
          title='Sensor Data Detail'>
          <DeviceSensorData
            onClickCloseSensorDataDisplay={this.closeDialog}
            selectedDevice={this.state.selectedDevice} />
        </Dialog>
      </div>
    );
  }
}
