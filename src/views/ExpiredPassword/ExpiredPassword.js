var debug = require('debug')('ice: ExpiredPassword'); // eslint-disable-line no-unused-vars

import React from 'react';

import { inject, observer } from 'mobx-react';

import { Card, CardText } from 'react-toolbox';

import PageChrome from '../../components/PageChrome/PageChrome';
import { ChangePasswordForm } from '../../components/Account';

@inject('store') @observer
export default class ExpiredPassword extends React.Component {
  constructor (props) {
    super(props);

    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
  }

  onSave = () => {
    this.context.router.transitionTo('/');
  }

  render () {
    return (
      <PageChrome>
        <h1>{this.wordStore.translate('Expired Password')}</h1>
        <Card raised style={{width: '90%', margin: '20px auto'}}>
          <CardText>
            <ChangePasswordForm onSave={this.onSave} />
          </CardText>
        </Card>
      </PageChrome>
    );
  }
}

ExpiredPassword.wrappedComponent.contextTypes = {
  router: React.PropTypes.object.isRequired
};
