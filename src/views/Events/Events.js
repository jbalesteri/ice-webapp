// File generated on 2016-11-17 by Jay
var debug = require('debug')('ice:views:events'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Card, Dialog, IconButton } from 'react-toolbox';
import ValueWrapper from '../../components/common/ValueWrapper/ValueWrapper';

import DataTable from '../../components/DataTable/DataTable';
import SubNav from '../../components/SubNav/SubNav';

let styles = require('./Events.css');

@inject('store') @observer
export default class Events extends React.Component {
  constructor (props) {
    super(props);
    this.wordStore = this.props.store.wordStore;
    this.eventStore = this.props.store.eventStore;
    this.columns = ['severity', 'summary', 'timestamp', 'info', 'type'];
    this.state = {
      active: false
    };
  }

  eventDialogToggle = (evt) => {
    this.setState({
      active: !this.state.active,
      selectedEvent: evt
    });
  }

  renderEvents () {
    let events = this.eventStore.all.map((event, index) => {
      return {
        id: index,
        type: event.type,
        summary: event.summary,
        severity: event.severity,
        timestamp: new Date(event.timestamp).toLocaleString(),
        info: <IconButton icon='info' onClick={this.eventDialogToggle.bind(this, event)} />
      };
    });

    let dataTableModel = {
      severity: {type: String},
      summary: {type: String},
      timestamp: {type: Date},
      info: {type: String},
      type: {type: String}
    };

    let columnMetadata = [
      {
        'columnName': this.wordStore.translate('severity'),
        'displayName': this.wordStore.translate('severity'),
        'order': 1,
        'visible': true
      },
      {
        'columnName': this.wordStore.translate('summary'),
        'displayName': this.wordStore.translate('summary'),
        'order': 2,
        'visible': true
      },
      {
        'columnName': this.wordStore.translate('timestamp'),
        'displayName': this.wordStore.translate('timestamp'),
        'order': 3,
        'visible': true
      },
      {
        'columnName': this.wordStore.translate('info'),
        'displayName': this.wordStore.translate('info'),
        'order': 4,
        'visible': true,
        'customComponentMetadata': {}
      },
      {
        'columnName': this.wordStore.translate('type'),
        'displayName': this.wordStore.translate('type'),
        'order': 5,
        'visible': true
      }
    ];
    return (
      <DataTable
        striped
        wordStore={this.wordStore}
        columns={this.columns}
        model={dataTableModel}
        columnMetadata={columnMetadata}
        data={events}
        source={events}
        onInfo={this.eventDialogToggle.bind(this)} />
    );
  }

  eventDialogActions = [
    { label: 'Close', onClick: this.eventDialogToggle.bind(this) }
  ];

  render () {
    let evt = this.state.selectedEvent;
    let summary = (evt && evt.summary) ? evt.summary : 'N/A';
    let severity = (evt && evt.severity) ? evt && evt.severity : 'N/A';
    let timestamp = (evt && evt.timestamp) ? evt && evt.timestamp : 'N/A';
    let device = (evt && evt.deviceName) ? evt.deviceName : 'N/A';
    let description = (evt && evt.description) ? evt.description : 'N/A';

    return (
      <div>
        <SubNav title='Events' />
        <Card raised style={{width: '90%', margin: '5rem auto', overflow: 'visible'}}>
          {this.renderEvents()}
        </Card>
        <Dialog
          actions={this.eventDialogActions}
          active={this.state.active}
          onEscKeyDown={this.eventDialogToggle}
          onOverlayClick={this.eventDialogToggle}
          title={this.state.selectedEvent && this.state.selectedEvent.name}>
          <div className={styles.eventDialogValues} data-qa-tag='event-dialog'>
            <ValueWrapper
              value={summary}
              size='med'
              labelFirst={true}
              label='Summary: '
              inline={false}
              align='left' />
            <ValueWrapper
              value={severity}
              size='med'
              labelFirst={true}
              label='Severity: '
              inline={false}
              align='left' />
            <ValueWrapper
              value={device}
              size='med'
              labelFirst={true}
              label='Device: '
              inline={false}
              align='left' />
            <ValueWrapper
              value={new Date(timestamp).toLocaleString()}
              size='med'
              labelFirst={true}
              label='Timestamp: '
              inline={false}
              align='left' />
            <ValueWrapper
              value={description}
              size='med'
              labelFirst={true}
              label='Description: '
              inline={false}
              align='left' />
          </div>
        </Dialog>
      </div>
    );
  }
}
