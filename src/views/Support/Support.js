var debug = require('debug')('ice: HealthCheck'); // eslint-disable-line no-unused-vars

import React from 'react';

import { inject, observer } from 'mobx-react';

import { Card } from 'react-toolbox';
import SubNav from '../../components/SubNav/SubNav';

@inject('store') @observer
export default class Support extends React.Component {
  constructor (props) {
    super(props);

    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
  }

  render () {
    return (
      <div>
        <SubNav title='Support' />
        <Card raised style={{width: '90%', margin: '2rem auto'}}>
          <p>support page data</p>
        </Card>
      </div>
    );
  }
}
