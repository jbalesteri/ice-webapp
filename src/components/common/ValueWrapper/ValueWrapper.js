const debug = require('debug')('ice:Common:ValueWrapper'); // eslint-disable-line no-unused-vars

import React, { PropTypes, Component } from 'react';

import ClassNames from 'classnames/bind';
let styles = require('./ValueWrapper.css');
var css = ClassNames.bind(styles);

export default class ValueWrapper extends Component {
  renderValue () {
    let value = this.props.value;
    // pad single digits
    let valueStyles = () => {
      return css({
        value: true,
        leading: (this.props.value < 10 && this.props.value > 0)
      });
    };

    if (typeof this.props.value === 'number' && this.props.formatNumber) {
      value = value.toFixed(3);
    } else if (typeof this.props.value === 'string' && this.props.value === '') {
      value = 'N/A';
    }

    let qaTag = this.props.label ? this.generateTag(this.props.label) : 'value-unlabeled';
    return <div className={valueStyles()} data-qa-tag={qaTag}>{value}{this.renderSuffix()}</div>;
  }

  generateTag (val) {
    val = val.replace(/\s+/g, '-').toLowerCase();
    return 'value-' + val;
  }

  renderLabel () {
    if (this.props.label) {
      return <div className={styles.label}>{this.props.label}</div>;
    }
  }

  renderSuffix () {
    if (this.props.suffix) {
      return <span className={styles.suffix}> {this.props.suffix}</span>;
    }
  }

  renderContent () {
    // alignment
    let setAlignment = () => {
      if (this.props.align === 'left') {
        return css({ alignLeft: true });
      } else {
        return css({ alignRight: true });
      }
    };

    // check if value is inline
    let inlineCheck = () => {
      if (this.props.inline === true) {
        return css({ valueInline: true });
      }
    };
    // get size for value and label
    let valueSize = () => {
      switch (this.props.size) {
        case 'title' :
          return css({ valueWrapperTitle: true });
        case 'lg' :
          return css({ valueWrapperLarge: true });
        case 'med':
          return css({ valueWrapperMedium: true });
        case 'xs':
          return css({ valueWrapperXS: true });
        default:
          return css({ valueWrapperSmall: true });
      }
    };

    // is value preceeded by bubble
    let hasBubble = () => {
      if (this.props.bubble === true) {
        let bubbleClasses = css({
          hasBubble: true,
          bubbleSuccess: this.props.bubbleColor === 'success',
          bubbleError: this.props.bubbleColor === 'error',
          bubbleWarning: this.props.bubbleColor === 'warning',
          bubblePrimary: this.props.bubbleColor === 'primary',
          bubbleAccent: this.props.bubbleColor === 'accent',
          bubbleInactive: this.props.bubbleColor === 'inactive',
          bubbleUnconfigured: this.props.bubbleColor === 'unconfigured'
        });

        return css(bubbleClasses);
      }
    };

    return (
      <div className={setAlignment()}>
        <div className={hasBubble()}>
          <div className={valueSize()}>
            <div className={inlineCheck()}>
              {
                (this.props.labelFirst === true)
                  ? <div className={styles.valueWrapper}>{this.renderLabel()}{this.renderValue()}</div>
                  : <div className={styles.valueWrapper}>{this.renderValue()}{this.renderLabel()}</div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }

  render () {
    return (
      <div>
        {this.renderContent()}
      </div>
    );
  }
}

ValueWrapper.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  label: PropTypes.string,
  align: PropTypes.oneOf(['left', 'right']),
  suffix: PropTypes.string,
  size: PropTypes.oneOf(['xs', 'sm', 'med', 'lg', 'title']).isRequired,
  formatNumber: PropTypes.bool,
  bubble: PropTypes.bool,
  bubbleColor: PropTypes.oneOf(['success', 'error', 'warning', 'primary', 'accent', 'inactive', 'unconfigured']),
  labelFirst: PropTypes.bool,
  inline: PropTypes.bool
};

ValueWrapper.defaultProps = {
  formatNumber: true,
  bubble: false,
  labelFirst: true,
  inline: false
};
