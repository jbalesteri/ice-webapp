## ValueWrapper


#### Instructions:
This component is meant to be used to display a label and value. It will also append a specified suffix *(ex. 'kW', '%', etc)*.

##### Example:
```html
<ValueWrapper
	prop={value}
/ >
```

##### Props:
| Name | Type  | Required | Default | Description  |
|------|-------|----------| ------- | -------------|
| value  | str | Y | none | the value you wish to represent. |
| label | str | N | none | label for the provided value |
| suffix | str | N | none | 'W', 'kW', '%', etc.  |
| size | 'xs', 'sm', 'med', 'lg' | Y | sm | pre-styled for size/colors |
| formatNumber | bool | N | false |format to 1000th |
| bubble | bool | N | none | prefix value with UI 'dot' |
| bubbleColor | 'success', 'error', 'warning', 'primary', 'accent' | N | 'primary' | color of bubble indicator |
| labelFirst | bool | N | none | is label before value |
| inline | bool | N | none | dislays values inline / block |
| align | 'left', 'right' | N | right | aligns text left or right for value and label |

----
