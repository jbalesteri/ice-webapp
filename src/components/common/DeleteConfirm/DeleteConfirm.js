import React, { Component, PropTypes } from 'react';

import { IconButton, Dialog } from 'react-toolbox';

export default class DeleteConfirm extends Component {
  constructor (props) {
    super(props);

    this.state = {
      confirmActive: false
    };
  }

  delete = () => {
    this.setState({ confirmActive: true });
  }

  confirm = () => {
    this.setState({ confirmActive: false }, () => {
      this.props.onClick();
    });
  }

  cancel = () => {
    this.setState({ confirmActive: false });
  }

  render () {
    let actions = [
      { label: 'Delete', onClick: this.confirm },
      { label: 'Cancel', onClick: this.cancel }
    ];

    return (
      <span>
        <IconButton icon='delete' onClick={this.delete} />
        <Dialog
          actions={actions}
          active={this.state.confirmActive}
          title='Delete?'
          type='small'>
          <p>Are you sure you want to delete this item?</p>
        </Dialog>
      </span>
    );
  }
}

DeleteConfirm.propTypes = {
  onClick: PropTypes.func.isRequired
};
