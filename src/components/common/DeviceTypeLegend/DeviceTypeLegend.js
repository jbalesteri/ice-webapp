var debug = require('debug')('ice:DeviceTypeLegend: '); // eslint-disable-line no-unused-vars
import { inject, observer } from 'mobx-react';
import React from 'react';
import TopologyDevice from '../TopologyDevice/TopologyDevice';

let styles = require('./DeviceTypeLegend.css');

@inject('store') @observer
export default class DeviceTypeLegend extends React.Component {
  render () {
    return (
      <div className={styles.deviceLegend}>
        <TopologyDevice type='iceblock' state={'inactive'} label='Iceblock / UPS'/>
        <TopologyDevice type='iceswitch' state={'inactive'} label='Ice Switch'/>
        <TopologyDevice type='pdu' state={'inactive'} label='PDU'/>
      </div>
    );
  }
}
