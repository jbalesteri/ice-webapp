var debug = require('debug')('ice:PowerStatusLegend: '); // eslint-disable-line no-unused-vars
import { inject, observer } from 'mobx-react';
import React from 'react';
import ValueWrapper from '../ValueWrapper/ValueWrapper';

let styles = require('./PowerStatusLegend.css');

@inject('store') @observer
export default class PowerStatusLegend extends React.Component {
  render () {
    return (
      <div className={styles.statusLegend}>
        <ValueWrapper inline={true} bubble={true} size='sm' bubbleColor='success' value='Active' label=' '/>
        <ValueWrapper inline={true} bubble={true} size='sm' bubbleColor='inactive' value='Inactive' label=' '/>
        <ValueWrapper inline={true} bubble={true} size='sm' bubbleColor='error' value='Off' label=' '/>
        <ValueWrapper inline={true} bubble={true} size='sm' bubbleColor='primary' value='UPS Mode' label=' '/>
        <ValueWrapper inline={true} bubble={true} size='sm' bubbleColor='unconfigured' value='Unconfigured' label=' '/>
      </div>
    );
  }
}
