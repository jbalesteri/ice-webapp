var debug = require('debug')('ice:PhaseData: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { Avatar } from 'react-toolbox';

let styles = require('./PhaseData.css');

export default class PhaseData extends React.Component {
  render () {
    let phaseNumber = this.props.index ? String(this.props.index + 1) : this.props.phase.slice(0, 1);
    let style = styles.phase1Avatar;
    if (phaseNumber === '2') {
      style = styles.phase2Avatar;
    } else if (phaseNumber === '3') {
      style = styles.phase3Avatar;
    }

    return (
      <div data-qa-tag={'phase-data'} className={styles.phaseRow}>
        <Avatar title={phaseNumber} className={style}/>
        <div className={styles.values}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
