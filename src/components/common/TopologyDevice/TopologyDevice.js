var debug = require('debug')('ice:TopologyDevice: '); // eslint-disable-line no-unused-vars

import React from 'react';

import ClassNames from 'classnames/bind';
let styles = require('./TopologyDevice.css');
var css = ClassNames.bind(styles);

const iceblockIcon = <svg width='10' height='10' viewBox='0 0 120 120' xmlns='http://www.w3.org/2000/svg'><rect x='10' y='10' width='100' height='100'/></svg>;
const iceswitchIcon = <svg width='11' height='11' viewBox='266 101 16 16' version='1.1' xmlns='http://www.w3.org/2000/svg'><rect transform='translate(274.000000, 109.000000) rotate(-315.000000) translate(-274.000000, -109.000000) ' x='269' y='104' width='11' height='11'></rect></svg>;
const pduIcon = <svg width='10' height='10' viewBox='473 231 10 10' version='1.1' xmlns='http://www.w3.org/2000/svg'><circle cx='478' cy='236' r='5'></circle></svg>;

export default class TopologyDevice extends React.Component {
  renderLabel () {
    if (this.props.label) {
      return <span className={styles.label}>{this.props.label}</span>;
    }
  }

  renderIcon () {
    switch (this.props.type) {
      case 'iceblock' || 'ups' :
        return iceblockIcon;
      case 'iceswitch' :
        return iceswitchIcon;
      case 'pdu' :
        return pduIcon;
      default :
        return pduIcon;
    }
  }

  render () {
    let status = () => {
      switch (this.props.state) {
        case 'on' :
          return css({ active: true });
        case 'inactive' :
          return css({ inactive: true });
        case 'off' :
          return css({ off: true });
        case 'ups' :
          return css({ ups: true });
        case 'notConnected' :
          return css({ unconfigured: true });
        default :
          return css({ unconfigured: true });
      }
    };

    return (
      <div className={styles.topologyDevice}><span className={status()}>{ this.renderIcon() }</span><span className={styles.label}>{this.renderLabel()}</span></div>
    );
  }
}
