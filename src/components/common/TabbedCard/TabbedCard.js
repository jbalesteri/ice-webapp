var debug = require('debug')('ice:TabbedCard: '); // eslint-disable-line no-unused-vars

import React from 'react';

export default class TabbedCard extends React.Component {
  render () {
    return (
      <div data-qa-tag={'tabbed-card'}>[ TabbedCard component ]</div>
    );
  }
}
