var debug = require('debug')('ice:FormWrapper: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

let styles = require('./FormWrapper.css');

@inject('store') @observer
export default class FormWrapper extends React.Component {
  constructor (props) {
    super(props);

    this.store = this.props.store;
    this.onSubmit = this.props.onSubmit;
  }

  componentDidMount () {
    document.querySelector('form input:first-child').focus();
    document.querySelector('form').onkeydown = (e) => {
      if (e.keyCode === 13) {
        this.props.onSubmit();
      }
    };
  }

  render () {
    return (
      <section className={styles.formWrapper} data-qa-tag='form-wrapper'>
        <form>
          {this.props.children}
        </form>
      </section>
    );
  }
}

FormWrapper.wrappedComponent.propTypes = {
  onSubmit: React.PropTypes.func.isRequired
};
