## FormWrapper


#### Instructions:
Wrapper for forms. This will allow the following:

- Enter key to save.
- Autofocus on first input. 

##### Example:
```html
<FormWrapper onSubmit={[this.submitAction]} >
	// form in here...
</FormWrapper>
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| onSubmit  | func  |    none    |      function to perform on enter (normally save)       |

