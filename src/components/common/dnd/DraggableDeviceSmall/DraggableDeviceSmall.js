// File generated on 2016-12-02 by Jay
var debug = require('debug')('ice:DragDropRackBuilder: '); // eslint-disable-line no-unused-vars

import React, { Component } from 'react';

import { ListItem } from 'react-toolbox';

import { DragSource } from 'react-dnd';

let styles = require('./DraggableDeviceSmall.css');

const sourceDevice = {
  beginDrag (props) {
    return {
      name: props.name,
      type: props.type,
      id: props.id
    };
  }
};

// *** DEVICES ***//
@DragSource(props => props.type, sourceDevice, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
export default class DraggableDeviceSmall extends Component {
  constructor (props) {
    super(props);
    this.connectDragSource = this.props.connectDragSource;
    this.isDragging = this.props.isDragging;
    this.name = this.props.name;
    this.type = this.props.type;
    this.id = this.props.id;
    this.isDropped = this.props.isDropped;
  }

  render () {
    const { name, type, isDropped, isDragging, connectDragSource } = this.props;
    const opacity = isDragging ? 0.4 : 1;

    return connectDragSource(
      <div className={styles.deviceStyle} style={{ opacity }} data-qa-tag='draggable-device' data-qa-id={this.props.id}>
        <ListItem
          caption={name.toUpperCase()}
          legend={type.toUpperCase()}
          ripple={false}
          disabled={isDropped}
          isDropped={isDropped}
        />
      </div>
    );
  }
}
