import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import { ListItem } from 'react-toolbox';

const styles = require('./GroupDragSource.css');

const sourceRack = {
  beginDrag (props) {
    return {
      name: props.name,
      id: props.id
    };
  }
};

@DragSource('group', sourceRack, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
export default class GroupDragSource extends Component {
  constructor (props) {
    super(props);

    this.connectDragSource = this.props.connectDragSource;
    this.isDragging = this.props.isDragging;
    this.isDropped = this.props.isDropped;
  }

  render () {
    const { name, isDropped, isDragging, connectDragSource } = this.props;
    const opacity = isDragging ? 0.4 : 1;

    return connectDragSource(
      <div className={styles.deviceStyle} style={{ opacity }} data-qa-tag='group-drag-source' data-qa-id={this.props.id}>
        <ListItem
          caption={name.toUpperCase()}
          ripple={false}
          disabled={isDropped}
          isDropped={isDropped} />
      </div>
    );
  }
}
