import React, { Component } from 'react';
import ClassNames from 'classnames/bind';

const styles = require('./RackDropTarget.css');
const css = ClassNames.bind(styles);

import { DropTarget } from 'react-dnd';

const RackTarget = {
  drop (props, monitor) {
    let item = monitor.getItem();
    props.onDrop(item);
  }
};

@DropTarget(['rack'], RackTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export default class RackDropTarget extends Component {
  render () {
    let { isOver, canDrop, connectDropTarget } = this.props;

    let isActive = isOver && canDrop;

    let classes = css({
      baseTargetStyle: true,
      activeTarget: isActive,
      canDropStyle: (!isActive && canDrop),
      inactiveTarget: (!isActive && !canDrop)
    });

    return connectDropTarget(
      <div className={classes} data-qa-tag='rack-drop-target'>
        Drag Racks Here
        <div>{this.props.children}</div>
      </div>
    );
  }
}
