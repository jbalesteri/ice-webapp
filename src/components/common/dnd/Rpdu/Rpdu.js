var debug = require('debug')('ice:DND: rpdu: '); // eslint-disable-line no-unused-vars

import React, { Component } from 'react';
import ClassNames from 'classnames/bind';
import { DropTarget, DragSource } from 'react-dnd';
import { Chip, ListItem } from 'react-toolbox';
const styles = require('./Rpdu.css');
const css = ClassNames.bind(styles);

const RackTarget = {
  drop (props, monitor) {
    let item = monitor.getItem();
    props.onDrop(item);
  }
};

@DropTarget(['rpdu'], RackTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export class RpduDropTarget extends Component {
  render () {
    let { isOver, canDrop, connectDropTarget } = this.props;

    let isActive = isOver && canDrop;

    let classes = css({
      targetBase: true,
      activeTarget: isActive,
      canDropStyle: (!isActive && canDrop)
    });

    return connectDropTarget(
      <div className={classes} data-qa-tag='drop-target' data-qa-id={this.props.id}>
        {isActive ? 'Release device to drop' : 'RPDU'}
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

const sourceRack = {
  beginDrag (props) {
    return {
      name: props.name,
      type: props.type,
      id: props.id
    };
  }
};

@DragSource('rpdu', sourceRack, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
export class RpduDragSource extends Component {
  constructor (props) {
    super(props);

    this.connectDragSource = this.props.connectDragSource;
    this.isDragging = this.props.isDragging;
    this.isDropped = this.props.isDropped;
  }

  render () {
    const { name, type, isDropped, isDragging, connectDragSource } = this.props;
    const opacity = isDragging ? 0.4 : 1;

    return connectDragSource(
      <div className={styles.dragDeviceStyle} data-qa-tag='drag-source' data-qa-id={this.props.id} style={{ opacity }}>
        <ListItem
          caption={name.toUpperCase()}
          legend={type.toUpperCase()}
          ripple={false}
          disabled={isDropped}
          isDropped={isDropped} />
      </div>
    );
  }
}

export class RpduDroppedCard extends Component {
  delete = () => {
    this.props.onDelete(this.props.rpdu);
  }

  truncateName = (name) => {
    if (name.length > 6) {
      return name.substring(0, 6) + '...';
    } else {
      return name;
    }
  };

  render () {
    let { name, id } = this.props.rpdu;

    return (
      <Chip
        deletable
        id={id}
        name={name}
        onDeleteClick={this.delete}
        className={styles.rackDroppedDevice}>{this.truncateName(name)}</Chip>
    );
  }
}

RpduDroppedCard.propTypes = {
  rpdu: React.PropTypes.object.isRequired,
  onDelete: React.PropTypes.func.isRequired
};
