import React, { Component } from 'react';
import ClassNames from 'classnames/bind';

const styles = require('./GroupDropTarget.css');
const css = ClassNames.bind(styles);

import { DropTarget } from 'react-dnd';

const RackTarget = {
  drop (props, monitor) {
    let item = monitor.getItem();
    props.onDrop(item);
  }
};

@DropTarget(['group'], RackTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export default class GroupDropTarget extends Component {
  render () {
    let { isOver, canDrop, connectDropTarget } = this.props;

    let isActive = isOver && canDrop;

    let classes = css({
      targetStyle: true,
      activeTarget: isActive,
      canDropStyle: (!isActive && canDrop),
      inactiveTarget: (!isActive && !canDrop)
    });

    return connectDropTarget(
      <div className={classes} data-qa-tag='group-drop-target'>
        Drag Groups Here
        <div>{this.props.children}</div>
      </div>
    );
  }
}
