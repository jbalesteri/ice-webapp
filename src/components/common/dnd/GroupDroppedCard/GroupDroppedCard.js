import React from 'react';
import { Chip } from 'react-toolbox';

let styles = require('./GroupDroppedCard.css');

export default class GroupDroppedCard extends React.Component {
  delete = () => {
    this.props.onDelete(this.props.group);
  }

  render () {
    let { name, id } = this.props.group;

    return (
      <Chip
        deletable
        id={id}
        name={name}
        onDeleteClick={this.delete}
        className={styles.groupDroppedDevice}>{name.toUpperCase()}</Chip>
    );
  }
}

GroupDroppedCard.propTypes = {
  group: React.PropTypes.object.isRequired,
  onDelete: React.PropTypes.func.isRequired
};
