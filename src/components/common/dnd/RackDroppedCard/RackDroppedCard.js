import React from 'react';
import { Chip } from 'react-toolbox';

let styles = require('./RackDroppedCard.css');

export default class RackDroppedCard extends React.Component {
  delete = () => {
    this.props.onDelete(this.props.rack);
  }

  render () {
    let { name, id } = this.props.rack;

    return (
      <Chip
        deletable
        id={id}
        name={name}
        onDeleteClick={this.delete}
        className={styles.groupDroppedDevice}>{name.toUpperCase()}</Chip>
    );
  }
}

RackDroppedCard.propTypes = {
  rack: React.PropTypes.object.isRequired,
  onDelete: React.PropTypes.func.isRequired
};
