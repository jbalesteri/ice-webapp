## SubNav


#### Instructions:
Component desc.

##### Example:
```html
<SubNav
	data-qa-tag='sub-nav'
	prop={value}
/ >
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| ?  | ?  |    ?    |      ?       |

##### Tagging:
The component name is ran through a regex to switch camel case to be seperated with dashes and made lowercase. All components will be setup for qa-tagging automatically. 'data-qa-id' will still need to be added manually if necessary.

###### Images:
----
[ SCREENSHOTS OR WALKTHROUGHS ]

----
