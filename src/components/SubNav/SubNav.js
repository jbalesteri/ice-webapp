var debug = require('debug')('ice:SubNav: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { AppBar, Button, Navigation, Tooltip } from 'react-toolbox';

const TooltipButton = Tooltip(Button);
let styles = require('./SubNav.css');

export default class SubNav extends React.Component {
  renderButtons () {
    if (this.props.buttons) {
      let buttons = this.props.buttons.map((button, index) => {
        return (
          <div key={'subnavButton-' + index} className={styles.tooltipButton}><TooltipButton floating mini icon={button.icon} tooltip={button.tooltip} onClick={button.onClick} /></div>
        );
      });
      return buttons;
    }
  }

  render () {
    return (
      <div className={styles.subNav}>
        <AppBar title={this.props.title} className={styles.appBar}>
          <Navigation type='horizontal'>
            {this.renderButtons()}
          </Navigation>
        </AppBar>
      </div>
    );
  }
}
