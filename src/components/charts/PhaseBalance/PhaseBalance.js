const debug = require('debug')('ice:charts:phaseBalance'); // eslint-disable-line no-unused-vars

import React, { Component, PropTypes } from 'react';

import styles from './PhaseBalance.css';

export default class PhaseBalance extends Component {
  render () {
    let circumference = 157;
    let { phase1, phase2, phase3 } = this.props;
    let total = phase1 + phase2 + phase3;
    let phase2Percent = Math.round((phase2 / total) * 100) / 100;
    let phase3Percent = Math.round((phase3 / total) * 100) / 100;
    let phase2Circumference = (circumference * phase2Percent);
    let phase3Circumference = (circumference * phase3Percent);

    return (
      <svg viewBox='0 0 100 100' className={styles.svg}>
        <defs>
          <mask id='donut-hole'>
            <rect width='100' height='100' x='0' y='0' fill='white' />
            <circle r='30' cx='50' cy='50' />
          </mask>
        </defs>
        <g mask='url(#donut-hole)'>
          <circle r='50' cx='50' cy='50' className={styles.phase1}/>
          <circle r='25' cx='50' cy='50' className={styles.phase2} style={{strokeDasharray: `${phase2Circumference} ${circumference}`}} />
          <circle r='25' cx='50' cy='50' className={styles.phase3} style={{strokeDasharray: `${phase3Circumference} ${circumference}`}} />
        </g>
      </svg>
    );
  }
}

PhaseBalance.propTypes = {
  phase1: PropTypes.number,
  phase2: PropTypes.number,
  phase3: PropTypes.number
};
