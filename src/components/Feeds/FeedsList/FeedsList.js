const debug = require('debug')('ice:feeds:list'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { IconButton } from 'react-toolbox';

import DataTable from '../../DataTable/DataTable';
import DeleteConfirm from '../../common/DeleteConfirm/DeleteConfirm';

@inject('store') @observer
export default class FeedsList extends React.Component {
  constructor (props) {
    super(props);

    this.feedStore = this.props.store.feedStore;
    this.wordStore = this.props.store.wordStore;

    this.model = {
      name: {type: String, title: this.wordStore.translate('Name')},
      symbol: {type: String, title: this.wordStore.translate('Symbol')},
      type: {type: String, title: this.wordStore.translate('Type')},
      edit: {type: String, title: this.wordStore.translate('Edit')},
      remove: {type: Boolean, title: this.wordStore.translate('Remove')}
    };
  }

  edit = (feed) => {
    this.props.onEdit(feed);
  }

  remove = (feed) => {
    this.props.onRemove(feed);
  }

  render () {
    let feeds = this.feedStore.all.map((feed) => {
      return {
        id: feed.id,
        name: feed.name,
        symbol: feed.symbol,
        type: feed.type,
        edit: <IconButton icon='edit' onClick={this.edit.bind(this, feed)} />,
        remove: <DeleteConfirm onClick={this.remove.bind(this, feed)} />
      };
    });

    return <DataTable striped model={this.model} source={feeds} />;
  }
}

FeedsList.wrappedComponent.propTypes = {
  onEdit: React.PropTypes.func.isRequired,
  onRemove: React.PropTypes.func.isRequired
};
