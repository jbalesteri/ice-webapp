var debug = require('debug')('ice:Feeds:Card'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { Avatar, Card, CardTitle, CardText } from 'react-toolbox';

import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';

import PhaseData from '../../common/PhaseData/PhaseData';

import ClassNames from 'classnames/bind';
let styles = require('./FeedCard.css');
var classBind = ClassNames.bind(styles);

@inject('store') @observer
export default class FeedCard extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
  }

  renderPhaseData () {
    if (this.props.feed.type === '3Phase') {
      return this.props.feed.metrics.map((phase, index) => {
        let input = phase.power;
        let peak = phase.maxPower;
        let limit = phase.limit;
        let allocated1n = phase.allocated1n;
        let maxAllocated1n = phase.maxAllocated1n;
        let allocated2n = phase.allocated2n;
        let maxAllocated2n = phase.maxAllocated2n;
        return (
          <div key={index} className={styles.threePhase}>
            <PhaseData index={index} phase={phase.phase} >
              <ValueWrapper value={input} suffix='kW' label='input' size='sm' labelFirst={false} />
              <ValueWrapper value={peak} suffix='kW' label='peak' size='sm' labelFirst={false} />
              <ValueWrapper value={limit} suffix='kW' label='limit' size='sm' labelFirst={false} />
            </PhaseData>
            <div className={styles.usage}>
              <div className={styles.usageCol}>1N - &nbsp;
                <ValueWrapper
                  value={allocated1n}
                  suffix='kW'
                  size='xs'
                  formatNumber={false} />
                &nbsp;of&nbsp;
                <ValueWrapper
                  value={maxAllocated1n}
                  suffix='kW'
                  size='xs'
                  formatNumber={false} />
              </div>
              <div className={styles.usageCol}>2N - &nbsp;
                <ValueWrapper
                  value={allocated2n}
                  suffix='kW'
                  size='xs'
                  formatNumber={false} />
                &nbsp;of&nbsp;
                <ValueWrapper
                  value={maxAllocated2n}
                  suffix='kW'
                  size='xs'
                  formatNumber={false} />
              </div>
            </div>
          </div>
        );
      });
    }
  }

  renderFeedData () {
    if (this.props.feed.type === '1Phase') {
      let metrics = this.props.feed.metrics[0];
      let input = metrics.power;
      let peak = metrics.maxPower;
      let limit = metrics.limit;
      let allocated1n = metrics.allocated1n;
      let maxAllocated1n = metrics.maxAllocated1n;
      let allocated2n = metrics.allocated2n;
      let maxAllocated2n = metrics.maxAllocated2n;

      return (
        <div className={styles.singlePhase}>
          <div className={styles.feedData}>
            <ValueWrapper size='title' labelFirst={false} suffix='kW' value={input} label='input' />
            <ValueWrapper size='title' labelFirst={false} suffix='kW' value={peak} label='peak' />
            <ValueWrapper size='title' labelFirst={false} suffix='kW' value={limit} label='limit' />
          </div>
          <div className={styles.usage}>
            <div className={styles.usageCol}>1N - &nbsp;
              <ValueWrapper
                value={allocated1n}
                suffix='kW'
                size='xs'
                formatNumber={false} />
              &nbsp;of&nbsp;
              <ValueWrapper
                value={maxAllocated1n}
                suffix='kW'
                size='xs'
                formatNumber={false} />
            </div>
            <div className={styles.usageCol}>2N - &nbsp;
              <ValueWrapper
                value={allocated2n}
                suffix='kW'
                size='xs'
                formatNumber={false} />
              &nbsp;of&nbsp;
              <ValueWrapper
                value={maxAllocated2n}
                suffix='kW'
                size='xs'
                formatNumber={false} />
            </div>
          </div>
        </div>
      );
    }
  }

  render () {
    let feed = this.props.feed;
    let title = feed.name;
    let feedSymbolIcon = 'feedMetricsSymbol' + feed.symbol;
    let symbolStyle = () => {
      return classBind({
        feedCardAvatar: true,
        [feedSymbolIcon]: true
      });
    };

    let cardSize = () => {
      if (this.props.type === '3Phase') {
        return classBind({ feedCardThreePhase: true });
      } else {
        return classBind({ feedCard: true });
      }
    };

    return (
      <Card raised className={cardSize()} >
        <div data-qa-tag='feed-card' data-qa-id={this.props.id}>
          <div className={styles.feedCardTitle}>
            <Avatar title={feed.symbol} className={symbolStyle()} />
            <CardTitle
              className={styles.feedCardTitleTxt}
              title={title} />
          </div>
          <CardText>
            {this.renderFeedData()}
            {this.renderPhaseData()}
          </CardText>
        </div>
      </Card>
    );
  }
}
