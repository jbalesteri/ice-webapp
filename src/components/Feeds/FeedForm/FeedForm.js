const debug = require('debug')('ice:FeedForm');

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Input, RadioGroup, RadioButton, Dialog } from 'react-toolbox';

import Validators from '../../../common/helpers/Validators';
import Notify from '../../../common/helpers/Notify';
import FormWrapper from '../../common/FormWrapper/FormWrapper';

const styles = require('./FeedForm.css');

const defaultState = {
  feed: null,
  name: '',
  nameError: '',
  symbol: '',
  symbolError: '',
  type: '1Phase'
};

@inject('store') @observer
export default class FeedForm extends React.Component {
  constructor (props) {
    super(props);

    this.feedStore = this.props.store.feedStore;
    this.wordStore = this.props.store.wordStore;

    let newState = defaultState;

    this.state = newState;
  }

  componentWillReceiveProps (nextProps) {
    let newState = Object.assign({}, defaultState);
    if (nextProps.feedId) {
      let feed = this.feedStore.getById(nextProps.feedId);

      newState.feed = feed;
      newState.name = feed.name;
      newState.symbol = feed.symbol;
      newState.type = feed.type;
    } else if (nextProps.active) {
      newState.feed = this.feedStore.createFeed();
    }
    this.setState(newState);
  }

  validate = (field) => {
    let error = '';
    let value = this.state[field];

    switch (field) {
      case 'name':
        let names = this.feedStore.all.map((feed) => (feed.name));
        let originalName = (this.state.feed) ? this.state.feed.name : null;
        error = Validators.feed.name(value, names, originalName);
        break;
      case 'symbol':
        let symbols = this.feedStore.all.map((feed) => (feed.symbol));
        let originalSymbol = (this.state.feed) ? this.state.feed.symbol : null;
        error = Validators.feed.symbol(value, symbols, originalSymbol);
        break;
    }

    return error;
  }

  isValid = () => {
    let p = new Promise((resolve, reject) => {
      let newState = {};

      ['name', 'symbol'].forEach((field) => {
        newState[field + 'Error'] = this.validate(field);
      });

      this.setState(newState, () => {
        if (this.state.nameError === '' && this.state.symbolError === '' && this.state.name !== '' && this.state.symbol !== '') {
          resolve();
        } else {
          reject(new Error('form not valid'));
        }
      });
    });

    return p;
  }

  handleChange = (name, value) => {
    if (name === 'symbol') {
      value = value.toUpperCase();
    }
    this.setState({ [name]: value });
  }

  handleBlur = (name) => {
    let error = this.validate(name);
    this.setState({ [name + 'Error']: error });
  }

  save = () => {
    this.isValid().then(() => {
      let feed = this.state.feed;
      feed.name = this.state.name;
      feed.symbol = this.state.symbol;
      feed.type = this.state.type;

      feed.save().then(() => {
        Notify.success('Feed saved');
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error saving feed');
        }
      });

      this.props.onSave();
    }, () => {
      debug('Feed was not valid');
    });
  }

  cancel = () => {
    this.props.onCancel();
  }

  render () {
    let title = (this.state.feed) ? this.wordStore.translate('Edit Feed') : this.wordStore.translate('Add Feed');

    let actions = [
      { label: this.wordStore.translate('Cancel'), onClick: this.cancel },
      { label: this.wordStore.translate('Save'), onClick: this.save }
    ];

    return (
      <Dialog title={title} actions={actions} active={this.props.active} onEscKeyDown={this.cancel}>
        <FormWrapper onSubmit={this.save}>
          <RadioGroup className={styles.radioGroup} name='type' value={this.state.type} onChange={this.handleChange.bind(this, 'type')}>
            <legend>Feed Phase:</legend>
            <RadioButton label='1 Ø' value='1Phase'/>
            <RadioButton label='3 Ø' value='3Phase'/>
          </RadioGroup>
          <div className={styles.twoColumn}>
            <div className={styles.column}>
              <Input
                type='text'
                label='Feed Name'
                value={this.state.name}
                error={this.state.nameError}
                onChange={this.handleChange.bind(this, 'name')}
                onBlur={this.handleBlur.bind(this, 'name')}
                />
            </div>
            <div className={styles.columnSmall}>
              <Input
                type='text'
                label='Feed Icon Label'
                value={this.state.symbol}
                error={this.state.symbolError}
                onChange={this.handleChange.bind(this, 'symbol')}
                onBlur={this.handleBlur.bind(this, 'symbol')}
                maxLength={1}
                />
            </div>
          </div>
        </FormWrapper>
      </Dialog>
    );
  }
}

FeedForm.wrappedComponent.propTypes = {
  feedId: React.PropTypes.string,
  active: React.PropTypes.bool.isRequired,
  onCancel: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired
};
