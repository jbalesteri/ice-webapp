const debug = require('debug')('ice:dataCenterForm'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { reaction } from 'mobx';

import { Button, Card, CardText, CardTitle, Input } from 'react-toolbox';

import Notify from '../../common/helpers/Notify';

const styles = require('./DataCenterForm.css');

@inject('store') @observer
export default class DataCenterForm extends React.Component {
  constructor (props) {
    super(props);

    this.store = this.props.store.dataCenterStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      name: this.store.name || '',
      description: this.store.description || '',
      location: this.store.location || ''
    };
  }

  componentDidMount () {
    this.reaction = reaction(
      () => this.store.toJson,
      () => {
        this.setState({
          name: this.store.name,
          description: this.store.description,
          location: this.store.location
        });
      }
    );
  }

  componentWillUnmount () {
    this.reaction();
  }

  handleInputChange = (name, value) => {
    this.setState({ [name]: value });
  }

  handleResetPeak = () => {
    this.store.metricsAggregateReset().then(() => {
      Notify.success(this.wordStore.translate('Data Center Updated'));
    }).catch((error) => {
      debug('Reset Error:', error);
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error Updating Data Center');
      }
    });
  }

  handleSave = () => {
    this.store.name = this.state.name;
    this.store.description = this.state.description;
    this.store.location = this.state.location;

    this.store.save().then(() => {
      Notify.success('Data Center Updated');
    }).catch((error) => {
      debug('Error Saving Datacenter:', error);
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error Updating Data Center');
      }
    });
  }

  render () {
    return (
      <Card className={styles.dataCenterCard} raised style={{width: '90%', margin: '5rem auto'}}>
        <CardTitle title='Data Center Settings' />
        <Button raised primary className={styles.btnResetPeak} onClick={this.handleResetPeak.bind(this)}>Reset Peak</Button>
        <CardText>
          <Input type='text' label='Name' value={this.state.name} onChange={this.handleInputChange.bind(this, 'name')} />
          <Input type='text' label='Description' value={this.state.description} onChange={this.handleInputChange.bind(this, 'description')} />
          <Input type='text' label='Location' value={this.state.location} onChange={this.handleInputChange.bind(this, 'location')} />
          <div>
            <Button icon='save' label='Save' onClick={this.handleSave} raised primary />
          </div>
        </CardText>
      </Card>
    );
  }
}
