import React from 'react';

import Navigation from '../../components/Navigation/Navigation';
import Notifications from 'react-notify-toast';

import { Layout, NavDrawer, Panel } from 'react-toolbox';
import TopBar from '../TopBar/TopBar';
import styles from './PageChrome.css';

export default class PageChrome extends React.Component {
  constructor (props) {
    super(props);
    this.props = props;

    this.state = {
      drawerActive: false,
      drawerPinned: false
    };
  }

  toggleDrawerActive = () => {
    this.setState({ drawerActive: !this.state.drawerActive });
  };

  toggleDrawerPinned = () => {
    this.setState({ drawerPinned: !this.state.drawerPinned });
  }

  renderNavigation () {
    return (
        <NavDrawer
          active={this.state.drawerActive}
          pinned={this.state.drawerPinned}
          permanentAt='lg'
          onOverlayClick={ this.toggleDrawerActive }>
          <Navigation onClickNavLink={this.toggleDrawerActive} />
        </NavDrawer>
    );
  }

  renderTopBar () {
    return <TopBar toggleDrawer={this.toggleDrawerActive} />;
  }

  renderFooter () {
    if (this.props.hideFooter) return;

    return (
      <footer className={styles.footer}>
        <div className={styles.footerTop} ></div>
        <div className={styles.footerBottom} >&copy; 2016 VSP Inc.</div>
      </footer>
    );
  }

  renderAuth () {
    return (
      <Layout className={styles.pageChrome}>
        <Notifications />
        {this.renderNavigation()}
        <Panel className={styles.panel}>
          {this.renderTopBar()}
          <div>
            {this.props.children}
          </div>
        </Panel>
      </Layout>
    );
  }
  renderNoAuth () {
    return (
      <div className={styles.pageChrome}>
        <Notifications />
        <div className={styles.panel}>
          {this.props.children}
        </div>
      </div>
    );
  }

  render () {
    return this.props.hideNav ? this.renderNoAuth() : this.renderAuth();
  }
}
