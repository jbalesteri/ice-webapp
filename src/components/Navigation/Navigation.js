import React from 'react';

import { Navigation as RTNavigation, Dialog } from 'react-toolbox';

import { inject, observer } from 'mobx-react';

import AccountForm from '../AccountForm/AccountForm';
import NavigationLink from '../NavigationLink/NavigationLink';

const styles = require('./Navigation.css');

@inject('store') @observer
export default class Navigation extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
    this.state = {
      active: false,
      accountFormActive: false
    };
  }

  onClickAccountSettings = () => {
    this.context.router.transitionTo('/account');
  };

  onClickLogOut = () => {
    this.context.router.transitionTo('/logout');
  };

  handleCloseAccountForm = () => {
    this.setState({accountFormActive: false});
  };

  onClickNavLinkOriginal = () => {
    this.setState({active: false});
  }

  closest = (el, selector) => {
    var parent;
    while (el) {
      parent = el.parentElement;
      if (parent && parent.dataset[selector]) {
        return parent;
      }
      el = parent;
    }
    return null;
  }

  onClickNavLink = (evt) => {
    if (evt.target.dataset.to) {
      this.store.history.push(evt.target.dataset.to);
    } else {
      let closestDataToEle = this.closest(evt.target, 'to');
      let linkTarget = closestDataToEle.dataset.to;
      this.store.history.push(linkTarget);
    }

    this.props.onClickNavLink();
  }

  render () {
    let accountFormProps = {
      active: this.state.accountFormActive,
      onCancel: this.handleCloseAccountForm.bind(this),
      onSave: this.handleCloseAccountForm.bind(this)
    };

    return (
      <div>
        <RTNavigation type='vertical' className={styles.drawerNav}>
          <div onClick={this.onClickNavLink.bind(this)}>
            <NavigationLink to='/' label={this.wordStore.translate('Dashboard')} icon='dashboard' id='navigationDashboard' />
            <NavigationLink to='/users' label={this.wordStore.translate('Users')} icon='people' id='navigationUsers' />
            <NavigationLink to='/devices' label={this.wordStore.translate('Devices')} icon='important_devices' id='navigationDevices' />
            <NavigationLink to='/racks' label={this.wordStore.translate('Racks')} icon='storage' id='navigationRacks' />
            <NavigationLink to='/groups' label={this.wordStore.translate('Groups')} icon='group_work' id='navigationGroups' />
            <NavigationLink to='/zones' label={this.wordStore.translate('Zones')} icon='donut_small' id='navigationZones' />
            <NavigationLink to='/feeds' label={this.wordStore.translate('Feeds')} icon='power' id='navigationFeeds' />
            <NavigationLink to='/events' label={this.wordStore.translate('Events')} icon='warning' id='navigationEvents' />
            <NavigationLink to='/healthCheck' label={this.wordStore.translate('Health Check')} icon='local_pharmacy' id='navigationHealth' />
            <NavigationLink to='/dataCenter' label={this.wordStore.translate('Data Center')} icon='settings' id='navigationSettings' />
          </div>
        </RTNavigation>
        <Dialog {...accountFormProps} title='My Account'>
          <AccountForm {...accountFormProps}/>
        </Dialog>
      </div>
    );
  }
}

// Navigation.wrappedComponent.contextTypes = {
//   router: React.PropTypes.object.isRequired
// };
