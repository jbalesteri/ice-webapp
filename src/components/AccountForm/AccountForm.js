import React from 'react';
import { inject, observer } from 'mobx-react';
// import { toJS } from 'mobx';

import { Input, Button } from 'react-toolbox';

import Notify from '../../common/helpers/Notify';

@inject('store') @observer
export default class AccountForm extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.userStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {
      user: this.store.getById(this.props.editId)

    };
  }

  handleChange = (name, value) => {
    let myUser = this.state.user;
    myUser[name] = value;
    this.setState({ user: myUser });
  };

  handleSave = () => {
    let updateObj = {
      email: this.state.user.email,
      role: this.state.user.roles,
      firstName: this.state.user.firstName,
      lastName: this.state.user.lastName
    };

    if (this.state.user.password && this.state.user.password.length > 0) {
      updateObj.password = this.state.user.password;
    }

    if (this.props.editId) {
      updateObj.id = this.props.editId;
    }
    this.store._updateFunction(updateObj).then(() => {
      Notify.success('Account changes saved');
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error saving account changes');
      }
    });
    this.onSave();
  }

  onSave () {
    if (typeof this.props.onSave === 'function') {
      this.props.onSave();
    }
  }

  render () {
    let myUser = this.state.user;
    return (
      <section>
        <Input type='text' label={this.wordStore.translate('First Name')} value={myUser.firstName} onChange={this.handleChange.bind(this, 'firstName')} />
        <Input type='text' label={this.wordStore.translate('Last Name')} value={myUser.lastName} onChange={this.handleChange.bind(this, 'lastName')} />
        <Input type='email' label='Email' value={myUser.email} onChange={this.handleChange.bind(this, 'email')} />
        <Input type='phone' label={this.wordStore.translate('Phone Number')} value={myUser.phoneNumber} onChange={this.handleChange.bind(this, 'password')} />
        <div>Roles: { myUser.roles }</div>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Button icon='save' label='Update Account' raised primary onClick={this.handleSave} style={{marginLeft: '0.5rem'}}/>
        </div>
      </section>
    );
  }
}
