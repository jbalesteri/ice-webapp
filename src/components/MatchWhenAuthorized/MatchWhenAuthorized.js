import React from 'react';
import { Match, Redirect } from 'react-router';
import { inject, observer } from 'mobx-react';

@inject('store') @observer
export default class MatchWhenAuthorized extends React.Component {
  constructor (props) {
    super(props);

    this.store = this.props.store.authStore;
  }

  render () {
    let Component = this.props.component;

    let needsPasswordReset = this.store.needsPasswordReset;

    return (
      <Match {...this.props} render={(props) => {
        if (!this.store.isAuthenticated) {
          return <Redirect to={{
            pathname: '/login',
            state: { from: props.location }
          }} />;
        } else if (needsPasswordReset) {
          return <Redirect to={{
            pathname: '/passwordExpired'
          }} />;
        } else {
          return <Component {...props}/>;
        }
      }} />
    );
  }
}
