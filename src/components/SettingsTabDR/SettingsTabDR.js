var debug = require('debug')('ice:SettingsTabDR'); // eslint-disable-line no-unused-vars

import React from 'react';
import { reaction } from 'mobx';
import { inject, observer } from 'mobx-react';
import { Button, Card, CardText, CardTitle, Input, Switch } from 'react-toolbox';

import FeedTypes from '../../common/constants/FeedTypes';
import Phases from '../../common/constants/Phase';
import Notify from '../../common/helpers/Notify';

let styles = require('./SettingsTabDR.css');

@inject('store') @observer
class SettingsTabDR extends React.Component {
  constructor (props) {
    super(props);

    this.drConfig = this.props.store.dynamicRedundancyStore;
    this.drState = this.props.store.dynamicRedundancyStateStore;
    this.feedStore = this.props.store.feedStore;
    this.wordStore = this.props.store.wordStore;

    let limits = this.parseLimits(this.drConfig.feedLimits);

    this.state = {
      enableDR: this.drConfig.enabled,
      limits: limits
    };
  }

  componentDidMount () {
    this.drState.load();

    this.reaction = reaction(
      () => this.drConfig.toJson,
      () => {
        debug('Updating DR Config State');
        this.setState({
          enableDR: this.drConfig.enabled,
          limits: this.parseLimits(this.drConfig.feedLimits)
        }, () => {
          this.drState.load();
        });
      }
    );
  }

  componentWillUnmount () {
    this.reaction();
  }

  parseLimits (configLimits) {
    let limits = {};

    configLimits.forEach((limit) => {
      let id = limit.feed;
      limit.phases.forEach((phase) => {
        let key = id + '--' + phase.phase;
        limits[key] = phase.limit;
      });
    });

    return limits;
  }

  handleChange = (field, value) => {
    this.setState({ [field]: value });
  };

  handleLimitChange = (feed, limit) => {
    let limits = this.state.limits;
    limits[feed] = limit;
    let newState = {
      limits: limits
    };

    this.setState(newState);
  }

  save = () => {
    this.drConfig.enabled = this.state.enableDR;

    let limits = {};
    Object.keys(this.state.limits).forEach((limit) => {
      let [ id, phase ] = limit.split('--');
      if (!limits[id]) {
        limits[id] = { feed: id, phases: [] };
      }
      limits[id].phases.push({ phase: phase, limit: this.state.limits[limit] });
    });
    limits = Object.keys(limits).map((id) => (limits[id]));

    this.drConfig.feedLimits.replace(limits);

    this.drConfig.save().then(() => {
      Notify.success('Dynamic Redundancy Configuration Updated');
      this.updateDrState();
    }).catch(() => {
      Notify.error('Error updating Dynamic Redundancy Configuration');
      this.updateDrState();
    });
  }

  updateDrState () {
    let x = 0;
    let interval = setInterval(() => {
      this.drState.load();
      x++;
      if (x > 12) {
        clearInterval(interval);
      }
    }, 1000);
  }

  renderFeedLimits () {
    return this.feedStore.all.map((feed) => {
      let ret = [];
      let phases = [Phases.ONE];
      if (feed.type === FeedTypes['3PHASE']) {
        phases = phases.concat([Phases.TWO, Phases.THREE]);
      }
      ret = ret.concat(phases.map((phase, index) => {
        let key = feed.id + '--' + phase;
        let name = feed.name + ' ' + phase;
        let value = this.state.limits[key] || '';

        return (
            <Input
              key={key}
              type='number'
              label={name}
              value={value}
              onChange={this.handleLimitChange.bind(this, key)}
              className={styles.phaseInput}
              data-qa-tag={'phase-' + (index + 1) + '-value'} />
        );
      }));

      return (
        <Card key={feed.id} className={styles.drLimitCard} raised data-qa-tag='dr-limits' data-qa-id={feed.id}>
          <h5>{feed.name} Limits</h5>
          <div className={styles.limitsWrapper}>
            {ret}
          </div>
        </Card>
      );
    });
  }

  renderDrState () {
    if (!this.drState.passed) {
      let messages = this.drState.messages;
      messages = messages.map((message, index) => {
        return (
          <div key={index} className={styles.drWarning}>
            <p className={styles.description}>{message.desc}</p>
            <p className={styles.rootCause}>{message.rootCause}</p>
            <p className={styles.fix}>{message.fix}</p>
          </div>
        );
      });

      return (
        <div className={styles.drWarningList}>
          <h6>Dynamic Redundancy State:</h6>
          {messages}
        </div>
      );
    }
  }

  render () {
    return (
      <Card raised style={{width: '90%', margin: '5rem auto'}}>
        <CardTitle title='Dynamic Redundancy Settings' />
        <CardText>
          <div style={{position: 'relative'}}>
            {this.renderDrState()}
            <Switch
                checked={this.state.enableDR}
                label='Enable DR'
                onChange={this.handleChange.bind(this, 'enableDR')}
                className={styles.drToggle}
              />
            <div className={styles.limitCards}>
              {this.renderFeedLimits()}
            </div>
            <div>
              <Button label={this.wordStore.translate('Save')} onClick={this.save} raised primary />
            </div>
          </div>
        </CardText>
      </Card>
    );
  }
}

export default SettingsTabDR;
