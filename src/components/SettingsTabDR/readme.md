 <!-- File generated on 2016-11-09 by Jay -->

## DynamicRedundancySettingTab


#### Instructions:
Component desc.

##### Example:
```html
<DynamicRedundancySettingTab
	prop={value} 
/ >
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| abc  | bool  |    ?    |      ?       |


###### Images:
----
Standard Implementation: *This will render component without any theming applied to it*

![Alt text](https://placeimg.com/480/320/animals "Standard Implentation")

```html
<DynamicRedundancySettingTab prop={value}  />
```
----

