var debug = require('debug')('ice:DeviceSensorData: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';
let styles = require('./DeviceSensorData.css');

@inject('store') @observer
export default class DeviceSensorData extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.deviceStore;
  }

  render () {
    let id = this.props.selectedDevice.id;
    let device = this.store.getById(id);
    let date = device.sensorData.timestamp;

    let deviceModel = device.model;
    let deviceType = device.type;
    let deviceIp = device.ip;
    let groupSensor = device.groupSensor ? 'yes' : 'no';

    let breakerRating = device.breakerRating;
    let inputPower = device.sensorData.inputPower;
    let inputCurrent = device.sensorData.inputCurrent;
    let inputVoltage = device.sensorData.inputVoltage;
    let outputPower = device.sensorData.outputPower;
    let outputCurrent = device.sensorData.outputCurrent;
    let outputVoltage = device.sensorData.outputVoltage;
    let battOutput = device.sensorData.battPowerOut;
    let battCurrent = device.sensorData.battCurrent;
    let battVoltage = device.sensorData.battVoltage;
    let battTempC = device.sensorData.battTemp;
    let battSocWh = device.sensorData.battSocWH;
    let battSoc = device.sensorData.battSoc;

    return (
      <section className={styles.sensorDataDisplay}>
        <div className={styles.deviceInformation}>
          <ValueWrapper
            value={deviceModel}
            size='med'
            labelFirst={true}
            label='Device Model'/>
          <ValueWrapper
            value={deviceType}
            size='med'
            labelFirst={true}
            label='Device Type'/>
          <ValueWrapper
            value={deviceIp}
            size='med'
            labelFirst={true}
            label='IP Address:Port'/>
          <ValueWrapper
            value={groupSensor}
            size='med'
            labelFirst={true}
            label='Group Sensor'/>
          <ValueWrapper
            value={breakerRating}
            size='med'
            suffix='kW'
            labelFirst={true}
            label='Breaker Rating'/>
        </div>
        <legend>Power Metrics</legend>
        <div className={styles.sensorDataDisplayRow}>
          <div className={styles.column}>
            <ValueWrapper
              value={inputPower}
              suffix='kW'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Input Power'/>
            <ValueWrapper
              value={inputCurrent}
              suffix='A'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Input Current'/>
            <ValueWrapper
              value={inputVoltage}
              suffix='V'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Input Voltage'/>
          </div>
          <div className={styles.column}>
            <ValueWrapper
              value={outputPower}
              suffix='kW'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Output Power'/>
            <ValueWrapper
              value={outputCurrent}
              suffix='A'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Output Current'/>
            <ValueWrapper
              value={outputVoltage}
              suffix='V'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Output Voltage'/>
          </div>
        </div>
        <legend>Battery Status</legend>
        <div className={styles.sensorDataDisplayRow}>
          <div className={styles.column}>
            <ValueWrapper
              value={battOutput}
              suffix='kW'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Battery Output'/>
            <ValueWrapper
              value={battCurrent}
              suffix='A'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Battery Current'/>
            <ValueWrapper
              value={battVoltage}
              suffix='V'
              size='sm'
              labelFirst={true}
              inline={true}
              label='Output Voltage'/>
          </div>
          <div className={styles.column}>
            <ValueWrapper
              value={battTempC}
              suffix={'\u2103'}
              size='sm'
              labelFirst={true}
              inline={true}
              formatNumber={false}
              label='Battery Temp'/>
            <ValueWrapper
              value={battSocWh}
              suffix='Wh'
              size='sm'
              labelFirst={true}
              inline={true}
              formatNumber={false}
              label='Battery SOC'/>
            <ValueWrapper
              value={battSoc}
              suffix='%'
              size='sm'
              labelFirst={true}
              inline={true}
              formatNumber={false}
              label='Battery SOC'/>
          </div>
        </div>
        <ValueWrapper
          value={date.toString()}
          size='sm'
          labelFirst={true}
          inline={true}
          formatNumber={false}
          label='Timestamp'/>
      </section>

    );
  }
}
