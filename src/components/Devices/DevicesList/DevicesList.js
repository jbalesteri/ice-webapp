const debug = require('debug')('ice:Devices:List'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { IconButton } from 'react-toolbox';

import DataTable from '../../DataTable/DataTable';
import ConnectedIndicator from '../../ConnectedIndicator/ConnectedIndicator';
import DeleteConfirm from '../../common/DeleteConfirm/DeleteConfirm';
import TopologyDevice from '../../common/TopologyDevice/TopologyDevice';

@inject('store') @observer
export default class DevicesList extends React.Component {
  constructor (props) {
    super(props);

    this.deviceStore = this.props.store.deviceStore;
    this.wordStore = this.props.store.wordStore;

    this.model = {
      connected: {type: String, title: this.wordStore.translate('Connected')},
      state: {type: String, title: this.wordStore.translate('State')},
      name: {type: String, title: this.wordStore.translate('Name')},
      ip: {type: String, title: this.wordStore.translate('Ip')},
      port: {type: String, title: this.wordStore.translate('Port')},
      type: {type: String, title: this.wordStore.translate('Type')},
      model: {type: String, title: this.wordStore.translate('Model')},
      edit: {type: Boolean, title: this.wordStore.translate('Edit')},
      remove: {type: Boolean, title: this.wordStore.translate('Remove')},
      info: {type: Boolean, title: this.wordStore.translate('Info')}
    };
  }

  edit = (device) => {
    this.props.onEdit(device);
  }

  info = (device) => {
    this.props.onInfo(device);
  }

  remove = (device) => {
    this.props.onRemove(device);
  }

  render () {
    let devices = this.deviceStore.all.map((device) => {
      return {
        id: device.id,
        name: device.name,
        ip: device.ip,
        port: device.port,
        type: device.type,
        model: device.model,
        state: <TopologyDevice state={device.state} type={device.type} />,
        connected: <ConnectedIndicator connected={device.isConnected} />,
        edit: <IconButton icon='edit' onClick={this.edit.bind(this, device)} />,
        remove: <DeleteConfirm onClick={this.remove.bind(this, device)} />,
        info: <IconButton icon='info' onClick={this.info.bind(this, device)} />
      };
    });

    return (
      <DataTable striped model={this.model} source={devices} />
    );
  }
}

DevicesList.wrappedComponent.propTypes = {
  onEdit: React.PropTypes.func.isRequired,
  onInfo: React.PropTypes.func.isRequired,
  onRemove: React.PropTypes.func.isRequired
};
