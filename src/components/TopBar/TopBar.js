var debug = require('debug')('ice:TopBar: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { AppBar, Navigation as RTNavigation, Button, Tooltip } from 'react-toolbox';
import {Logo} from '../../common/helpers/Icons';
import { inject, observer } from 'mobx-react';

import styles from './TopBar.css';
const TooltipButton = Tooltip(Button);

@inject('store') @observer
export default class TopBar extends React.Component {
  toggleDrawer = () => {
    this.props.toggleDrawer();
  }

  renderButton () {
    return <div className={styles.menuToggle}><Button icon='menu' floating primary mini onClick={this.toggleDrawer} /></div>;
  }

  render () {
    return (
      <div data-qa-tag={'top-bar'}>
        <AppBar className={styles.appBar} fixed>
          <div className={styles.appBarInner}>
            <div className={styles.appBarLeft}>
              {this.renderButton()}
              <div className={styles.logoHolder}><Logo /></div>
            </div>
            <div className={styles.appBarRight}>
              <RTNavigation type='horizontal'>
                <div className={styles.rtLinkRight}>
                  <TooltipButton icon='person' floating mini primary tooltip='My Account' onClick={() => this.props.store.history.push('/account')}/>
                  <TooltipButton icon='notifications' floating mini primary tooltip='Events' onClick={() => { this.props.store.history.push('/events'); }} />
                  <TooltipButton icon='help_outline' floating mini primary tooltip='Support' onClick={() => { this.props.store.history.push('/support'); }} />
                  <TooltipButton icon='exit_to_app' floating mini primary tooltip='Logout' onClick={() => { this.props.store.history.push('/logout'); }} />
                </div>
              </RTNavigation>
            </div>
          </div>
        </AppBar>
      </div>
    );
  }
}

// TopBar.wrappedComponent.contextTypes = {
//   router: React.PropTypes.object.isRequired
// };
