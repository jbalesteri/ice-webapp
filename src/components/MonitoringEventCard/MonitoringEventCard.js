var debug = require('debug')('ice:MonitoringEventCard: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import {Avatar, Card, CardText, ProgressBar} from 'react-toolbox';
import ValueWrapper from '../../components/common/ValueWrapper/ValueWrapper';

let styles = require('./MonitoringEventCard.css');

export class ProgressChart extends React.Component {
  render () {
    let val = Number(this.props.value);
    let label = this.props.label;

    return (
      <div className={styles.circleChartWrapper}>
        <div className={styles.dataWrapper}>
          <div className={styles.data}>
            <span style={{display: 'block'}}>{val}%</span>
            <span style={{display: 'block', fontSize: '0.5em'}}>{label}</span>
          </div>
        </div>
        <ProgressBar type='circular' mode='determinate' value={val}/>
      </div>
    );
  }
}

@inject('store') @observer
export default class MonitoringEventCard extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
  }

  render () {
    let usedMem = this.props.store.healthStore.usedMemory ? Math.floor(this.props.store.healthStore.usedMemory * 100) : 'N/A';
    let usedDisk = this.props.store.healthStore.usedDiskSpace ? Math.floor(this.props.store.healthStore.usedDiskSpace[0] * 100) : 'N/A';

    return (
      <Card raised className={styles.MonitoringEventCard}>
        <div data-qa-tag='health-card'>
          <CardText className={styles.iconWrapper}>
          <div className={styles.events}>
            <div className={styles.eventType}>
              <div className={styles.eventWrapper}>
                <Avatar className={styles.colorError} icon='notifications' />
                <ValueWrapper
                  value={this.props.store.eventStore.criticalCount}
                  label={this.wordStore.translate('critical')}
                  size='title'
                  formatNumber={false}
                  labelFirst={false} />
              </div>
              <div className={styles.eventWrapper}>
                <Avatar className={styles.colorWarning} icon='notifications' />
                <ValueWrapper
                  value={this.props.store.eventStore.warningCount}
                  label={this.wordStore.translate('warnings')}
                  size='title'
                  formatNumber={false}
                  labelFirst={false} />
              </div>
              <div className={styles.eventWrapper}>
                <Avatar className={styles.colorSuccess} icon='notifications' />
                <ValueWrapper
                  value={this.props.store.eventStore.warningCount}
                  label={this.wordStore.translate('info')}
                  size='title'
                  formatNumber={false}
                  labelFirst={false} />
              </div>
            </div>
          </div>
          <div className={styles.usage}>
            <ProgressChart value={usedMem} label='mem' />
            <ProgressChart value={usedDisk} label='disk' />
          </div>
        </CardText>
        </div>
      </Card>
    );
  }
}
