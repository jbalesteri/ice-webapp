import React from 'react';

import ProgressBar from 'react-toolbox/lib/progress_bar';
import Table from '../Table/Table';

const styles = require('./DataTable.css');

export default class DataTable extends React.Component {
  constructor (props) {
    super(props);
    this.striped = this.props.striped;
  }

  handleRowClick = (data) => {
    if (typeof this.props.onRowClick === 'function') {
      this.props.onRowClick(data);
    }
  }

  render () {
    let loading = this.props.loading;
    let source = this.props.source;
    let model = this.props.model;
    let noItems = this.props.noItems;

    if (loading) {
      return <ProgressBar type='circular' mode='indeterminate' />;
    }

    if (source.length === 0) {
      return <div className={styles.noItemsWrapper}>{noItems}</div>;
    }

    if (this.props.striped || this.props.stiped === true) {
      return (
      <div className={styles.tableContainer}>
        <div className={ styles.striped }>
          <Table model={model} heading={true} source={source} onRowClick={this.handleRowClick} />
        </div>
      </div>
      );
    } else {
      return (
        <div>
          <Table model={model} heading={true} source={source} onRowClick={this.handleRowClick} />
        </div>
      );
    }
  }
}

DataTable.propTypes = {
  loading: React.PropTypes.bool,
  source: React.PropTypes.array,
  model: React.PropTypes.object.isRequired,
  noItems: React.PropTypes.string,
  onRowClick: React.PropTypes.func
};

DataTable.defaultProps = {
  loading: false,
  data: [],
  noItems: 'No items found'
};
