var debug = require('debug')('ice:ZoneMetrics: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';
let styles = require('./ZoneMetrics.css');

@inject('store') @observer
export default class ZoneMetrics extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.zoneStore;
  }

  render () {
    let zoneId = this.props.selectedZone;
    let zone = this.store.getById(zoneId);
    let data = zone && zone.metrics;
    let maxInput = data && data.maxInputPowerW ? data.maxInputPowerW / 1000 : 'N/A';
    let inputPower = data && data.instantaneousInputPowerW ? data.instantaneousInputPowerW / 1000 : 'N/A';
    let maxUtil = data && data.maxUtilizationProp ? data.maxUtilizationProp / 1000 : 'N/A';
    let availCap = data && data.availableCapacityW ? data.availableCapacityW / 1000 : 'N/A';
    let powerLimits = data && data.powerLimits ? data.powerLimits : 'N/A';
    let soc = data && data.averageSocProp ? data.averageSocProp * 100 : 'N/A';
    let renderPowerLimits;
    let phasePower;

    if (data && data.powerLimits) {
      renderPowerLimits = powerLimits.map((pLimit, index) => {
        let feed = this.props.store.feedStore.getById(pLimit.feed);
        let limit = pLimit.limitW;
        return <ValueWrapper size='med' labelFirst={true} value={limit} suffix='kW' label={feed.name} key={index}/>;
      });
    }

    if (data && data.phasePowerIn) {
      phasePower = data.phasePowerIn.map((ppi) => {
        let phase = ppi.phase ? ppi.phase : 'N/A';
        let input = ppi.instantaneousW ? ppi.instantaneousW / 1000 : 'N/A';
        let max = ppi.maxW ? ppi.maxW / 1000 : 'N/A';
        let breaker = ppi.breakerLimitW ? ppi.breakerLimitW : 'N/A';
        return (
          <div key={ppi.phase} className={styles.phaseColumn}>
            <ValueWrapper size='sm' inline={true} labelFirst={true} label='Phase' value={phase} />
            <ValueWrapper size='sm' inline={true} labelFirst={true} label='Input' value={input} suffix='kW' />
            <ValueWrapper size='sm' inline={true} labelFirst={true} label='Max' value={max} suffix='kW' />
            <ValueWrapper size='sm' inline={true} labelFirst={true} label='Breaker Limit' value={breaker} suffix='kW' />
          </div>
        );
      });
    }

    return (
      <div className={styles.zoneMetrics}>
        <div className={styles.row}>
          <ValueWrapper
            size='med'
            labelFirst={true}
            value={maxInput}
            suffix='kW'
            label='Max Input' />
          <ValueWrapper
            size='med'
            labelFirst={true}
            value={inputPower}
            suffix='kW'
            label='Input Power' />
          <ValueWrapper
            size='med'
            labelFirst={true}
            value={maxUtil}
            suffix='kW'
            label='Max Utilization' />
          <ValueWrapper
            size='med'
            labelFirst={true}
            value={availCap}
            suffix='kW'
            label='Available Capacity' />
        </div>
        <div className={styles.row}>
          <div className={styles.powerLimits}>
            <legend>Power Limits</legend>
            <div className={styles.row}>{renderPowerLimits}</div>
          </div>
          <div className={styles.stateOfCharge}>
            <ValueWrapper
              size='med'
              labelFirst={true}
              value={soc}
              suffix='%'
              label='State of Charge' />
          </div>
        </div>
        <div>
          <legend>Phase Power</legend>
          <div className={styles.row}>
            {phasePower}
          </div>
        </div>
      </div>
    );
  }
}
