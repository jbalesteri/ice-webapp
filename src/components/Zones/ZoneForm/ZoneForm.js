var debug = require('debug')('ice:zones:ZoneForm'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Dropdown, Input, Dialog } from 'react-toolbox';

import Validators from '../../../common/helpers/Validators';
import Notify from '../../../common/helpers/Notify';
import FormWrapper from '../../common/FormWrapper/FormWrapper';

const defaultState = {
  name: '',
  nameError: '',
  zone: null,
  type: 'row',
  allocatedPower: 0
};

const zoneTypes = [
  { value: 'row', label: 'Row' }
];

@inject('store') @observer
export default class ZoneForm extends React.Component {
  constructor (props) {
    super(props);

    this.zoneStore = this.props.store.zoneStore;
    this.wordStore = this.props.store.wordStore;
    this.state = defaultState;
  }

  componentWillReceiveProps (nextProps) {
    let newState = Object.assign({}, defaultState);

    if (nextProps.zoneId) {
      let zone = this.zoneStore.getById(nextProps.zoneId);

      newState.zone = zone;
      newState.name = zone.name;
      newState.allocatedPower = zone.allocatedPower;
      newState.type = 'row';
    } else if (nextProps.active) {
      newState.zone = this.zoneStore.createZone();
    }

    this.setState(newState);
  }

  validate = (field, value) => {
    let error = '';
    switch (field) {
      case 'name':
        let names = this.zoneStore.all.map((zone) => (zone.name));
        let originalName = (this.state.zone) ? this.state.zone.name : null;
        error = Validators.zone.name(value, names, originalName);
        break;

      case 'type':
        error = Validators.zone.type(value);
        break;

      case 'allocatedPower':
        error = Validators.zone.allocatedPower(value);
        break;
    }
    return error;
  }

  isValid = () => {
    let p = new Promise((resolve, reject) => {
      let newState = {};
      ['name', 'type', 'allocatedPower'].forEach((field) => {
        let value = this.state[field];
        newState[field + 'Error'] = this.validate(field, value);
      });

      this.setState(newState, () => {
        if (this.state.nameError === '' && this.state.name !== '') {
          resolve();
        } else {
          reject(new Error('form not valid'));
        }
      });
    });

    return p;
  }

  handleChange = (name, value) => {
    let nameErrorMesg = '';
    let typeErrorMesg = '';
    let allocatedPowerErrorMesg = '';

    switch (name) {
      case 'name':
        nameErrorMesg = this.validate(name, value);
        break;

      case 'type':
        typeErrorMesg = this.validate(name, value);
        break;

      case 'allocatedPower':
        allocatedPowerErrorMesg = this.validate(name, value);
        break;
    }
    this.setState({
      [name]: value,
      nameErrorMesg,
      typeErrorMesg,
      allocatedPowerErrorMesg

    });
  }

  save = () => {
    this.isValid().then(() => {
      let zone = this.state.zone;

      zone.name = this.state.name;
      zone.type = this.state.type;
      zone.allocatedPower = this.state.allocatedPower;

      zone.save().then(() => {
        Notify.success('Zone saved');
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error saving zone');
        }
      });

      this.props.onSave();
    });
  }

  cancel = () => {
    this.props.onCancel();
  }

  render () {
    let saveButtonDisabled = false;
    if (this.state.nameErrorMesg && this.state.nameErrorMesg.length > 0) {
      saveButtonDisabled = true;
    }

    if (this.state.typeErrorMesg && this.state.typeErrorMesg.length > 0) {
      saveButtonDisabled = true;
    }

    if (this.state.allocatedPowerErrorMesg && this.state.allocatedPowerErrorMesg.length > 0) {
      saveButtonDisabled = true;
    }

    let title = (this.state.zone) ? this.wordStore.translate('Edit Zone') : this.wordStore.translate('Add Zone');

    let actions = [
      { label: this.wordStore.translate('Cancel'), onClick: this.cancel },
      { label: this.wordStore.translate('Save'), onClick: this.save, disabled: saveButtonDisabled }
    ];

    return (
      <Dialog title={title} actions={actions} active={this.props.active} onEscKeyDown={this.cancel}>
        <FormWrapper onSubmit={this.save} >
          <Input
            required
            type='text'
            error={this.state.nameErrorMesg}
            label={this.wordStore.translate('Name')}
            value={this.state.name}
            onChange={this.handleChange.bind(this, 'name')} />
          <Dropdown
             auto
             required
             error={this.state.typeErrorMesg}
             label={this.wordStore.translate('Type')}
             onChange={this.handleChange.bind(this, 'type')}
             source={zoneTypes}
             value={this.state.type} />
          <Input
            required
            type='number'
            error={this.state.allocatedPowerErrorMesg}
            label={this.wordStore.translate('Allocated Power')}
            value={this.state.allocatedPower} onChange={this.handleChange.bind(this, 'allocatedPower')} />
        </FormWrapper>
      </Dialog>
    );
  }
}

ZoneForm.wrappedComponent.propTypes = {
  zoneId: React.PropTypes.string,
  active: React.PropTypes.bool.isRequired,
  onCancel: React.PropTypes.func,
  onSave: React.PropTypes.func
};
