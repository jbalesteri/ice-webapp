const debug = require('debug')('ice:racks:list'); // eslint-disable-line no-unused-vars

import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { IconButton } from 'react-toolbox';

import DataTable from '../../DataTable/DataTable';

@inject('store') @observer
export default class ZonesList extends Component {
  constructor (props) {
    super(props);

    this.zoneStore = this.props.store.zoneStore;
    this.wordStore = this.props.store.wordStore;

    this.ZonesModel = {
      name: { type: String, title: this.wordStore.translate('Name') },
      type: { type: String, title: this.wordStore.translate('Type') },
      allocated: { type: String, title: this.wordStore.translate('Allocated Power') },
      racks: { type: String, title: this.wordStore.translate('Racks') },
      groups: { type: String, title: this.wordStore.translate('Groups') },
      edit: { type: Boolean, title: this.wordStore.translate('Edit') },
      configure: { type: Boolean, title: this.wordStore.translate('Configure') },
      remove: { type: Boolean, title: this.wordStore.translate('Remove') },
      info: { type: Boolean, title: this.wordStore.translate('Info') }
    };
  }

  edit = (zone) => {
    this.props.onEdit(zone);
  }

  remove = (zone) => {
    this.props.onRemove(zone);
  }

  config = (zone) => {
    this.props.onConfig(zone);
  }

  info = (zone) => {
    this.props.onInfo(zone);
  }

  render () {
    let zones = this.zoneStore.all.map((zone) => {
      return {
        id: zone.id,
        name: zone.name,
        type: zone.type,
        allocated: zone.allocatedPower,
        racks: (zone.rackIds) ? zone.rackIds.length : 0,
        groups: (zone.groups) ? zone.groups.length : 0,
        edit: <IconButton icon='edit' data-qa-tag='editButton' onClick={this.edit.bind(this, zone)} />,
        configure: <IconButton icon='settings' data-qa-tag='configButton' onClick={this.config.bind(this, zone)} />,
        remove: <IconButton icon='delete' data-qa-tag='removeButton' onClick={this.remove.bind(this, zone)} />,
        info: <IconButton data-qa-tag='infoButton' icon='info' onClick={this.info.bind(this, zone)} />
      };
    });

    return <DataTable striped model={this.ZonesModel} source={zones} />;
  }
}

ZonesList.wrappedComponent.propTypes = {
  onEdit: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onConfig: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired
};
