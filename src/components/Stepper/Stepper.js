// File generated on 2016-11-09 by Jay
var debug = require('debug')('ice:AppStore'); // eslint-disable-line no-unused-vars

import React from 'react';

import {Button, FontIcon} from 'react-toolbox';

import ClassNames from 'classnames/bind';

let styles = require('./Stepper.css');

// use classnames module to push multiple classes to css modules
var classBind = ClassNames.bind(styles);

// utilities for stepper sizing and rendering
import Utils from './stepperUtils';

class Stepper extends React.Component {
  constructor (props) {
    super(props);
    this.type = this.props.type;

    this.state = {
      documentSize: Utils.Client.documentSize(),
      selected: props.currentStep,
      maxSteps: props.steps.length,
      currentStep: props.currentStep,
      activeColor: props.activeColor,
      inactiveColor: props.inactiveColor
    };
  }

  componentDidMount () {
    let self = this;

    // check document size (used to automatically switch to verical layout)
    window.addEventListener('resize', function () {
      self.setState({
        documentSize: Utils.Client.documentSize()
      });
    }, true);
  }

  // determine if the stepper is linear or not
  selectStepper (callback, index) {
    if (this.props.linear === false) {
      this.setState({
        selected: index,
        currentStep: index
      });

      if (callback) {
        return callback();
      }
    }
  }

  // forward one slide
  continueStepper (callback) {
    let nextStep = this.state.currentStep + 1;

    this.setState({
      selected: nextStep,
      currentStep: nextStep
    });

    if (callback) {
      return callback(nextStep);
    }
  }

  // back one slide
  backStepper (callback) {
    let nextStep = this.state.currentStep - 1;
    this.setState({
      selected: nextStep,
      currentStep: nextStep
    });

    if (callback) {
      return callback(nextStep);
    }
  }

  // horizontal layout
  renderHorizontal () {
    debug('render horizontal!');

    let self = this;
    let stepsHeader = null;
    let stepsContent = null;
    let steps = this.props.steps;

    if (steps.length > 0) {
      stepsHeader = steps.map(function (item, key) {
        if (!item.title) return;

        // join styles for horizontal layout
        let horizontalStyles = () => {
          return classBind({
            progress: self.state.currentStep > key,
            active: self.state.selected === key
          });
        };
        // join icon styles
        let iconStyles = () => {
          return classBind({
            stepIcon: true,
            iconClass: (self.state.currentStep > key || self.state.selected === key ? self.state.activeColor : ''),
            iconBackground: (self.state.currentStep < key ? self.state.inactiveColor : '')
          });
        };

        // join styles for titles
        let titleStyle = () => {
          return classBind({
            title: true,
            hasOptional: item.optional
          });
        };

        return (
          <li
            key={'stepper-' + key}
            onClick={self.selectStepper.bind(self, item.callback, key)}
            className={ horizontalStyles() }>
            <a>
              <span className={ iconStyles() }>
                {self.state.currentStep > key ? <FontIcon value='check' className={styles.iconActionDone} /> : (key + 1)}</span>
                <span className={ titleStyle() }>
                {item.title}
                {item.optional ? (<div className={styles.optional}>{item.optional}</div>) : null}
                </span>
                {(steps.length - 1) !== key && item.title ? <span className={styles.connector} /> : null}
            </a>
          </li>
        );
      });

      stepsContent = steps.map(function (item, key) {
        let active = (self.state.selected === key);

        let stepContentStyles = () => {
          return classBind({
            stepperContent: true,
            active: active
          });
        };

        return (
          <div key={'stepper-content-' + key} className={stepContentStyles()}>
            {item.content}
          </div>
        );
      });

      // return steps
      return (
          <div>
              <nav className={styles.steppers}>
                  <ul className={styles.steppersList} style={{ padding: '0' }} >
                      {stepsHeader}
                  </ul>
              </nav>
              <div className={styles.steppersContainer}>
                  {stepsContent}
                  {self.renderActions()}
              </div>
          </div>
      );
    }
  }

  // vertical layout
  renderVertical () {
    debug('render vertical!');

    let self = this;
    let stepsItems = null;
    let steps = this.props.steps;

    if (steps.length > 0) {
      stepsItems = steps.map(function (item, key) {
        if (!item.title) return;

        // join styles for vertical layout
        let verticalStyles = () => {
          return classBind({
            progress: self.state.currentStep > key,
            active: self.state.selected === key
          });
        };

        // join icon styles
        let iconStyles = () => {
          return classBind({
            stepIcon: true,
            iconClass: (self.state.currentStep > key || self.state.selected === key ? self.state.activeColor : ''),
            iconBackground: (self.state.currentStep < key ? self.state.inactiveColor : '')
          });
        };

        // join content styles
        let contentStyles = () => {
          return classBind({
            stepperContent: true,
            active: (self.state.selected === key)
          });
        };

        // return steps - vertical
        return (
          <div key={'stepper-' + key} className={ verticalStyles() }>
            <div className={ styles.steppersList }>
              <span className={ iconStyles() }>
                {self.state.currentStep > key ? <FontIcon value='check' className={ styles.iconActionDone } /> : (key + 1)}</span>
                <span className={styles.title}>
                {item.title}
                {item.optional ? (<div className={styles.optional}>{item.optional}</div>) : null}
                </span>
            </div>
            <div className={styles.steppersContainer}>
              <div className={ contentStyles() }>
                {(steps.length - 1) !== key ? <span className={styles.connector} /> : null}
                {item.content}
                {self.renderActions()}
              </div>
            </div>
          </div>
        );
      });

      // bind classes for vertical layout
      let vertStepperStyle = () => {
        return classBind({
          steppers: true,
          vertical: true
        });
      };

      return <div className={vertStepperStyle()}>{stepsItems}</div>;
    }
  }

  renderActions () {
    if (this.props.steps.length > 0) {
      return (
        <div className={styles.stepActions}>
          {
            (this.state.documentSize <= 3 && this.state.currentStep > 0) || (this.props.onBack && this.state.currentStep > 0)
            ? (<Button
              onClick={this.backStepper.bind(this, this.props.onBack)}
              label='BACK'
              raised />)
            : null
          }
          {
            (this.state.documentSize <= 3 && this.state.currentStep < (this.state.maxSteps - 1)) || (this.props.onContinue && this.state.currentStep < this.state.maxSteps)
            ? (<Button
                onClick={this.continueStepper.bind(this, this.props.onContinue)}
                label='CONTINUE'
                raised
                primary />)
            : null
          }
        </div>
      );
    }
  }

  render () {
    debug('maxSteps: ', this.state.maxSteps);
    if (this.state.documentSize === 1) {
      return this.renderVertical();
    }
    return (this.props.type === 'vertical') ? this.renderVertical() : this.renderHorizontal();
  }
}

Stepper.defaultProps = {
  steps: {},
  type: 'horizontal',
  activeColor: 'blue',
  inactiveColor: '#ccc',
  linear: false,
  currentStep: 0,
  onContinue: null,
  onBack: null
};

export default Stepper;
