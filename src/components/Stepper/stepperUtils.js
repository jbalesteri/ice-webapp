// browser sniff for mobile device to ensure proper rendering
function isMobile () {
  if (navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/Windows Phone/i)
    ) {
    return true;
  } else {
    return false;
  }
}

// set screen size
function screenSize () {
  var screenWidth = screen.width;

  if (screenWidth <= 360) {
    return 1; // "small";
  } else if (screenWidth > 360 && screenWidth <= 800) {
    return 2; // "medium";
  } else {
    return 3; // "large";
  }
}

// set document size (element not window)
function documentSize () {
  var docSize = document.querySelector('body').offsetWidth;

  if (docSize <= 480) {
    return 1; // "small";
  } else if (docSize > 480 && docSize <= 768) {
    return 2; // "medium";
  } else {
    return 3; // "large";
  }
}

export default {
  Client: {
    isMobile: isMobile,
    screenSize: screenSize,
    documentSize: documentSize
  }
};
