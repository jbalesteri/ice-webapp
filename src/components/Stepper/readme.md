 <!-- File generated on 2016-11-09 by Jay -->

## Stepper


#### Instructions:
Stepper will help guid the user through a series of steps. 

##### Example:
You will need to import the Stepper inside of whatever component/view it is to be rendered in:

```js
import Stepper from '[path-to-components]'
```
After import you will be able to use it and pass various props to the stepper:

```html
<Stepper
	prop={value} 
/ >
```
Additionally you will have to declare the steps in the component/view where you returned the Stepper:

```js
const Steps = {
  'stepperTest': [
    {
      'title': (<span>Step Number One</span>),
      'optional': (<span>Optional Sub-title</span>),
      'content': (<span>[content]</span>)
    },
    {
      'title': (<span>Step Number Two</span>),
      'optional': (<span>Optional Sub-title</span>),
      'content': (<span>[content]</span>)
    },
    {
      'title': (<span>Step Number Three</span>),
      'optional': (<span>Optional Sub-title</span>),
      'content': (<span>[content]</span>)
    }
  ]
};
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| steps  | data  | undefined | Can be used to render steps, better to use JSON |
| activeColor | hex code | primary color | Used to override active color |
| inactiveColor | hex code | neutral color | Used to override inactive color |
| linear | bool | false | Linear forces steps first to last with no ability to skip around  |
| type | string | horizontal | Horizontal or vertical layout ('horizontal' / 'vertical')|
| currentStep | num | 0 | Will start on first step unless specified |
| onContinue | func | undefined | Callback for continue |
| onBack | func | undefined | Callback for back |



##### Images:
----
#####Horizontal: *This will render component horizontally*

```html
<Stepper linear={false} steps={Steps['stepperTest']} onContinue={this.continueStep} onBack={this.backStep} />
```

######Light theme: *Theme is automatically appied with global config*

![Alt text](assets/docs/images/stepper-h-light.png?raw=true "Horizontal Light Theme")


######Dark theme:*Theme is automatically appied with global cofig*

![Alt text](assets/docs/images/stepper-h-dark.png?raw=true "Horizontal Light Theme")

----

#####Vertical: *This will render component vertically*

```html
<Stepper type={vertical} linear={false} steps={Steps['stepperTest']} onContinue={this.continueStep} onBack={this.backStep} />
```

######Light theme: *Theme is automatically appied with global config*
![Alt text](assets/docs/images/stepper-v-light.png?raw=true "Vertical Implentation Light")

######Dark theme: *Theme is automatically appied with global config*
![Alt text](assets/docs/images/stepper-v-dark.png?raw=true "Vertical Implentation Dark")



