import React from 'react';

import { FontIcon } from 'react-toolbox';
import ClassNames from 'classnames/bind';
let styles = require('./Accordion.css');
var css = ClassNames.bind(styles);

export default class CollapsingPanel extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      open: false
    };
  }

  togglePanel () {
    this.setState({
      open: !this.state.open
    });
  }

  render () {
    let collapseStyles = () => {
      return css({
        collapsingPanel: true,
        open: this.state.open,
        cardSection: this.props.cardSection
      });
    };

    let accordionIcon = () => {
      if (this.state.open === false) {
        return 'keyboard_arrow_down';
      } else {
        return 'keyboard_arrow_up';
      }
    };

    let panelStyles = () => {
      return css({
        panelHead: true,
        withTitle: this.props.title
      });
    };

    return (
      <div className={collapseStyles()}>
        <button className={styles.collapseButton}></button>
        <div className={panelStyles()} onClick={this.togglePanel.bind(this)}>
          {this.props.title ? <span className={styles.title}>{this.props.title}</span> : null}
          {this.props.data ? <span className={styles.data}>{this.props.data}</span> : null}
          <div className={styles.iconHolder}>
            <FontIcon value={accordionIcon()} style={{color: styles.colorPrimary}}/>
          </div>
        </div>
        <div className={styles.articlewrap}>
          <div className={styles.article}>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}
