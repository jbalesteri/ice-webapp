var debug = require('debug')('ice:Accordion: '); // eslint-disable-line no-unused-vars

import React from 'react';
let styles = require('./Accordion.css');

export default class Accordion extends React.Component {
  render () {
    return (
      <div data-qa-tag={'accordion'} className={styles.accordion}>
        {this.props.title ? <h5>{this.props.title}</h5> : null}
        {this.props.children}
      </div>
    );
  }
}
