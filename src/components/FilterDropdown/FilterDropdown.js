// File generated on 2016-11-28 by Jay

import React from 'react';

import {Dropdown} from 'react-toolbox';

const source = [
  { value: 'criteria1', label: 'Criteria 1' },
  { value: 'criteria2', label: 'Criteria 2' },
  { value: 'criteria3', label: 'Criteria 3' },
  { value: 'criteria4', label: 'Criteria 4' }
];

export default class FilterDropdown extends React.Component {
  state = { value: 'criteria1' };

  handleChange = (value) => {
    this.setState({value: value});
  };

  render () {
    return (
      <Dropdown
        auto
        label='Filter'
        onChange={this.handleChange}
        source={source}
        value={this.state.value}
      />
    );
  }
}
