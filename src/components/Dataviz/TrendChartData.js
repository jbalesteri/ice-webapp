const debug = require('debug')('ice:validators'); // eslint-disable-line no-unused-vars

const totalTimePoints = 60;
const startHour = 6;

export const outputRawData = () => { // eslint-disable-line no-unused-vars
  let _outputData = generateTrendChartData(120, 30);
  return _outputData;
};

export const loadRawData = () => { // eslint-disable-line no-unused-vars
  let _loadData = generateTrendChartData(100, 20);
  return _loadData;
};

export const phase3RawData = () => { // eslint-disable-line no-unused-vars
  let _phase3Data = generateTrendChartData(30, 5);
  return _phase3Data;
};

export const phase2RawData = () => { // eslint-disable-line no-unused-vars
  let _phase2Data = generateTrendChartData(30, 5);
  return _phase2Data;
};

export const phase1RawData = () => { // eslint-disable-line no-unused-vars
  let _phase1Data = generateTrendChartData(30, 5);
  return _phase1Data;
};

// Helper function to genarate array of timeseries objects - {'time PST': '', value: ''}
let generateTrendChartData = (uVal, lVal) => {
  let formattedMin = '';
  let graphPoint = '';
  let retData = [];
  for (var i = 0; i <= totalTimePoints; i++) {
    formattedMin = i < 10 ? '0' + i : i;
    graphPoint = Math.floor((Math.random() * uVal) + lVal);
    retData.push(
      {
        'time PST': startHour.toString() + ':' + formattedMin.toString(),
        'value': graphPoint
      }
    );
  };
  debug('---------retData [generateTrendChartData] ---------');
  debug(retData);
  return retData;
};

export const sensorSummaryDataMin = () => { // eslint-disable-line no-unused-vars
  /* eslint-disable quotes */
  let zoneSummaryMinInterval = [
    {
      'timestamp': 1484761860000,
      'outputPowerW': 20085,
      'battAvgSocProp': 0.7246428571428573,
      'powerLimitW': 20000,
      'outputPower1W': 23137.751003333335,
      'outputPower2W': 26936.94715714286,
      'outputPower3W': 24250.2386852381
    },
    {
      'timestamp': 1484762040000,
      'outputPowerW': 29163.285714285714,
      'battAvgSocProp': 0.7835714285714286,
      'powerLimitW': 19999.14285714286,
      'outputPower1W': 14572.874173809523,
      'outputPower2W': 27003.93983714286,
      'outputPower3W': 21759.34324047619
    },
    {
      'timestamp': 1484762220000,
      'outputPowerW': 24049.322033898305,
      'battAvgSocProp': 0.7432415254237288,
      'powerLimitW': 19926.70338983051,
      'outputPower1W': 21767.379819661015,
      'outputPower2W': 22107.96630398305,
      'outputPower3W': 23375.229909745765
    },
    {
      'timestamp': 1484762280000,
      'outputPowerW': 23693.683333333334,
      'battAvgSocProp': 0.7465624999999999,
      'powerLimitW': 19999.25,
      'outputPower1W': 21738.33290113889,
      'outputPower2W': 22335.08456558334,
      'outputPower3W': 22198.0752085
    },
    {
      'timestamp': 1484762340000,
      'outputPowerW': 23710.308333333334,
      'battAvgSocProp': 0.7404583333333334,
      'powerLimitW': 19999.175,
      'outputPower1W': 23313.69263997222,
      'outputPower2W': 22047.95591413889,
      'outputPower3W': 22474.389213083334
    },
    {
      'timestamp': 1484762400000,
      'outputPowerW': 25200.658333333333,
      'battAvgSocProp': 0.7466666666666666,
      'powerLimitW': 19999.175,
      'outputPower1W': 22571.40178572222,
      'outputPower2W': 22774.36640663889,
      'outputPower3W': 22835.728802805555
    },
    {
      'timestamp': 1484762460000,
      'outputPowerW': 25636.291666666664,
      'battAvgSocProp': 0.7438333333333333,
      'powerLimitW': 19999.175,
      'outputPower1W': 22656.74835797222,
      'outputPower2W': 22295.605012527776,
      'outputPower3W': 22277.367229666663
    },
    {
      'timestamp': 1484762520000,
      'outputPowerW': 24429.525,
      'battAvgSocProp': 0.745125,
      'powerLimitW': 19999,
      'outputPower1W': 22289.491098722225,
      'outputPower2W': 23288.903593305557,
      'outputPower3W': 22261.816555194444
    },
    {
      'timestamp': 1484762580000,
      'outputPowerW': 24167.525,
      'battAvgSocProp': 0.7434791666666667,
      'powerLimitW': 19999,
      'outputPower1W': 22336.412315499998,
      'outputPower2W': 22514.55158102778,
      'outputPower3W': 22877.184990555557
    },
    {
      'timestamp': 1484762640000,
      'outputPowerW': 24192.716666666667,
      'battAvgSocProp': 0.7374583333333333,
      'powerLimitW': 19999.35,
      'outputPower1W': 22442.32033605556,
      'outputPower2W': 22549.91485130556,
      'outputPower3W': 22235.689071305555
    },
    {
      'timestamp': 1484762700000,
      'outputPowerW': 24936.7,
      'battAvgSocProp': 0.7487083333333333,
      'powerLimitW': 19999.35,
      'outputPower1W': 23949.620745222222,
      'outputPower2W': 22558.980758722224,
      'outputPower3W': 22727.26597566667
    },
    {
      'timestamp': 1484762760000,
      'outputPowerW': 24993.833333333332,
      'battAvgSocProp': 0.7364375000000001,
      'powerLimitW': 19999.35,
      'outputPower1W': 22905.913875305552,
      'outputPower2W': 23117.937679666666,
      'outputPower3W': 22028.165142722224
    },
    {
      'timestamp': 1484762820000,
      'outputPowerW': 23762.61666666667,
      'battAvgSocProp': 0.7304583333333334,
      'powerLimitW': 19999.35,
      'outputPower1W': 21454.05510447222,
      'outputPower2W': 22278.819062222217,
      'outputPower3W': 21931.00593297222
    },
    {
      'timestamp': 1484762880000,
      'outputPowerW': 23760.833333333332,
      'battAvgSocProp': 0.7510416666666666,
      'powerLimitW': 19999.35,
      'outputPower1W': 21911.54838636111,
      'outputPower2W': 22175.11379916667,
      'outputPower3W': 22215.888669666667
    },
    {
      'timestamp': 1484762940000,
      'outputPowerW': 23563.158333333333,
      'battAvgSocProp': 0.7424791666666667,
      'powerLimitW': 19999.35,
      'outputPower1W': 23006.393809333335,
      'outputPower2W': 23693.982942055554,
      'outputPower3W': 23094.01461861111
    },
    {
      'timestamp': 1484763000000,
      'outputPowerW': 23828.125,
      'battAvgSocProp': 0.733625,
      'powerLimitW': 19999.325,
      'outputPower1W': 24131.756905388895,
      'outputPower2W': 22492.722567694444,
      'outputPower3W': 21469.766562222223
    },
    {
      'timestamp': 1484763060000,
      'outputPowerW': 24634.358333333334,
      'battAvgSocProp': 0.7460833333333332,
      'powerLimitW': 19999.375,
      'outputPower1W': 21196.871339583333,
      'outputPower2W': 21589.381774694444,
      'outputPower3W': 22956.38646102778
    },
    {
      'timestamp': 1484763120000,
      'outputPowerW': 24090.391666666666,
      'battAvgSocProp': 0.7610416666666667,
      'powerLimitW': 19999,
      'outputPower1W': 22671.892834333335,
      'outputPower2W': 21522.126302750003,
      'outputPower3W': 21888.57137386111
    },
    {
      'timestamp': 1484763180000,
      'outputPowerW': 24175.958333333336,
      'battAvgSocProp': 0.7463333333333333,
      'powerLimitW': 19999.175,
      'outputPower1W': 21330.303942611114,
      'outputPower2W': 22271.952580555557,
      'outputPower3W': 21399.74623147222
    },
    {
      'timestamp': 1484763240000,
      'outputPowerW': 24727.011904761905,
      'battAvgSocProp': 0.737827380952381,
      'powerLimitW': 19999.25,
      'outputPower1W': 22249.112065158726,
      'outputPower2W': 23020.82088920635,
      'outputPower3W': 23437.502309880954
    },
    {
      'timestamp': 1484763300000,
      'outputPowerW': 22775.85714285714,
      'battAvgSocProp': 0.7424999999999999,
      'powerLimitW': 19999,
      'outputPower1W': 21886.216051428568,
      'outputPower2W': 21535.053419999997,
      'outputPower3W': 24520.773865714284
    },
    {
      'timestamp': 1484764020000,
      'outputPowerW': 27084.333333333332,
      'battAvgSocProp': 0.71875,
      'powerLimitW': 19999.833333333332,
      'outputPower1W': 26124.82012888889,
      'outputPower2W': 17293.753791111114,
      'outputPower3W': 19796.83002611111
    },
    {
      'timestamp': 1484764620000,
      'outputPowerW': 23023.833333333336,
      'battAvgSocProp': 0.7520833333333332,
      'powerLimitW': 19999.166666666668,
      'outputPower1W': 21913.08053111111,
      'outputPower2W': 18395.389921666665,
      'outputPower3W': 19603.26961666667
    },
    {
      'timestamp': 1484764740000,
      'outputPowerW': 19211.444444444445,
      'battAvgSocProp': 0.8169444444444445,
      'powerLimitW': 17885.666666666668,
      'outputPower1W': 23801.794799629628,
      'outputPower2W': 17939.20258296296,
      'outputPower3W': 16956.91084851852
    },
    {
      'timestamp': 1484764800000,
      'outputPowerW': 23822.03125,
      'battAvgSocProp': 0.732421875,
      'powerLimitW': 19999.9375,
      'outputPower1W': 21988.065244375,
      'outputPower2W': 22877.149369687497,
      'outputPower3W': 21498.149151666665
    },
    {
      'timestamp': 1484764860000,
      'outputPowerW': 25023.141666666666,
      'battAvgSocProp': 0.7506458333333332,
      'powerLimitW': 19999.175,
      'outputPower1W': 21420.829417527777,
      'outputPower2W': 22470.433155194445,
      'outputPower3W': 22555.672875527776
    },
    {
      'timestamp': 1484764920000,
      'outputPowerW': 24930.25,
      'battAvgSocProp': 0.7349375,
      'powerLimitW': 19999,
      'outputPower1W': 22680.535277833333,
      'outputPower2W': 22040.434127333334,
      'outputPower3W': 22353.779414722223
    },
    {
      'timestamp': 1484764980000,
      'outputPowerW': 23520.8,
      'battAvgSocProp': 0.7538333333333334,
      'powerLimitW': 19999,
      'outputPower1W': 22604.211794916668,
      'outputPower2W': 23777.682236666667,
      'outputPower3W': 21906.08716341666
    },
    {
      'timestamp': 1484765040000,
      'outputPowerW': 24670.358333333334,
      'battAvgSocProp': 0.7437708333333333,
      'powerLimitW': 19999.175,
      'outputPower1W': 23675.370095,
      'outputPower2W': 22730.86541425,
      'outputPower3W': 21731.744630194444
    },
    {
      'timestamp': 1484765100000,
      'outputPowerW': 24059.541666666664,
      'battAvgSocProp': 0.7541249999999999,
      'powerLimitW': 19999,
      'outputPower1W': 22607.697586833336,
      'outputPower2W': 23820.316998055554,
      'outputPower3W': 21125.207500861114
    },
    {
      'timestamp': 1484765160000,
      'outputPowerW': 23842.308333333334,
      'battAvgSocProp': 0.7315,
      'powerLimitW': 19999,
      'outputPower1W': 21662.630736555555,
      'outputPower2W': 23133.724186805557,
      'outputPower3W': 21710.284057611112
    },
    {
      'timestamp': 1484765220000,
      'outputPowerW': 25102.233333333334,
      'battAvgSocProp': 0.7488333333333332,
      'powerLimitW': 19999.35,
      'outputPower1W': 22503.374421805554,
      'outputPower2W': 22291.12231275,
      'outputPower3W': 22312.46588913889
    },
    {
      'timestamp': 1484765280000,
      'outputPowerW': 24829.708333333332,
      'battAvgSocProp': 0.7293958333333332,
      'powerLimitW': 19999.141666666666,
      'outputPower1W': 22806.165393000003,
      'outputPower2W': 22184.11121772222,
      'outputPower3W': 22154.782120916665
    }
  ];
  return zoneSummaryMinInterval;
};

export const sensorSummaryDataSec = () => { // eslint-disable-line no-unused-vars
  let zoneSummarySecInterval = [ // eslint-disable-line no-unused-vars
    {
      'timestamp': 1484691120000,
      'outputPowerW': 24412.241379310344,
      'battAvgSocProp': 0.7305172413793104,
      'powerLimitW': 16551.137931034482,
      'outputPower1W': 22066.667008582375,
      'outputPower2W': 22689.505853524905,
      'outputPower3W': 22020.395783065134
    },
    {
      'timestamp': 1484691180000,
      'outputPowerW': 22782.99166666667,
      'battAvgSocProp': 0.746375,
      'powerLimitW': 19999.175,
      'outputPower1W': 21859.72307980556,
      'outputPower2W': 21964.47768238889,
      'outputPower3W': 21879.80440638889
    },
    {
      'timestamp': 1484691240000,
      'outputPowerW': 23858.941666666666,
      'battAvgSocProp': 0.7488333333333335,
      'powerLimitW': 19999.166666666668,
      'outputPower1W': 22354.892818388886,
      'outputPower2W': 21729.784793416664,
      'outputPower3W': 22715.222008138888
    },
    {
      'timestamp': 1484691300000,
      'outputPowerW': 24951.041666666668,
      'battAvgSocProp': 0.7408958333333333,
      'powerLimitW': 19999.525,
      'outputPower1W': 22844.763233888887,
      'outputPower2W': 22476.25050975,
      'outputPower3W': 23664.957827638886
    },
    {
      'timestamp': 1484691360000,
      'outputPowerW': 23532.875,
      'battAvgSocProp': 0.7323749999999999,
      'powerLimitW': 19999.591666666667,
      'outputPower1W': 22710.721783027773,
      'outputPower2W': 22977.861504194443,
      'outputPower3W': 22209.808213305558
    },
    {
      'timestamp': 1484691420000,
      'outputPowerW': 23462.916666666664,
      'battAvgSocProp': 0.7613125000000001,
      'powerLimitW': 19999.108333333334,
      'outputPower1W': 22689.790766027778,
      'outputPower2W': 20897.645706666666,
      'outputPower3W': 22337.818538833337
    },
    {
      'timestamp': 1484691480000,
      'outputPowerW': 23769.983333333334,
      'battAvgSocProp': 0.7331874999999999,
      'powerLimitW': 19999.316666666666,
      'outputPower1W': 21326.164783361113,
      'outputPower2W': 21220.11908875,
      'outputPower3W': 23427.363212805554
    },
    {
      'timestamp': 1484691540000,
      'outputPowerW': 24714.891666666666,
      'battAvgSocProp': 0.7401875,
      'powerLimitW': 19999.558333333334,
      'outputPower1W': 22434.303094972223,
      'outputPower2W': 22178.416103722222,
      'outputPower3W': 22390.279211555557
    },
    {
      'timestamp': 1484691600000,
      'outputPowerW': 24242.25833333333,
      'battAvgSocProp': 0.7536874999999998,
      'powerLimitW': 19999.175,
      'outputPower1W': 21210.963295444446,
      'outputPower2W': 22279.70631936111,
      'outputPower3W': 21814.750104694445
    },
    {
      'timestamp': 1484691660000,
      'outputPowerW': 25424.716666666667,
      'battAvgSocProp': 0.7575,
      'powerLimitW': 19999.175,
      'outputPower1W': 23844.07052925,
      'outputPower2W': 23488.175213416667,
      'outputPower3W': 22060.85877186111
    },
    {
      'timestamp': 1484691720000,
      'outputPowerW': 24289.3,
      'battAvgSocProp': 0.7505833333333333,
      'powerLimitW': 19999.183333333334,
      'outputPower1W': 22744.180677638888,
      'outputPower2W': 22422.507849722224,
      'outputPower3W': 22398.31168336111
    },
    {
      'timestamp': 1484691780000,
      'outputPowerW': 25068.8,
      'battAvgSocProp': 0.7514166666666666,
      'powerLimitW': 19999.175,
      'outputPower1W': 22620.10022569444,
      'outputPower2W': 22422.680461888893,
      'outputPower3W': 23001.791184027777
    },
    {
      'timestamp': 1484691840000,
      'outputPowerW': 23348.125,
      'battAvgSocProp': 0.7317291666666668,
      'powerLimitW': 19999.175,
      'outputPower1W': 23771.777497916664,
      'outputPower2W': 22648.621331111113,
      'outputPower3W': 23414.82999308333
    },
    {
      'timestamp': 1484691900000,
      'outputPowerW': 23918.75,
      'battAvgSocProp': 0.7564791666666667,
      'powerLimitW': 19999.175,
      'outputPower1W': 22549.869526333336,
      'outputPower2W': 23480.78101086111,
      'outputPower3W': 22800.520227694447
    },
    {
      'timestamp': 1484691960000,
      'outputPowerW': 24852.25833333333,
      'battAvgSocProp': 0.7566041666666666,
      'powerLimitW': 19999.175,
      'outputPower1W': 22986.051623666666,
      'outputPower2W': 23112.768364916665,
      'outputPower3W': 21846.68044641667
    },
    {
      'timestamp': 1484692020000,
      'outputPowerW': 24030.825,
      'battAvgSocProp': 0.7394791666666667,
      'powerLimitW': 19999.175,
      'outputPower1W': 22098.95688788889,
      'outputPower2W': 22647.612349694442,
      'outputPower3W': 22891.353516416668
    },
    {
      'timestamp': 1484692080000,
      'outputPowerW': 24989.24166666667,
      'battAvgSocProp': 0.7533124999999999,
      'powerLimitW': 19999.341666666667,
      'outputPower1W': 21583.08846308333,
      'outputPower2W': 22191.862038555555,
      'outputPower3W': 22113.210355805557
    },
    {
      'timestamp': 1484692140000,
      'outputPowerW': 23706.791666666668,
      'battAvgSocProp': 0.7401875,
      'powerLimitW': 19999.183333333334,
      'outputPower1W': 23602.487962805553,
      'outputPower2W': 22217.668013000002,
      'outputPower3W': 22866.43652530556
    },
    {
      'timestamp': 1484692200000,
      'outputPowerW': 23757.191666666666,
      'battAvgSocProp': 0.7374583333333333,
      'powerLimitW': 19999.35,
      'outputPower1W': 22283.29748111111,
      'outputPower2W': 21787.57498913889,
      'outputPower3W': 23462.685861777776
    },
    {
      'timestamp': 1484692260000,
      'outputPowerW': 23827.39166666667,
      'battAvgSocProp': 0.7487916666666667,
      'powerLimitW': 19999.175,
      'outputPower1W': 22545.237262944444,
      'outputPower2W': 22707.902500666663,
      'outputPower3W': 22314.740878333338
    },
    {
      'timestamp': 1484692320000,
      'outputPowerW': 24340.341666666667,
      'battAvgSocProp': 0.753125,
      'powerLimitW': 19999.175,
      'outputPower1W': 22057.770348722224,
      'outputPower2W': 22044.69470191667,
      'outputPower3W': 22242.6166915
    },
    {
      'timestamp': 1484692380000,
      'outputPowerW': 24376.00833333333,
      'battAvgSocProp': 0.7334375,
      'powerLimitW': 19999.383333333335,
      'outputPower1W': 22424.932939111113,
      'outputPower2W': 24229.207909944445,
      'outputPower3W': 22357.589480305556
    }
  ];
  return zoneSummarySecInterval;
};

export const sensorSummaryData3Day = () => { // eslint-disable-line no-unused-vars
  let zoneSummary3dayInterval = [
    {
      "timestamp": 1491854400000,
      "outputPowerW": 3001.5901693355227,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1768.2835820895523,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491856200000,
      "outputPowerW": 3319.068597222227,
      "battAvgSocProp": 0.49999166666666667,
      "powerLimitW": 1769.5611111111111,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491858000000,
      "outputPowerW": 3636.0737333333423,
      "battAvgSocProp": 0.4999895833333334,
      "powerLimitW": 1889.2145666666756,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491859800000,
      "outputPowerW": 3634.118802777786,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1891.3035250000087,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491861600000,
      "outputPowerW": 3633.3187305555643,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1890.3698416666755,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491863400000,
      "outputPowerW": 3633.7587472222312,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1894.4948583333428,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491865200000,
      "outputPowerW": 3535.073988888892,
      "battAvgSocProp": 0.458254861111111,
      "powerLimitW": 1898.4916666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491867000000,
      "outputPowerW": 3297.16672398835,
      "battAvgSocProp": 0.38337632459564974,
      "powerLimitW": 2468.0805555555557,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491868800000,
      "outputPowerW": 3085.027777777778,
      "battAvgSocProp": 0.5,
      "powerLimitW": 3085.027777777778,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491870600000,
      "outputPowerW": 3311.265966136385,
      "battAvgSocProp": 0.4187677923527779,
      "powerLimitW": 2574.766666666667,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491872400000,
      "outputPowerW": 3591.7212111111103,
      "battAvgSocProp": 0.37286805555555536,
      "powerLimitW": 2821.717600000005,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491874200000,
      "outputPowerW": 3661.984638888888,
      "battAvgSocProp": 0.4554222222222261,
      "powerLimitW": 2588.9790833333373,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491876000000,
      "outputPowerW": 3669.66315,
      "battAvgSocProp": 0.4857652777777805,
      "powerLimitW": 2118.9381500000013,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491877800000,
      "outputPowerW": 3668.566841666666,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1956.4204527777777,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491879600000,
      "outputPowerW": 3663.757038888889,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1929.0731499999997,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491881400000,
      "outputPowerW": 3658.3384388888885,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1929.140661111111,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491883200000,
      "outputPowerW": 3641.2037055555547,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1918.7564833333329,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491885000000,
      "outputPowerW": 3640.5862027777775,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1928.6800916666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491886800000,
      "outputPowerW": 3645.6258499999994,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1916.6094611111107,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491888600000,
      "outputPowerW": 3646.1680027777766,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1913.3449472222214,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491890400000,
      "outputPowerW": 3659.431841666666,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1917.0690638888882,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491892200000,
      "outputPowerW": 3659.314530555555,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1923.0817527777774,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491894000000,
      "outputPowerW": 3649.2547694444434,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1908.4150472222211,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491895800000,
      "outputPowerW": 3663.0498472222216,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1924.8404027777776,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491897600000,
      "outputPowerW": 3667.6180805555555,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1920.6794694444443,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491899400000,
      "outputPowerW": 3668.8798416666664,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1919.5784527777773,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491901200000,
      "outputPowerW": 3667.8105444444445,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1925.6888777777774,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491903000000,
      "outputPowerW": 3667.011686111111,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1923.2764083333332,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491904800000,
      "outputPowerW": 3669.4562055555557,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1922.7384277777778,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491906600000,
      "outputPowerW": 3667.2584944444443,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1920.9493277777772,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491908400000,
      "outputPowerW": 3665.974197222221,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1914.805308333332,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491910200000,
      "outputPowerW": 3664.9887166666663,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1911.127883333333,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491912000000,
      "outputPowerW": 3653.6853138888887,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1922.508369444444,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491913800000,
      "outputPowerW": 3654.2201888888885,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1922.1560222222222,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491915600000,
      "outputPowerW": 3653.393049999999,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1914.8180499999996,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491917400000,
      "outputPowerW": 3642.805036111111,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1913.021980555555,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491919200000,
      "outputPowerW": 3633.0679916666663,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1902.5263249999996,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491921000000,
      "outputPowerW": 3631.8951888888887,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1906.8332444444443,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491922800000,
      "outputPowerW": 3635.424247222222,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1912.9339694444443,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491924600000,
      "outputPowerW": 3633.779788888889,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1910.9853444444448,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491926400000,
      "outputPowerW": 3629.895808333332,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1901.9166416666653,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491928200000,
      "outputPowerW": 3630.8716083333325,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1901.3993861111103,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491930000000,
      "outputPowerW": 3631.8150194444443,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1895.8736305555553,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491931800000,
      "outputPowerW": 3630.7460772295417,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1895.9100367759715,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491933600000,
      "outputPowerW": 3564.0533564879133,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1900.2761961656013,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491935400000,
      "outputPowerW": 3515.585405555554,
      "battAvgSocProp": 0.49257361111111114,
      "powerLimitW": 1719.2277777777779,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491937200000,
      "outputPowerW": 3338.865555555555,
      "battAvgSocProp": 0.35265902777777797,
      "powerLimitW": 1989.8333333333333,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491939000000,
      "outputPowerW": 2977.1222222222223,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2977.1222222222223,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491940800000,
      "outputPowerW": 2991.972222222222,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2991.972222222222,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491942600000,
      "outputPowerW": 2972.7944444444443,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2972.7944444444443,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491944400000,
      "outputPowerW": 3472.253708333331,
      "battAvgSocProp": 0.33812569444444457,
      "powerLimitW": 2664.4195416666703,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491946200000,
      "outputPowerW": 3564.6894999999995,
      "battAvgSocProp": 0.42669444444444393,
      "powerLimitW": 2879.393388888894,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491948000000,
      "outputPowerW": 3564.930697222222,
      "battAvgSocProp": 0.4782458333333373,
      "powerLimitW": 2104.9495861111122,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491949800000,
      "outputPowerW": 3565.6908166666663,
      "battAvgSocProp": 0.4979930555555567,
      "powerLimitW": 2003.3594277777781,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491951600000,
      "outputPowerW": 3567.279344444444,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1926.5637888888884,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491953400000,
      "outputPowerW": 3568.8581999999997,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1927.7487555555556,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491955200000,
      "outputPowerW": 3572.098694444444,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1925.610916666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491957000000,
      "outputPowerW": 3573.9629027777773,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1928.188736111111,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491958800000,
      "outputPowerW": 3596.299236111111,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1931.4942361111114,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491960600000,
      "outputPowerW": 3596.6483305555557,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1923.9199972222225,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491962400000,
      "outputPowerW": 3599.7276999999995,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1930.0185333333325,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491964200000,
      "outputPowerW": 3600.094472222222,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1926.6241944444441,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491966000000,
      "outputPowerW": 3599.2922944444445,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1923.2142388888885,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491967800000,
      "outputPowerW": 3583.840677777777,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1924.299288888888,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491969600000,
      "outputPowerW": 3569.6443916666667,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1925.035502777778,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491971400000,
      "outputPowerW": 3574.7698416666663,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1918.2937305555552,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491973200000,
      "outputPowerW": 3576.7051111111114,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1926.1162222222224,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491975000000,
      "outputPowerW": 3580.614654901688,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1921.5411583333337,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491976800000,
      "outputPowerW": 3595.1932520300325,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1928.4353138888882,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491978600000,
      "outputPowerW": 3587.02268716523,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1913.2278111111107,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491980400000,
      "outputPowerW": 3583.047365487442,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1917.3757249999996,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491982200000,
      "outputPowerW": 3595.6957749999992,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1913.498552777777,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491984000000,
      "outputPowerW": 3594.047330555555,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1914.2464972222222,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491985800000,
      "outputPowerW": 3594.7238472222216,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1913.713569444444,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491987600000,
      "outputPowerW": 3595.835458333333,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1914.970458333333,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491989400000,
      "outputPowerW": 3600.9645972222215,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1916.4451527777774,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491991200000,
      "outputPowerW": 3593.9198055555553,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1915.465083333333,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491993000000,
      "outputPowerW": 3594.4967944444443,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1915.3076277777775,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491994800000,
      "outputPowerW": 3599.192694444444,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1916.0090833333331,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491996600000,
      "outputPowerW": 3592.753691666666,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1913.2234138888882,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1491998400000,
      "outputPowerW": 3578.729247222222,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1908.718691666667,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492000200000,
      "outputPowerW": 3577.5008249999996,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1907.3194361111107,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492002000000,
      "outputPowerW": 3586.5778055555547,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1909.5047499999994,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492003800000,
      "outputPowerW": 3588.0532749999993,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1909.7668861111108,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492005600000,
      "outputPowerW": 3570.604002777778,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1903.6020583333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492007400000,
      "outputPowerW": 3561.642608333333,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1901.5576083333333,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492009200000,
      "outputPowerW": 3564.7970861111107,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1904.1545861111108,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492011000000,
      "outputPowerW": 3565.214738888888,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1899.9058499999994,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492012800000,
      "outputPowerW": 3562.4183666666668,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1897.1864222222223,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492014600000,
      "outputPowerW": 3560.0558444444437,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1895.111677777777,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492016400000,
      "outputPowerW": 3559.620066666666,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1895.9297888888884,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492018200000,
      "outputPowerW": 3558.7348111111114,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1895.9756444444445,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492020000000,
      "outputPowerW": 3563.7668555555547,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1895.342688888888,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492021800000,
      "outputPowerW": 3471.7587916666644,
      "battAvgSocProp": 0.47524722222222243,
      "powerLimitW": 1778.0944444444444,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492023600000,
      "outputPowerW": 3254.021899601741,
      "battAvgSocProp": 0.36702695193109186,
      "powerLimitW": 2197.1833333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492025400000,
      "outputPowerW": 2976.6,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2976.6,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492027200000,
      "outputPowerW": 2975.072222222222,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2975.072222222222,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492029000000,
      "outputPowerW": 2969.8777777777777,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2969.8777777777777,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492030800000,
      "outputPowerW": 2973.266666666667,
      "battAvgSocProp": 0.5,
      "powerLimitW": 2973.266666666667,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492032600000,
      "outputPowerW": 2908.7279447226856,
      "battAvgSocProp": 0.43412006121313074,
      "powerLimitW": 2908.7944444444443,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492034400000,
      "outputPowerW": 3566.9308944444438,
      "battAvgSocProp": 0.3336298611111112,
      "powerLimitW": 2694.9122833333313,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492036200000,
      "outputPowerW": 3786.0081972222165,
      "battAvgSocProp": 0.4292090277777769,
      "powerLimitW": 2979.056252777767,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492038000000,
      "outputPowerW": 3871.6856416666606,
      "battAvgSocProp": 0.4788798611111156,
      "powerLimitW": 5636.364997222215,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492039800000,
      "outputPowerW": 3857.6900527777725,
      "battAvgSocProp": 0.4984868055555558,
      "powerLimitW": 11986.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492041600000,
      "outputPowerW": 3857.9723499999955,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492043400000,
      "outputPowerW": 3844.394188888884,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492045200000,
      "outputPowerW": 3728.721249999996,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11986.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492047000000,
      "outputPowerW": 3726.5723027777744,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492048800000,
      "outputPowerW": 3738.8455083333292,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492050600000,
      "outputPowerW": 3726.0796916666627,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11991.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492052400000,
      "outputPowerW": 3722.4689833333296,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11993.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492054200000,
      "outputPowerW": 3736.3365666666623,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492056000000,
      "outputPowerW": 3736.031272222219,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492057800000,
      "outputPowerW": 3730.277152777774,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11985,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492059600000,
      "outputPowerW": 3729.57984444444,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492061400000,
      "outputPowerW": 3716.6434418796853,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492063200000,
      "outputPowerW": 3730.552734797513,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492065000000,
      "outputPowerW": 3740.423204581977,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11983.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492066800000,
      "outputPowerW": 3722.3799341997915,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11985,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492068600000,
      "outputPowerW": 3724.120155555551,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492070400000,
      "outputPowerW": 3724.4967751691174,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11986.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492072200000,
      "outputPowerW": 3717.969572781481,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492074000000,
      "outputPowerW": 3734.523654643548,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11991.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492075800000,
      "outputPowerW": 3734.1913292419567,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11986.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492077600000,
      "outputPowerW": 3733.7428749420774,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11985,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492079400000,
      "outputPowerW": 3735.5135461287146,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492081200000,
      "outputPowerW": 3726.0855916666633,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492083000000,
      "outputPowerW": 3716.7523755814987,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11991.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492084800000,
      "outputPowerW": 3706.9322822861595,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11993.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492086600000,
      "outputPowerW": 3707.937211111107,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492088400000,
      "outputPowerW": 3710.7642294827224,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11993.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492090200000,
      "outputPowerW": 3709.2558277777744,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11985,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492092000000,
      "outputPowerW": 3722.839716666662,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492093800000,
      "outputPowerW": 3732.327080555551,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492095600000,
      "outputPowerW": 3715.456938888885,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492097400000,
      "outputPowerW": 3717.797086111107,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11993.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492099200000,
      "outputPowerW": 3736.6193499999954,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492101000000,
      "outputPowerW": 3718.6660277777733,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11983.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492102800000,
      "outputPowerW": 3606.1373749999984,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11993.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492104600000,
      "outputPowerW": 3459.3181805555555,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11991.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492106400000,
      "outputPowerW": 3459.933325,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492108200000,
      "outputPowerW": 3454.261275,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492110000000,
      "outputPowerW": 3492.4602055555556,
      "battAvgSocProp": 0.49999583333333336,
      "powerLimitW": 11990,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492111800000,
      "outputPowerW": 3511.6571416666666,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11983.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492113600000,
      "outputPowerW": 3528.9400277777777,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492115400000,
      "outputPowerW": 3513.320966666667,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11988.333333333334,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492117200000,
      "outputPowerW": 3523.2780388888887,
      "battAvgSocProp": 0.5,
      "powerLimitW": 11991.666666666666,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492119000000,
      "outputPowerW": 2710.640618244704,
      "battAvgSocProp": 0.5,
      "powerLimitW": 9305.169538632574,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492120800000,
      "outputPowerW": 1762.3702392211403,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492122600000,
      "outputPowerW": 1766.2690724233983,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492124400000,
      "outputPowerW": 1764.353980168659,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492126200000,
      "outputPowerW": 1762.3249794467627,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492128000000,
      "outputPowerW": 1761.2459909158324,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492129800000,
      "outputPowerW": 1764.797734757041,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492131600000,
      "outputPowerW": 1781.284774624374,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492133400000,
      "outputPowerW": 1775.5218569042318,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492135200000,
      "outputPowerW": 1775.644394360972,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492137000000,
      "outputPowerW": 1769.9066768688556,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492138800000,
      "outputPowerW": 1767.877216652743,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492140600000,
      "outputPowerW": 1764.0837552585995,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492142400000,
      "outputPowerW": 1759.0254201446855,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492144200000,
      "outputPowerW": 1764.8083239066605,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492146000000,
      "outputPowerW": 1765.6402664130871,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492147800000,
      "outputPowerW": 1772.143865702096,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492149600000,
      "outputPowerW": 1773.7674391563844,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492151400000,
      "outputPowerW": 1773.7290114174325,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492153200000,
      "outputPowerW": 1779.893623484286,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492155000000,
      "outputPowerW": 1778.57930756396,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492156800000,
      "outputPowerW": 1781.8241003524392,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492158600000,
      "outputPowerW": 1776.9956771219995,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492160400000,
      "outputPowerW": 1781.366293255404,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492162200000,
      "outputPowerW": 1781.8592675572872,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492164000000,
      "outputPowerW": 1781.8094017569908,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492165800000,
      "outputPowerW": 1779.752916048008,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492167600000,
      "outputPowerW": 1784.9210247722308,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492169400000,
      "outputPowerW": 1780.3878906829991,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492171200000,
      "outputPowerW": 1772.7933268374163,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492173000000,
      "outputPowerW": 1767.6152858203416,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492174800000,
      "outputPowerW": 1777.9625627938628,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492176600000,
      "outputPowerW": 1780.7368901268958,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492178400000,
      "outputPowerW": 1771.24618733958,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492180200000,
      "outputPowerW": 1769.7941467260248,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492182000000,
      "outputPowerW": 1772.1649836055433,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492183800000,
      "outputPowerW": 1768.5701225285436,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492185600000,
      "outputPowerW": 1763.2389988876528,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492187400000,
      "outputPowerW": 1760.0115943238732,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492189200000,
      "outputPowerW": 1757.655892012246,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492191000000,
      "outputPowerW": 1762.8380752265984,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492192800000,
      "outputPowerW": 1758.5329072214672,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492194600000,
      "outputPowerW": 1756.3610977474737,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492196400000,
      "outputPowerW": 1754.3825539509444,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492198200000,
      "outputPowerW": 1756.6126125958922,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492200000000,
      "outputPowerW": 1756.291131366546,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492201800000,
      "outputPowerW": 1752.0650566388115,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492203600000,
      "outputPowerW": 1754.567300194932,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492205400000,
      "outputPowerW": 1758.4768744203302,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492207200000,
      "outputPowerW": 1757.6116825838355,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492209000000,
      "outputPowerW": 1756.8401929678078,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    },
    {
      "timestamp": 1492210800000,
      "outputPowerW": 1755.4951110492452,
      "battAvgSocProp": 0.5,
      "powerLimitW": 6000,
      "outputPower1W": 0,
      "outputPower2W": 0,
      "outputPower3W": 0
    }
  ];
  return zoneSummary3dayInterval;
};

export const sensorSummarySinglePhase = () => { // eslint-disable-line no-unused-vars
  let sensorSummarySinglePhaseData = [
    {
      "timestamp": 1491854400000,
      "outputPowerW": 3001.5901693355227,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1768.2835820895523
    },
    {
      "timestamp": 1491856200000,
      "outputPowerW": 3319.068597222227,
      "battAvgSocProp": 0.49999166666666667,
      "powerLimitW": 1769.5611111111111
    },
    {
      "timestamp": 1491858000000,
      "outputPowerW": 3636.0737333333423,
      "battAvgSocProp": 0.4999895833333334,
      "powerLimitW": 1889.2145666666756
    },
    {
      "timestamp": 1491859800000,
      "outputPowerW": 3634.118802777786,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1891.3035250000087
    },
    {
      "timestamp": 1491861600000,
      "outputPowerW": 3633.3187305555643,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1890.3698416666755
    },
    {
      "timestamp": 1491863400000,
      "outputPowerW": 3633.7587472222312,
      "battAvgSocProp": 0.5,
      "powerLimitW": 1894.4948583333428
    },
    {
      "timestamp": 1491865200000,
      "outputPowerW": 3535.073988888892,
      "battAvgSocProp": 0.458254861111111,
      "powerLimitW": 1898.4916666666666
    },
    {
      "timestamp": 1491867000000,
      "outputPowerW": 3297.16672398835,
      "battAvgSocProp": 0.38337632459564974,
      "powerLimitW": 2468.0805555555557
    },
    {
      "timestamp": 1491868800000,
      "outputPowerW": 3085.027777777778,
      "battAvgSocProp": 0.5,
      "powerLimitW": 3085.027777777778
    },
    {
      "timestamp": 1491870600000,
      "outputPowerW": 3311.265966136385,
      "battAvgSocProp": 0.4187677923527779,
      "powerLimitW": 2574.766666666667
    }
  ];
  return sensorSummarySinglePhaseData;
};

export default {
  outputRawData,
  loadRawData,
  phase3RawData,
  phase2RawData,
  phase1RawData,
  sensorSummaryDataMin,
  sensorSummaryDataSec,
  sensorSummaryData3Day,
  sensorSummarySinglePhase
};
