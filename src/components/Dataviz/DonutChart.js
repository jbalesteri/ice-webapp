var debug = require('debug')('ice:DonutChart'); // eslint-disable-line no-unused-vars

import { inject, observer } from 'mobx-react';
import React from 'react';
let ReactD3 = require('react-d3-components');

const PieChart = ReactD3.PieChart;

const defaultState = {
  phaseData: [],
  chartWidth: 250,
  chartHeight: 200,
  colorScale: [],
  marginTop: 35,
  marginBottom: 0,
  marginLeft: 90,
  marginRight: 50,
  hideLabels: true,
  sort: true

};

@inject('store') @observer
class DonutChart extends React.Component {
  constructor (props) {
    super(props);
    this.state = defaultState;
  }

  componentWillReceiveProps (nextProps) {
    let newState = Object.assign({}, defaultState);
    newState.phaseData = nextProps.phaseData || [];
    newState.chartWidth = nextProps.chartWidth || defaultState.chartWidth;
    newState.chartHeight = nextProps.chartHeight || defaultState.chartHeight;
    newState.colorScale = nextProps.colorScale || [];
    newState.marginTop = nextProps.marginTop || defaultState.marginTop;
    newState.marginBottom = nextProps.marginBottom || defaultState.marginTop;
    newState.marginLeft = nextProps.marginLeft || defaultState.marginTop;
    newState.marginRight = nextProps.marginRight || defaultState.marginTop;
    newState.hideLabels = nextProps.hideLabels || true;
    newState.sort = nextProps.sort || null;
    this.setState(newState);
  }

  render () {
    let colrIdx = -1;
    let _colorScale = this.state.colorScale;
    let _marginTop = this.state.marginTop;
    let _marginBottom = this.state.marginBottom;
    let _marginLeft = this.state.marginLeft;
    let _marginRight = this.state.marginRight;
    // Currently supports phase data. Can be extended as need arises. Also - Is only drawn if feed is 3 phase.
    if (this.state.phaseData && this.state.phaseData.values && this.state.phaseData.values.length > 2) {
      return (
        <PieChart
            data={this.state.phaseData}
            width={this.state.chartWidth}
            height={this.state.chartHeight}
            colorScale={function () { colrIdx++; return _colorScale[colrIdx]; }}
            margin={{top: _marginTop, bottom: _marginBottom, left: _marginLeft, right: _marginRight}}
            hideLabels={this.state.hideLabels}
            sort={this.state.sort}
        />
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}

export default DonutChart;
