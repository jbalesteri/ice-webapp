var debug = require('debug')('ice:PowerMetricsAnimation'); // eslint-disable-line no-unused-vars

import { inject, observer } from 'mobx-react';
import React from 'react';

const BATT_CHARGE_COLOR = 'cyan';
const BATT_DISCHARGE_COLOR = 'lightgreen';
const DEFAULT_TICK_COLOR = 'white';
const DEFAULT_TICKTEXT_COLOR = '#666666';
const POWER_ANIM_BASE_COLOR = 'orange';
const POWER_INPUT_OPACITY = 0.4;
const POWER_OUTPUT_OPACITY = 0.8;

@inject('store') @observer
class PowerMetricsAnimation extends React.Component {
  render () {
    let wordStore = this.props.store.wordStore;

    this.barHeight = parseInt(this.props.barHeight);
    this.backGroundHeight = 2 * this.barHeight;
    this.inputPowerColor = this.props.inputPowerColor;
    this.inputPowerW = this.props.inputPowerW;
    this.outputPowerW = this.props.outputPowerW;
    this.powerLimit = this.props.powerLimit;
    this.width = this.props.width;
    this.tickColor = this.props.tickColor || DEFAULT_TICK_COLOR;
    this.tickTextColor = this.props.tickTextColor || DEFAULT_TICKTEXT_COLOR;

    let battDiff, battFillColor, battFrom;
    let valuesInput = this.inputPowerW;
    let valuesOutput = this.outputPowerW;
    let valuesDiff = 0;
    let startY = 5;
    let startY1 = this.barHeight + startY;

    // determine battery charge/discharge value
    battDiff = Math.abs(this.outputPowerW - this.inputPowerW);
    valuesDiff = (battDiff - 10) + ';' + battDiff;

    // determine battery color - ie charge discharge
    if (this.outputPowerW > this.inputPowerW) {
      battFillColor = BATT_CHARGE_COLOR;
      battFrom = this.inputPowerW;
    } else if (this.outputPowerW < this.inputPowerW) {
      battFillColor = BATT_DISCHARGE_COLOR;
      battFrom = this.outputPowerW;
    } else {
        // when they are equal animate the edges, so pass a couple of values and starts from - 10
      valuesInput = this.inputPowerW - 10 + ';' + this.inputPowerW;
      valuesOutput = this.outputPowerW - 10 + ';' + this.outputPowerW;
    }

    return (
        <svg id='svgelement' width={this.width} height={this.backGroundHeight + 10}>
            <rect x='0' y={startY} width='1050' height={this.backGroundHeight} fill='grey'/>

            {/* Input bar - light orange - opacity:0.4 */}
            <rect x='0' y={startY} height={this.barHeight} fill={POWER_ANIM_BASE_COLOR} style={{opacity: POWER_INPUT_OPACITY}}>
              <animate attributeName='width' dur='2s' values={valuesInput} repeatCount='indefinite'/>
            </rect>

            {/* grey separator between input and output graphs */}
            <line x1='0' y1={startY1 + 2} x2='1050' y2={startY1 + 2} strokeWidth='0.5' stroke='#666666'/>

            {/* Output bar - orange - opacity:0.8 */}
            <rect x='0' y={startY1} height={this.barHeight} fill={POWER_ANIM_BASE_COLOR} style={{opacity: POWER_OUTPUT_OPACITY}}>
              <animate attributeName='width' dur='5s' values={valuesOutput} repeatCount='indefinite'/>
            </rect>

            {/* Diff in cyan() or light green */}
            <rect x={battFrom} y={startY1} height={this.barHeight} fill={battFillColor}>
              <animate attributeName='width' dur='2s' values={valuesDiff} repeatCount='indefinite'/>
            </rect>

            <line x1={this.powerLimit} y1={startY} x2={this.powerLimit} y2={this.backGroundHeight + startY} strokeWidth='2' stroke='yellow'/>

            {/* TODO - Have a separate function, returning this jsx - {this.drawTicks()}; */}
            {/* tick lines */}
            <line x1='50' y1='5' x2='50' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='100' y1='5' x2='100' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='150' y1='5' x2='150' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='200' y1='5' x2='200' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='250' y1='5' x2='250' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='300' y1='5' x2='300' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='350' y1='5' x2='350' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='400' y1='5' x2='400' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='450' y1='5' x2='450' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='500' y1='5' x2='500' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='550' y1='5' x2='550' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='600' y1='5' x2='600' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='650' y1='5' x2='650' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='700' y1='5' x2='700' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='750' y1='5' x2='750' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='800' y1='5' x2='800' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='850' y1='5' x2='850' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='900' y1='5' x2='900' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='950' y1='5' x2='950' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='1000' y1='5' x2='1000' y2='10' strokeWidth='1' stroke={this.tickColor}/>
            <line x1='1050' y1='5' x2='1050' y2='10' strokeWidth='1' stroke={this.tickColor}/>

            {/* tick text */}
            <text x='0' y='20' fill={this.tickTextColor}>0</text>
            <text x='280' y='20' fill={this.tickTextColor}>{wordStore.translate('750w')}</text>
            <text x='525' y='20' fill={this.tickTextColor}>{wordStore.translate('1500w')}</text>
            <text x='780' y='20' fill={this.tickTextColor}>{wordStore.translate('2250w')}</text>
            <text x='1050' y='20' fill={this.tickTextColor}>{wordStore.translate('3000w')}</text>

        </svg>

    );
  }
}

export default PowerMetricsAnimation;
