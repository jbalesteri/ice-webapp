import React from 'react';
import moment from 'moment';

// Pond
import { TimeSeries } from 'pondjs';

// Imports from the charts library
import { Charts, ChartContainer, ChartRow, YAxis, LineChart, Baseline, Resizable, Legend, styler } from 'react-timeseries-charts';

const styles = require('./TrendChart.css');

const LABEL_COLOR = '#9E9E9E'; // colorGrey
const TIME_TRACKER_DISPLAY_COLOR = '#FFFFFF'; // colorWhite
const AXIS_COLOR = '#607D8B'; // colorBlueGrey
const BASELINE_COLOR = '#607D8B'; // colorBlueGrey

const PHASE1_COLOR = '#9e9e9e'; // colorGrey
const PHASE2_COLOR = '#FF5722'; // colorDeepOrange
const PHASE3_COLOR = '#4FC3F7'; // colorLightBlue
const LOAD_COLOR = '#FFC107'; // colorAmber
const BATTERY_CHARGE_COLOR = '#4CAF50'; // colorGreen
const LIMIT_COLOR = '#FFEB3B'; // colorYellow

// Styles
const lineChartStyler = styler([
    {key: 'phase1', color: PHASE1_COLOR, width: 1},
    {key: 'phase2', color: PHASE2_COLOR, width: 1},
    {key: 'phase3', color: PHASE3_COLOR, width: 1},
    {key: 'load', color: LOAD_COLOR, width: 1},
    {key: 'batterySOC', color: BATTERY_CHARGE_COLOR, width: 1},
    {key: 'limit', color: LIMIT_COLOR, width: 1, dashed: true}
]);

const baselineStyleLite = {
  line: {
    stroke: BASELINE_COLOR,
    strokeWidth: 1,
    opacity: 0.5
  }
};
const DEFAULT_PHASE = 1;
class TrendChart extends React.Component {
  constructor (props) {
    super(props);

    let trendChartObj = this.buildTrendChart(this.props.sensorSummaryData);
    this.state = {
      tracker: null,
      plotPoints: trendChartObj.plotPoints || {},
      allPhaseSeries: trendChartObj.AllPhaseSeries || {},
      batterySOCSeries: trendChartObj.batterySOCSeries || {},
      timerange: trendChartObj.AllPhaseSeries ? trendChartObj.AllPhaseSeries.range() : '',
      is3Phase: (trendChartObj.plotPoints.phase === 3),
      active: {
        phase1: (trendChartObj.plotPoints.phase === 3),
        phase2: (trendChartObj.plotPoints.phase === 3),
        phase3: (trendChartObj.plotPoints.phase === 3),
        load: true,
        batterySOC: true,
        limit: true
      }
    };
  }

  componentWillReceiveProps (nextProps) {
    let trendChartObj = this.buildTrendChart(nextProps.sensorSummaryData);
    this.setState({
      tracker: null,
      plotPoints: trendChartObj.plotPoints || {},
      allPhaseSeries: trendChartObj.AllPhaseSeries || {},
      batterySOCSeries: trendChartObj.batterySOCSeries || {},
      timerange: trendChartObj.AllPhaseSeries.range()
    });
  }

  buildTrendChart (sensorSummaryData) {
    let chartObj = {plotPoints: {}, AllPhaseSeries: {}, batterySOCSeries: {}};
    let plotPoints = this.buildTimeSeriesObj(sensorSummaryData);
    let AllPhaseSeries = new TimeSeries({
      name: 'allPhase',
      columns: plotPoints.series1Columns,
      points: plotPoints.series1Arr
    });
    let batterySOCSeries = new TimeSeries({
      name: 'batterySOC',
      columns: ['time', 'batterySOC'],
      points: plotPoints.series2Arr
    });
    chartObj.plotPoints = plotPoints;
    chartObj.AllPhaseSeries = AllPhaseSeries;
    chartObj.batterySOCSeries = batterySOCSeries;
    return chartObj;
  }

  buildTimeSeriesObj = (zoneSensorSummary) => {
    let _series1Columns = []; // consists of timeseries for phase(es), load and limit;, series2 consits only of Battery SOC
    let _series1Arr = [];
    let _series2Arr = [];
    let _series1Max = 0;
    let _series2Max = 0;
    let _phase = DEFAULT_PHASE;
    let _plotPoints = {series1Columns: [], series1Arr: [], series2Arr: [], series1Max: 0, series2Max: 0, phase: _phase};
    let phase1Value, phase2Value, phase3Value;

    zoneSensorSummary.forEach((val) => {
      _phase = this.getPhase(val); // checking phase with each item to make sure its accurate;
      let _timestamp = val['timestamp'];
      let loadValue = val['outputPowerW'] ? (val['outputPowerW'] / 1000).toFixed(3) : 0.000;
      let socValue = val['battAvgSocProp'] ? val['battAvgSocProp'] * 100 : 0; // Battery SOC needs to be displayed as a %age
      let powerLimitW = val['powerLimitW'] ? (val['powerLimitW'] / 1000).toFixed(3) : 0.000;
      if (_phase === 1) {
        _series1Max = Math.max(_series1Max, loadValue, powerLimitW);
        _series1Arr.push([_timestamp, loadValue, powerLimitW]);
        _series1Columns = ['time', 'load', 'limit'];
      } else if (_phase === 3) {
        phase1Value = val['outputPower1W'] ? (val['outputPower1W'] / 1000).toFixed(3) : 0.000;
        phase2Value = val['outputPower2W'] ? (val['outputPower2W'] / 1000).toFixed(3) : 0.000;
        phase3Value = val['outputPower3W'] ? (val['outputPower3W'] / 1000).toFixed(3) : 0.000;
        _series1Max = Math.max(_series1Max, phase1Value, phase2Value, phase3Value, loadValue, powerLimitW);
        _series1Arr.push([_timestamp, phase1Value, phase2Value, phase3Value, loadValue, powerLimitW]);
        _series1Columns = ['time', 'phase1', 'phase2', 'phase3', 'load', 'limit'];
      }
      _series2Max = Math.max(_series2Max, socValue);
      _series2Arr.push([_timestamp, socValue]);
    });
    _plotPoints.phase = _phase;
    _plotPoints.series1Arr = _series1Arr;
    _plotPoints.series2Arr = _series2Arr;
    _plotPoints.series1Max = _series1Max;
    _plotPoints.series2Max = _series2Max;
    _plotPoints.series1Columns = _series1Columns;
    return _plotPoints;
  }

  getPhase (sensorData) {
    let hasPhase1 = sensorData.hasOwnProperty('outputPower1W');
    let hasPhase2 = sensorData.hasOwnProperty('outputPower2W');
    let hasPhase3 = sensorData.hasOwnProperty('outputPower3W');
    // either 3 phase or single phase.
    let retPhase = hasPhase1 && hasPhase2 && hasPhase3 ? 3 : 1;
    return retPhase;
  }

  selectionChangeHandler (selection) {
    if (this.state.selection === selection) {
      this.setState({selection: ''});
    } else {
      this.setState({selection});
    }
  }

  renderChart () {
    const axisStyle = {
      labels: {
        labelColor: LABEL_COLOR,
        labelWeight: 100,
        labelSize: 11
      },
      axis: {
        axisColor: AXIS_COLOR,
        axisWidth: 0
      }
    };

    let series1Columns = [];
    let series2Columns = [];
    // populate this.series1Columns according to column active flag.
    if (this.state.active.phase1) {
      series1Columns.push('phase1');
    };
    if (this.state.active.phase2) {
      series1Columns.push('phase2');
    };
    if (this.state.active.phase3) {
      series1Columns.push('phase3');
    };
    if (this.state.active.load) {
      series1Columns.push('load');
    };
    if (this.state.active.limit) {
      series1Columns.push('limit');
    };
    // populate this.series2Columns according to series1 active flag.
    if (this.state.active.batterySOC) {
      series2Columns.push('batterySOC');
    };
    return (
          <ChartContainer
                timeRange={this.state.allPhaseSeries.range()}
                maxTime={this.state.allPhaseSeries.range().end()}
                minTime={this.state.allPhaseSeries.range().begin()}
                trackerPosition={this.state.tracker}
                onTrackerChanged={this.handleTrackerChanged.bind(this)}
                showGrid={true}
                timeAxisStyle={axisStyle}>
                <ChartRow height='300'>
                    <YAxis
                        id='phaseAxis'
                        label='Power (kW)'
                        transition={300}
                        style={axisStyle}
                        labelOffset={-10}
                        min={0} max={this.state.plotPoints.series1Max}
                        format='.3f'
                        width='60'
                        type='linear' />
                    <Charts>
                        <LineChart
                            axis='phaseAxis'
                            series={this.state.allPhaseSeries}
                            columns={series1Columns}
                            style={lineChartStyler}
                            selection={this.state.selection}
                            onSelectionChange={this.selectionChangeHandler.bind(this)}
                        />
                        <LineChart
                            key='batterySOC'
                            axis='axis5'
                            series={this.state.batterySOCSeries}
                            columns={series2Columns}
                            style={lineChartStyler}
                            selection={this.state.selection}
                            onSelectionChange={this.selectionChangeHandler.bind(this)}
                        />
                        <Baseline
                            axis='phaseAxis'
                            style={baselineStyleLite}
                            value={0.0} />
                    </Charts>
                    <YAxis
                          id='axis5'
                          label='Battery SOC (%)'
                          transition={300}
                          style={axisStyle}
                          labelOffset={-10}
                          min={0} max={this.state.plotPoints.series2Max}
                          format=',.2f'
                          width='0'
                          type='linear' />
                </ChartRow>
            </ChartContainer>
    );
  }

  handleTrackerChanged (t) {
    this.setState({tracker: t});
  }

  handleActiveChange (key) {
    const active = this.state.active;
    active[key] = !active[key];
    this.setState({active});
  }

  render () {
    const timeDisplayStyle = {
      fontSize: '1rem',
      color: TIME_TRACKER_DISPLAY_COLOR,
      height: '20px'
    };
    let phase1Value, phase2Value, phase3Value, loadValue, batterySOCValue, limitValue;
    if (this.state.tracker) {
      const index5 = this.state.batterySOCSeries.bisect(this.state.tracker);
      const index = this.state.allPhaseSeries.bisect(this.state.tracker);

      const trackerEventbatterySOC = this.state.batterySOCSeries.at(index5);
      const trackerEvent = this.state.allPhaseSeries.at(index);

      phase1Value = trackerEvent.get('phase1');
      phase2Value = trackerEvent.get('phase2');
      phase3Value = trackerEvent.get('phase3');
      loadValue = trackerEvent.get('load');
      limitValue = trackerEvent.get('limit');
      batterySOCValue = trackerEventbatterySOC.get('batterySOC');
    }
    const legend = [
      {
        key: 'load',
        label: 'Load',
        disabled: !this.state.active.load,
        value: loadValue ? parseFloat(loadValue).toFixed(3) + ' kW' : 0
      }, {
        key: 'limit',
        label: 'Power Limit',
        disabled: !this.state.active.limit,
        value: limitValue ? parseFloat(limitValue).toFixed(3) + ' kW' : 0
      }, {
        key: 'batterySOC',
        label: 'Battery SOC (%)',
        disabled: !this.state.active.batterySOC,
        value: batterySOCValue ? batterySOCValue.toFixed(2) + '%' : 0.00
      }
    ];

    // The legend for the three phases is there only if there are 3 phases.
    if (this.state.is3Phase) {
      // insert phase1 object in 1st. position of legend array
      legend.splice(0, 0, {
        key: 'phase1',
        label: 'Phase 1',
        disabled: !this.state.active.phase1,
        value: phase1Value ? parseFloat(phase1Value).toFixed(3) + ' kW' : 0
      });
      // insert phase2 object in 2nd. position of legend array
      legend.splice(1, 0, {
        key: 'phase2',
        label: 'Phase 2',
        disabled: !this.state.active.phase2,
        value: phase2Value ? parseFloat(phase2Value).toFixed(3) + ' kW' : 0
      });
      // insert phase3 object in 3rd. position of legend array
      legend.splice(2, 0, {
        key: 'phase3',
        label: 'Phase 3',
        disabled: !this.state.active.phase3,
        value: phase3Value ? parseFloat(phase3Value).toFixed(3) + ' kW' : 0
      });
    }
    const trackerRow = {
      height: '35px'
    };

    let dateDisplay = this.state.tracker ? moment(new Date(this.state.tracker)).format('LLL') : '';
    let startDate = this.props.sensorSummaryData[0] ? new Date(this.props.sensorSummaryData[0].timestamp).toLocaleString() : 'N/A';
    let endDate = this.props.sensorSummaryData[this.props.sensorSummaryData.length - 1] ? new Date(this.props.sensorSummaryData[this.props.sensorSummaryData.length - 1].timestamp).toLocaleString() : 'N/A';
    return (
            <div>
                <div style={timeDisplayStyle}>
                    {dateDisplay}
                </div>
                <div style={trackerRow}>
                    <div data-trend-chart='legend'>
                        <Legend
                            type='line'
                            key='example'
                            style={lineChartStyler}
                            categories={legend}
                            selection={this.state.selection}
                            onSelectionChange={this.handleActiveChange.bind(this)}
                            />

                    </div>
                </div>

                <hr/>

                <div>
                    <div>
                        <Resizable>
                            {this.renderChart()}
                        </Resizable>
                    </div>
                </div>
                <div className={styles.chartTimeStampLabel}>Zone Power Analytics from {startDate} to {endDate}</div>
            </div>
    );
  }
};

export default TrendChart;
