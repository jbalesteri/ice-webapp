const debug = require('debug')('ice:Sparkline:Dynamic1'); // eslint-disable-line no-unused-vars

import React from 'react';
import { Sparklines, SparklinesLine, SparklinesSpots } from 'react-sparklines';
import { toJS } from 'mobx';

let _isMounted = false;

class Dynamic1 extends React.Component {
  constructor (props) {
    super(props);
    let self = this;
    self.state = { data: [] };
    setInterval(function () {
      if (_isMounted && props.store.currentSensorData.id === props.id) {
        self.setState({
          data: self.state.data.concat(parseInt(toJS(props.store.currentSensorData.inputPowerW)))
        });
      }
    }, 100);
  }

  componentDidMount () {
    _isMounted = true;
  };

  componentWillUnmount () {
    _isMounted = false;
  };

  render () {
    return (
        <Sparklines data={this.state.data} limit={20}>
            <SparklinesLine color='#1c8cdc' />
            <SparklinesSpots />
        </Sparklines>
    );
  }
}

export default Dynamic1;
