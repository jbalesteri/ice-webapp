const debug = require('debug')('ice:Sparkline:Dynamic3'); // eslint-disable-line no-unused-vars

import React from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';

function boxMullerRandom () {
  let phase = false;
  let x1, x2, w;

  return (function () {
    // if (phase = !phase) {
    if (phase === false) {
      do {
        x1 = 2.0 * Math.random() - 1.0;
        x2 = 2.0 * Math.random() - 1.0;
        w = x1 * x1 + x2 * x2;
      } while (w >= 1.0);

      w = Math.sqrt((-2.0 * Math.log(w)) / w);
      return x1 * w;
    } else {
      return x2 * w;
    }
  })();
}

class Dynamic3 extends React.Component {
  constructor (props) {
    super(props);
    this.state = { data: [] };
    setInterval(() =>
            this.setState({
              data: this.state.data.concat([boxMullerRandom()])
            }), 100);
  }

  render () {
    return (
            <Sparklines data={this.state.data} limit={20}>
                <SparklinesLine style={{ stroke: 'none', fill: '#253e56', fillOpacity: '1' }} />
            </Sparklines>
    );
  }
}

export default Dynamic3;
