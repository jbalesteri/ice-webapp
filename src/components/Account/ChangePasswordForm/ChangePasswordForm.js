const debug = require('debug')('ice:Account:ChangePasswordForm'); // eslint-disable-line no-unused-vars

import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { Input, Button } from 'react-toolbox';

import Notify from '../../../common/helpers/Notify';

const defaultState = {
  oldPassword: '',
  password1: '',
  password2: ''
};

const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{10,})');// eslint-disable-line

@inject('store') @observer
export default class ChangePasswordForm extends Component {
  constructor (props) {
    super(props);

    this.wordStore = this.props.store.wordStore;
    this.authStore = this.props.store.authStore;

    this.state = defaultState;
  }

  handleChange = (field, value) => {
    this.setState({ [field]: value });
  }

  save = () => {
    if (this.state.oldPassword === this.state.password1) {
      this.setState({ passwordError: this.wordStore.translate('Old and new passwords cannot be the same') });
    } else if (this.state.password1 !== this.state.password2) {
      this.setState({ passwordError: this.wordStore.translate('Passwords do not match') });
    } else if (!strongRegex.test(this.state.password1)) {
      this.setState({ passwordError: this.wordStore.translate('You need a stronger password') });
    } else {
      debug('Save new password!');
      this.authStore.changePassword(this.state.oldPassword, this.state.password1).then(() => {
        Notify.success('Password updated');
        if (typeof this.props.onSave === 'function') {
          this.props.onSave();
        }
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error updating password');
        }
      });
    }
  }

  render () {
    return (
      <div>
        <Input type='password' value={this.state.oldPassword} label={this.wordStore.translate('Current Password')} onChange={this.handleChange.bind(this, 'oldPassword')}/>
        <Input type='password' value={this.state.password1} error={this.state.passwordError} label={this.wordStore.translate('New Password')} onChange={this.handleChange.bind(this, 'password1')}/>
        <Input type='password' value={this.state.password2} label={this.wordStore.translate('Confirm New Password')} onChange={this.handleChange.bind(this, 'password2')} />
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Button icon='save' label={this.wordStore.translate('Update Password')} raised primary onClick={this.save} style={{marginLeft: '0.5rem'}}/>
        </div>
      </div>
    );
  }
}

ChangePasswordForm.wrappedComponent.propTypes = {
  onSave: PropTypes.func
};
