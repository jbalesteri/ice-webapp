import React from 'react';

export default class TableRow extends React.Component {
  renderCells () {
    let { model } = this.props;
    return Object.keys(model).map((key) => {
      return <td key={key}>{this.renderCell(key)}</td>;
    });
  }

  renderCell (key) {
    let value = this.props.data[key];
    if (React.isValidElement(value)) { return value; }
    return value && value.toString();
  }

  handleRowClick = (evt) => {
    if (typeof this.props.onRowClick === 'function') {
      let buttonIndex = evt.nativeEvent.target.className.indexOf('button');// bc - check if we clicked a button, if not, onRowClick
      if (buttonIndex === -1) {
        this.props.onRowClick(this.props.data);
      }
    }
  }

  render () {
    return (
      <tr onClick={this.handleRowClick}>
        {this.renderCells()}
      </tr>
    );
  }
}

TableRow.propTypes = {
  model: React.PropTypes.object,
  data: React.PropTypes.object,
  onRowClick: React.PropTypes.func
};
