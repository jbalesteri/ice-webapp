import React from 'react';

export default class TableHead extends React.Component {
  render () {
    let { model } = this.props;
    let cells = Object.keys(model).map((key) => {
      let name = model[key].title || key;
      return <th key={key}>{name}</th>;
    });

    return (
      <thead>
        <tr>{cells}</tr>
      </thead>
    );
  }
}

TableHead.propTypes = {
  model: React.PropTypes.object
};
