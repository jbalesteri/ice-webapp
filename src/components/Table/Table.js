import React from 'react';

import TableHead from './TableHead';
import TableRow from './TableRow';

let styles = require('./Table.css');

export default class Table extends React.Component {
  renderTableHead () {
    const { model } = this.props;

    return (
      <TableHead model={model} />
    );
  }

  handleRowClick = (data) => {
    if (typeof this.props.onRowClick === 'function') {
      this.props.onRowClick(data);
    }
  }

  renderTableBody () {
    const { source, model } = this.props;

    return (
      <tbody>
        {source.map((data, index) => {
          return <TableRow data={data} key={index} model={model} onRowClick={this.handleRowClick} />;
        })}
      </tbody>
    );
  }

  render () {
    let heading = null;
    if (this.props.heading) {
      heading = this.renderTableHead();
    }
    return (
      <table>
        {heading}
        {this.renderTableBody()}
      </table>
    );
  }
}

Table.propTypes = {
  model: React.PropTypes.object,
  source: React.PropTypes.array,
  onRowClick: React.PropTypes.func
};

Table.defaultProps = {
  modle: {},
  source: []
};
