// File generated on 2016-10-31 by Jay
var debug = require('debug')('ice:AppStore'); // eslint-disable-line no-unused-vars

import React from 'react';
import {IconButton} from 'react-toolbox/lib/button';

let styles = require('./Badge.css');

class Badge extends React.Component {
  constructor (props) {
    super(props);

    this.badgeData = this.props.badgeData;
    this.badgeContent = this.props.badgeContent || null;
    this.badgeIcon = this.props.badgeIcon || 'notifications';
    this.primary = this.props.primary || false;
    this.accent = this.props.accent || false;
  }

  renderContent () {
    if (this.badgeContent) {
      return (this.badgeContent);
    } else {
      if (this.badgeIcon) {
        return (<IconButton icon={this.badgeIcon} />);
      } else {
        return (<IconButton icon='notifications' />);
      }
    }
  }

  render () {
    let themeClasses = styles.badgeData;

    if (this.primary) {
      themeClasses += ' ' + styles.primary;
    } else if (this.accent) {
      themeClasses += ' ' + styles.accent;
    }

    let classes = '';

    if (this.badgeClass) {
      classes += this.badgeClass;
    }

    return (
      <div className={styles.badge}>
      <span className={themeClasses + ' ' + classes} >
      {this.badgeData}
      </span>
      {this.renderContent()}
      </div>
    );
  }
}

export default Badge;
