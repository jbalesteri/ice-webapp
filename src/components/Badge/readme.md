 <!-- File generated on 2016-10-31 by Jay -->

## Badge
*Created on 2016-10-31*


#### Instructions:
This component generates a small badge to the top-right of its child(ren).

##### Example:
```html
<Badge
	badgeData={5} 
	primary={true}
	badgeIcon='icon_name'
	badgeClass='exampleClass'
	badgeContent={<Button primary label="button label" />}
/ >
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| badgeIcon | string | notification icon | This assigns the child icon of the badge.
| badgeContent  |  node  | none | This is the override content that the badge is applied to.  |
| badgeData | object | none | Number shown in the actual badge area |   
| badgeClass  | string  | none  | The css class name of the root element.  |   
| primary	| bool	| false | If true, badge will use the primary colors.
| accent | bool | false | 	If true, badge will use the accent colors.

###### Images:
----
Standard Implementation: *This will render component without any theming applied to it*

![Standard](assets/docs/standard.png?raw=true "Standard Implentation")

```html
<Badge badgeData={5}  />
```
----
"Primary" Implementation: *This is subject to colors selected in theme*

![Primary](assets/docs/standard-color.png?raw=true "Primary Implentation")

```html
<Badge badgeData={5} primary={true} />
```
----
Non-standard Implementation: *Utilizes an item other than the standard icon*

![Non-standard](assets/docs/with-other-content.png?raw=true "Standard Implentation")

```html
<Badge
	badgeData={5} 
	primary={true}
	badgeContent={<Button primary label="button label"}
/ >
```
----