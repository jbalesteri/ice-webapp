const debug = require('debug')('ice:users:list'); // eslint-disable-line no-unused-vars

import React, { Component, PropTypes } from 'react';
import { observer, inject } from 'mobx-react';
import { IconButton } from 'react-toolbox';

import DataTable from '../../DataTable/DataTable';
import DeleteConfirm from '../../common/DeleteConfirm/DeleteConfirm';

@inject('store') @observer
export default class UsersList extends Component {
  constructor (props) {
    super(props);

    this.wordStore = this.props.store.wordStore;
    this.userStore = this.props.store.userStore;
    this.model = {
      email: {type: String, title: this.wordStore.translate('Email')},
      role: {type: String, title: this.wordStore.translate('Role')},
      activity: {type: Boolean, title: this.wordStore.translate('Activity')},
      edit: {type: Boolean, title: this.wordStore.translate('Edit')},
      updatePassword: {type: Boolean, title: this.wordStore.translate('Update Password')},
      remove: {type: Boolean, title: this.wordStore.translate('Remove')}
    };
  }

  activity = (user) => {
    this.props.onActivity(user);
  }

  edit = (user) => {
    this.props.onEdit(user);
  }

  remove = (user) => {
    this.props.onRemove(user);
  }

  updatePassword = (user) => {
    this.props.onUpdatePassword(user);
  }

  render () {
    let users = this.userStore.all.map((user) => {
      let roles = user.roles ? user.roles.slice().toString() : 'No Roles Assigned';
      return {
        id: user.id,
        email: user.email,
        role: roles,
        activity: <IconButton icon='info' onClick={this.activity.bind(this, user)} />,
        edit: <IconButton icon='edit' onClick={this.edit.bind(this, user)} />,
        updatePassword: <IconButton icon='edit' onClick={this.updatePassword.bind(this, user)} />,
        remove: <DeleteConfirm onClick={this.remove.bind(this, user)} />
      };
    });
    return <DataTable striped model={this.model} source={users} onRowClick={this.edit}/>;
  }
}

UsersList.wrappedComponent.propTypes = {
  onActivity: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onUpdatePassword: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired
};
