import React from 'react';
import { inject, observer } from 'mobx-react';
import { Input, Autocomplete, Dialog, Switch } from 'react-toolbox';
import {regexes} from '../../../common/helpers/Validators';

import Notify from '../../../common/helpers/Notify';
import FormWrapper from '../../common/FormWrapper/FormWrapper';

const styles = require('./UserForm.css');

let strongPasswordRegex = regexes.strongPasswordRegex;
let hasLowerCaseRegex = regexes.hasLowerCaseRegex;
let hasUpperCaseRegex = regexes.hasUpperCaseRegex;
let hasNumberRegex = regexes.hasNumberRegex;
let hasSpecialCharacterRegex = regexes.hasSpecialCharacterRegex;
let hasMinimumCharactersRegex = regexes.hasMinimumCharactersRegex;
let emailRegex = regexes.emailRegex;
let phoneNumberRegex = regexes.phoneNumberRegex;
let phoneNumberRegex2 = regexes.phoneNumberRegex2;

@inject('store') @observer
export default class UserForm extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.userStore;
    this.wordStore = this.props.store.wordStore;

    this.roles = [
      'user',
      'expert',
      'manager',
      'admin'
    ];
    if (this.props.editId) {
      let myUser = this.store.getById(this.props.editId);
      let active = (myUser.active === 'true') ? Boolean(myUser.active) : false;

      this.state = {
        isUpdate: true,
        passwordErrorMesg: '',
        user: myUser,
        active: active
      };
    } else {
      this.state = {
        user: this.store.createUser(),
        active: true
      };
    }
  }

  handleFirstNameChange = (value) => {
    let myUser = this.state.user;
    myUser.firstName = value;
    this.setState({user: myUser});
  };

  handleLastNameChange = (value) => {
    let myUser = this.state.user;
    myUser.lastName = value;
    this.setState({user: myUser});
  };

  handleEmailChange = (value) => {
    let myUser = this.state.user;
    myUser.email = value;
    this.setState({user: myUser});

    this.testEmailRegex(value);
  };

  testEmailRegex = (value) => {
    if (emailRegex.test(value)) {
      this.setState({emailErrorMesg: ''});
      return true;
    } else {
      this.setState({emailErrorMesg: 'Email should be in the form xxx@xxx.xxx'});
      return false;
    }
  }

  handleRolesChange = (value) => {
    let myUser = this.state.user;
    if (Array.isArray(value)) {
      myUser.roles = value;
    } else if (Object.keys(value)) {
      myUser.roles = Object.keys(value);
    }
    this.setState({user: myUser});
  };

  handlePasswordChange = (value) => {
    let myUser = this.state.user;
    myUser.password = value;
    this.setState({user: myUser});
    this.testPasswordRegex(value);
  };

  testPasswordRegex = (value) => {
    // individually test each password regex pattern, so we can display error messages specific to that situation
    // this section is currently not updating the ui, but added here to facilitate adding matching specific password patterns later
    if (hasLowerCaseRegex.test(value)) {
      this.setState({lowerCaseRegexErrorMesg: ''});
    } else {
      this.setState({lowerCaseRegexErrorMesg: this.wordStore.translate('You need to have at least one lowercase character')});
    }

    if (hasUpperCaseRegex.test(value)) {
      this.setState({upperCaseRegexErrorMesg: ''});
    } else {
      this.setState({upperCaseRegexErrorMesg: this.wordStore.translate('You need to have at least one uppercase character')});
    }

    if (hasNumberRegex.test(value)) {
      this.setState({numberRegexErrorMesg: ''});
    } else {
      this.setState({numberRegexErrorMesg: this.wordStore.translate('You need to have at least one uppercase character')});
    }

    if (hasSpecialCharacterRegex.test(value)) {
      this.setState({specialCharacterRegexErrorMesg: ''});
    } else {
      this.setState({specialCharacterRegexErrorMesg: this.wordStore.translate('You need to have at least one special character')});
    }

    if (hasMinimumCharactersRegex.test(value)) {
      this.setState({minimumCharactersRegexErrorMesg: ''});
    } else {
      this.setState({minimumCharactersRegexErrorMesg: this.wordStore.translate('You need to have at least 10 characters')});
    }

    // test all the password regex patterns
    if (strongPasswordRegex.test(value)) {
      this.setState({passwordErrorMesg: ''});
      return true;
    } else {
      this.setState({passwordErrorMesg: this.wordStore.translate('Password should be a minimum of 10 characters and contain at least one lower case character, one upper case character, one number and one special character')});
      return false;
    }
  }

  handlePhoneNumberChange = (value) => {
    let myUser = this.state.user;
    myUser.phoneNumber = value;
    this.setState({user: myUser});

    this.testPhoneNumberRegex(value);
  };

  testPhoneNumberRegex = (value) => {
    if (phoneNumberRegex.test(value) || phoneNumberRegex2.test(value)) {
      this.setState({phoneNumberErrorMesg: ''});
      return true;
    } else {
      this.setState({phoneNumberErrorMesg: this.wordStore.translate('Phone number is a mandatory field and should be in the form 1234567890, 123-456-7890 or (123)456-7890')});
      return false;
    }
  }

  handleSave = () => {
    let self = this;
    let updateObj = this.state.user;
    if (!updateObj.roles) {
      updateObj.roles = ['user'];
    }

    if (this.testPhoneNumberRegex(this.state.user.phoneNumber) && this.testEmailRegex(this.state.user.email)) {
      updateObj.active = this.state.active;
      this.store.save(updateObj).then(() => {
        Notify.success('User updated');
        self.onSave();
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error updating user');
        }
      });
    }
  }

  onSave () {
    if (typeof this.props.onSave === 'function') {
      this.props.onSave();
    }
  }

  handleCancel = () => {
    this.setState({ email: '', role: '' });
    this.onCancel();
  }

  onCancel () {
    if (typeof this.props.onCancel === 'function') {
      this.props.onCancel();
    }
  }

  handleUserActivityToggle = () => {
    this.setState({ active: !this.state.active });
    /*
     let myUser = this.state.user;
     myUser.active = !myUser.active;
     this.setState({user: myUser});
     */
  }

  render () {
    let myUser = this.state.user || {};

    let actions = [
      { label: 'Cancel', onClick: this.handleCancel.bind(this) },
      { label: 'Save', onClick: this.handleSave.bind(this) }
    ];

    let passwordActionText = this.wordStore.translate('Password');
    let passwordContainer = null;

    if (!this.props.editId || (this.props.editId && this.props.editId.length === 0)) {
      passwordContainer = <div className={styles.passwordContainer}>
        <Input
          type='password'
          label={this.wordStore.translate(passwordActionText)}
          value={myUser.password} onChange={this.handlePasswordChange.bind(this)}
          error={this.state.passwordErrorMesg}
          required />
      </div>;
    }

    let myUserRolesDropdownLabelText = '';
    if (this.state.user.roles && this.state.user.roles.length === this.roles.length) {
      myUserRolesDropdownLabelText = this.wordStore.translate('All roles assigned');
    } else {
      myUserRolesDropdownLabelText = this.wordStore.translate('Choose roles');
    }

    return (
      <Dialog actions={actions} active={this.props.active} title={this.props.title} >
        <div className={styles.switchHolder}>
          <Switch
            label={this.wordStore.translate('Active')}
            checked={Boolean(this.state.active)}
            onChange={ this.handleUserActivityToggle.bind(this)}
            className={ styles.activitySwitch } />
        </div>
        <FormWrapper onSubmit={this.handleSave} >
          <div className={styles.names}>
            <Input
              className={styles.name}
              type='text'
              label={this.wordStore.translate('First Name')}
              value={myUser.firstName}
              onChange={this.handleFirstNameChange.bind(this)} />
            <Input
              className={styles.name}
              type='text'
              label={this.wordStore.translate('Last Name')}
              value={myUser.lastName}
              onChange={this.handleLastNameChange.bind(this)} />
          </div>
          <div>
            <Input
              type='email'
              label={this.wordStore.translate('Email')}
              value={myUser.email}
              onChange={this.handleEmailChange.bind(this)}
              error={this.state.emailErrorMesg}
              required />
          </div>
          <div>
            <Input
              type='tel'
              label={this.wordStore.translate('Telephone Number')}
              value={myUser.phoneNumber} onChange={this.handlePhoneNumberChange.bind(this)}
              error={this.state.phoneNumberErrorMesg}
              required />
          </div>
          {passwordContainer}
          <div className={styles.roles}>
            <div className={styles.rolesLabel}>Roles: </div>
          </div>

          <div className={styles.roleDropdownWrapper}>
            <Autocomplete
              allowCreate={false}
              direction='down'
              source={this.roles}
              label={myUserRolesDropdownLabelText}
              onChange={this.handleRolesChange.bind(this)}
              value={this.wordStore.translate(this.state.user.roles)}
              className={styles.rolesDropdown} />

          </div>

        </FormWrapper>
      </Dialog>
    );
  }
}
