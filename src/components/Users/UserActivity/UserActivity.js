import React from 'react';
import { inject, observer } from 'mobx-react';
import { Avatar, Dialog } from 'react-toolbox';

const styles = require('./UserActivity.css');

@inject('store') @observer
export default class UserActivity extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.userStore;
    this.wordStore = this.props.store.wordStore;

    this.state = {user: this.store.getById(this.props.userId)};
  }

  handleClose = () => {
    this.props.onCancel();
  }

  handleEditMember = () => {
    this.props.onEditUser(this.state.user);
  }

  render () {
    let firstName = 'Firsty';
    let lastName = 'LastNamerson';
    if (this.state.user && this.state.user.name) {
      let bothNames = this.state.user.name.split(' ');
      if (bothNames && bothNames.length === 2) {
        firstName = bothNames[0];
        lastName = bothNames[1];
      }
    } else if (this.state.user && this.state.user.firstName && this.state.user.lastName) {
      firstName = this.state.user.firstName;
      lastName = this.state.user.lastName;
    }
    let fullUserName = firstName + ' ' + lastName;
    let userEmail = this.state.user && this.state.user.email;

    let loginActivity = this.wordStore.translate('Login');// bc - Login is currently the only user event we are tracking

    let activityList;
    let activities = [];
    if (this.state.user.lastLoginDate) {
      activities = this.state.user.lastLoginDate;
      activityList = activities.map((activity, index) => {
        return (
          <li key={index}>
            <div>{loginActivity}</div>
            <div>{new Date(activity).toString()}</div>
          </li>
        );
      });
    } else {
      activityList = <div>{this.wordStore.translate('No activities for this user')}</div>;
    }

    let actions = [
      { label: this.wordStore.translate('Close'), onClick: this.handleClose.bind(this) },
      { label: this.wordStore.translate('Edit User'), onClick: this.handleEditMember.bind(this) }
    ];

    return (
        <Dialog actions={actions} active={this.props.active} title={this.props.title}>
            <div className={styles.userActivityHeader}>
              <div className={styles.userInfo}>
                <Avatar title={fullUserName} style={{backgroundColor: styles.colorPrimary}}/>
                <div style={{marginLeft: '1rem'}}>
                  <h4 className={styles.userName}>{fullUserName}</h4>
                  <h5 className={styles.userEmail}>{userEmail}</h5>
                </div>
              </div>
            </div>
            <div className={styles.userActivityBody}>
              <h4>Member Activity</h4>
              <ul className={styles.memberActivityList}>
                {activityList}
              </ul>
            </div>
          </Dialog>
    );
  }
}
