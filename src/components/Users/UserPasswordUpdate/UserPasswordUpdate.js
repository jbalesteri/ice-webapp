import React from 'react';
import { inject, observer } from 'mobx-react';

import { Input, Dialog } from 'react-toolbox';
import {regexes} from '../../../common/helpers/Validators';
import Notify from '../../../common/helpers/Notify';
import FormWrapper from '../../common/FormWrapper/FormWrapper';

const styles = require('./UserPasswordUpdate.css');

let strongPasswordRegex = regexes.strongPasswordRegex;
let hasLowerCaseRegex = regexes.hasLowerCaseRegex;
let hasUpperCaseRegex = regexes.hasUpperCaseRegex;
let hasNumberRegex = regexes.hasNumberRegex;
let hasSpecialCharacterRegex = regexes.hasSpecialCharacterRegex;
let hasMinimumCharactersRegex = regexes.hasMinimumCharactersRegex;

const defaultState = {
  isUpdate: false,
  passwordErrorMesg: '',
  user: {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    password: '',
    roles: []
  },
  active: false
};

@inject('store') @observer
export default class UserPasswordUpdate extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store.userStore;
    this.wordStore = this.props.store.wordStore;
    this.messageStore = this.props.store.messageStore;

    if (this.props.updatePasswordId) {
      let myUser = Object.assign({}, this.store.getById(this.props.updatePasswordId));
      myUser.password = '';// we didn't get back user password, but need to have default to prevent react warning
      this.state = {
        isUpdate: true,
        passwordErrorMesg: '',
        user: myUser
      };
    } else {
      this.state = defaultState;
    }
  }

  handlePasswordChange = (value) => {
    let myUser = this.state.user;
    myUser.password = value;
    this.setState({user: myUser});
    this.testPasswordRegex(value);
  };

  testPasswordRegex = (value) => {
    // individually test each password regex pattern, so we can display error messages specific to that situation
    // this section is currently not updating the ui, but added here to facilitate adding matching specific password patterns later
    if (hasLowerCaseRegex.test(value)) {
      this.setState({lowerCaseRegexErrorMesg: ''});
    } else {
      this.setState({lowerCaseRegexErrorMesg: this.wordStore.translate('You need to have at least one lowercase character')});
    }

    if (hasUpperCaseRegex.test(value)) {
      this.setState({upperCaseRegexErrorMesg: ''});
    } else {
      this.setState({upperCaseRegexErrorMesg: this.wordStore.translate('You need to have at least one uppercase character')});
    }

    if (hasNumberRegex.test(value)) {
      this.setState({numberRegexErrorMesg: ''});
    } else {
      this.setState({numberRegexErrorMesg: this.wordStore.translate('You need to have at least one uppercase character')});
    }

    if (hasSpecialCharacterRegex.test(value)) {
      this.setState({specialCharacterRegexErrorMesg: ''});
    } else {
      this.setState({specialCharacterRegexErrorMesg: this.wordStore.translate('You need to have at least one special character')});
    }

    if (hasMinimumCharactersRegex.test(value)) {
      this.setState({minimumCharactersRegexErrorMesg: ''});
    } else {
      this.setState({minimumCharactersRegexErrorMesg: this.wordStore.translate('You need to have at least 10 characters')});
    }

    // test all the password regex patterns
    if (strongPasswordRegex.test(value)) {
      this.setState({passwordErrorMesg: ''});
      return true;
    } else {
      this.setState({passwordErrorMesg: this.wordStore.translate('Password should be a minimum of 10 characters and contain at least one lower case character, one upper case character, one number and one special character')});
      return false;
    }
  }

  handleSave = () => {
    let self = this;
    if (this.testPasswordRegex(this.state.user.password)) {
      this.store.adminPassword(this.state.user._id, this.state.user.password).then((res) => {
        if (res.error) {
          this.messageStore.addMessage(res.error);
        } else {
          self.onSave();
        }
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error updating user');
        }
      });
    } else {
      this.setState({passwordErrorMesg: this.wordStore.translate('Password should be a minimum of 10 characters and contain at least one lower case character, one upper case character, one number and one special character')});
    }
  }

  onSave () {
    if (typeof this.props.onSave === 'function') {
      this.props.onSave();
    }
  }

  handleCancel = () => {
    this.setState({ email: '', role: '' });
    this.onCancel();
  }

  onCancel () {
    if (typeof this.props.onCancel === 'function') {
      this.props.onCancel();
    }
  }

  render () {
    let myUser = this.state.user || {};
    let actions = [
      { label: 'Cancel', onClick: this.handleCancel.bind(this) },
      { label: 'Save', onClick: this.handleSave.bind(this) }
    ];

    return (
      <Dialog actions={actions} active={this.props.active} title={this.props.title} >
        <FormWrapper onSubmit={this.handleSave} >
          <div className={styles.passwordContainer}>
            <Input type='password' label={this.wordStore.translate('Input Password')} value={myUser.password} onChange={this.handlePasswordChange.bind(this)} />
            <div>{this.state.passwordErrorMesg}</div>
          </div>
        </FormWrapper>
      </Dialog>
    );
  }
}
