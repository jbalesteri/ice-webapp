// File generated on 2016-11-28 by Jay

import React from 'react';

import FontIcon from 'react-toolbox/lib/font_icon';

let styles = require('./ConnectedIndicator.css');

export default class ConnectedIndicator extends React.Component {
  render () {
    if (this.props.connected === true) {
      return <FontIcon value='flash_on' className={styles.connected} />;
    } else {
      return <FontIcon value='flash_off' className={styles.disconnected} />;
    }
  }
}
