var debug = require('debug')('ice:GroupForm'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Input, Switch, Dialog } from 'react-toolbox';

import Validators from '../../../common/helpers/Validators';
import Notify from '../../../common/helpers/Notify';
import FormWrapper from '../../common/FormWrapper/FormWrapper';

const defaultState = {
  name: '',
  nameError: '',
  group: null,
  rackshareEnabled: false,
  limit: 0
};

@inject('store') @observer
export default class GroupForm extends React.Component {
  constructor (props) {
    super(props);

    this.groupStore = this.props.store.groupStore;
    this.wordStore = this.props.store.wordStore;

    this.state = defaultState;
  }

  componentWillReceiveProps (nextProps) {
    let newState = Object.assign({}, defaultState);

    if (nextProps.groupId) {
      let group = this.groupStore.getById(nextProps.groupId);

      newState.group = group;
      newState.name = group.name;
      newState.rackshareEnabled = group.rackshareEnabled;
      newState.limit = group.limit;
    } else if (nextProps.active) {
      newState.group = this.groupStore.createGroup();
    }

    this.setState(newState);
  }

  validate = (field) => {
    let error = '';
    let value = this.state[field];

    switch (field) {
      case 'name':
        let names = this.groupStore.all.map((group) => (group.name));
        let originalName = (this.state.group) ? this.state.group.name : null;
        error = Validators.group.name(value, names, originalName);
        break;
    }

    return error;
  }

  isValid = () => {
    let p = new Promise((resolve, reject) => {
      let newState = {};
      ['name'].forEach((field) => {
        newState[field + 'Error'] = this.validate(field);
      });

      this.setState(newState, () => {
        if (this.state.nameError === '' && this.state.name !== '') {
          resolve();
        } else {
          reject(new Error('form not valid'));
        }
      });
    });

    return p;
  }

  handleChange = (name, value) => {
    debug('HandleChange:', name, value);
    this.setState({ [name]: value });
  }

  handleBlur = (name) => {
    let error = this.validate(name);
    this.setState({ [name + 'Error']: error });
  }

  save = () => {
    this.isValid().then(() => {
      let group = this.state.group;

      group.name = this.state.name;
      group.rackshareEnabled = this.state.rackshareEnabled;
      group.limit = this.state.limit || 1E9;

      group.save().then(() => {
        Notify.success('Group saved');
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error saving group');
        }
      });

      this.props.onSave();
    }, () => {
      debug('Form data was invalid');
    });
  }

  cancel = () => {
    this.props.onCancel();
  }

  render () {
    let title = (this.state.group) ? this.wordStore.translate('Edit Group') : this.wordStore.translate('Add Group');

    let actions = [
      { label: this.wordStore.translate('Cancel'), onClick: this.cancel },
      { label: this.wordStore.translate('Save'), onClick: this.save }
    ];

    return (
      <Dialog title={title} actions={actions} active={this.props.active} onEscKeyDown={this.cancel}>
        <FormWrapper onSubmit={this.save} >
          <Input type='text' label={this.wordStore.translate('Name')} value={this.state.name} error={this.state.nameError} onChange={this.handleChange.bind(this, 'name')} onBlur={this.handleBlur.bind(this, 'name')} />
          <Switch checked={this.state.rackshareEnabled} label={this.wordStore.translate('Rackshare Enabled')} onChange={this.handleChange.bind(this, 'rackshareEnabled')} />
          <Input type='number' label={this.wordStore.translate('Max Limit')} value={this.state.limit} onChange={this.handleChange.bind(this, 'limit')} />
        </FormWrapper>
      </Dialog>
    );
  }
}

GroupForm.wrappedComponent.propTypes = {
  groupId: React.PropTypes.string,
  active: React.PropTypes.bool.isRequired,
  onCancel: React.PropTypes.func,
  onSave: React.PropTypes.func
};
