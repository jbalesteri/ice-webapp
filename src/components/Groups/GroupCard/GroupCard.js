var debug = require('debug')('ice:GroupCard'); // eslint-disable-line no-unused-vars
import { inject, observer } from 'mobx-react';
import React from 'react';
import { Card, CardTitle, Switch } from 'react-toolbox';
import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';
import GroupCardDeviceList from './GroupCardDeviceList/GroupCardDeviceList';

let styles = require('./GroupCard.css');

@inject('store') @observer
class GroupCard extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
    this.group = this.props.group;

    this.state = {
      showDevices: false
    };
  }

  handleChange = (field, value) => {
    this.setState({[field]: value});
  };

  renderDeviceList () {
    if (this.state.showDevices === true) {
      return (
          <GroupCardDeviceList group={this.group} />
      );
    }
  }

  render () {
    let sensorData = this.group.sensorData;
    let battChargePowerW = sensorData.battChargePowerW ? Math.floor(sensorData.battChargePowerW) : 0;
    let battDischargePowerW = sensorData.battDischargePowerW ? Math.floor(sensorData.battDischargePowerW) : 0;
    let inputPowerW = sensorData.inputPowerW ? Math.floor(sensorData.inputPowerW) : 0;
    let outputPowerW = sensorData.outputPowerW ? Math.floor(sensorData.outputPowerW) : 0;
    let powerLimitW = sensorData.powerLimitW ? Math.floor(sensorData.powerLimitW) : 0;
    let battAvgSocProp = sensorData.battAvgSocProp ? Math.floor(sensorData.battAvgSocProp * 100) : 0;

    return (
      <Card raised className={styles.groupCard} style={{marginBottom: '1rem'}}>
        <div data-qa-tag='group-card' data-qa-id={this.group.id}>
        <CardTitle
          className={styles.cardTitle}
          title={this.group.name}
          subtitle={'Devices: ' + this.group.devices.length}>
          <div className={styles.deviceToggle}>
            <Switch
              checked={this.state.showDevices}
              label='Show Devices'
              onChange={this.handleChange.bind(this, 'showDevices')}
              className={styles.invertedToggle}/>
          </div>
        </CardTitle>
        <div className={styles.cardData}>
          <div className={styles.groupData}>
            <div className={styles.limits}>
              <div className={styles.inputOutput}>
                <ValueWrapper
                  value={inputPowerW / 1000}
                  label={this.wordStore.translate('Input')}
                  suffix='kW'
                  size='sm'
                  labelFirst={false} />
                <ValueWrapper
                  value={outputPowerW / 1000}
                  label={this.wordStore.translate('Output')}
                  suffix='kW'
                  size='sm'
                  labelFirst={false} />
              </div>
              <div className={styles.limitHolder}>
                <ValueWrapper
                  value={powerLimitW / 1000}
                  label={this.wordStore.translate('Limit')}
                  suffix='kW'
                  size='sm'
                  labelFirst={false} />
              </div>
              <div className={styles.chargeDischarge}>
                <ValueWrapper
                  value={battChargePowerW / 1000}
                  label={this.wordStore.translate('Charge')}
                  suffix='kW'
                  size='sm'
                  labelFirst={false} />
                <ValueWrapper
                  value={battDischargePowerW / 1000}
                  label={this.wordStore.translate('Discharge')}
                  suffix='kW'
                  size='sm'
                  labelFirst={false} />
              </div>
              <div className={styles.socHolder} style={{textAlign: 'right'}}>
                <ValueWrapper
                  value={battAvgSocProp}
                  label={this.wordStore.translate('SOC')}
                  suffix='%'
                  size='sm'
                  formatNumber={false}
                  labelFirst={false} />
              </div>
            </div>
          </div>
          {this.renderDeviceList()}
        </div>
        </div>
      </Card>
    );
  }
}

export default GroupCard;
