var debug = require('debug')('ice:GroupCardDeviceList'); // eslint-disable-line no-unused-vars
import { inject, observer } from 'mobx-react';
import React from 'react';
import ValueWrapper from '../../../common/ValueWrapper/ValueWrapper';
let styles = require('./GroupCardDeviceList.css');

@inject('store') @observer
export default class GroupCardDeviceList extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
    this.group = this.props.group;
    this.deviceSensorDataStore = this.props.store.deviceSensorData;
    this.deviceStore = this.props.store.deviceStore;
  }

  renderDevices () {
    let devices = this.props.group.deviceIds.map((deviceId) => {
      let device = this.deviceStore.getById(deviceId);
      let deviceName = device.name;
      let deviceData = device.sensorData;
      if (deviceData) {
        let dischargeStatus = deviceData.battPowerOutW / 1000;

        let input = deviceData.inputPower;
        let output = deviceData.outputPower;
        let limit = deviceData.powerLimitW;
        let soc = deviceData.battSoc;

        return (
          <div key={deviceId} className={styles.row}>
            <ValueWrapper value={deviceName} size='xs' />
            <ValueWrapper value={input} suffix='kW' size='xs' />
            <ValueWrapper value={output} suffix='kW' size='xs' />
            <ValueWrapper value={limit} suffix='kW' size='xs' />
            <ValueWrapper value={dischargeStatus} suffix='kW' size='xs' />
            <ValueWrapper value={soc} suffix='%' size='xs' formatNumber={true}/>
          </div>
        );
      }
    });
    return devices;
  }

  render () {
    return (
      <div className={styles.deviceList}>
        <div className={styles.row}>
          <ValueWrapper value={'name'} size='xs' />
          <ValueWrapper value={'input'} size='xs' />
          <ValueWrapper value={'output'} size='xs' />
          <ValueWrapper value={'limit'} size='xs' />
          <ValueWrapper value={'discharge'} size='xs' />
          <ValueWrapper value={'soc'} size='xs' />
        </div>
        {this.renderDevices()}
      </div>
    );
  }
}
