 <!-- File generated on 2016-11-22 by Jay -->

## FeedCard


#### Instructions:
Component desc.

##### Example:
```html
<FeedCard
	prop={value} 
/ >
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| abc  | bool  |    ?    |      ?       |


###### Images:
----
Standard Implementation: *This will render component without any theming applied to it*

![Alt text](https://placeimg.com/480/320/animals "Standard Implentation")

```html
<FeedCard prop={value}  />
```
----

