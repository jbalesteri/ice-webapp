import React from 'react';
import { Chip } from 'react-toolbox';

let styles = require('./GroupBuilder.css');

export default class GroupCard extends React.Component {
  delete = () => {
    this.props.onDelete(this.props.device);
  }

  render () {
    let { name, id } = this.props.device;

    return (
      <Chip
        deletable
        id={id}
        name={name}
        onDeleteClick={this.delete}
        className={styles.groupDroppedDevice}>{name.toUpperCase()}</Chip>
    );
  }
}

GroupCard.propTypes = {
  device: React.PropTypes.object.isRequired,
  onDelete: React.PropTypes.func.isRequired
};
