// File generated on 2016-12-02 by Jay
var debug = require('debug')('ice:Racks:SourceDevice'); // eslint-disable-line no-unused-vars

import React, { Component } from 'react';

import { ListItem } from 'react-toolbox';

import { DragSource } from 'react-dnd';

let styles = require('./GroupBuilder.css');

const sourceDevice = {
  beginDrag (props) {
    debug('beginDrag()==>props:', props);
    return {
      name: props.name,
      type: props.type,
      id: props.id
    };
  }
};

// *** DEVICES ***//
@DragSource(props => props.type, sourceDevice, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
export default class SourceDevice extends Component {
  render () {
    const { name, type, isDropped, isDragging, connectDragSource } = this.props;
    const opacity = isDragging ? 0.4 : 1;

    return connectDragSource(
      <div className={styles.deviceStyle} style={{ opacity }}>
        <ListItem
          leftIcon='developer_board'
          caption={name.toUpperCase()}
          legend={type.toUpperCase()}
          ripple={false}
          disabled={isDropped}
          isDropped={isDropped}
        />
      </div>
    );
  }
}
