var debug = require('debug')('ice:DragDropGroupBuilder: '); // eslint-disable-line no-unused-vars

import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { Dialog } from 'react-toolbox';

import Notify from '../../../common/helpers/Notify';
import DraggableDeviceSmall from '../../common/dnd/DraggableDeviceSmall/DraggableDeviceSmall';
import GroupBuilderTarget from './GroupBuilderTarget';
import GroupCard from './GroupCard';

let styles = require('./GroupBuilder.css');

const defaultState = {
  group: null,
  groupDevices: [],
  unusedDevices: []
};

@inject('store') @observer
export default class GroupBuilder extends Component {
  constructor (props) {
    super(props);

    this.deviceStore = this.props.store.deviceStore;
    this.groupStore = this.props.store.groupStore;
    this.wordStore = this.props.store.wordStore;

    this.state = defaultState;
  }

  componentWillReceiveProps (nextProps) {
    let newState = Object.assign({}, defaultState);

    if (nextProps.groupId) {
      let group = this.groupStore.getById(nextProps.groupId);

      newState.group = group;
      newState.groupDevices = group.devices.slice();
    }

    this.setState(newState);
  }

  save = () => {
    this.state.group.devices.replace(this.state.groupDevices);
    this.state.group.save().then(() => {
      Notify.success(this.wordStore.translate('Group Saved'));
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.error('Error saving group');
      }
    });
    this.props.onSave();
  }

  cancel = () => {
    this.props.onCancel();
  }

  onDrop = (id) => {
    let droppedDevice = this.deviceStore.getById(id);
    let groupDevices = this.state.groupDevices.concat(droppedDevice);

    this.setState({groupDevices: groupDevices});
  }

  onDelete = (device) => {
    let groupDevices = this.state.groupDevices.filter((checkDevice) => {
      return checkDevice.id !== device.id;
    });

    this.setState({groupDevices: groupDevices});
  }

  renderUnusedDevices () {
    let groupDeviceIds = this.state.groupDevices.map((device) => (device.id));
    let currentDevices = (this.state.group) ? this.state.group.deviceIds : [];
    let usedDevices = this.groupStore.usedDevices.filter((deviceId) => {
      return currentDevices.indexOf(deviceId) === -1;
    }).concat(groupDeviceIds);

    return this.deviceStore.all.filter((device) => {
      return usedDevices.indexOf(device.id) === -1;
    }).map(({ name, type, id }, index) => {
      return <DraggableDeviceSmall name={name} type={type} key={index} id={id} />;
    });
  }

  renderGroupDevices () {
    return this.state.groupDevices.map((device) => {
      return (
        <GroupCard device={device} onDelete={this.onDelete} key={device.id} />
      );
    });
  }

  render () {
    let actions = [
      { label: this.wordStore.translate('Cancel'), onClick: this.cancel },
      { label: this.wordStore.translate('Save'), onClick: this.save }
    ];

    let name = this.state.group ? this.state.group.name : '';

    return (
      <Dialog
        title={this.wordStore.translate('Configure Group')}
        active={this.props.active}
        actions={actions}>
        <div className={styles.dragWrapper}>
          <div>
            <h5>{name}:</h5>
            <div className={styles.targetsWrapper} >
              <GroupBuilderTarget onDrop={this.onDrop}>
                {this.renderGroupDevices()}
              </GroupBuilderTarget>
            </div>
          </div>
          <div className={styles.devicesWrapper}>
            <h5>Devices:</h5>
            <div className={styles.devicesContent}>
              <div className={styles.devicesList}>
                {this.renderUnusedDevices()}
              </div>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

GroupBuilder.wrappedComponent.propTypes = {
  groupId: React.PropTypes.string,
  active: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};
