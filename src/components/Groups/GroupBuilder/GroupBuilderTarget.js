// File generated on 2016-12-02 by Jay
var debug = require('debug')('ice:DragDropRackBuilder: '); // eslint-disable-line no-unused-vars

import React, { Component } from 'react';

import { DropTarget } from 'react-dnd';

import DeviceTypes from '../../../common/constants/DeviceTypes';

const AllDevices = Object.keys(DeviceTypes).map((type) => (DeviceTypes[type]));

import ClassNames from 'classnames/bind';
let styles = require('./GroupBuilder.css');
var classBind = ClassNames.bind(styles);

const GroupDeviceTarget = {
  drop (props, monitor) {
    let item = monitor.getItem();
    debug('drop()', item);
    props.onDrop(item.id);
    // props.onDrop(monitor.getItem());
  }
};

// *** TARGETS ***//
@DropTarget(AllDevices, GroupDeviceTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export default class GroupBuilderTarget extends Component {
  render () {
    const accepts = AllDevices;
    const { isOver, canDrop, connectDropTarget } = this.props;

    const isActive = isOver && canDrop;

    // conditional styling
    let dropTargetStyles = () => {
      if (isActive) {
        return classBind({
          targetStyle: true,
          activeTarget: true
        });
      } else if (canDrop) {
        return classBind({
          targetStyle: true,
          canDropStyle: true
        });
      } else {
        return classBind({
          targetStyle: true,
          targetInactive: true
        });
      }
    };

    return connectDropTarget(
      <div className={dropTargetStyles()} data-qa-tag='group-drop-target'>
        {isActive ? 'Release device to drop' : (accepts.join(', ')).toUpperCase()}
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}
