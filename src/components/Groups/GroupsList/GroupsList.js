const debug = require('debug')('ice:Groups:List'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';

import { IconButton } from 'react-toolbox';

import DataTable from '../../DataTable/DataTable';
import DeleteConfirm from '../../common/DeleteConfirm/DeleteConfirm';

@inject('store') @observer
export default class GroupsList extends React.Component {
  constructor (props) {
    super(props);

    this.groupStore = this.props.store.groupStore;
    this.wordStore = this.props.store.wordStore;

    this.groupModel = {
      name: {type: String, title: this.wordStore.translate('Name')},
      deviceCount: {type: String, title: this.wordStore.translate('Device Count')},
      rackshare: {type: String, title: this.wordStore.translate('RackShare')},
      limit: {type: String, title: this.wordStore.translate('Limit')},
      edit: {type: Boolean, title: this.wordStore.translate('Edit')},
      configure: {type: Boolean, title: this.wordStore.translate('Configure')},
      remove: {type: Boolean, title: this.wordStore.translate('Remove')}
    };
  }

  edit = (group) => {
    this.props.onEdit(group);
  }

  config = (group) => {
    this.props.onConfig(group);
  }

  remove = (group) => {
    this.props.onRemove(group);
  }

  render () {
    let groups = this.groupStore.all.map((group) => {
      return {
        id: group.id,
        name: group.name,
        deviceCount: group.devices.length,
        rackshare: (group.rackshareEnabled) ? this.wordStore.translate('Enabled') : this.wordStore.translate('Disabled'),
        limit: group.limit,
        edit: <IconButton icon='edit' data-qa-tag='editButton' onClick={this.edit.bind(this, group)} />,
        configure: <IconButton icon='settings' data-qa-tag='configButton' onClick={this.config.bind(this, group)} />,
        remove: <DeleteConfirm data-qa-tag='removeButton' onClick={this.remove.bind(this, group)} />
      };
    });

    return <DataTable striped model={this.groupModel} source={groups} />;
  }
}

GroupsList.wrappedComponent.propTypes = {
  onEdit: React.PropTypes.func.isRequired,
  onConfig: React.PropTypes.func.isRequired,
  onRemove: React.PropTypes.func.isRequired
};
