var debug = require('debug')('ice:capacityReport: '); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Avatar, Dialog } from 'react-toolbox';
import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';
import DataTable from '../../DataTable/DataTable';

let styles = require('./Capacity.css');

@inject('store') @observer
export default class Capacity extends React.Component {
  constructor (props) {
    super(props);
    this.wordStore = this.props.store.wordStore;
    this.capacityReportStore = this.props.store.capacityReportStore;
    this.dataCenterStore = this.props.store.dataCenterStore;

    // the model below is used in the Datacenter capacity table
    this.model = {
      name: {type: String, title: this.wordStore.translate('name')},
      allocated: {type: Number, title: this.wordStore.translate('allocated')},
      breaker: {type: Number, title: this.wordStore.translate('breaker')},
      peak: {type: Number, title: this.wordStore.translate('peak')},
      available: {type: Number, title: this.wordStore.translate('available')}
    };
  }

  handlePrint = () => {
    window.print();
  }

  renderZoneInfo (zoneName, reportDate) {
    let peakSinceDate = this.dataCenterStore.metricsAggregateResetTimestamp ? this.dataCenterStore.metricsReset.toLocaleString() : 'N/A';

    return (
      <div className={styles.zoneInfo}>
        <ValueWrapper
          value={zoneName}
          label={this.wordStore.translate('zone name')}
          size='title'
          labelFirst={false} />
        <ValueWrapper
          value={reportDate.toLocaleString()}
          label={this.wordStore.translate('report date')}
          size='title'
          labelFirst={false} />
        <ValueWrapper
          value={peakSinceDate}
          label={this.wordStore.translate('peak since')}
          size='title'
          labelFirst={false} />
      </div>
    );
  }

  renderZoneFeedLimits (zoneAllocated, zoneFeedBreakers) {
    let allocated = zoneAllocated / 1000;
    let breakers = zoneFeedBreakers;
    let i = 65;
    let feedBreakerArr = breakers.map((breaker) => {
      return {
        feedId: breaker.feed || '-1',
        breakerLimit: breaker.limitW / 1000 || 0,
        feedSymbol: breaker.feedSymbol || String.fromCharCode(i++)
      };
    });
    return (
      <div className={styles.feedCapacity}>
        <div className={styles.cell}>
          <Avatar icon='flash_on' className={styles.avatar}/>
          <ValueWrapper
            value={allocated}
            label={this.wordStore.translate('allocated')}
            size='title'
            suffix='kW'
            labelFirst={false} />
        </div>
        {this.renderFeedBreakerLimits(feedBreakerArr)}
      </div>
    );
  }

  renderFeedBreakerLimits (breakerArr) {
    let feedBreaker = breakerArr.map((breaker) => {
      let feedName = 'feed ' + breaker.feedSymbol;
      return (
        <div key={breaker.feedId} className={styles.cell}>
          <Avatar title={breaker.feedSymbol} className={styles.avatar}/>
          <ValueWrapper
            value={breaker.breakerLimit}
            label={this.wordStore.translate('breaker ' + feedName)}
            size='title'
            suffix='kW'
            labelFirst={false} />
        </div>
      );
    });
    return feedBreaker;
  };

  renderZoneCapacity (zonecapacity) {
    let zonePeak = zonecapacity.zonePeak / 1000;
    let zone1nPeak = zonecapacity.zone1nPeak / 1000 || 0;
    let zone2nPeak = zonecapacity.zone2nPeak / 1000 || 0;
    let availableCapacity = zonecapacity.availableCapacity / 1000 || 0;
    let available1nCapacity = zonecapacity.available1nCapacity / 1000 || 0;
    let available2nCapacity = zonecapacity.available2nCapacity / 1000 || 0;

    return (
      <div className={styles.zoneCapacity}>
        <div className={styles.cell}>
          <Avatar title='Z' className={styles.avatar} />
          <div className={styles.twoHigh}>
            <ValueWrapper
              value={zonePeak}
              label={this.wordStore.translate('peak Zone')}
              size='title'
              suffix='kW'
              labelFirst={false} />
            <ValueWrapper
              value={availableCapacity}
              label={this.wordStore.translate('available Zone')}
              size='title'
              suffix='kW'
              labelFirst={false} />
          </div>
        </div>
        <div className={styles.cell}>
          <Avatar title='1' className={styles.avatar} />
          <div className={styles.twoHigh}>
            <ValueWrapper
              value={zone1nPeak}
              label={this.wordStore.translate('peak 1N')}
              size='title'
              suffix='kW'
              labelFirst={false} />
            <ValueWrapper
              value={available1nCapacity}
              label={this.wordStore.translate('available 1N')}
              size='title'
              suffix='kW'
              labelFirst={false} />
          </div>
        </div>
        <div className={styles.cell}>
          <Avatar title='2' className={styles.avatar} />
          <div className={styles.twoHigh}>
            <ValueWrapper
              value={zone2nPeak}
              label={this.wordStore.translate('peak 2N')}
              size='title'
              suffix='kW'
              labelFirst={false} />
            <ValueWrapper
              value={available2nCapacity}
              label={this.wordStore.translate('available 2N')}
              size='title'
              suffix='kW'
              labelFirst={false} />
          </div>
        </div>
      </div>
    );
  }

  renderRackCapacity (rackData) {
    let rackCapacityTableData = rackData.map((rack) => {
      let name = rack.rackName;
      let allocated = rack.allocatedPower / 1000;
      let breaker = rack.breakerlimitW / 1000;
      let peak = rack.rackPeak / 1000;
      let availability = rack.availableCapacity / 1000;

      return {
        name: <ValueWrapper value={name} size='sm' />,
        allocated: <ValueWrapper value={allocated} size='sm' suffix='kW' />,
        breaker: <ValueWrapper value={breaker} size='sm' suffix='kW' />,
        peak: <ValueWrapper value={peak} size='sm' suffix='kW' />,
        available: <ValueWrapper value={availability} size='sm' suffix='kW' />
      };
    });
    return (
      <div>
        <h5>Rack Reports:</h5>
        <DataTable striped model={this.model} source={rackCapacityTableData} />
      </div>
    );
  };

  render () {
    // this is the name of the zone that was clicked on in the zone card.
    let selectedZone = this.props.zone;
    let selectedZoneName = selectedZone.name;
    let defaultZoneCapacity = {
      allocatedPower: 0,
      zonePeak: 0,
      zone2nPeak: 0,
      zone1nPeak: 0,
      availableCapacity: 0,
      available2nCapacity: 0,
      available1nCapacity: 0,
      breakers: [],
      rackData: [],
      reportDate: 'N/A',
      zoneName: 'N/A'
    };
    let zoneCapacityData = {};
    this.capacityReportStore.all.map((data) => {
      if (data.zoneName === selectedZoneName) {
        zoneCapacityData = {
          reportDate: data.reportDate || 'N/A',
          zoneName: data.zoneName || 'N/A',
          allocatedPower: data.allocatedPower || 0,
          zonePeak: data.zonePeak || 0,
          zone2nPeak: data.zone2nPeak || 0,
          zone1nPeak: data.zone1nPeak || 0,
          availableCapacity: data.availableCapacity || 0,
          available2nCapacity: data.available2nCapacity || 0,
          available1nCapacity: data.available1nCapacity || 0,
          breakers: data.breakers,
          rackData: data.rackData
        };
      } else {
        zoneCapacityData = defaultZoneCapacity;
      }
    });
    let zoneName = zoneCapacityData ? zoneCapacityData.zoneName : 'N/A';
    let reportDate = zoneCapacityData ? String(new Date(zoneCapacityData.reportDate).toLocaleString()) : 'N/A';
    let allocatedPower = zoneCapacityData.allocatedPower || 0;
    let breakers = zoneCapacityData.breakers || [];
    let rackData = zoneCapacityData.rackData || [];
    return (
       <Dialog
         actions={this.props.actions}
         active={this.props.active}
         onEscKeyDown={this.props.handleToggle}
         onOverlayClick={this.props.handleToggle}
         title='Zone Capacity Report'>
         <div className={styles.capacityReport}>
           {this.renderZoneInfo(zoneName, reportDate)}
           {this.renderZoneFeedLimits(allocatedPower, breakers)}
           {this.renderZoneCapacity(zoneCapacityData)}
           {this.renderRackCapacity(rackData)}
         </div>
       </Dialog>
    );
  };
}
