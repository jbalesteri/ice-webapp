var debug = require('debug')('ice:reports:ZoneCapacity'); // eslint-disable-line no-unused-vars

import React, { Component, PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { Avatar } from 'react-toolbox';

import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';
import DataTable from '../../DataTable/DataTable';

let styles = require('./Capacity.css');

let defaultZoneCapacity = {
  allocatedPower: 0,
  zonePeak: 0,
  zone2nPeak: 0,
  zone1nPeak: 0,
  availableCapacity: 0,
  available2nCapacity: 0,
  available1nCapacity: 0,
  breakers: [],
  rackData: [],
  reportDate: 'N/A',
  zoneName: 'N/A'
};

@inject('store') @observer
export default class ZoneCapacity extends Component {
  constructor (props) {
    super(props);

    this.wordStore = this.props.store.wordStore;
    this.zoneStore = this.props.store.zoneStore;
    this.capacityReportStore = this.props.store.capacityReportStore;
    this.dataCenterStore = this.props.store.dataCenterStore;

    this.model = {
      name: {type: String, title: this.wordStore.translate('name')},
      allocated: {type: Number, title: this.wordStore.translate('allocated')},
      breaker: {type: Number, title: this.wordStore.translate('breaker')},
      peak: {type: Number, title: this.wordStore.translate('peak')},
      available: {type: Number, title: this.wordStore.translate('available')}
    };
  }

  render () {
    let resetTime = this.dataCenterStore.metricsReset.toLocaleString();
    let zone = this.zoneStore.getById(this.props.zoneId);
    let name = (zone && zone.name) ? zone.name : 'N/A';
    let report = this.capacityReportStore.getById(name) || defaultZoneCapacity;

    debug('Report:', report);

    let reportDate = new Date(report.reportDate).toLocaleString();

    let allocated = report.allocatedPower / 1000 || 0;
    let breakers = report.breakers || [];

    let zonePeak = report.zonePeak / 1000;
    let zone1nPeak = report.zone1nPeak / 1000 || 0;
    let zone2nPeak = report.zone2nPeak / 1000 || 0;
    let availableCapacity = report.availableCapacity / 1000 || 0;
    let available1nCapacity = report.available1nCapacity / 1000 || 0;
    let available2nCapacity = report.available2nCapacity / 1000 || 0;

    let racks = report.rackData || [];

    let rackCapacityTableData = racks.map((rack) => {
      let name = rack.rackName;
      let allocated = rack.allocatedPower / 1000;
      let breaker = rack.breakerlimitW / 1000;
      let peak = rack.rackPeak / 1000;
      let availability = rack.availableCapacity / 1000;

      return {
        name: <ValueWrapper value={name} align='left' size='sm' />,
        allocated: <ValueWrapper value={allocated} align='left' size='sm' suffix='kW' />,
        breaker: <ValueWrapper value={breaker} align='left' size='sm' suffix='kW' />,
        peak: <ValueWrapper value={peak} align='left' size='sm' suffix='kW' />,
        available: <ValueWrapper value={availability} align='left' size='sm' suffix='kW' />
      };
    });

    return (
      <div style={{padding: '10px'}}>
        <div className={styles.capacityReport} style={{padding: '10px'}}>
          <div className={styles.zoneInfo}>
            <ValueWrapper
              value={name}
              label={this.wordStore.translate('zone name')}
              size='title'
              labelFirst={false} />
            <ValueWrapper
              value={reportDate}
              label={this.wordStore.translate('report date')}
              size='title'
              labelFirst={false} />
            <ValueWrapper
              value={resetTime}
              label={this.wordStore.translate('peak since')}
              size='title'
              labelFirst={false} />
          </div>

          <div className={styles.feedCapacity}>
            <div className={styles.cell}>
              <Avatar icon='flash_on' className={styles.avatar}/>
              <ValueWrapper
                value={allocated}
                label={this.wordStore.translate('allocated')}
                size='title'
                suffix='kW'
                labelFirst={false} />
            </div>
            {breakers.map((breaker) => {
              let feed = this.props.store.feedStore.getById(breaker.feed);
              let feedName = 'feed ' + feed.symbol;
              let limit = breaker.limitW / 1000;

              return (
                <div key={breaker.feed} className={styles.cell}>
                  <Avatar title={feed.symbol} className={styles.avatar}/>
                  <ValueWrapper
                    value={limit}
                    label={this.wordStore.translate('breaker ' + feedName)}
                    size='title'
                    suffix='kW'
                    labelFirst={false} />
                </div>
              );
            })}
          </div>

          <div className={styles.zoneCapacity}>
            <div className={styles.cell}>
              <Avatar title='Z' className={styles.avatar} />
              <div className={styles.twoHigh}>
                <ValueWrapper
                  value={zonePeak}
                  label={this.wordStore.translate('peak Zone')}
                  size='title'
                  suffix='kW'
                  labelFirst={false} />
                <ValueWrapper
                  value={availableCapacity}
                  label={this.wordStore.translate('available Zone')}
                  size='title'
                  suffix='kW'
                  labelFirst={false} />
              </div>
            </div>
            <div className={styles.cell}>
              <Avatar title='1' className={styles.avatar} />
              <div className={styles.twoHigh}>
                <ValueWrapper
                  value={zone1nPeak}
                  label={this.wordStore.translate('peak 1N')}
                  size='title'
                  suffix='kW'
                  labelFirst={false} />
                <ValueWrapper
                  value={available1nCapacity}
                  label={this.wordStore.translate('available 1N')}
                  size='title'
                  suffix='kW'
                  labelFirst={false} />
              </div>
            </div>
            <div className={styles.cell}>
              <Avatar title='2' className={styles.avatar} />
              <div className={styles.twoHigh}>
                <ValueWrapper
                  value={zone2nPeak}
                  label={this.wordStore.translate('peak 2N')}
                  size='title'
                  suffix='kW'
                  labelFirst={false} />
                <ValueWrapper
                  value={available2nCapacity}
                  label={this.wordStore.translate('available 2N')}
                  size='title'
                  suffix='kW'
                  labelFirst={false} />
              </div>
            </div>
          </div>

          <div>
            <h5>Rack Reports:</h5>
            <DataTable striped model={this.model} source={rackCapacityTableData} />
          </div>
        </div>
      </div>
    );
  }
}

ZoneCapacity.wrappedComponent.propTypes = {
  zoneId: PropTypes.string.isRequired
};
