const debug = require('debug')('ice:Racks:List'); // eslint-disable-line no-unused-vars

import React, { PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { IconButton } from 'react-toolbox';

import DataTable from '../../DataTable/DataTable';
import DeleteConfirm from '../../common/DeleteConfirm/DeleteConfirm';

@inject('store') @observer
export default class RacksList extends React.Component {
  constructor (props) {
    super(props);

    this.rackStore = this.props.store.rackStore;
    this.wordStore = this.props.store.wordStore;

    this.racksModel = {
      name: {type: String, title: this.wordStore.translate('Name')},
      priority: {type: String, title: this.wordStore.translate('Priority')},
      allocated: {type: String, title: this.wordStore.translate('Allocated')},
      redundancy: {type: String, title: this.wordStore.translate('Redundancy')},
      edit: {type: Boolean, title: this.wordStore.translate('Edit')},
      configure: {type: Boolean, title: this.wordStore.translate('Configure')},
      remove: {type: Boolean, title: this.wordStore.translate('Remove')}
    };
  }

  edit = (rack) => {
    this.props.onEdit(rack);
  }

  config = (rack) => {
    this.props.onConfig(rack);
  }

  remove = (rack) => {
    this.props.onRemove(rack);
  }

  render () {
    let racks = this.rackStore.all.map((rack) => {
      return {
        id: rack.id,
        name: rack.name,
        priority: rack.priority,
        allocated: rack.allocatedPower,
        redundancy: rack.redundancy,
        edit: <IconButton icon='edit' data-qa-tag='editButton' onClick={this.edit.bind(this, rack)} />,
        configure: <IconButton icon='settings' data-qa-tag='configButton' onClick={this.config.bind(this, rack)} />,
        remove: <DeleteConfirm data-qa-tag='removeButton' onClick={this.remove.bind(this, rack)} />
      };
    });

    return <DataTable striped model={this.racksModel} source={racks} />;
  }
}

RacksList.wrappedComponent.propTypes = {
  onEdit: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onConfig: PropTypes.func.isRequired
};
