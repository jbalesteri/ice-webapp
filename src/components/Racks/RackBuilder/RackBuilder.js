var debug = require('debug')('ice:DragDropRackBuilder: '); // eslint-disable-line no-unused-vars

import React, { PropTypes } from 'react';
import { inject, observer } from 'mobx-react';
import { Dialog, Dropdown } from 'react-toolbox';
import { RpduDropTarget, RpduDragSource, RpduDroppedCard } from '../../common/dnd/Rpdu/Rpdu';
import Notify from '../../../common/helpers/Notify';

let styles = require('./RackBuilder.css');

const defaultState = {
  rack: null,
  rpdu1: null,
  rpdu2: null,
  defaultRpdu: null
};

@inject('store') @observer
export default class DragDropRackBuilder extends React.Component {
  constructor (props) {
    super(props);

    this.deviceStore = this.props.store.deviceStore;
    this.racks = this.props.store.rackStore;
    this.wordStore = this.props.store.wordStore;

    this.state = defaultState;
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.rackId) {
      let rack = this.racks.getById(nextProps.rackId);
      let rpdu1 = (rack && rack.rpdus.length > 0) ? rack.rpdus[0] : null;
      let rpdu2 = (rack && rack.rpdus.length > 1) ? rack.rpdus[1] : null;
      let defaultRpdu = (rack && rack.defaultRpdus.length > 0) ? rack.defaultRpdus[0] : null;
      this.setState({
        rack: rack,
        rpdu1: rpdu1,
        rpdu2: rpdu2,
        defaultRpdu: defaultRpdu
      });
    } else {
      this.setState(defaultState);
    }
  }

  drop1 = (rpdu) => {
    let rpdu1 = this.deviceStore.getById(rpdu.id);
    this.setState({ rpdu1: rpdu1 });
  }

  drop2 = (rpdu) => {
    let rpdu2 = this.deviceStore.getById(rpdu.id);
    this.setState({ rpdu2: rpdu2 });
  }

  deleteRpdu1 = () => {
    this.setState({ rpdu1: null });
  }

  deleteRpdu2 = () => {
    this.setState({ rpdu2: null });
  }

  renderSources () {
    if (!this.state.rack) { return; }

    let rackRpduIds = (this.state.rack && this.state.rack.rpduIds) ? this.state.rack.rpduIds : [];
    let rpdu1Id = this.state.rpdu1 ? this.state.rpdu1.id : null;
    let rpdu2Id = this.state.rpdu2 ? this.state.rpdu2.id : null;
    let usedDevices = this.racks.usedDevices.filter((device) => {
      return rackRpduIds.indexOf(device) === -1;
    }).concat([rpdu1Id, rpdu2Id]);

    let devices = this.deviceStore.rackConfigurationDevices.filter((device) => {
      return usedDevices.indexOf(device.id) === -1;
    }).map((device, index) => {
      return <RpduDragSource key={index} type={device.model} name={device.name} id={device.id} data-qa-tag='random-tag'/>;
    });
    return (
      <div>{devices}</div>
    );
  }

  renderRpdu1 () {
    if (this.state.rpdu1) {
      return <RpduDroppedCard rpdu={this.state.rpdu1} onDelete={this.deleteRpdu1} />;
    }
  }

  renderRpdu2 () {
    if (this.state.rpdu2) {
      return <RpduDroppedCard rpdu={this.state.rpdu2} onDelete={this.deleteRpdu2}/>;
    }
  }

  cancel = () => {
    this.rack = null;
    this.props.onCancel();
  }

  save = () => {
    let rpdus = [];
    if (this.state.rpdu1) { rpdus.push(this.state.rpdu1); }
    if (this.state.rpdu2) { rpdus.push(this.state.rpdu2); }

    if (this.state.defaultRpdu) {
      let defaultRpdu = [].concat(this.state.defaultRpdu);
      this.state.rack.defaultRpdus.replace(defaultRpdu);
    } else {
      this.state.rack.defaultRpdus.replace([]);
    }

    this.state.rack.rpdus.replace(rpdus);
    this.state.rack.save().then(() => {
      Notify.success(this.wordStore.translate('Rack saved'));
    }).catch((error) => {
      if (error.response && error.response.data && error.response.data.errorMessage) {
        Notify.error(error.response.data.errorMessage);
      } else {
        Notify.success(this.wordStore.translate('Error saving rack'));
      }
    });
    this.props.onSave();
  }

  defaultRpduChanged = (value) => {
    this.setState({ defaultRpdu: value });
  }

  renderDefaultRpdu () {
    // Only show this for 1N racks
    if (!this.state.rack || (this.state.rack && this.state.rack.redundancy === '2N')) {
      return;
    }

    let rpdus = [
      { value: null, label: 'Both' }
    ];

    if (this.state.rpdu1) {
      rpdus.push({ value: this.state.rpdu1.id, label: this.state.rpdu1.name });
    }

    if (this.state.rpdu2) {
      rpdus.push({ value: this.state.rpdu2.id, label: this.state.rpdu2.name });
    }

    return (
      <div>
        <Dropdown
          auto
          label={this.props.store.wordStore.translate('Default RPDU')}
          source={rpdus}
          value={this.state.defaultRpdu}
          onChange={this.defaultRpduChanged}
        />
      </div>
    );
  }

  render () {
    return (
      <Dialog
        title='Configure Rack'
        active={this.props.active}
        actions={[
          { label: 'Cancel', onClick: this.cancel },
          { label: 'Save', onClick: this.save }
        ]}
        onEscKeyDown={this.props.onEscKeyDown}>
        <div className={styles.dragWrapper}>
          <div className={styles.targetsWrapper}>
            <div className={styles.targets} >
              <RpduDropTarget onDrop={this.drop1} id='pdu1'>
                {this.renderRpdu1()}
              </RpduDropTarget>
              <div className={styles.rackCenter}></div>
              <RpduDropTarget onDrop={this.drop2} id='pdu2'>
                {this.renderRpdu2()}
              </RpduDropTarget>
            </div>
            {this.renderDefaultRpdu()}
          </div>
          <div className={styles.sources}>
            <h5>Devices:</h5>
            <div className={styles.devicesContent}>
              <div className={styles.deviceList}>
                {this.renderSources('devices')}
              </div>
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

DragDropRackBuilder.wrappedComponent.propTypes = {
  active: PropTypes.bool.isRequired,
  rackId: PropTypes.string,
  onCancel: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};
