## RackCard


#### Instructions:
Component desc.

##### Example:
```html
<RackCard
	prop={value} 
/ >
```

##### Props:
| Name | Type  | Default | Description  |
|------|-------|---------|--------------|
| abc  | bool  |    ?    |      ?       |


###### Images:
----
Standard Implementation: *This will render component without any theming applied to it*

![Alt text](https://placeimg.com/480/320/animals "Standard Implentation")

```html
<RackCard prop={value}  />
```
----

