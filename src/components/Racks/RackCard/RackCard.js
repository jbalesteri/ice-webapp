var debug = require('debug')('ice:RackCard: '); // eslint-disable-line no-unused-vars

import { inject, observer } from 'mobx-react';
import React from 'react';
import { Card, CardText, CardTitle, Tab, Tabs } from 'react-toolbox';
import ValueWrapper from '../../common/ValueWrapper/ValueWrapper';
import TopologyDevice from '../../common/TopologyDevice/TopologyDevice';
import { PhaseBalanceIcon, DynamicRedundancyIcon } from '../../../common/helpers/Icons';
import PhaseData from '../../common/PhaseData/PhaseData';
import {CollapsingPanel} from '../../../components/Accordion';

let styles = require('./RackCard.css');

@inject('store') @observer
export default class RackCard extends React.Component {
  constructor (props) {
    super(props);
    this.store = this.props.store;
    this.wordStore = this.props.store.wordStore;
    this.rack = this.props.rack;
    this.sensorData = this.props.store.deviceSensorData;

    this.state = {
      index: 0
    };
  }

  handleTabChange = (index) => {
    this.setState({index});
  };

  renderRedundancyData () {
    let redundancy = this.rack.redundancy ? this.rack.redundancy : 'N/A';
    let allocatedPower = this.rack.allocatedPower ? (this.rack.allocatedPower / 1000) : 'N/A';
    let priority = () => {
      let p = this.rack.priority ? this.rack.priority : 'N/A';
      if (this.rack.priority) {
        if (p === 1) {
          return 'HIGH';
        } else {
          return 'LOW';
        }
      } else {
        return 'N/A';
      }
    };
    return (
      <div className={styles.redundancyData}>
        <ValueWrapper
          value={redundancy}
          label={this.wordStore.translate('redundancy')}
          size='title'
          labelFirst={false} />
        <ValueWrapper
          value={priority()}
          label={this.wordStore.translate('priority')}
          formatNumber={false}
          size='title'
          labelFirst={false} />
        <ValueWrapper
          value={allocatedPower}
          label={this.wordStore.translate('allocated')}
          suffix='kW'
          size='title'
          labelFirst={false} />
      </div>
    );
  }

  renderRpdus () {
    if (this.rack.rpdus) {
      let pdus = this.rack.rpdus.map((pdu, index) => {
        return (
          <div key={'rpdu' + index} className={styles.device}>
            <TopologyDevice type={pdu.type} state={pdu.state} />
            <ValueWrapper label='input' value={pdu.sensorData.inputPower} size='title' suffix='kW' labelFirst={false} />
            <div></div>
          </div>
        );
      });
      return pdus;
    };
  }

  renderIceblocks () {
    if (this.rack.rpdus) {
      let pdus = this.rack.rpdus.map((pdu, index) => {
        if (pdu.upstreamFailureSensor) {
          let ib = this.props.store.deviceStore.getById(pdu.upstreamFailureSensor);
          let { type, state } = ib;
          let ibInput = ib.sensorData.inputPowerW / 1000;
          return (
            <div key={'iceblock' + index} style={{display: 'flex'}}>
              <TopologyDevice type={type} state={state} />
              <ValueWrapper label='input' value={ibInput} size='title' suffix='kW' labelFirst={false} />
              <div></div>
            </div>
          );
        }
      });
      return pdus;
    };
  }

  renderRackPowerStats () {
    let input = this.rack.metrics.instantaneousInputPower;
    let peak = this.rack.metrics.maxInputPower;
    return (
      <div className={styles.rackPowerStats} data-qa-tag='rack-power'>
          <ValueWrapper
            value={input}
            label={this.wordStore.translate('input')}
            size='title'
            suffix='kW'
            formatNumber={true}
            labelFirst={false} />
          <ValueWrapper
            value={peak}
            label={this.wordStore.translate('peak')}
            size='title'
            suffix='kW'
            formatNumber={true}
            labelFirst={false} />
      </div>
    );
  }

  renderPhaseData () {
    if (this.rack.metrics.phasePowerIn.length === 3) {
      let phases = this.rack.metrics.phasePowerIn.map((phase) => {
        let input = phase.instantaneousW / 1000;
        let peak = phase.maxW / 1000;
        let breaker = phase.breakerLimitW / 1000;

        return (
          <PhaseData phase={phase.phase} key={this.rack.id + phase.phase}>
            <ValueWrapper value={input} suffix='kW' label='input' size='sm' labelFirst={false} />
            <ValueWrapper value={peak} suffix='kW' label='peak' size='sm' labelFirst={false} />
            <ValueWrapper value={breaker} suffix='kW' label='breaker' size='sm' labelFirst={false} />
          </PhaseData>
        );
      });
      return phases;
    }
  }

  renderThreePhaseContent () {
    return (
      <Tabs index={this.state.index} onChange={this.handleTabChange} className={styles.tabs}>
        <Tab className={styles.tab} label={<span><PhaseBalanceIcon /></span>}>
          <div className={styles.tabContent}>
            {this.renderPhaseData()}
          </div>
        </Tab>
        <Tab className={styles.tab} label={<span><DynamicRedundancyIcon /></span>}>
          <div className={styles.tabContent}>
            <div className={styles.dataColumns}>
              <div className={styles.redundancy}>
                {this.renderRedundancyData()}
              </div>
              <div className={styles.devices}>
                <div className={styles.rpdus}>
                  {this.renderRpdus()}
                </div>
              </div>
            </div>
          </div>
        </Tab>
      </Tabs>
    );
  }

  renderSinglePhaseContent () {
    return (
      <CardText>
        <div className={styles.dataColumns}>
          <div className={styles.redundancy}>
            { this.renderRedundancyData()}
          </div>
          <div className={styles.devices}>
            <div className={styles.rpdus}>
              {this.renderRpdus()}
            </div>
          </div>
        </div>
      </CardText>
    );
  }

  fetchCardContent () {
    if (this.rack.metrics.phasePowerIn.slice().length === 3) {
      return this.renderThreePhaseContent();
    } else {
      return this.renderSinglePhaseContent();
    }
  }

  render () {
    let name = this.rack.name || 'N/A';
    return (
      <Card raised key={this.rack.id} className={styles.rackCard}>
        <div data-qa-tag='rack-card' data-qa-id={this.rack.id}>
          <CardTitle
            title={name}
            className={styles.cardTitle} />
          <div className={styles.collapseWrapper}>
            <CollapsingPanel>
              <div className={styles.powerStatWrapper}>{this.renderRackPowerStats()}</div>
              {this.fetchCardContent()}
            </CollapsingPanel>
          </div>
        </div>
      </Card>
    );
  }
}
