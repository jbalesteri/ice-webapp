const debug = require('debug')('ice:Racks:Form'); // eslint-disable-line no-unused-vars

import React from 'react';
import { inject, observer } from 'mobx-react';
import { Dialog, Input, RadioGroup, RadioButton } from 'react-toolbox';

import Validators from '../../../common/helpers/Validators';
import Notify from '../../../common/helpers/Notify';
import FormWrapper from '../../common/FormWrapper/FormWrapper';

const styles = require('./RackForm.css');

const defaultState = {
  rack: null,
  name: '',
  nameError: '',
  limit: '',
  limitError: '',
  redundancy: '2N',
  priority: '1'
};

@inject('store') @observer
export default class RackForm extends React.Component {
  constructor (props) {
    super(props);

    this.rackStore = this.props.store.rackStore;
    this.wordStore = this.props.store.wordStore;

    this.state = defaultState;
  }

  componentWillReceiveProps (nextProps) {
    let newState = Object.assign({}, defaultState);

    if (nextProps.rackId) {
      let rack = this.rackStore.getById(nextProps.rackId);

      newState.rack = rack;
      newState.name = rack.name;
      newState.limit = rack.allocatedPower;
      newState.redundancy = rack.redundancy;
      newState.priority = String(rack.priority);
    } else if (nextProps.active) {
      newState.rack = this.rackStore.createRack();
    }

    this.setState(newState);
  }

  validate = (field) => {
    let error = '';
    let value = this.state[field];

    switch (field) {
      case 'name':
        let names = this.rackStore.all.map((rack) => (rack.name));
        let originalName = (this.state.rack) ? this.state.rack.name : null;
        error = Validators.rack.name(value, names, originalName);
        break;
      case 'limit':
        error = Validators.rack.limit(value);
        break;
    }

    return error;
  }

  isValid = () => {
    let p = new Promise((resolve, reject) => {
      let newState = {};

      ['name', 'limit'].forEach((field) => {
        newState[field + 'Error'] = this.validate(field);
      });

      this.setState(newState, () => {
        if (this.state.nameError === '' && this.state.limitError === '' && this.state.name !== '' && this.state.limit !== '') {
          resolve();
        } else {
          reject(new Error('form not valid'));
        }
      });
    });

    return p;
  }

  handleChange = (name, value) => {
    this.setState({ [name]: value });
  }

  handleBlur = (name) => {
    let error = this.validate(name);

    this.setState({ [name + 'Error']: error });
  }

  save = () => {
    this.isValid().then(() => {
      let rack = this.state.rack;

      rack.name = this.state.name;
      rack.allocatedPower = this.state.limit;
      rack.priority = this.state.redundancy === '1N' ? this.state.priority : '1';
      rack.redundancy = this.state.redundancy;

      rack.save().then(() => {
        Notify.success('Rack saved');
      }).catch((error) => {
        if (error.response && error.response.data && error.response.data.errorMessage) {
          Notify.error(error.response.data.errorMessage);
        } else {
          Notify.error('Error saving rack');
        }
      });

      this.props.onSave();
    }, () => {
      debug('Form is not valid');
    });
  }

  cancel = () => {
    this.props.onCancel();
  }

  render () {
    let title = (this.state.rack) ? this.wordStore.translate('Edit Rack') : this.wordStore.translate('Add Rack');

    let actions = [
      { label: this.wordStore.translate('Cancel'), onClick: this.cancel },
      { label: this.wordStore.translate('Save'), onClick: this.save }
    ];

    return (
      <Dialog title={title} actions={actions} active={this.props.active} onEscKeyDown={this.cancel}>
        <FormWrapper onSubmit={this.save}>
          <Input type='text' label='Name' value={this.state.name} error={this.state.nameError} onChange={this.handleChange.bind(this, 'name')} onBlur={this.handleBlur.bind(this, 'name')} />
          <Input type='number' label='Limit' value={this.state.limit} error={this.state.limitError} onChange={this.handleChange.bind(this, 'limit')} onBlur={this.handleBlur.bind(this, 'limit')} />
          <RadioGroup className={styles.radioGroup} name='redundancy' value={this.state.redundancy} onChange={this.handleChange.bind(this, 'redundancy')}>
            <legend>Redundancy:</legend>
            <RadioButton label='2N' value='2N'/>
            <RadioButton label='1N' value='1N'/>
          </RadioGroup>
          <RadioGroup className={styles.radioGroup} name='priority' value={this.state.priority} disabled={this.state.redundancy !== '1N'} onChange={this.handleChange.bind(this, 'priority')}>
            <legend>Priority:</legend>
            <RadioButton label='High' value='1'/>
            <RadioButton label='Low' value='2'/>
          </RadioGroup>
        </FormWrapper>
      </Dialog>
    );
  }
}

RackForm.wrappedComponent.propTypes = {
  rackId: React.PropTypes.string,
  active: React.PropTypes.bool.isRequired,
  onCancel: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired
};
