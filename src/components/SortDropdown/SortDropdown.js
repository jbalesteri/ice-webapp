// File generated on 2016-11-28 by Jay

import React from 'react';

import {Dropdown} from 'react-toolbox';

let styles = require('./SortDropdown.css');

const sorting = [
  { value: 'criteria1', label: 'Criteria 1' },
  { value: 'criteria2', label: 'Criteria 2' },
  { value: 'criteria3', label: 'Criteria 3' },
  { value: 'criteria4', label: 'Criteria 4' }
];

class SortDropdown extends React.Component {
  state = { value: 'criteria1' };

  handleChange = (value) => {
    this.setState({value: value});
  };

  render () {
    return (
      <Dropdown
        auto
        label='Sort'
        onChange={this.handleChange}
        source={sorting}
        value={this.state.value}
        className={styles.sortDropdown}
      />
    );
  }
}

export default SortDropdown;
