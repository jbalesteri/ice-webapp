// Generated Component File
import React from 'react';
import { observer, inject } from 'mobx-react';
import { reaction } from 'mobx';
var debug = require('debug')('ice:SnackBar:: '); // eslint-disable-line no-unused-vars

import { Snackbar } from 'react-toolbox';

let _isMounted = false;

@inject('store') @observer
class SnackBar extends React.Component {
  constructor (props) {
    super(props);

    this.store = this.props.store.messageStore;

    this.state = {
      active: false,
      message: '',
      icon: null
    };

    reaction(() => this.store._messages.length,
      () => {
        if (!this.state.active) {
          let message = this.store.getMessage();
          if (_isMounted && message) {
            if (typeof message === 'object') {
              let { text, icon } = message;
              this.setState({ active: true, message: text, icon: icon });
            } else {
              this.setState({ active: true, message: message });
            }
          }
        }
      });
  }

  componentDidMount = () => {
    debug('Snackbar Mounted!!!');
    _isMounted = true;
  }

  componentWillUnmount = () => {
    _isMounted = false;
  }

  handleSnackbarClose = () => {
    let message = this.store.getMessage();
    if (message) {
      this.setState({ message: message });
    } else {
      this.setState({ active: false });
    }
  }

  render () {
    let { active, message, icon } = this.state;

    return (
      <section>
        <Snackbar
          action='Dismiss'
          active={active}
          label={message}
          icon={icon}
          timeout={3000}
          onTimeout={this.handleSnackbarClose}
          onClick={this.handleSnackbarClose}
          type='cancel'
        />
      </section>
    );
  }
}

export default SnackBar;
