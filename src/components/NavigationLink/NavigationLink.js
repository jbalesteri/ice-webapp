import React from 'react';
import { Link as ToolboxLink } from 'react-toolbox';

let styles = require('./NavigationLink.css');

class NavigationLink extends React.Component {
  render () {
    return (
      <div className={this.props.className}>

        <div id={this.props.id} className={styles[this.props.className]}>
          <ToolboxLink data-to={this.props.to} label={this.props.label} icon={this.props.icon} id={this.props.id}>
          </ToolboxLink>
        </div>

      </div>
    );
  }
}

export default NavigationLink;
